/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$("#fwbl-transfer-btn").live("click", function () {
    $("#fwbl-transfer-btn").attr("disabled",true);
    var fwblPanNum = $("#fwbl-pan-input").val();
    var fwblCnicNum = $("#fwbl-cnic-input").val();
    var fwblIvrFlow = $('#fwbl-ivrflow-input').val();
    var agentConfirmInfoIsValid = $("#fwbl-agent-verification").is(":checked");
    var isFormValid = true;
    var panStandardLength = 16;
    var cnicStandardLength = 13;

    //console.log(fwblPanNum, fwblCnicNum, fwblIvrFlow, agentConfirmInfoIsValid);

    if (fwblPanNum.length != panStandardLength) {
        $("#fwbl-pan-input").parent().css("border", "1px solid red");
        isFormValid = false;
    } else {
        $("#fwbl-pan-input").parent().css("border", "none");
    }

    if (fwblCnicNum.length != cnicStandardLength) {
        $("#fwbl-cnic-input").parent().css("border", "1px solid red");
        isFormValid = false;
    } else {
        $("#fwbl-cnic-input").parent().css("border", "none");
    }

    if (fwblIvrFlow == "") {
        $("#fwbl-ivrflow-input").parent().css("border", "1px solid red");
        isFormValid = false;
    } else {
        $("#fwbl-ivrflow-input").parent().css("border", "none");
    }

    if (isFormValid) {
        if (agentConfirmInfoIsValid) {
            //disable here
//          $('#fwbl-agent-verification').attr("disabled", true);
            $("#fwbl-checkbox span").css("color", "white");
            var dataToExe = fwblIvrFlow.split(" ")[1].trim().toUpperCase() +','+ fwblCnicNum +','+fwblPanNum;
            fwblLogTransferActivity(clientObj.getUsername(), fwblCallGui, fwblIvrFlow, dataToExe);
            console.log("Event Sent", dataToExe);
            //"FWBLBackToIvr|ATM,4420303818719,2205550001000005"
            clientObj.logSend("FWBLBackToIvr" + "|" + dataToExe);
        } else {
            $("#fwbl-checkbox span").css("color", "red");
            $("#fwbl-transfer-btn").attr("disabled",false);
        }
    } else {
        console.log("Form values are not valid");
        $("#fwbl-transfer-btn").attr("disabled",false);
    }
});
$("#fwbl-fetch-details-btn").live("click",function(){
    $(".fwlb-fetch-loader").css("display","inline-block");
    if(fwblCallGui !== ""){
        fwblFetchCustomerDetails(fwblCallGui);
    }else{
        Notifier.error("Call id not found","Alert");
        $(".fwlb-fetch-loader").css("display","none");
    }
});
function disableFWBLForm() {
    $('#fwbl-agent-verification').attr("disabled", true);
    $('#fwbl-pan-input').attr("disabled", true);
    $('#fwbl-cnic-input').attr("disabled", true);
    $('#fwbl-ivrflow-option').attr("disabled", true);
}

function enableFWBLForm() {
    $('#fwbl-transfer-btn').attr("disabled",false);
    $('#fwbl-agent-verification').attr("disabled", false);
    $('#fwbl-pan-input').attr("disabled", false);
    $('#fwbl-cnic-input').attr("disabled", false);
    $('#fwbl-ivrflow-option').attr("disabled", false);
}

function clearFWBLForm(){
    $('#fwbl-transfer-btn').attr("disabled",false);
    $('#fwbl-agent-verification').attr("disabled", false);
    $('#fwbl-agent-verification').prop("checked", true);
    $('#fwbl-pan-input').val('');
    $('#fwbl-cnic-input').val('');
    //$("#fwbl-ivrflow-option option[value='']").attr("selected",true).change();
    $("#fwbl-ivrflow-input").val('');
}

    function enableFeedbackBtn(){
        if($('#btnFeedback').attr('disabled', true)){
            $('#btnFeedback').attr('disabled', false);
        }
        else{
            $('#btnFeedback').attr('disabled', false);
        }
    }

function fwblLogTransferActivity(agentId, callId, ivrFlowName, dataToExe){
    //Send Ajax Call
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//  var server = getServerURL();
    var server = getThirdPartyServerURL();
    //server = "http://localhost:8086/Hash3/";
    WebLogs('WS:Going to call fwblLogTransferActivity, Time:[' + req_time + '], Server:[' + server + '], Request:[ ' + dataToExe + ' ]', 0);
    $.ajax({
        type:"GET",
        url: server + '' + ajaxPage ,
        data:{
            "function":"fwblLogPinChangeActivity",
            agentId:agentId,
            callId:callId,
            ivrFlowName: ivrFlowName
        },
        success: function(data){
            WebLogs('WS:fwblLogTransferActivity, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + '], Response:' + data, 0);
        }
    });
}

//Calling it in CallAnsweredEvent & CallDisconnect Event HashClient
function fwblCustomerCli(agentId, msisdn, status, callId){
    //Send Ajax Call
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//  var server = getServerURL();
    var server = getThirdPartyServerURL();
    //server = "http://localhost:8086/Hash3/";
    WebLogs('WS:Going to call fwblCustomerCli, Time:[' + req_time + '], Server:[' + server + '], Request:[' + ']', 0);
    $.ajax({
        type:"GET",
        url:server + '' + ajaxPage,
        data:{
            "function":"fwblCreateUpdateCustomerCli",
            agentId:agentId,
            msisdn:msisdn,
            status: status,
            callId:callId
        },
        success: function(data){
            WebLogs('WS:fwblCustomerCli, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + '], Response:' + data, 0);
        }
    });
}

function fwblFetchCustomerDetails(guid){
    var ajaxTimeOut = 60*1000;
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//  var server = getServerURL();
    var server = getThirdPartyServerURL();
    //server = "http://localhost:8086/Hash3/";
    //ajaxPage = "HashServlet_KHI";
    WebLogs('WS:Going to call fwblFetchCustomerDetails, Time:[' + req_time + '], Server:[' + server + '], Request:[' + ']', 0);
    $.ajax({
        type:"GET",
        url:server + '' + ajaxPage,
        data:{
            "function":"fwblFetchCustomerDetails",
            guid:guid
        },
        success: function(data){
            WebLogs('WS:fwblFetchCustomerDetails, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + '], Response:' + data, 0);
            $(".fwlb-fetch-loader").css("display","none");
            data = JSON.parse(data);
            if(data.error){
                Notifier.error(data.error,"Message");
                console.log(data.error);
            }else{
                //            $('#fwbl-transfer-btn').attr("disabled",false);
//            $('#fwbl-agent-verification').attr("disabled", false);
            $('#fwbl-pan-input').val(data.pan);
            $('#fwbl-cnic-input').val(data.cnic);
            $('#fwbl-ivrflow-input').val(data.ivrflow);
            }
        },
        timeout: ajaxTimeOut
    });
}

function getPinChangeStatus(callId){
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//  var server = getServerURL();
    var server = getThirdPartyServerURL();
    //server = "http://localhost:8086/Hash3/"; //Remove following line and below one.
    //ajaxPage = "HashServlet_KHI";
    WebLogs('WS:Going to call getPinChangeStatus, Time:[' + req_time + '], Server:[' + server + '], Request:[' + ']', 0);
    $.ajax({
        url:server + '' + ajaxPage,
        type:"GET",
        data:{
            "function":"getPinChangeStatus",
            callId: callId
        },
        success:function(data){
            WebLogs('WS:getPinChangeStatus, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + '], Response:' + data, 0);
            data = JSON.parse(data);
            if(data.error){
                Notifier.error(data.error,"Message");
            }else{
                //data.callId
                if(data.result.toLowerCase() !== "ok"){
                    var response = "Pin Change Status: " + data.result + " Reason for failure: " + data.reason + " Attempts: " + data.attempts;
                    Notifier.error(response,"Message");
                }else{
                    var response = "Pin Change Status: " + data.result + " Attempts: " + data.attempts;
                    Notifier.info(response,"Message");
                }
            }
        }
    });
}

function notifyCrmOfCallEnd(callId){
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//  var server = getServerURL();
    var server = getThirdPartyServerURL();
    //server = "http://localhost:8086/Hash3/"; //Remove following line and below one.
    //ajaxPage = "HashServlet_KHI";
    WebLogs('WS:Going to call notifyCrmOfCallEnd, Time:[' + req_time + '], Server:[' + server + '], Request:[' + ']', 0);
    $.ajax({
        url:server + '' + ajaxPage,
        type:"GET",
        data:{
            "function":"notifyCrmOfCallEndFwbl",
            callId: callId
        },
        success:function(data){
            WebLogs('WS:notifyCrmOfCallEnd, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + '], Response:' + data, 0);
            console.log("Response notifyCrmOfCallEnd: " + data);
        }
    });
}

function initializeFwblHash(){
    //Hide five portals if agent is of fwbl.
    //
    $("#profitSanctuariesIdMain").hide();
    $("#fwbl-left-col").show();
    //Hiding Location Portal.
    $("#location-portlet-enable").hide();
    $("#location-portlet-disable").show();
    
    //Hiding CMS portal
    $("#cms-portlet-enable").hide();
    $("#cms-portlet-disable").show();
    
    //Package Details portal
    $("#package-portlet-enable").hide();
    $("#package-portlet-disable").show();
    
    //customer engagemnt preference portal
    $(".customerEngagementPreference-portlet-enable").hide();
    $(".customerEngagementPreference-portlet-enable").show();
    //Super Card Portal
    $(".supercard-portlet-enable").hide();
    $(".supercard-portlet-disable").show();
    
    //Last Activities
    $("#lastactivities-portlet-enable").hide();
    $("#lastactivities-portlet-disable").show();
}