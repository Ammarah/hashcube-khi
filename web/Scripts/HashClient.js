var _call_cli_anis = null;
var _call_punchedin_subnumber = null;
var _agentOnInternalCall = false;
function HashHandlerClass() {
    //var worker = new Worker('Scripts/HashWebWorker.js');
    var worker = new WebSocket(txtConnect);
    var attachedData = null;
    var username = null;
    var password = null;
    var ipAddr = null;
    var caller_msisdn = null;
    var CDN = null;
    var DNIS = null;
    var loggedIn = false;
    var caresLoggedIn = false;
    var NDFCall = false;
    var ipPhoneType = null;
    var callSessionID = null;
    var caresSessionID = null;
    var agentOnCall = false;
    var customerType = null;
    var EM_CDN = null;
    var switchID = null;
    var callType = null;
    var isSupvsr = false;
    var callProductType = 'both';
    var supvsrAcwDuration = null;
    var NRforNextCall = false;
    var isCaresDown = false;
    var isTDDown = false;
    var isUnicaDown = false;
    var isCallTransfered = false;
    var acwNRCode = null;
    var isCharged = false;
    var isCallDTLCalled = false;
    var agentSkillSet = null;
    var isFirstHold = true;
    var caresCustomerInfo = null;
    var thirdParam = null;
    var customerCurrentHandset = null;
    var agentSetNotReadyCode = null;

    var unica_sessionID = null;
    var unicaCardsNumber = null;
    var unicaCardsToFetch = null;
    var unicaSkillSet = null;
    var unicaResegOfferCode = null;
    var unicaResegDropDownLimit = 2;
    var callStartTime = null;
    var callEndTime = null;
    var callTransType = null;
    var callTransToIVRPrompt = null;
    var callDNIn = null;
    var callDNOut = null;

    var caresUrl = null;
    var unicaUrl = null;
    var TDUrl = null;

    var DNIS_Hangup_Disabled = null;
    var DNIS_Direct_Cares = null;
    var DNIS_Auto_Call_Pickup = null;
    var DNIS_Description_Text = null;
    var DNIS_Feedback_Disabled = null;
    var DNIS_Charging = null;
    var DNIS_Consent = null;
    var CDN_Description = null;
    var chargingRate = null;

    var agentID = 0000;

    var commonPwdCCT = 'ptml@123';

    var caresUserlogOnString = null;
    var systemInfo = null;

    //customer conscent variable 2 is no
    var customerEngagementPreference = 2;
    this.getCustomerEngagementPreference = function () {
        return customerEngagementPreference;
    };

    this.setCustomerEngagementPreference = function (str) {
        customerEngagementPreference = str;
    };


    worker.onopen = OnOpen;
    worker.onclose = OnClose;
    worker.onerror = OnError;

    worker.onmessage = function (evnt) {

        console.log(evnt);

        var eventName = evnt.data.eventName;
        if (eventName === 'OnOpen') {
            OnOpen();
        } else if (eventName === 'OnMessage') {
            OnMessage(evnt.data);
        } else if (eventName === 'OnClose') {
            OnClose();
        } else if (eventName === 'OnError') {
            OnError();
        } else if (eventName === 'Exception') {
            Notifier.error('WEBSOCKET EXCEPTION: ' + evnt.data.ex);
            LogEvent("HashHandlerClass::Exception::Exception Thrown : " + evnt.data.ex, 0);
        } else
        {
            OnMessage(evnt);
        }
    };

    this.Connect = function (server) {
//		Send({
//			eventName: 'connect', 
//			serverName: server
//		});



        LogEvent("HashHandlerClass::Connect::Connected to WebSocket Server=" + server, 0);

        connectedToWSServer = true;

        //$('#txtConnect').attr('disabled', true);
        $('#btnConnect').attr('disabled', true);

        //$('#btnLogin').attr('disabled', false);
        $('#btnLogout').attr('disabled', false);
        $('#btnReady').attr('disabled', false);
        $('#btnNot_Ready').attr('disabled', false);
        $('#btnFeedback').attr('disabled', false);
        $('#btnAnswer_Call').attr('disabled', false);
        $('#btnIVR_Transfer').attr('disabled', false);
        $('#btnIVR_Conference').attr('disabled', false);
        $('#btnHang_up').attr('disabled', false);
        $('#btnHold').attr('disabled', false);
        $('#btnUnHold').attr('disabled', false);
        $('#btnSingle_Step_Transfer').attr('disabled', false);
        $('#btnTwo_Step_Transfer').attr('disabled', false);

        $('#btn_consult').attr('disabled', false);
        $('#btn_transfer').attr('disabled', false);
        $('#btn_consult_end').attr('disabled', false);
        $('#btn_transfer_end').attr('disabled', false);
        $('#btn_transfer_complete').attr('disabled', false);

        $('#btnClose').attr('disabled', false);


    };

    this.logSend = function (data) {
        Send(data);
    };

    var Send = function (data) {
//		worker.postMessage(data);
//		if (worker.OPEN == 1 || worker.CONNECTING == 0)
        {
            worker.send(data);

            if (data.eventName) {
            } else {
                //$('#msgtxtarea').val($('#msgtxtarea').val() + "You: " + data + "\n");
                LogEvent("HashHandlerClass::Send::Data Send to WebSocket Server=" + data, 0);
            }
        }
    };

    this.Close = function () {
//		Send({
//			eventName: 'close'
//		});
        Notifier.error('WebSocket Client closed the connection');
        LogEvent("HashHandlerClass::Close::Client closed the connection", 0);

        //$('#txtConnect').attr('disabled', false);
        $('#btnConnect').attr('disabled', false);

        //$('#btnLogin').attr('disabled', true);
        $('#btnLogout').attr('disabled', true);
        $('#btnReady').attr('disabled', true);
        $('#btnNot_Ready').attr('disabled', true);
        $('#btnFeedback').attr('disabled', true);
        $('#btnAnswer_Call').attr('disabled', true);
        $('#btnIVR_Transfer').attr('disabled', true);
        $('#btnIVR_Conference').attr('disabled', true);
        $('#btnHang_up').attr('disabled', true);
        $('#btnHold').attr('disabled', true);
        $('#btnUnHold').attr('disabled', true);
        $('#btnSingle_Step_Transfer').attr('disabled', true);
        $('#btnTwo_Step_Transfer').attr('disabled', true);

        $('#btn_consult').attr('disabled', false);
        $('#btn_transfer').attr('disabled', false);
        $('#btn_consult_end').attr('disabled', false);
        $('#btn_transfer_end').attr('disabled', false);
        $('#btn_transfer_complete').attr('disabled', false);

        $('#btnClose').attr('disabled', true);
    };

    var OnOpen = function () {
        LogEvent("HashHandlerClass::OnOpen::WebSocket Opened", 0);
    };

    var OnMessage = function (fromserver) {
        LogEvent("HashHandlerClass::OnMessage::Message received from WebSocket Server=" + fromserver.data, 0);
        MessageHandler(fromserver.data);
    };

    var OnClose = function () {
        LogEvent("HashHandlerClass::OnClose::Disconnected from WebSocket Server", 0);

        Notifier.error('Disconnected from WebSocket Server');
        //Closing Hash
        $('#divDashboard').fadeOut('slow', function () {
            $('#divLogin').fadeIn('slow');
        });

        connectedToWSServer = true;

        if (loggedIn) {
            caresUserLogOut('2');
        }
        attachedData = null;
        clearInterval(intervalVariable);
        intervalVariable = null;

        data_top = [0, 0, 0, 0];
        data_middle = [0, 0, 0, 0];
        data_bottom = [0, 0, 0, 0];

        $('div#chart01').html('');
        $('div#chart02').html('');
        $('div#chart03').html('');

        $('#LIT_span').html('');
        $('#IT_span').html('');
        $('#BT_span').html('');
        $('#NRT_span').html('');


        $('#PD_span').html('');
        $('#ACW_span').html('');
        $('#HT_span').html('');
        $('#AVG_span').html('');


        $('#CO_span').html('');
        $('#CA_span').html('');
        $('#CRQ_span').html('');
        $('#SC_span').html('');

        //ClearPortlets();
        clearPortals();

        resetButtonSelection($('#btnNRforNextCall'));
        clientObj.setNRforNextCall(false);

        $('div.left-col div.body').hide();
        $('div.middle-col div.body').hide();
        $('div.right-col div.body').hide();

        $('div.customer-demo-lightbox div.body').hide();
        $('.map-controls-wrapper').hide();
        $("#mapImage").attr("src", "images/error_location.png");
        $('div.profit-sanc-lightbox div.body').hide();
        $('div.package-details-lightbox div.body').hide();
        $('div.latest-activities-lightbox div.body').hide();
        resetButtons();

        $('#spanBreakNotice').html('');
        $('#spanBreakTimer').countdown('destroy');

        clearInterval(intervalBreakVariable);

        $('#txtBrkPwd').val('');

        splash_screen_hide_hash();

        try {
            $.fancybox.close();
        } catch (ex) {
        }
        close_break_timer_lightbox();

        close_login_area();
        //

        //$('#txtConnect').attr('disabled', false);
        $('#btnConnect').attr('disabled', false);

        //$('#btnLogin').attr('disabled', true);
        $('#btnLogout').attr('disabled', true);
        $('#btnReady').attr('disabled', true);
        $('#btnNot_Ready').attr('disabled', true);
        $('#btnFeedback').attr('disabled', true);
        $('#btnAnswer_Call').attr('disabled', true);
        $('#btnIVR_Transfer').attr('disabled', true);
        $('#btnIVR_Conference').attr('disabled', true);
        $('#btnHang_up').attr('disabled', true);
        $('#btnHold').attr('disabled', true);
        $('#btnUnHold').attr('disabled', true);
        $('#btnSingle_Step_Transfer').attr('disabled', true);
        $('#btnTwo_Step_Transfer').attr('disabled', true);

        $('#btn_consult').attr('disabled', false);
        $('#btn_transfer').attr('disabled', false);
        $('#btn_consult_end').attr('disabled', false);
        $('#btn_transfer_end').attr('disabled', false);
        $('#btn_transfer_complete').attr('disabled', false);

        $('#btnClose').attr('disabled', true);
    };

    var OnError = function () {
        LogEvent("HashHandlerClass::OnError::Error Occured", 0);
        Notifier.error('WebSocket Error Occured');
    };

    var MessageHandler = function (data) {
        //alert(data);
        var dataArray = data.split('|');
        switch (dataArray[0]) {
            case 'ConferenceCallConnected':
                switch (ipPhoneType) {
                    case "Nortel":
                        break;
                    case "Cisco_LHR":
                        break;
                    case "Cisco_ISB":
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            console.log("Conf Connected");
                            disableFWBLForm();
                        }
                        break;
                }
                break;
            case 'ConferenceCallEnded':
                switch (ipPhoneType) {
                    case "Nortel":
                        break;
                    case "Cisco_LHR":
                        break;
                    case "Cisco_ISB":
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            enableFWBLForm();
                        }
                        getPinChangeStatus(fwblCallGui);
                        break;
                }
                break;
            case 'FwblGuiNumber':
                switch (ipPhoneType) {
                    case "Nortel":
                        break;
                    case "Cisco_LHR":
                        break;
                    case "Cisco_ISB":
                        break;
                    case "Cisco_KHI":
                        fwblCallGui = dataArray[1];
                        intervalFeedbackEnable = setInterval(enableFeedbackBtn(), 3000);
                        break;
                }
                break;
            case 'AgentSession':
                //alert('AgentSession Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentSessionSuccess';
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentSessionFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentSessionSuccess';
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentSessionFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentSessionSuccess';
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentSessionFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentSessionSuccess';
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentSessionFailure';
                        }
                        break;

                }
                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'AgentLoggedIn':
                //alert('AgentLoggedIn Event');
                var buttonState = '';
                hashloginmutex = false;
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentLoggedInSuccess';
                            $('#divLogin').fadeOut('slow', function () {
                                //$('#divDashboard').fadeIn('slow');
                                $('#divStatsScreen').fadeIn('slow');
                                $('#txtUsername').val('');
                                $('#txtUserpass').val('');
                            });
                            close_login_area();
                            $('#txtUsername').removeAttr('readonly');
                            $('#txtUserpass').removeAttr('readonly');
                            $('#txtUsername').focus();
                            getSupervisor();
                            //clientObj.caresSeparateOpen(caresUserlogOnString);
                            loggedIn = true;
                            $('#agentIDSpan').html(clientObj.getUsername());

                            if (intervalVariable == null)
                            {
                                getAgentStats();
                                intervalVariable = setInterval(function () {
                                    getAgentStats();
                                    //alert('interval ' + intervalVariable);
                                }, 5 * 60 * 1000);
                            }

                            getNewsUpdate();
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentLoggedInFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentLoggedInSuccess';
                            $('#divLogin').fadeOut('slow', function () {
                                //$('#divDashboard').fadeIn('slow');
                                $('#divStatsScreen').fadeIn('slow');
                                $('#txtUsername').val('');
                                $('#txtUserpass').val('');
                            });
                            close_login_area();
                            $('#txtUsername').removeAttr('readonly');
                            $('#txtUserpass').removeAttr('readonly');
                            $('#txtUsername').focus();
                            //getSupervisor();
                            /*
                             clientObj.setIsSupervisor(false);
                             if(dataArray[2] == 't'){
                             clientObj.setIsSupervisor(true);
                             }
                             else
                             {
                             clientObj.setIsSupervisor(false);
                             }
                             */
                            //clientObj.caresSeparateOpen(caresUserlogOnString);
                            loggedIn = true;
                            $('#agentIDSpan').html(clientObj.getUsername());

                            getNewsUpdate();
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentLoggedInFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentLoggedInSuccess';
                            $('#divLogin').fadeOut('slow', function () {
                                //$('#divDashboard').fadeIn('slow');
                                $('#divStatsScreen').fadeIn('slow');
                                $('#txtUsername').val('');
                                $('#txtUserpass').val('');
                            });
                            close_login_area();
                            $('#txtUsername').removeAttr('readonly');
                            $('#txtUserpass').removeAttr('readonly');
                            $('#txtUsername').focus();
                            //getSupervisor();
                            /*
                             clientObj.setIsSupervisor(false);
                             if(dataArray[2] == 't'){
                             clientObj.setIsSupervisor(true);
                             }
                             else
                             {
                             clientObj.setIsSupervisor(false);
                             }
                             */
                            //clientObj.caresSeparateOpen(caresUserlogOnString);
                            loggedIn = true;
                            $('#agentIDSpan').html(clientObj.getUsername());

                            getNewsUpdate();
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentLoggedInFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentLoggedInSuccess';
                            $('#divLogin').fadeOut('slow', function () {
                                //$('#divDashboard').fadeIn('slow');
                                $('#divStatsScreen').fadeIn('slow');
                                $('#txtUsername').val('');
                                $('#txtUserpass').val('');
                            });
                            close_login_area();
                            $('#txtUsername').removeAttr('readonly');
                            $('#txtUserpass').removeAttr('readonly');
                            $('#txtUsername').focus();
                            //getSupervisor();
                            /*
                             clientObj.setIsSupervisor(false);
                             if(dataArray[2] == 't'){
                             clientObj.setIsSupervisor(true);
                             }
                             else
                             {
                             clientObj.setIsSupervisor(false);
                             }
                             */
                            //clientObj.caresSeparateOpen(caresUserlogOnString);
                            loggedIn = true;
                            $('#agentIDSpan').html(clientObj.getUsername());

                            getNewsUpdate();
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentLoggedInFailure';
                        }
                        break;

                }
                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'AgentLoggedOut':
                //alert('AgentLoggedOut Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentLoggedOutSuccess';
                            agentOnCall = false;
                            acw_ivr_transfered = false;
                            $('#divDashboard').fadeOut('slow', function () {
                                //$('#divLogin').fadeIn('slow');
                                getAgentStatsScreenData(agentID, "present");
                                $('#divStatsScreen').fadeIn('slow');
                                agentID = null;
                            });
                            if (loggedIn) {
                                caresUserLogOut('3');
                            }
                            attachedData = null;
                            clearInterval(intervalVariable);
                            intervalVariable = null;

                            isCharged = false;

                            data_top = [0, 0, 0, 0];
                            data_middle = [0, 0, 0, 0];
                            data_bottom = [0, 0, 0, 0];

                            $('div#chart01').html('');
                            $('div#chart02').html('');
                            $('div#chart03').html('');

                            $('#LIT_span').html('');
                            $('#IT_span').html('');
                            $('#BT_span').html('');
                            $('#NRT_span').html('');


                            $('#PD_span').html('');
                            $('#ACW_span').html('');
                            $('#HT_span').html('');
                            $('#AVG_span').html('');


                            $('#CO_span').html('');
                            $('#CA_span').html('');
                            $('#CRQ_span').html('');
                            $('#SC_span').html('');

                            //ClearPortlets();
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();
                            resetButtons();
                            loggedIn = false;
                            //agentID = null;

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);

                            //mainScreenTrans();
                            $('#spanBreakNotice').html('');
                            $('#spanBreakTimer').countdown('destroy');

                            clearInterval(intervalBreakVariable);

                            $('#txtBrkPwd').val('');

                            splash_screen_hide_hash();

                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            close_break_timer_lightbox();

                            $('#agentIDSpan').html('');
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentLoggedOutFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentLoggedOutSuccess';
                            agentOnCall = false;
                            acw_ivr_transfered = false;
                            $('#divDashboard').fadeOut('slow', function () {
                                //$('#divLogin').fadeIn('slow');
                                getAgentStatsScreenData(agentID, "present");
                                $('#divStatsScreen').fadeIn('slow');
                                agentID = null;
                            });
                            if (loggedIn) {
                                caresUserLogOut('3');
                            }
                            attachedData = null;
                            clearInterval(intervalVariable);
                            intervalVariable = null;

                            isCharged = false;

                            data_top = [0, 0, 0, 0];
                            data_middle = [0, 0, 0, 0];
                            data_bottom = [0, 0, 0, 0];

                            $('div#chart01').html('');
                            $('div#chart02').html('');
                            $('div#chart03').html('');

                            $('#LIT_span').html('');
                            $('#IT_span').html('');
                            $('#BT_span').html('');
                            $('#NRT_span').html('');


                            $('#PD_span').html('');
                            $('#ACW_span').html('');
                            $('#HT_span').html('');
                            $('#AVG_span').html('');


                            $('#CO_span').html('');
                            $('#CA_span').html('');
                            $('#CRQ_span').html('');
                            $('#SC_span').html('');

                            //ClearPortlets();
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();
                            resetButtons();
                            loggedIn = false;
                            //agentID = null;

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);

                            //mainScreenTrans();
                            $('#spanBreakNotice').html('');
                            $('#spanBreakTimer').countdown('destroy');

                            clearInterval(intervalBreakVariable);

                            $('#txtBrkPwd').val('');

                            splash_screen_hide_hash();

                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            close_break_timer_lightbox();

                            $('#agentIDSpan').html('');
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentLoggedOutFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentLoggedOutSuccess';
                            agentOnCall = false;
                            acw_ivr_transfered = false;
                            $('#divDashboard').fadeOut('slow', function () {
                                //$('#divLogin').fadeIn('slow');
                                getAgentStatsScreenData(agentID, "present");
                                $('#divStatsScreen').fadeIn('slow');
                                agentID = null;
                            });
                            if (loggedIn) {
                                caresUserLogOut('3');
                            }
                            attachedData = null;
                            clearInterval(intervalVariable);
                            intervalVariable = null;

                            isCharged = false;

                            data_top = [0, 0, 0, 0];
                            data_middle = [0, 0, 0, 0];
                            data_bottom = [0, 0, 0, 0];

                            $('div#chart01').html('');
                            $('div#chart02').html('');
                            $('div#chart03').html('');

                            $('#LIT_span').html('');
                            $('#IT_span').html('');
                            $('#BT_span').html('');
                            $('#NRT_span').html('');


                            $('#PD_span').html('');
                            $('#ACW_span').html('');
                            $('#HT_span').html('');
                            $('#AVG_span').html('');


                            $('#CO_span').html('');
                            $('#CA_span').html('');
                            $('#CRQ_span').html('');
                            $('#SC_span').html('');

                            //ClearPortlets();
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();
                            resetButtons();
                            loggedIn = false;
                            //agentID = null;

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);

                            //mainScreenTrans();
                            $('#spanBreakNotice').html('');
                            $('#spanBreakTimer').countdown('destroy');

                            clearInterval(intervalBreakVariable);

                            $('#txtBrkPwd').val('');

                            splash_screen_hide_hash();

                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            close_break_timer_lightbox();

                            $('#agentIDSpan').html('');
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentLoggedOutFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentLoggedOutSuccess';
                            agentOnCall = false;
                            acw_ivr_transfered = false;
                            $('#divDashboard').fadeOut('slow', function () {
                                //$('#divLogin').fadeIn('slow');
                                getAgentStatsScreenData(agentID, "present");
                                $('#divStatsScreen').fadeIn('slow');
                                agentID = null;
                            });
                            if (loggedIn) {
                                caresUserLogOut('3');
                            }
                            attachedData = null;
                            clearInterval(intervalVariable);
                            intervalVariable = null;

                            isCharged = false;

                            data_top = [0, 0, 0, 0];
                            data_middle = [0, 0, 0, 0];
                            data_bottom = [0, 0, 0, 0];

                            $('div#chart01').html('');
                            $('div#chart02').html('');
                            $('div#chart03').html('');

                            $('#LIT_span').html('');
                            $('#IT_span').html('');
                            $('#BT_span').html('');
                            $('#NRT_span').html('');


                            $('#PD_span').html('');
                            $('#ACW_span').html('');
                            $('#HT_span').html('');
                            $('#AVG_span').html('');


                            $('#CO_span').html('');
                            $('#CA_span').html('');
                            $('#CRQ_span').html('');
                            $('#SC_span').html('');

                            //ClearPortlets();
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();
                            resetButtons();
                            loggedIn = false;
                            //agentID = null;

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);

                            //mainScreenTrans();
                            $('#spanBreakNotice').html('');
                            $('#spanBreakTimer').countdown('destroy');

                            clearInterval(intervalBreakVariable);

                            $('#txtBrkPwd').val('');

                            splash_screen_hide_hash();

                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            close_break_timer_lightbox();

                            $('#agentIDSpan').html('');
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentLoggedOutFailure';
                        }
                        break;

                }

                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'AgentReadyStatus':
                //alert('AgentReadyStatus Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentReadySuccess';
                            //attachedData = null;
                            //$('a#fbox_close_a').click();
                            var state = $.trim($('#btnReady').parent().find('div.label span').html());
                            //setButtonSelection($('#btnReady'));

                            $('#btnReady').css('background-position-x', '-275px');

                            $('#btnReady').parent().find('div.label span').html('not ready');
                            $('#btnReady').attr('title', 'Not Ready');
                            $('#btnReady').attr('id', 'btnNot_Ready');
                            //mainScreenTrans();
                            $('#spanBreakNotice').html('');
                            $('#spanBreakTimer').countdown('destroy');

                            clearInterval(intervalBreakVariable);

                            $('#txtBrkPwd').val('');

                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            close_break_timer_lightbox();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            resetButtonSelection($('#btnParentTransfer'));

                            $("#freqACWDiv div.checkbox-wrapper div.checkbox-container a").attr('class', 'checkbox-generic');
                            $("#freqIVRDiv div.radio-btn-wrapper div.radio-btn-container a").attr('class', 'radio-btn-generic');

                            toggle_agent_state(1);

                            acw_check = true;
                            brk_check = true;

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentReadyFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentReadySuccess';
                            //attachedData = null;
                            //$('a#fbox_close_a').click();
                            var state = $.trim($('#btnReady').parent().find('div.label span').html());
                            //setButtonSelection($('#btnReady'));

                            $('#btnReady').css('background-position-x', '-275px');

                            $('#btnReady').parent().find('div.label span').html('not ready');
                            $('#btnReady').attr('title', 'Not Ready');
                            $('#btnReady').attr('id', 'btnNot_Ready');
                            //mainScreenTrans();
                            $('#spanBreakNotice').html('');
                            $('#spanBreakTimer').countdown('destroy');

                            clearInterval(intervalBreakVariable);

                            $('#txtBrkPwd').val('');

                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            close_break_timer_lightbox();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            resetButtonSelection($('#btnParentTransfer'));

                            $("#freqACWDiv div.checkbox-wrapper div.checkbox-container a").attr('class', 'checkbox-generic');
                            $("#freqIVRDiv div.radio-btn-wrapper div.radio-btn-container a").attr('class', 'radio-btn-generic');

                            toggle_agent_state(1);

                            acw_check = true;
                            brk_check = true;

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentReadyFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentReadySuccess';
                            //attachedData = null;
                            //$('a#fbox_close_a').click();
                            var state = $.trim($('#btnReady').parent().find('div.label span').html());
                            //setButtonSelection($('#btnReady'));

                            $('#btnReady').css('background-position-x', '-275px');

                            $('#btnReady').parent().find('div.label span').html('not ready');
                            $('#btnReady').attr('title', 'Not Ready');
                            $('#btnReady').attr('id', 'btnNot_Ready');
                            //mainScreenTrans();
                            $('#spanBreakNotice').html('');
                            $('#spanBreakTimer').countdown('destroy');

                            clearInterval(intervalBreakVariable);

                            $('#txtBrkPwd').val('');

                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            close_break_timer_lightbox();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            resetButtonSelection($('#btnParentTransfer'));

                            $("#freqACWDiv div.checkbox-wrapper div.checkbox-container a").attr('class', 'checkbox-generic');
                            $("#freqIVRDiv div.radio-btn-wrapper div.radio-btn-container a").attr('class', 'radio-btn-generic');

                            toggle_agent_state(1);

                            acw_check = true;
                            brk_check = true;

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentReadyFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentReadySuccess';
                            //attachedData = null;
                            //$('a#fbox_close_a').click();
                            var state = $.trim($('#btnReady').parent().find('div.label span').html());
                            //setButtonSelection($('#btnReady'));

                            $('#btnReady').css('background-position-x', '-275px');

                            $('#btnReady').parent().find('div.label span').html('not ready');
                            $('#btnReady').attr('title', 'Not Ready');
                            $('#btnReady').attr('id', 'btnNot_Ready');
                            //mainScreenTrans();
                            $('#spanBreakNotice').html('');
                            $('#spanBreakTimer').countdown('destroy');

                            clearInterval(intervalBreakVariable);

                            $('#txtBrkPwd').val('');

                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            close_break_timer_lightbox();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            resetButtonSelection($('#btnParentTransfer'));

                            $("#freqACWDiv div.checkbox-wrapper div.checkbox-container a").attr('class', 'checkbox-generic');
                            $("#freqIVRDiv div.radio-btn-wrapper div.radio-btn-container a").attr('class', 'radio-btn-generic');

                            toggle_agent_state(1);

                            acw_check = true;
                            brk_check = true;

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentReadyFailure';
                        }
                        break;

                }

                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'AgentNotReadyStatus':
                //alert('AgentNotReadyStatus Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentNReadySuccess';
                            //attachedData = null;
                            //ClearPortlets();
                            var state = $.trim($('#btnNot_Ready').parent().find('div.label span').html());
                            resetButtonSelection($('#btnNot_Ready'));

                            $('#btnNot_Ready').css('background-position-x', '-110px');

                            $('#btnNot_Ready').parent().find('div.label span').html('ready');
                            $('#btnNot_Ready').attr('title', 'Ready');
                            $('#btnNot_Ready').attr('id', 'btnReady');
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            toggle_agent_state(0);

                            isCharged = false;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentNReadyFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentNReadySuccess';
                            //attachedData = null;
                            //ClearPortlets();
                            var state = $.trim($('#btnNot_Ready').parent().find('div.label span').html());
                            resetButtonSelection($('#btnNot_Ready'));

                            $('#btnNot_Ready').css('background-position-x', '-110px');

                            $('#btnNot_Ready').parent().find('div.label span').html('ready');
                            $('#btnNot_Ready').attr('title', 'Ready');
                            $('#btnNot_Ready').attr('id', 'btnReady');
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            toggle_agent_state(0);

                            isCharged = false;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentNReadyFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentNReadySuccess';
                            //attachedData = null;
                            //ClearPortlets();
                            var state = $.trim($('#btnNot_Ready').parent().find('div.label span').html());
                            resetButtonSelection($('#btnNot_Ready'));

                            $('#btnNot_Ready').css('background-position-x', '-110px');

                            $('#btnNot_Ready').parent().find('div.label span').html('ready');
                            $('#btnNot_Ready').attr('title', 'Ready');
                            $('#btnNot_Ready').attr('id', 'btnReady');
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            toggle_agent_state(0);

                            isCharged = false;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentNReadyFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'AgentNReadySuccess';
                            //attachedData = null;
                            //ClearPortlets();
                            var state = $.trim($('#btnNot_Ready').parent().find('div.label span').html());
                            resetButtonSelection($('#btnNot_Ready'));

                            $('#btnNot_Ready').css('background-position-x', '-110px');

                            $('#btnNot_Ready').parent().find('div.label span').html('ready');
                            $('#btnNot_Ready').attr('title', 'Ready');
                            $('#btnNot_Ready').attr('id', 'btnReady');
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            toggle_agent_state(0);

                            isCharged = false;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'AgentNReadyFailure';
                        }
                        break;
                }

                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'AgentWorkReadyStatus':
                var buttonState = '';

                switch (ipPhoneType) {
                    case "Nortel":

                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallDisconnectedSuccess';

                            agentOnCall = false;
                            toggle_agent_state(0);
                            clearTimeout(timeoutCallAnswerVariable);
                            clearInterval(intervalUnicaHeartBeat);
                            clientObj.setCallEndTime();

                            $(".sop-pop-div").html("");
                            $("input.sop-search-field").val("");

                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                if (!isCallDTLCalled) {
                                    clientObj.setAfterCallInfo();
                                    isCallDTLCalled = true;
                                    if (acw_check) {
                                        LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);
                                        setTimeout(function () {
                                            if (acw_ivr_transfered) {
                                                WebLogs("AWC_JS::submitAWC_btn.click()::workcodes selected", 0);
                                                WebLogs("AWC_JS::submitAWC_btn.click()::calling caresWorkCode function", 0);

                                                setTimeout(function () {
                                                    $('#btnReady').attr("disabled", false);
                                                    $('#btnReady').css('opacity', 1);
                                                    console.log("#btnReady state now : " + $('#btnReady').attr("disabled"));
                                                }, 500);

                                                //caresWorkCode(ACW_idArray);

                                                WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT on call(closing acw popup)", 0);
                                                if (isPIAAgent == false && isPIAWCEnabled == false) {
                                                    /*
                                                     * If condition added by Qaseem for this mousedown statement
                                                     * to bypass PIA ACW instant closing.
                                                     */
                                                    $('#close-acw-lightbox').mousedown();
                                                }
                                                switch (clientObj.getIpPhoneType()) {
                                                    case "Nortel":
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            $('#btnReady').css('background-position-x', '-275px');

                                                            $('#btnReady').parent().find('div.label span').html('not ready');
                                                            $('#btnReady').attr('id', 'btnNot_Ready');
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);
                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);
                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_LHR":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_ISB":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_KHI":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                }
                                            } else {
                                                afterCallPopup();
                                            }
                                        }, 100);

//										LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);
//										setTimeout(function() {
//											afterCallPopup();
//										}, 100);
                                    }
                                    isCallDTLCalled = true;
                                }
                            } else {
                                LogEvent("HashHandlerClass::Third parameter else condition(call requeued) :: " + thirdParam + "", 0);
                                $('#btnReady').attr('disabled', 'false');

                                $('#btnReady').css('background-position-x', '-275px');

                                $('#btnReady').parent().find('div.label span').html('not ready');
                                $('#btnReady').attr('title', 'Not Ready');
                                $('#btnReady').attr('id', 'btnNot_Ready');
                            }

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallDisconnectedFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallDisconnectedSuccess';

                            agentOnCall = false;
                            toggle_agent_state(0);
                            clearTimeout(timeoutCallAnswerVariable);
                            clearInterval(intervalUnicaHeartBeat);
                            clientObj.setCallEndTime();

                            $(".sop-pop-div").html("");
                            $("input.sop-search-field").val("");

                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                if (!isCallDTLCalled) {
                                    clientObj.setAfterCallInfo();
                                    isCallDTLCalled = true;
                                    if (acw_check) {
                                        LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);
                                        setTimeout(function () {
                                            if (acw_ivr_transfered) {
                                                WebLogs("AWC_JS::submitAWC_btn.click()::workcodes selected", 0);
                                                WebLogs("AWC_JS::submitAWC_btn.click()::calling caresWorkCode function", 0);

                                                setTimeout(function () {
                                                    $('#btnReady').attr("disabled", false);
                                                    $('#btnReady').css('opacity', 1);
                                                    console.log("#btnReady state now : " + $('#btnReady').attr("disabled"));
                                                }, 500);

                                                //caresWorkCode(ACW_idArray);

                                                WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT on call(closing acw popup)", 0);
                                                $('#close-acw-lightbox').mousedown();
                                                switch (clientObj.getIpPhoneType()) {
                                                    case "Nortel":
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            $('#btnReady').css('background-position-x', '-275px');

                                                            $('#btnReady').parent().find('div.label span').html('not ready');
                                                            $('#btnReady').attr('id', 'btnNot_Ready');
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);
                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);
                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_LHR":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_ISB":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_KHI":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                }
                                            } else {
                                                afterCallPopup();
                                            }
                                        }, 100);

//										LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);
//										setTimeout(function() {
//											afterCallPopup();
//										}, 100);
                                    }
                                    isCallDTLCalled = true;
                                }
                            } else {
                                LogEvent("HashHandlerClass::Third parameter else condition(call requeued) :: " + thirdParam + "", 0);
                                $('#btnReady').attr('disabled', 'false');

                                $('#btnReady').css('background-position-x', '-275px');

                                $('#btnReady').parent().find('div.label span').html('not ready');
                                $('#btnReady').attr('title', 'Not Ready');
                                $('#btnReady').attr('id', 'btnNot_Ready');
                            }

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallDisconnectedFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallDisconnectedSuccess';

                            agentOnCall = false;
                            toggle_agent_state(0);
                            clearTimeout(timeoutCallAnswerVariable);
                            clearInterval(intervalUnicaHeartBeat);
                            clientObj.setCallEndTime();

                            $(".sop-pop-div").html("");
                            $("input.sop-search-field").val("");

                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                if (!isCallDTLCalled) {
                                    clientObj.setAfterCallInfo();
                                    isCallDTLCalled = true;
                                    if (acw_check) {
                                        LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);

                                        setTimeout(function () {
                                            if (acw_ivr_transfered) {
                                                WebLogs("AWC_JS::submitAWC_btn.click()::workcodes selected", 0);
                                                WebLogs("AWC_JS::submitAWC_btn.click()::calling caresWorkCode function", 0);

                                                setTimeout(function () {
                                                    $('#btnReady').attr("disabled", false);
                                                    $('#btnReady').css('opacity', 1);
                                                    console.log("#btnReady state now : " + $('#btnReady').attr("disabled"));
                                                }, 500);

                                                //caresWorkCode(ACW_idArray);

                                                WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT on call(closing acw popup)", 0);
                                                if (isPIAAgent == false && isPIAWCEnabled == false) {
                                                    /*
                                                     * If condition added by Qaseem for this mousedown statement
                                                     * to bypass PIA ACW instant closing.
                                                     */
                                                    $('#close-acw-lightbox').mousedown();
                                                }
                                                switch (clientObj.getIpPhoneType()) {
                                                    case "Nortel":
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            $('#btnReady').css('background-position-x', '-275px');

                                                            $('#btnReady').parent().find('div.label span').html('not ready');
                                                            $('#btnReady').attr('id', 'btnNot_Ready');
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);
                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);
                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_LHR":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_ISB":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_KHI":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                }
                                            } else {
                                                afterCallPopup();
                                            }
                                        }, 100);

//										LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);
//										setTimeout(function() {
//											afterCallPopup();
//										}, 100);
                                    }
                                    isCallDTLCalled = true;
                                }
                            } else {
                                LogEvent("HashHandlerClass::Third parameter else condition(call requeued) :: " + thirdParam + "", 0);
                                $('#btnReady').attr('disabled', 'false');

                                $('#btnReady').css('background-position-x', '-275px');

                                $('#btnReady').parent().find('div.label span').html('not ready');
                                $('#btnReady').attr('title', 'Not Ready');
                                $('#btnReady').attr('id', 'btnNot_Ready');
                            }

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallDisconnectedFailure';
                        }
                        break;

                }
                thirdParam = null;
                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " (AgentWorkReadyStatus block) event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;

            case 'CallAnsweredEvent':
                //alert('CallAnsweredEvent Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallAnsweredSuccess';
                            //setCaresIntegration(caller_msisdn);
                            clientObj.setCallStartTime();
                            //setButtonSelection($('#btnAnswer_Call'));

                            $('#btnAnswer_Call').css('background-position-x', '-275px');

                            $('#btnAnswer_Call').attr('title', 'HangUp');
                            $('#btnAnswer_Call').parent().find('div.label span').html('hangup');
                            $('#btnAnswer_Call').attr('id', 'btnHang_up');

                            lightbox_black_hover = true;

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            $("#lightbox-bg-blackout").mouseenter();

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);

                            if (!isCharged) {
                                //$('#callCounterSpan').css('color','red');
                                $('#callCounterSpan').countdown({
                                    since: new Date(),
                                    compact: true,
                                    format: 'HMS',
                                    description: ''
                                });
                                intervalCallVariable = setInterval(function () {
                                    $('#callCounterSpan').countdown('resume');
                                }, 1);
                            }

                            if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Charging()) != -1) {
                                if (!isCharged) {
                                    LogEvent("HashHandlerClass::MessageHandler::CallAnsweredEvent DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "Charging_DNIS=" + clientObj.getDNIS_Charging() + ";", 0);
                                    if (!NDFCall) {
                                        var tmp_charging_cli = clientObj.getAttachedData()[2];
                                        if (tmp_charging_cli == 'NDF') {
                                            tmp_charging_cli = $.trim(clientObj.getAttachedData()[1]);
                                        }
                                        if (starts_with('92', tmp_charging_cli)) {

                                        } else {
                                            tmp_charging_cli = '92' + tmp_charging_cli;
                                        }
                                        caresPostCallAdjustment(tmp_charging_cli);
                                    } else {
                                        caresPostCallAdjustment(caller_msisdn);
                                    }
                                    isCharged = true;
                                }
                            }

                            $("#lightbox-bg-blackout").css('display', 'none');

                            acw_check = true;
                            brk_check = true;

                            ivr_click_state = 3;
                            awc_click_state = 1;

                            isCallDTLCalled = false;
                            isFirstHold = true;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallAnsweredFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallAnsweredSuccess';
                            //setCaresIntegration(caller_msisdn);
                            clientObj.setCallStartTime();
                            //setButtonSelection($('#btnAnswer_Call'));

                            $('#btnAnswer_Call').css('background-position-x', '-275px');

                            $('#btnAnswer_Call').attr('title', 'HangUp');
                            $('#btnAnswer_Call').parent().find('div.label span').html('hangup');
                            $('#btnAnswer_Call').attr('id', 'btnHang_up');

                            lightbox_black_hover = true;

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            $("#lightbox-bg-blackout").mouseenter();

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);

                            if (!isCharged) {
                                //$('#callCounterSpan').css('color','red');
                                $('#callCounterSpan').countdown({
                                    since: new Date(),
                                    compact: true,
                                    format: 'HMS',
                                    description: ''
                                });
                                intervalCallVariable = setInterval(function () {
                                    $('#callCounterSpan').countdown('resume');
                                }, 1);
                            }

                            if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Charging()) != -1) {
                                if (!isCharged) {
                                    LogEvent("HashHandlerClass::MessageHandler::CallAnsweredEvent DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "Charging_DNIS=" + clientObj.getDNIS_Charging() + ";", 0);
                                    if (!NDFCall) {
                                        var tmp_charging_cli = clientObj.getAttachedData()[2];
                                        if (tmp_charging_cli == 'NDF') {
                                            tmp_charging_cli = $.trim(clientObj.getAttachedData()[1]);
                                        }
                                        if (starts_with('92', tmp_charging_cli)) {

                                        } else {
                                            tmp_charging_cli = '92' + tmp_charging_cli;
                                        }
                                        caresPostCallAdjustment(tmp_charging_cli);
                                    } else {
                                        caresPostCallAdjustment(caller_msisdn);
                                    }
                                    isCharged = true;
                                }
                            }

                            $("#lightbox-bg-blackout").css('display', 'none');

                            acw_check = true;
                            brk_check = true;

                            ivr_click_state = 3;
                            awc_click_state = 1;

                            isCallDTLCalled = false;
                            isFirstHold = true;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallAnsweredFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallAnsweredSuccess';
                            //setCaresIntegration(caller_msisdn);
                            clientObj.setCallStartTime();
                            //setButtonSelection($('#btnAnswer_Call'));

                            $('#btnAnswer_Call').css('background-position-x', '-275px');

                            $('#btnAnswer_Call').attr('title', 'HangUp');
                            $('#btnAnswer_Call').parent().find('div.label span').html('hangup');
                            $('#btnAnswer_Call').attr('id', 'btnHang_up');

                            lightbox_black_hover = true;

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            $("#lightbox-bg-blackout").mouseenter();

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);

                            if (!isCharged) {
                                //$('#callCounterSpan').css('color','red');
                                $('#callCounterSpan').countdown({
                                    since: new Date(),
                                    compact: true,
                                    format: 'HMS',
                                    description: ''
                                });
                                intervalCallVariable = setInterval(function () {
                                    $('#callCounterSpan').countdown('resume');
                                }, 1);
                            }

                            if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Charging()) != -1) {
                                if (!isCharged) {
                                    LogEvent("HashHandlerClass::MessageHandler::CallAnsweredEvent DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "Charging_DNIS=" + clientObj.getDNIS_Charging() + ";", 0);
                                    if (!NDFCall) {
                                        var tmp_charging_cli = clientObj.getAttachedData()[2];
                                        if (tmp_charging_cli == 'NDF') {
                                            tmp_charging_cli = $.trim(clientObj.getAttachedData()[1]);
                                        }
                                        if (starts_with('92', tmp_charging_cli)) {

                                        } else {
                                            tmp_charging_cli = '92' + tmp_charging_cli;
                                        }
                                        caresPostCallAdjustment(tmp_charging_cli);
                                    } else {
                                        caresPostCallAdjustment(caller_msisdn);
                                    }
                                    isCharged = true;
                                }
                            }

                            $("#lightbox-bg-blackout").css('display', 'none');

                            acw_check = true;
                            brk_check = true;

                            ivr_click_state = 3;
                            awc_click_state = 1;

                            isCallDTLCalled = false;
                            isFirstHold = true;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallAnsweredFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallAnsweredSuccess';
                            //setCaresIntegration(caller_msisdn);
                            clientObj.setCallStartTime();
                            //setButtonSelection($('#btnAnswer_Call'));

                            $('#btnAnswer_Call').css('background-position-x', '-275px');

                            $('#btnAnswer_Call').attr('title', 'HangUp');
                            $('#btnAnswer_Call').parent().find('div.label span').html('hangup');
                            $('#btnAnswer_Call').attr('id', 'btnHang_up');
                            //If agent is of fwbl.
                            if (isFwblAgent) {
                                fwblCustomerCli(clientObj.getUsername(), caller_msisdn, 1, fwblCallGui);
                            }
                            lightbox_black_hover = true;

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            $("#lightbox-bg-blackout").mouseenter();

                            resetButtonSelection($('#btnNRforNextCall'));
                            clientObj.setNRforNextCall(false);

                            if (!isCharged) {
                                //$('#callCounterSpan').css('color','red');
                                $('#callCounterSpan').countdown({
                                    since: new Date(),
                                    compact: true,
                                    format: 'HMS',
                                    description: ''
                                });
                                intervalCallVariable = setInterval(function () {
                                    $('#callCounterSpan').countdown('resume');
                                }, 1);
                            }

                            if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Charging()) != -1) {
                                if (!isCharged) {
                                    LogEvent("HashHandlerClass::MessageHandler::CallAnsweredEvent DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "Charging_DNIS=" + clientObj.getDNIS_Charging() + ";", 0);
                                    if (!NDFCall) {
                                        var tmp_charging_cli = clientObj.getAttachedData()[2];
                                        if (tmp_charging_cli == 'NDF') {
                                            tmp_charging_cli = $.trim(clientObj.getAttachedData()[1]);
                                        }
                                        if (starts_with('92', tmp_charging_cli)) {

                                        } else {
                                            tmp_charging_cli = '92' + tmp_charging_cli;
                                        }
                                        caresPostCallAdjustment(tmp_charging_cli);
                                    } else {
                                        caresPostCallAdjustment(caller_msisdn);
                                    }
                                    isCharged = true;
                                }
                            }

                            $("#lightbox-bg-blackout").css('display', 'none');

                            acw_check = true;
                            brk_check = true;

                            ivr_click_state = 3;
                            awc_click_state = 1;

                            isCallDTLCalled = false;
                            isFirstHold = true;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallAnsweredFailure';
                        }
                        break;
                }
                thirdParam = null;
                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'CallConnected':
                //alert('CallConnected Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallConnectedSuccess';
                            agentOnCall = true;
                            acw_ivr_transfered = false;

                            $('.screenWorkCodes').show();
                            $('#divACW_WorkSpace').show();

                            $("#search_acw").val('');
                            $("#search_ivr").val('');

                            ivr_click_state = 3;
                            awc_click_state = 1;

                            isCallDTLCalled = false;
                            isFirstHold = true;

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallConnectedFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallConnectedSuccess';
                            agentOnCall = true;
                            acw_ivr_transfered = false;

                            $('.screenWorkCodes').show();
                            $('#divACW_WorkSpace').show();

                            $("#search_acw").val('');
                            $("#search_ivr").val('');

                            ivr_click_state = 3;
                            awc_click_state = 1;

                            isCallDTLCalled = false;
                            isFirstHold = true;

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallConnectedFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallConnectedSuccess';
                            agentOnCall = true;
                            acw_ivr_transfered = false;

                            $('.screenWorkCodes').show();
                            $('#divACW_WorkSpace').show();

                            $("#search_acw").val('');
                            $("#search_ivr").val('');

                            ivr_click_state = 3;
                            awc_click_state = 1;

                            isCallDTLCalled = false;
                            isFirstHold = true;

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallConnectedFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallConnectedSuccess';
                            agentOnCall = true;
                            acw_ivr_transfered = false;

                            $('.screenWorkCodes').show();
                            $('#divACW_WorkSpace').show();

                            $("#search_acw").val('');
                            $("#search_ivr").val('');

                            ivr_click_state = 3;
                            awc_click_state = 1;

                            isCallDTLCalled = false;
                            isFirstHold = true;

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallConnectedFailure';
                        }
                        break;
                }

                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'CallDisconnect':
                //alert('CallDisconnect Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallDisconnectedSuccess';
                            agentOnCall = false;
                            lightbox_black_hover = true;
                            clearTimeout(timeoutCallAnswerVariable);
                            clearInterval(intervalUnicaHeartBeat);

                            $(".sop-pop-div").html("");
                            $("input.sop-search-field").val("");

                            thirdParam = "";
                            try {
                                thirdParam = $.trim(dataArray[2]);
                            } catch (exception) {
                                thirdParam = "";
                            }
                            LogEvent("HashHandlerClass::Third Parameter:: " + thirdParam + "", 0);
                            clientObj.setCallEndTime();
                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                if (!isCallDTLCalled) {
                                    clientObj.Not_Ready(clientObj.getACWNRCode());
                                    clientObj.setAfterCallInfo();

                                }
                            } else {
                                if (!isCallDTLCalled) {
                                    clientObj.setCallTransType(null);
                                    clientObj.setCallStartTime();
                                    clientObj.setCallEndTime();
                                    isCallDTLCalled = true;
                                }
                            }

                            $('#callCounterSpan').countdown('destroy');
                            $('#callCounterSpan').css('color', 'white');
                            $('#callCounterSpan').html('00:00:00');
                            clearInterval(intervalCallVariable);

                            $('#holdCounterSpan').countdown('destroy');
                            //$('#holdCounterSpan').css('color','white');
                            $('#holdCounterSpan').html('00:00:00');
                            $('#holdCounterSpan').hide();
                            clearInterval(intervalHoldVariable);


                            //attachedData = null;
                            resetButtonSelection($('#btnHang_up'));

                            $('.transfer-pop-out-wrapper').css('display', 'none');
                            $(".telephony-bar-wrapper").stop();
                            $(".telephony-bar-wrapper").animate({
                                right: -294
                            }, 10, 'easeInOutCirc');

                            $('#btnHang_up').css('background-position-x', '-110px');

                            $('#btnHang_up').attr('title', 'Answer');
                            $('#btnHang_up').parent().find('div.label span').html('answer');
                            $('#btnHang_up').attr('id', 'btnAnswer_Call');
                            resetButtons();
                            //ClearPortlets();
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();
                            //workcodeScreenTrans();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            $("#search_acw").val('');
                            $("#search_ivr").val('');

                            $(".complaint-management-lightbox .search-wrapper input").val('');

                            isCharged = false;
                            isFirstHold = true;

                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                if (!isCallDTLCalled) {
                                    isCallDTLCalled = true;
                                    if (acw_check) {
                                        LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);
                                        setTimeout(function () {
                                            if (acw_ivr_transfered) {
                                                WebLogs("AWC_JS::submitAWC_btn.click()::workcodes selected", 0);
                                                WebLogs("AWC_JS::submitAWC_btn.click()::calling caresWorkCode function", 0);

                                                setTimeout(function () {
                                                    $('#btnReady').attr("disabled", false);
                                                    $('#btnReady').css('opacity', 1);
                                                    console.log("#btnReady state now : " + $('#btnReady').attr("disabled"));
                                                }, 500);

                                                //caresWorkCode(ACW_idArray);

                                                WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT on call(closing acw popup)", 0);
                                                $('#close-acw-lightbox').mousedown();
                                                switch (clientObj.getIpPhoneType()) {
                                                    case "Nortel":
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            $('#btnReady').css('background-position-x', '-275px');

                                                            $('#btnReady').parent().find('div.label span').html('not ready');
                                                            $('#btnReady').attr('id', 'btnNot_Ready');
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);
                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);
                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_LHR":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_ISB":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                    case "Cisco_KHI":

                                                        $('#btnReady').css('background-position-x', '-275px');

                                                        $('#btnReady').parent().find('div.label span').html('not ready');
                                                        $('#btnReady').attr('id', 'btnNot_Ready');
                                                        if (!clientObj.getNRforNextCall()) {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to ready agent", 0);

                                                            clientObj.Ready();
                                                        } else {
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::agent is NOT ready for next call", 0);
                                                            WebLogs("AWC_JS::submitAWC_btn.click()::going to NOT ready agent", 0);

                                                            clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
                                                        }
                                                        break;
                                                }
                                            } else {
                                                afterCallPopup();
                                            }
                                        }, 100);
                                    }
                                    isCallDTLCalled = true;
                                }
                            } else {
                                LogEvent("HashHandlerClass::Third parameter else condition(call requeued) :: " + thirdParam + "", 0);
                                $('#btnReady').attr('disabled', 'false');

                                $('#btnReady').css('background-position-x', '-275px');

                                $('#btnReady').parent().find('div.label span').html('not ready');
                                $('#btnReady').attr('title', 'Not Ready');
                                $('#btnReady').attr('id', 'btnNot_Ready');
                            }

                            unicaEndSession();
                            //$('.screenWorkCodes').hide();
                            //$('#divACW_WorkSpace').hide();		                
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallDisconnectedFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallDisconnectedSuccess';
                            agentOnCall = false;
                            lightbox_black_hover = true;

                            clearTimeout(timeoutCallAnswerVariable);
                            clearInterval(intervalUnicaHeartBeat);

                            $(".sop-pop-div").html("");
                            $("input.sop-search-field").val("");

                            thirdParam = "";
                            try {
                                thirdParam = $.trim(dataArray[2]);
                            } catch (exception) {
                                thirdParam = "";
                            }
                            LogEvent("HashHandlerClass::Third Parameter:: " + thirdParam + "", 0);
                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                if (!isCallDTLCalled) {
                                    //clientObj.setAfterCallInfo();									
                                }
                            } else {
                                if (!isCallDTLCalled) {
                                    clientObj.setCallTransType(null);
                                    clientObj.setCallStartTime();
                                    clientObj.setCallEndTime();
                                    isCallDTLCalled = true;
                                }
                            }

                            $('#callCounterSpan').countdown('destroy');
                            $('#callCounterSpan').css('color', 'white');
                            $('#callCounterSpan').html('00:00:00');
                            clearInterval(intervalCallVariable);

                            $('#holdCounterSpan').countdown('destroy');
                            //$('#holdCounterSpan').css('color','white');
                            $('#holdCounterSpan').html('00:00:00');
                            $('#holdCounterSpan').hide();
                            clearInterval(intervalHoldVariable);


                            //attachedData = null;
                            resetButtonSelection($('#btnHang_up'));

                            $('.transfer-pop-out-wrapper').css('display', 'none');
                            $(".telephony-bar-wrapper").stop();
                            $(".telephony-bar-wrapper").animate({
                                right: -294
                            }, 10, 'easeInOutCirc');

                            $('#btnHang_up').css('background-position-x', '-110px');

                            $('#btnHang_up').attr('title', 'Answer');
                            $('#btnHang_up').parent().find('div.label span').html('answer');
                            $('#btnHang_up').attr('id', 'btnAnswer_Call');
                            resetButtons();
                            //ClearPortlets();
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();
                            //workcodeScreenTrans();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            $("#search_acw").val('');
                            $("#search_ivr").val('');

                            $(".complaint-management-lightbox .search-wrapper input").val('');

                            isCharged = false;
                            isFirstHold = true;

                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                /*if(acw_check){
                                 LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);
                                 clientObj.Not_Ready(clientObj.getACWNRCode());
                                 setTimeout(function(){
                                 afterCallPopup();
                                 },1000);
                                 }*/
                            } else {
                                LogEvent("HashHandlerClass::Third parameter else condition(call requeued) :: " + thirdParam + "", 0);
                                $('#btnReady').attr('disabled', 'false');

                                $('#btnReady').css('background-position-x', '-275px');

                                $('#btnReady').parent().find('div.label span').html('not ready');
                                $('#btnReady').attr('title', 'Not Ready');
                                $('#btnReady').attr('id', 'btnNot_Ready');
                            }

                            unicaEndSession();

                            //$('.screenWorkCodes').hide();
                            //$('#divACW_WorkSpace').hide();		                
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallDisconnectedFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallDisconnectedSuccess';
                            agentOnCall = false;
                            lightbox_black_hover = true;

                            clearTimeout(timeoutCallAnswerVariable);
                            clearInterval(intervalUnicaHeartBeat);

                            $(".sop-pop-div").html("");
                            $("input.sop-search-field").val("");

                            thirdParam = "";
                            try {
                                thirdParam = $.trim(dataArray[2]);
                            } catch (exception) {
                                thirdParam = "";
                            }
                            LogEvent("HashHandlerClass::Third Parameter:: " + thirdParam + "", 0);
                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                if (!isCallDTLCalled) {
                                    //clientObj.setAfterCallInfo();									
                                }
                            } else {
                                if (!isCallDTLCalled) {
                                    clientObj.setCallTransType(null);
                                    clientObj.setCallStartTime();
                                    clientObj.setCallEndTime();
                                    isCallDTLCalled = true;
                                }
                            }

                            $('#callCounterSpan').countdown('destroy');
                            $('#callCounterSpan').css('color', 'white');
                            $('#callCounterSpan').html('00:00:00');
                            clearInterval(intervalCallVariable);

                            $('#holdCounterSpan').countdown('destroy');
                            //$('#holdCounterSpan').css('color','white');
                            $('#holdCounterSpan').html('00:00:00');
                            $('#holdCounterSpan').hide();
                            clearInterval(intervalHoldVariable);


                            //attachedData = null;
                            resetButtonSelection($('#btnHang_up'));

                            $('.transfer-pop-out-wrapper').css('display', 'none');
                            $(".telephony-bar-wrapper").stop();
                            $(".telephony-bar-wrapper").animate({
                                right: -294
                            }, 10, 'easeInOutCirc');

                            $('#btnHang_up').css('background-position-x', '-110px');

                            $('#btnHang_up').attr('title', 'Answer');
                            $('#btnHang_up').parent().find('div.label span').html('answer');
                            $('#btnHang_up').attr('id', 'btnAnswer_Call');
                            resetButtons();
                            //ClearPortlets();
                            clearPortals();
                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();
                            //workcodeScreenTrans();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            $("#search_acw").val('');
                            $("#search_ivr").val('');

                            $(".complaint-management-lightbox .search-wrapper input").val('');

                            isCharged = false;
                            isFirstHold = true;

                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                /*if(acw_check){
                                 LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);
                                 clientObj.Not_Ready(clientObj.getACWNRCode());
                                 setTimeout(function(){
                                 afterCallPopup();
                                 },1000);
                                 }*/
                            } else {
                                LogEvent("HashHandlerClass::Third parameter else condition(call requeued) :: " + thirdParam + "", 0);
                                $('#btnReady').attr('disabled', 'false');

                                $('#btnReady').css('background-position-x', '-275px');

                                $('#btnReady').parent().find('div.label span').html('not ready');
                                $('#btnReady').attr('title', 'Not Ready');
                                $('#btnReady').attr('id', 'btnNot_Ready');
                            }

                            unicaEndSession();

                            //$('.screenWorkCodes').hide();
                            //$('#divACW_WorkSpace').hide();		                
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallDisconnectedFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallDisconnectedSuccess';
                            agentOnCall = false;
                            lightbox_black_hover = true;
                            if (isFwblAgent) {
                                fwblCustomerCli(clientObj.getUsername(), caller_msisdn, 0, fwblCallGui);
                                notifyCrmOfCallEnd(fwblCallGui);
                                fwblCallGui = "";
                                //clearinterval feedback enable
                                clearInterval(intervalFeedbackEnable);
                            }
                            clearTimeout(timeoutCallAnswerVariable);
                            clearInterval(intervalUnicaHeartBeat);

                            $(".sop-pop-div").html("");
                            $("input.sop-search-field").val("");

                            thirdParam = "";
                            try {
                                thirdParam = $.trim(dataArray[2]);
                            } catch (exception) {
                                thirdParam = "";
                            }
                            LogEvent("HashHandlerClass::Third Parameter:: " + thirdParam + "", 0);
                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                if (!isCallDTLCalled) {
                                    //clientObj.setAfterCallInfo();									
                                }
                            } else {
                                if (!isCallDTLCalled) {
                                    clientObj.setCallTransType(null);
                                    clientObj.setCallStartTime();
                                    clientObj.setCallEndTime();
                                    isCallDTLCalled = true;
                                }
                            }

                            $('#callCounterSpan').countdown('destroy');
                            $('#callCounterSpan').css('color', 'white');
                            $('#callCounterSpan').html('00:00:00');
                            clearInterval(intervalCallVariable);

                            $('#holdCounterSpan').countdown('destroy');
                            //$('#holdCounterSpan').css('color','white');
                            $('#holdCounterSpan').html('00:00:00');
                            $('#holdCounterSpan').hide();
                            clearInterval(intervalHoldVariable);


                            //attachedData = null;
                            resetButtonSelection($('#btnHang_up'));

                            $('.transfer-pop-out-wrapper').css('display', 'none');
                            $(".telephony-bar-wrapper").stop();
                            $(".telephony-bar-wrapper").animate({
                                right: -294
                            }, 10, 'easeInOutCirc');

                            $('#btnHang_up').css('background-position-x', '-110px');

                            $('#btnHang_up').attr('title', 'Answer');
                            $('#btnHang_up').parent().find('div.label span').html('answer');
                            $('#btnHang_up').attr('id', 'btnAnswer_Call');

                            resetButtons();
                            //ClearPortlets();
                            clearPortals();
                            clearFWBLForm();


                            $('div.left-col div.body').hide();
                            $('div.middle-col div.body').hide();
                            $('div.right-col div.body').hide();

                            $('div.customer-demo-lightbox div.body').hide();
                            $('.map-controls-wrapper').hide();
                            $("#mapImage").attr("src", "images/error_location.png");
                            $('div.profit-sanc-lightbox div.body').hide();
                            $('div.package-details-lightbox div.body').hide();
                            $('div.latest-activities-lightbox div.body').hide();
                            //workcodeScreenTrans();

                            splash_screen_hide_hash();
                            $("#lightbox-bg-blackout").css('display', 'block');

                            $("#search_acw").val('');
                            $("#search_ivr").val('');

                            $(".complaint-management-lightbox .search-wrapper input").val('');

                            isCharged = false;
                            isFirstHold = true;

                            if (thirdParam != "DA" && thirdParam != "RQ") {
                                /*if(acw_check){
                                 LogEvent("HashHandlerClass::Third parameter if condition(call not requeued) :: " + thirdParam + "", 0);
                                 clientObj.Not_Ready(clientObj.getACWNRCode());
                                 setTimeout(function(){
                                 afterCallPopup();
                                 },1000);
                                 }*/
                            } else {
                                LogEvent("HashHandlerClass::Third parameter else condition(call requeued) :: " + thirdParam + "", 0);
                                $('#btnReady').attr('disabled', 'false');

                                $('#btnReady').css('background-position-x', '-275px');

                                $('#btnReady').parent().find('div.label span').html('not ready');
                                $('#btnReady').attr('title', 'Not Ready');
                                $('#btnReady').attr('id', 'btnNot_Ready');
                            }

                            unicaEndSession();

                            //$('.screenWorkCodes').hide();
                            //$('#divACW_WorkSpace').hide();		                
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallDisconnectedFailure';
                        }
                        break;
                }
                if (ipPhoneType == "Nortel") {
                    var buttonStatesObj = new ButtonStatesClass();
                    LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                    SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                }
                break;
            case 'CallHold':
                //alert('CallHold Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallHoldSuccess';
                            //setButtonSelection($('#btnHold'));

                            $('#holdCounterSpan').show();
                            $('#holdCounterSpan').css('color', '#FFD200');
                            $('#holdCounterSpan').countdown({
                                since: new Date(),
                                compact: true,
                                format: 'HMS',
                                description: ''
                            });
                            intervalHoldVariable = setInterval(function () {
                                $('#holdCounterSpan').countdown('resume');
                            }, 1);
                            isFirstHold = false;
                            /*
                             if(isFirstHold){
                             $('#holdCounterSpan').show();
                             $('#holdCounterSpan').css('color','#FFD200');
                             $('#holdCounterSpan').countdown({
                             since: new Date(), 
                             compact: true, 
                             format: 'HMS', 
                             description: ''
                             });
                             
                             isFirstHold = false;
                             }
                             else{
                             $('#holdCounterSpan').show();
                             $('#holdCounterSpan').css('color','#FFD200');
                             $('#holdCounterSpan').countdown('resume');
                             }
                             */
                            $('#btnHold').css('background-position-x', '-220px');

                            $('#btnHold').attr('title', 'UnHold');
                            $('#btnHold').parent().find('div.label span').html('unhold');
                            $('#btnHold').attr('id', 'btnUnHold');
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallHoldFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallHoldSuccess';
                            //setButtonSelection($('#btnHold'));

                            $('#holdCounterSpan').show();
                            $('#holdCounterSpan').css('color', '#FFD200');
                            $('#holdCounterSpan').countdown({
                                since: new Date(),
                                compact: true,
                                format: 'HMS',
                                description: ''
                            });
                            intervalHoldVariable = setInterval(function () {
                                $('#holdCounterSpan').countdown('resume');
                            }, 1);
                            isFirstHold = false;
                            /*
                             if(isFirstHold){
                             $('#holdCounterSpan').show();
                             $('#holdCounterSpan').css('color','#FFD200');
                             $('#holdCounterSpan').countdown({
                             since: new Date(), 
                             compact: true, 
                             format: 'HMS', 
                             description: ''
                             });
                             
                             isFirstHold = false;
                             }
                             else{
                             $('#holdCounterSpan').show();
                             $('#holdCounterSpan').css('color','#FFD200');
                             $('#holdCounterSpan').countdown('resume');
                             }
                             */

                            $('#btnHold').css('background-position-x', '-220px');

                            $('#btnHold').attr('title', 'UnHold');
                            $('#btnHold').parent().find('div.label span').html('unhold');
                            $('#btnHold').attr('id', 'btnUnHold');
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallHoldFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallHoldSuccess';
                            //setButtonSelection($('#btnHold'));

                            $('#holdCounterSpan').show();
                            $('#holdCounterSpan').css('color', '#FFD200');
                            $('#holdCounterSpan').countdown({
                                since: new Date(),
                                compact: true,
                                format: 'HMS',
                                description: ''
                            });
                            intervalHoldVariable = setInterval(function () {
                                $('#holdCounterSpan').countdown('resume');
                            }, 1);
                            isFirstHold = false;
                            /*
                             if(isFirstHold){
                             $('#holdCounterSpan').show();
                             $('#holdCounterSpan').css('color','#FFD200');
                             $('#holdCounterSpan').countdown({
                             since: new Date(), 
                             compact: true, 
                             format: 'HMS', 
                             description: ''
                             });
                             
                             isFirstHold = false;
                             }
                             else{
                             $('#holdCounterSpan').show();
                             $('#holdCounterSpan').css('color','#FFD200');
                             $('#holdCounterSpan').countdown('resume');
                             }
                             */

                            $('#btnHold').css('background-position-x', '-220px');

                            $('#btnHold').attr('title', 'UnHold');
                            $('#btnHold').parent().find('div.label span').html('unhold');
                            $('#btnHold').attr('id', 'btnUnHold');
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallHoldFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallHoldSuccess';
                            //setButtonSelection($('#btnHold'));
                            $('#holdCounterSpan').show();
                            $('#holdCounterSpan').css('color', '#FFD200');
                            $('#holdCounterSpan').countdown({
                                since: new Date(),
                                compact: true,
                                format: 'HMS',
                                description: ''
                            });
                            intervalHoldVariable = setInterval(function () {
                                $('#holdCounterSpan').countdown('resume');
                            }, 1);
                            isFirstHold = false;
                            /*
                             if(isFirstHold){
                             $('#holdCounterSpan').show();
                             $('#holdCounterSpan').css('color','#FFD200');
                             $('#holdCounterSpan').countdown({
                             since: new Date(), 
                             compact: true, 
                             format: 'HMS', 
                             description: ''
                             });
                             
                             isFirstHold = false;
                             }
                             else{
                             $('#holdCounterSpan').show();
                             $('#holdCounterSpan').css('color','#FFD200');
                             $('#holdCounterSpan').countdown('resume');
                             }
                             */

                            $('#btnHold').css('background-position-x', '-220px');

                            $('#btnHold').attr('title', 'UnHold');
                            $('#btnHold').parent().find('div.label span').html('unhold');
                            $('#btnHold').attr('id', 'btnUnHold');
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallHoldFailure';
                        }
                        break;
                }

                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'CallUnHold':
                //alert('CallUnHold Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallUnHoldSuccess';
                            resetButtonSelection($('#btnUnHold'));

                            $('#holdCounterSpan').countdown('destroy');
                            //$('#holdCounterSpan').countdown('pause');
                            //$('#holdCounterSpan').css('color','white');
                            $('#holdCounterSpan').hide();

                            clearInterval(intervalHoldVariable);

                            $('#btnUnHold').css('background-position-x', '0px');

                            $('#btnUnHold').attr('title', 'Hold');
                            $('#btnUnHold').parent().find('div.label span').html('hold');
                            $('#btnUnHold').attr('id', 'btnHold');

                            btn_transfer_pressed = false;
                            btn_consult_pressed = false;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallUnHoldFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            if (_agentOnInternalCall && !clientObj.isSupervisor()) {
                            } else {
                                buttonState = 'CallUnHoldSuccess';
                                resetButtonSelection($('#btnUnHold'));

                                $('#holdCounterSpan').countdown('destroy');
                                //$('#holdCounterSpan').countdown('pause');
                                //$('#holdCounterSpan').css('color','white');
                                $('#holdCounterSpan').hide();

                                clearInterval(intervalHoldVariable);

                                $('#btnUnHold').css('background-position-x', '0px');

                                $('#btnUnHold').attr('title', 'Hold');
                                $('#btnUnHold').parent().find('div.label span').html('hold');
                                $('#btnUnHold').attr('id', 'btnHold');

                                btn_transfer_pressed = false;
                                btn_consult_pressed = false;
                            }
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallUnHoldFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            if (_agentOnInternalCall && !clientObj.isSupervisor()) {
                            } else {
                                buttonState = 'CallUnHoldSuccess';
                                resetButtonSelection($('#btnUnHold'));

                                $('#holdCounterSpan').countdown('destroy');
                                //$('#holdCounterSpan').countdown('pause');
                                //$('#holdCounterSpan').css('color','white');
                                $('#holdCounterSpan').hide();

                                clearInterval(intervalHoldVariable);

                                $('#btnUnHold').css('background-position-x', '0px');

                                $('#btnUnHold').attr('title', 'Hold');
                                $('#btnUnHold').parent().find('div.label span').html('hold');
                                $('#btnUnHold').attr('id', 'btnHold');

                                btn_transfer_pressed = false;
                                btn_consult_pressed = false;
                            }
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallUnHoldFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            if (_agentOnInternalCall && !clientObj.isSupervisor()) {
                            } else {
                                buttonState = 'CallUnHoldSuccess';
                                resetButtonSelection($('#btnUnHold'));

                                $('#holdCounterSpan').countdown('destroy');
                                //$('#holdCounterSpan').countdown('pause');
                                //$('#holdCounterSpan').css('color','white');
                                $('#holdCounterSpan').hide();

                                clearInterval(intervalHoldVariable);

                                $('#btnUnHold').css('background-position-x', '0px');

                                $('#btnUnHold').attr('title', 'Hold');
                                $('#btnUnHold').parent().find('div.label span').html('hold');
                                $('#btnUnHold').attr('id', 'btnHold');

                                btn_transfer_pressed = false;
                                btn_consult_pressed = false;
                            }
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallUnHoldFailure';
                        }
                        break;

                }
                if (_agentOnInternalCall && !clientObj.isSupervisor()) {
                } else {
                    var buttonStatesObj = new ButtonStatesClass();
                    LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                    SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                }
                break;
            case 'InternalCallPresentation':
                //alert('AgentSession Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICPresentSuccess';
                            //$('#anchrBtn_internalCallDiv').click();
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICPresentFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICPresentSuccess';
                            //$('#anchrBtn_internalCallDiv').click();
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICPresentFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICPresentSuccess';
                            //$('#anchrBtn_internalCallDiv').click();
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICPresentFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICPresentSuccess';
                            //$('#anchrBtn_internalCallDiv').click();
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICPresentFailure';
                        }
                        break;
                }

                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'TransferCallPresentation':
                //alert('TransferCallPresentation Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallTCSuccess';

                            isCallTransfered = true;

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallTCFailure';

                            isCallTransfered = false;
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallTCSuccess';

                            isCallTransfered = true;

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallTCFailure';

                            isCallTransfered = false;
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallTCSuccess';

                            isCallTransfered = true;

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallTCFailure';

                            isCallTransfered = false;
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallTCSuccess';

                            isCallTransfered = true;

                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallTCFailure';

                            isCallTransfered = false;
                        }
                        break;
                }

                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                //SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'InternalCallEstablished':
                //alert('InternalCallEstablished Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICESuccess';
                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICEFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICESuccess';
                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICEFailure';
                        }
                        break;
                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICESuccess';
                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            _agentOnInternalCall = true;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICEFailure';
                        }
                        break;
                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICESuccess';
                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            agentOnInternalCall = true;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICEFailure';
                        }
                        break;
                }

                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'InternalCallDisconnected':
                //alert('InternalCallDisconnected Event');
                var buttonState = '';
                switch (ipPhoneType) {
                    case "Nortel":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICDSuccess';
                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            btn_transfer_pressed = false;
                            btn_consult_pressed = false;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICDFailure';
                        }
                        break;
                    case "Cisco_LHR":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICDSuccess';
                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            btn_transfer_pressed = false;
                            btn_consult_pressed = false;
                            _agentOnInternalCall = false;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICDFailure';
                        }
                        break;

                    case "Cisco_ISB":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICDSuccess';
                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            btn_transfer_pressed = false;
                            btn_consult_pressed = false;
                            _agentOnInternalCall = false;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICDFailure';
                        }
                        break;

                    case "Cisco_KHI":
                        if (dataArray[1] == 0) {
                            buttonState = 'CallICDSuccess';
                            try {
                                $.fancybox.close();
                            } catch (ex) {
                            }
                            btn_transfer_pressed = false;
                            btn_consult_pressed = false;
                            _agentOnInternalCall = false;
                        } else if (dataArray[1] == 1) {
                            buttonState = 'CallICDFailure';
                        }
                        break;
                }

                var buttonStatesObj = new ButtonStatesClass();
                LogEvent("HashHandlerClass::MessageHandler::" + buttonState + " event recieved", 0);
                SetButtonStates(buttonStatesObj.GetButtonStates(buttonState));
                break;
            case 'CallAttachData':

                switch (ipPhoneType) {
                    case "Nortel":
                        //$("#lightbox-bg-blackout").css('display', 'block');
                        //$(".telephony-bar-wrapper").mouseenter();

                        attachedData = null;

                        unicaEventsPosted = 0;
                        unicaResegEventsPosted = 0;
                        acw_ivr_transfered = false;

                        clientObj.setCallTransType(null);

                        if (dataArray.length < 10) {
                            attachedData = dataArray;
                            NDFCall = true;
                        } else {
                            attachedData = formatArray(dataArray);
                            NDFCall = false;
                        }
                        if (attachedData.length < 10) {
                            attachedData = attachedData[1].split(',');
                            DNIS = attachedData[2];
                            caller_msisdn = attachedData[0];

                            //caller_msisdn = "3367966692";
                            //caller_msisdn = "3335171211";
                            //caller_msisdn = "3365896255";
                            //caller_msisdn = "3002063437";
                            //caller_msisdn = "3002029511";
                            //caller_msisdn = "3335100019";
                            //caller_msisdn = "3334838982";

                            if (caller_msisdn.length > 0) {

                                clientObj.setUnicaSessionID("unica_" + caller_msisdn + "_" + new Date().getTime());

                                if ((starts_with('92', caller_msisdn) && caller_msisdn.length < 12)) {
                                    //localnumber with 92 city code
                                    caller_msisdn = '92' + caller_msisdn;
                                } else if (starts_with('00', caller_msisdn)) {

                                } else {
                                    if (starts_with('92', caller_msisdn)) {

                                    } else {
                                        if (starts_with('0', caller_msisdn)) {
                                            caller_msisdn = caller_msisdn.substring(1);
                                        }
                                        caller_msisdn = '92' + caller_msisdn;
                                    }
                                }

                            }
                            switchID = attachedData[3];
                            callType = null;
                            CDN = null;
                            customerType = null;
                            clientObj.setCallSessionID("A" + clientObj.getAgentID() + "M" + caller_msisdn + "D" + new Date().getTime());
                        } else {
                            //DNIS = dataArray[42];
                            DNIS = dataArray[dataArray.length - 2];
                            caller_msisdn = $.trim(attachedData[1]);
                            if (caller_msisdn == 'NDF') {
                                caller_msisdn = $.trim(attachedData[2]);
                            }

                            //caller_msisdn = "3002063437";
                            //caller_msisdn = "3002063437";
                            //caller_msisdn = "3002029511";
                            //caller_msisdn = "3335720072";
                            //caller_msisdn = "3334838982";

                            if (caller_msisdn.length > 0) {

                                clientObj.setUnicaSessionID("unica_" + caller_msisdn + "_" + new Date().getTime());

                                if ((starts_with('92', caller_msisdn) && caller_msisdn.length < 12)) {
                                    //localnumber with 92 city code
                                    caller_msisdn = '92' + caller_msisdn;
                                } else if (starts_with('00', caller_msisdn)) {

                                } else {
                                    if (starts_with('92', caller_msisdn)) {

                                    } else {
                                        if (starts_with('0', caller_msisdn)) {
                                            caller_msisdn = caller_msisdn.substring(1);
                                        }
                                        caller_msisdn = '92' + caller_msisdn;
                                    }
                                }

                            }
                            CDN = $.trim(attachedData[37]);
                            customerType = attachedData[9];
                            callType = attachedData[41];

                            //switchID = dataArray[43];
                            switchID = dataArray[dataArray.length - 1];
                            clientObj.setCallSessionID(attachedData[40]);
                        }

                        $('div.left-col div.body').show();
                        $('div.middle-col div.body').show();
                        $('div.right-col div.body').show();

                        $('div.customer-demo-lightbox div.body').show();
                        $('.map-controls-wrapper').show();
                        $('div.profit-sanc-lightbox div.body').show();
                        $('div.package-details-lightbox div.body').show();
                        $('div.latest-activities-lightbox div.body').show();

                        $("div.cards-wrapper div.body").hide();
                        $("div.card-action-btns-wrapper").hide();


                        //if(ACW_idArray.length > 0)
                        //code commented by Qaseem
//                        {
//                            //caresWorkCode(ACW_idArray);                            
//                            $('#close-acw-lightbox').mousedown();
//                            $('#acwSelectedName').html('');
//                            $('#acwListCodeName').html('');
//                            ACW_idArray = new Array();
//                        }
//
//                        $('#submitAWC_btn').css('display', 'none');
//                        $('#close-acw-lightbox').css('display', '');
//                        $('#minimize-acw-lightbox').css('display', 'none');
//                      /Code commented by Qaseem upto here

                        if (isPIAAgent == false && isPIAWCEnabled == false) {
                            //If Added by Qaseem for PIA
                            {
                                //caresWorkCode(ACW_idArray);                            
                                $('#close-acw-lightbox').mousedown();
                                $('#acwSelectedName').html('');
                                $('#acwListCodeName').html('');
                                ACW_idArray = new Array();
                            }

                            $('#submitAWC_btn').css('display', 'none');
                            $('#close-acw-lightbox').css('display', '');
                            $('#minimize-acw-lightbox').css('display', 'none');

                        }

                        if (clientObj.isUnicaDown()) {
                            $("#unicaOfferLoadImage").parent().parent().find("div.body").hide();
                        }

                        if (caller_msisdn.length > 0) {
                            setCaresIntegration(caller_msisdn);
                            getTDViewResult(caller_msisdn);
                        }
                        clearInterval(intervalUnicaHeartBeat);
                        if (caller_msisdn.length > 0) {
                            unicaStartSession(caller_msisdn);
                        }
                        intervalUnicaHeartBeat = setInterval(function () {
                            unicaHeartBeat();
                        }, 3 * 60 * 1000);

                        if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Direct_Cares()) != -1) {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "DirectCares_DNIS=" + clientObj.getDNIS_Direct_Cares() + ";", 0);
                            getCustomerInfo(caller_msisdn);
                        } else if (attachedData.length < 10) {
                            //getCustomerInfo(caller_msisdn);
                            if (starts_with('9292', caller_msisdn) || starts_with('00', caller_msisdn)) {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            } else if (starts_with('92', caller_msisdn)) {
                                $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                            } else {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            }
                            getCustomerInfo(caller_msisdn);
                        } else {
                            //populateVAS();
                            //getCustomerInfo_VAS(caller_msisdn);
                            $('#splashName span').html(attachedData[5]);
                            var customer_name = attachedData[5];
                            if (customer_name.length > 19) {
                                customer_name = customer_name.substring(0, 15) + '...';
                            }
                            $('#divCustomerName').html('<span title="' + attachedData[5] + '" class="h1-lucida-bold-yellow">' + customer_name + '</span>');
                            $('#divCustomerName_bg').html('<span class="big-bold-lucida-white">' + attachedData[5] + '</span>');
                            $('#splashPackageDetails span').html(attachedData[7]);
                            if (starts_with('9292', caller_msisdn) || starts_with('00', caller_msisdn)) {
                                $('#splashNumber span').html(caller_msisdn);
                                //$('#divCustomerNumber').html('<span class="regular-lucida-italic">'+caller_msisdn+'</span>');
                                //$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">'+caller_msisdn+'</span>');
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[2] + '</span>');
                            } else {
                                if (caller_msisdn.length == 12 && starts_with('923', caller_msisdn)) {
                                    setCaresIntegration('92' + attachedData[2]);
                                    $('#splashNumber span').html('0' + attachedData[2]);
                                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attachedData[2] + '</span>');
                                    $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[1] + '</span>');
                                } else {
                                    setCaresIntegration('92' + attachedData[2]);
                                    $('#splashNumber span').html('0' + attachedData[2]);
                                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attachedData[2] + '</span>');
                                    $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[2] + '</span>');
                                }
                            }
                            getCustomerInfo_VAS(caller_msisdn);
                        }

                        if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Auto_Call_Pickup()) != -1) {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "AutoCallPickup_DNIS=" + clientObj.getDNIS_Auto_Call_Pickup() + ";", 0);
                            if (!clientObj.isSupervisor()) {
                                timeoutCallAnswerVariable = setTimeout(function () {
                                    clientObj.Answer_Call();
                                }, 1500);

                            }
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData AutoCallPickUp" + ";" + "AgentID=" + clientObj.getAgentID() + ";" + "IsSupervisor=" + clientObj.isSupervisor() + ";", 0);
                        } else {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS DOESNOT exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "AutoCallPickup_DNIS=" + clientObj.getDNIS_Auto_Call_Pickup() + ";", 0);
                        }

                        //splash_screen_show_hash();
                        if (splash_mutex)
                        {
                            splash_mutex = false;
                            //$("button").css('z-index', '0');
                            $("#lightbox-bg-blackout").css('display', 'block');
                            $("#lightbox-bg-blackout").css('z-index', '1000');
                            $("#lightbox-bg-blackout").css('opacity', '0.8');
                            /*
                             $("#lightbox-bg-blackout").animate({
                             opacity: 0.8
                             }, 10, 'easeOutCirc');
                             */
                            $(".splash-screen-lightbox").css('display', 'block');
                            $(".splash-screen-lightbox").css('z-index', '2000');
                            $(".splash-screen-lightbox").css('opacity', '1');
                            /*
                             $(".splash-screen-lightbox").animate({
                             opacity: 1
                             }, 10, 'easeOutCirc');
                             */
                            $(".splash-screen-lightbox").focus();
                            splash_mutex = true;
                        }
                        $(".telephony-bar-wrapper").mouseenter();

                        lightbox_black_hover = false;
                        if (caller_msisdn.length > 0) {
                            getSessionHistory(caller_msisdn);
                        }

                        $("#search-msisdn").val(caller_msisdn);
                        getCustomerLocation(caller_msisdn);

                        $(".sop-pop-div").html("");
                        $("input.sop-search-field").val("");

                        var _number = $("#search-msisdn").val().trim();
                        if (starts_with("03", _number) && _number.length == 11) {
                            getCustomerComplaints("92" + _number.substring(1));
                        } else if (starts_with("923", _number) && _number.length == 12) {
                            getCustomerComplaints(_number);
                        }

                        var DNIS_desc = clientObj.getDNIS_Description_Text();
                        var dnis = clientObj.getDNIS();
                        for (var i = 0; i < DNIS_desc.length; i++) {
                            if (DNIS_desc[i].split('=')[0] == dnis) {
                                $('#DNISDescriptionDiv').html(DNIS_desc[i].split('=')[1]);
                                $('#DNISDescriptionSpan').html(DNIS_desc[i].split('=')[1]);
                            }
                        }

                        if (clientObj.getCallCDN() != null) {
                            var CDN_desc = clientObj.getCDN_Description();
                            var cdn = clientObj.getCallCDN();
                            for (var i = 0; i < CDN_desc.length; i++) {
                                if (CDN_desc[i].split('=')[0] == cdn) {
                                    $('#CDNDescriptionDiv').html(CDN_desc[i].split('=')[1]);
                                    $('#CDNDescriptionSpan').html(CDN_desc[i].split('=')[1]);
                                }
                            }
                        }
                        break;
                    case "Cisco_LHR":
                        //$("#lightbox-bg-blackout").css('display', 'block');
                        //$(".telephony-bar-wrapper").mouseenter();

                        attachedData = null;

                        acw_ivr_transfered = false;
                        unicaEventsPosted = 0;
                        unicaResegEventsPosted = 0;

                        clientObj.setCallTransType(null);

                        if (dataArray.length < 10) {
                            attachedData = dataArray;
                            NDFCall = true;
                        } else {
                            attachedData = (dataArray);
                            NDFCall = false;
                        }

                        var _call_status = dataArray[5];
                        var _cares_msisdn = null;
                        _call_cli_anis = dataArray[2];
                        _call_punchedin_subnumber = dataArray[1];

                        if (_call_punchedin_subnumber.length == "") {
                            _call_punchedin_subnumber = _call_cli_anis;
                        }

                        if (_call_status == "Transferred") {
                            _cares_msisdn = dataArray[1];
                        } else if (_call_status == "NotOMO") {
                            _cares_msisdn = dataArray[2];
                        } else if (_call_status == "Verified") {
                            _cares_msisdn = dataArray[1];
                        } else if (_call_status == "NotVerified") {
                            _cares_msisdn = dataArray[2];
                        } else {
                            _cares_msisdn = null;
                        }
                        $("#divCallStatus").html('<span class="regular-body-lucida-light-grey">Call Status </span><span class="regular-lucida-bold-11">' + _call_status + '</span>');
                        if (_cares_msisdn !== null) {
                            if ((starts_with('92', _cares_msisdn) && _cares_msisdn.length < 12)) {
                                //localnumber with 92 city code
                                _cares_msisdn = '92' + _cares_msisdn;
                            } else if (starts_with('00', _cares_msisdn)) {

                            } else if (starts_with('3', _cares_msisdn) && _cares_msisdn.length == 10) {
                                _cares_msisdn = '92' + _cares_msisdn;
                            } else {
                                if (starts_with('92', _cares_msisdn)) {

                                } else {
                                    if (starts_with('0', _cares_msisdn)) {
                                        _cares_msisdn = _cares_msisdn.substring(1);
                                    }
                                    _cares_msisdn = '92' + _cares_msisdn;
                                }
                            }
                        }
                        if (_call_cli_anis != "") {
                            if ((starts_with('92', _call_cli_anis) && _call_cli_anis.length < 12)) {
                                //localnumber with 92 city code
                                _call_cli_anis = '92' + _call_cli_anis;
                            } else if (starts_with('00', _call_cli_anis)) {

                            } else if (starts_with('3', _call_cli_anis) && _call_cli_anis.length == 10) {
                                _call_cli_anis = '92' + _call_cli_anis;
                            } else {
                                if (starts_with('92', _call_cli_anis)) {

                                } else {
                                    if (starts_with('0', _call_cli_anis)) {
                                        _call_cli_anis = _call_cli_anis.substring(1);
                                    }
                                    _call_cli_anis = '92' + _call_cli_anis;
                                }
                            }
                        }
                        if (_call_punchedin_subnumber != "") {
                            if ((starts_with('92', _call_punchedin_subnumber) && _call_punchedin_subnumber.length < 12)) {
                                //localnumber with 92 city code
                                _call_punchedin_subnumber = '92' + _call_punchedin_subnumber;
                            } else if (starts_with('00', _call_punchedin_subnumber)) {

                            } else if (starts_with('3', _call_punchedin_subnumber) && _call_punchedin_subnumber.length == 10) {
                                _call_punchedin_subnumber = '92' + _call_punchedin_subnumber;
                            } else {
                                if (starts_with('92', _call_punchedin_subnumber)) {

                                } else {
                                    if (starts_with('0', _call_punchedin_subnumber)) {
                                        _call_punchedin_subnumber = _call_punchedin_subnumber.substring(1);
                                    }
                                    _call_punchedin_subnumber = '92' + _call_punchedin_subnumber;
                                }
                            }
                        }
                        $("#div_cli_anis_bg").html('<span class="regular-body-12">Caller CLI: </span><span class="regular-body-white-bold">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                        $("#div_punchedin_subnumber_bg").html('<span class="regular-body-12">Punched In Number: </span><span class="regular-body-white-bold">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                        $("#_cli_anis_Span").html('0' + _call_cli_anis.substring(2, _call_cli_anis.length));
                        $("#_punchedin_subnumber_Span").html('0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length));


                        if (attachedData.length < 10) {
                            attachedData = attachedData[1].split(',');
                            DNIS = attachedData[2];
                            caller_msisdn = attachedData[0];
                            clientObj.setUnicaSessionID("unica_" + caller_msisdn + "_" + new Date().getTime());
                            if ((starts_with('92', caller_msisdn) && caller_msisdn.length < 12)) {
                                //localnumber with 92 city code
                                caller_msisdn = '92' + caller_msisdn;
                            } else if (starts_with('00', caller_msisdn)) {

                            } else {
                                if (starts_with('92', caller_msisdn)) {

                                } else {
                                    if (starts_with('0', caller_msisdn)) {
                                        caller_msisdn = caller_msisdn.substring(1);
                                    }
                                    caller_msisdn = '92' + caller_msisdn;
                                }
                            }
                            switchID = attachedData[3];
                            callType = null;
                            CDN = null;
                            customerType = null;
                            //clientObj.setCallSessionID("A" + clientObj.getAgentID() + "M" + caller_msisdn + "D" + new Date().getTime());
                            clientObj.setCallSessionID(switchID);
                        } else {
                            //DNIS = dataArray[42];
                            DNIS = dataArray[dataArray.length - 2];
                            caller_msisdn = $.trim(attachedData[1]);
                            if (caller_msisdn == 'NDF' || caller_msisdn.length < 5) {
                                caller_msisdn = $.trim(attachedData[2]);
                            }
                            clientObj.setUnicaSessionID("unica_" + caller_msisdn + "_" + new Date().getTime());
                            if ((starts_with('92', caller_msisdn) && caller_msisdn.length < 12)) {
                                //localnumber with 92 city code
                                caller_msisdn = '92' + caller_msisdn;
                            } else if (starts_with('00', caller_msisdn)) {

                            } else {
                                if (starts_with('92', caller_msisdn)) {

                                } else {
                                    if (starts_with('0', caller_msisdn)) {
                                        caller_msisdn = caller_msisdn.substring(1);
                                    }
                                    caller_msisdn = '92' + caller_msisdn;
                                }
                            }
                            //CDN = null;
                            CDN = dataArray[dataArray.length - 3];
                            customerType = attachedData[3];
                            callType = "";
                            switchID = dataArray[dataArray.length - 1];
                            //clientObj.setCallSessionID(attachedData[40]);
                            //clientObj.setCallSessionID("A" + clientObj.getAgentID() + "M" + caller_msisdn + "D" + new Date().getTime());
                            clientObj.setCallSessionID(switchID);
                        }

                        $('div.left-col div.body').show();
                        $('div.middle-col div.body').show();
                        $('div.right-col div.body').show();

                        $('div.customer-demo-lightbox div.body').show();
                        $('.map-controls-wrapper').show();
                        $('div.profit-sanc-lightbox div.body').show();
                        $('div.package-details-lightbox div.body').show();
                        $('div.latest-activities-lightbox div.body').show();

                        //if(ACW_idArray.length > 0)
                        {
                            //caresWorkCode(ACW_idArray);
                            $('#close-acw-lightbox').mousedown();
                            $('#acwSelectedName').html('');
                            $('#acwListCodeName').html('');
                            ACW_idArray = new Array();
                        }

                        $('#submitAWC_btn').css('display', 'none');
                        $('#close-acw-lightbox').css('display', '');
                        $('#minimize-acw-lightbox').css('display', 'none');

                        if (clientObj.isUnicaDown()) {
                            $("#unicaOfferLoadImage").parent().parent().find("div.body").hide();
                        }
                        if (!NDFCall) {
                            if (caller_msisdn.length < 12) {
                                NDFCall = true;
                            }
                        }
                        if (!NDFCall) {
                            if ($.trim(attachedData[1]).length == 0) {
                                NDFCall = true;
                            }
                        }

                        if (_cares_msisdn !== null) {
                            setCaresIntegration(_cares_msisdn);
                        } else {
                            setCaresIntegration(caller_msisdn);
                        }

//						setCaresIntegration(caller_msisdn);

                        getTDViewResult(caller_msisdn);

                        clearInterval(intervalUnicaHeartBeat);

                        unicaStartSession(caller_msisdn);

                        intervalUnicaHeartBeat = setInterval(function () {
                            unicaHeartBeat();
                        }, 3 * 60 * 1000);

                        if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Direct_Cares()) != -1) {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "DirectCares_DNIS=" + clientObj.getDNIS_Direct_Cares() + ";", 0);
                            getCustomerInfo(caller_msisdn);
                        } else if (attachedData.length < 10 || ($.trim(attachedData[1]).length == 0 && $.trim(attachedData[3]).length == 0 && $.trim(attachedData[4]).length == 0 && $.trim(attachedData[5]).length == 0 && $.trim(attachedData[6]).length == 0 && $.trim(attachedData[7]).length == 0 && $.trim(attachedData[8]).length == 0 && $.trim(attachedData[9]).length == 0 && $.trim(attachedData[10]).length == 0 && $.trim(attachedData[11]).length == 0 && $.trim(attachedData[12]).length == 0)) {
                            //getCustomerInfo(caller_msisdn);
                            if (starts_with('9292', caller_msisdn) || starts_with('00', caller_msisdn)) {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            } else if (starts_with('92', caller_msisdn)) {
                                $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
//								$('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
//								$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                            } else {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            }
                            getCustomerInfo(caller_msisdn);
                        } else {
                            //populateVAS();
                            //getCustomerInfo_VAS(caller_msisdn);
                            $('#splashName span').html(attachedData[7]);
                            var customer_name = attachedData[7];
                            if (customer_name.length > 19) {
                                customer_name = customer_name.substring(0, 15) + '...';
                            }
                            $('#divCustomerName').html('<span title="' + attachedData[7] + '" class="h1-lucida-bold-yellow">' + customer_name + '</span>');
                            $('#divCustomerName_bg').html('<span class="big-bold-lucida-white">' + attachedData[7] + '</span>');
                            $('#splashPackageDetails span').html(attachedData[6]);

                            if (starts_with('9292', caller_msisdn) || starts_with('00', caller_msisdn)) {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            } else {
                                if (attachedData[2].length < 5) {
                                    $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
                                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                    $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                } else {
                                    if ($.trim(attachedData[1]).length == 0) {
                                        $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
                                        $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                        $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                    } else {
                                        if (starts_with('0', attachedData[2]) && attachedData[2].length == 11) {
//											setCaresIntegration('92' + attachedData[2].substring(1, attachedData[2].length));
                                            if (_cares_msisdn !== null) {
                                                setCaresIntegration(_cares_msisdn);
                                            } else {
                                                setCaresIntegration('92' + attachedData[2].substring(1, attachedData[2].length));
                                            }
                                            $('#splashNumber span').html('' + attachedData[2]);
//											$('#divCustomerNumber').html('<span class="regular-lucida-italic">' + attachedData[2] + '</span>');
//											$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[1].substring(2, attachedData[1].length) + '</span>');

                                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                                        } else {
//											setCaresIntegration('92' + attachedData[2]);
                                            if (_cares_msisdn !== null) {
                                                setCaresIntegration(_cares_msisdn);
                                            } else {
                                                setCaresIntegration('92' + attachedData[2]);
                                            }
                                            $('#splashNumber span').html('0' + attachedData[2]);
//											$('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attachedData[2] + '</span>');
//											$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[1].substring(2, attachedData[1].length) + '</span>');

                                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                                        }
                                    }
                                }
                            }
                            getCustomerInfo_VAS(caller_msisdn);
                        }

                        if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Auto_Call_Pickup()) != -1) {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "AutoCallPickup_DNIS=" + clientObj.getDNIS_Auto_Call_Pickup() + ";", 0);
                            if (!clientObj.isSupervisor()) {
                                timeoutCallAnswerVariable = setTimeout(function () {
                                    clientObj.Answer_Call();
                                }, 1500);
                            }
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData AutoCallPickUp" + ";" + "AgentID=" + clientObj.getAgentID() + ";" + "IsSupervisor=" + clientObj.isSupervisor() + ";", 0);
                        } else {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS DOESNOT exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "AutoCallPickup_DNIS=" + clientObj.getDNIS_Auto_Call_Pickup() + ";", 0);
                        }

                        //splash_screen_show_hash();
                        if (splash_mutex)
                        {
                            splash_mutex = false;
                            //$("button").css('z-index', '0');
                            $("#lightbox-bg-blackout").css('display', 'block');
                            $("#lightbox-bg-blackout").css('z-index', '1000');
                            $("#lightbox-bg-blackout").css('opacity', '0.8');
                            /*
                             $("#lightbox-bg-blackout").animate({
                             opacity: 0.8
                             }, 10, 'easeOutCirc');
                             */
                            $(".splash-screen-lightbox").css('display', 'block');
                            $(".splash-screen-lightbox").css('z-index', '2000');
                            $(".splash-screen-lightbox").css('opacity', '1');
                            /*
                             $(".splash-screen-lightbox").animate({
                             opacity: 1
                             }, 10, 'easeOutCirc');
                             */
                            $(".splash-screen-lightbox").focus();
                            splash_mutex = true;
                        }
                        $(".telephony-bar-wrapper").mouseenter();

                        lightbox_black_hover = false;

                        getSessionHistory(caller_msisdn);

                        $("#search-msisdn").val(caller_msisdn);
                        getCustomerLocation(caller_msisdn);

                        $(".sop-pop-div").html("");
                        $("input.sop-search-field").val("");

                        var _number = $("#search-msisdn").val().trim();
                        if (starts_with("03", _number) && _number.length == 11) {
                            getCustomerComplaints("92" + _number.substring(1));
                        } else if (starts_with("923", _number) && _number.length == 12) {
                            getCustomerComplaints(_number);
                        }

                        var DNIS_desc = clientObj.getDNIS_Description_Text();
                        var dnis = clientObj.getDNIS();
                        for (var i = 0; i < DNIS_desc.length; i++) {
                            if (DNIS_desc[i].split('=')[0] == dnis) {
                                $('#DNISDescriptionDiv').html(DNIS_desc[i].split('=')[1]);
                                $('#DNISDescriptionSpan').html(DNIS_desc[i].split('=')[1]);
                            }
                        }

                        $('#CDNDescriptionDiv').parent().parent().hide();
                        $('#CDNDescriptionSpan').parent().hide();

                        /*
                         if (clientObj.getCallCDN() != null) {
                         var CDN_desc = clientObj.getCDN_Description();
                         var cdn = clientObj.getCallCDN();
                         for (var i = 0; i < CDN_desc.length; i++) {
                         if (CDN_desc[i].split('=')[0] == cdn) {
                         $('#CDNDescriptionDiv').html(CDN_desc[i].split('=')[1]);
                         $('#CDNDescriptionSpan').html(CDN_desc[i].split('=')[1]);		                        
                         }
                         }
                         }
                         */
                        break;
                    case "Cisco_ISB":
                        //$("#lightbox-bg-blackout").css('display', 'block');
                        //$(".telephony-bar-wrapper").mouseenter();

                        attachedData = null;

                        acw_ivr_transfered = false;
                        unicaEventsPosted = 0;
                        unicaResegEventsPosted = 0;

                        clientObj.setCallTransType(null);

                        if (dataArray.length < 10) {
                            attachedData = dataArray;
                            NDFCall = true;
                        } else {
                            attachedData = (dataArray);
                            NDFCall = false;
                        }

                        var _call_status = dataArray[9];
                        var _cares_msisdn = null;
                        _call_cli_anis = dataArray[2];
                        _call_punchedin_subnumber = dataArray[1];

                        if (_call_punchedin_subnumber.length == "") {
                            _call_punchedin_subnumber = _call_cli_anis;
                        }

                        if (_call_status == "Transferred") {
                            _cares_msisdn = dataArray[1];
                        } else if (_call_status == "NotOMO") {
                            _cares_msisdn = dataArray[2];
                        } else if (_call_status == "Verified") {
                            _cares_msisdn = dataArray[1];
                        } else if (_call_status == "NotVerified") {
                            _cares_msisdn = dataArray[2];
                        } else {
                            _cares_msisdn = null;
                        }
                        $("#divCallStatus").html('<span class="regular-body-lucida-light-grey">Call Status </span><span class="regular-lucida-bold-11">' + _call_status + '</span>');
                        if (_cares_msisdn !== null) {
                            if ((starts_with('92', _cares_msisdn) && _cares_msisdn.length < 12)) {
                                //localnumber with 92 city code
                                _cares_msisdn = '92' + _cares_msisdn;
                            } else if (starts_with('00', _cares_msisdn)) {

                            } else if (starts_with('3', _cares_msisdn) && _cares_msisdn.length == 10) {
                                _cares_msisdn = '92' + _cares_msisdn;
                            } else {
                                if (starts_with('92', _cares_msisdn)) {

                                } else {
                                    if (starts_with('0', _cares_msisdn)) {
                                        _cares_msisdn = _cares_msisdn.substring(1);
                                    }
                                    _cares_msisdn = '92' + _cares_msisdn;
                                }
                            }
                        }
                        if (_call_cli_anis != "") {
                            if ((starts_with('92', _call_cli_anis) && _call_cli_anis.length < 12)) {
                                //localnumber with 92 city code
                                _call_cli_anis = '92' + _call_cli_anis;
                            } else if (starts_with('00', _call_cli_anis)) {

                            } else if (starts_with('3', _call_cli_anis) && _call_cli_anis.length == 10) {
                                _call_cli_anis = '92' + _call_cli_anis;
                            } else {
                                if (starts_with('92', _call_cli_anis)) {

                                } else {
                                    if (starts_with('0', _call_cli_anis)) {
                                        _call_cli_anis = _call_cli_anis.substring(1);
                                    }
                                    _call_cli_anis = '92' + _call_cli_anis;
                                }
                            }
                        }
                        if (_call_punchedin_subnumber != "") {
                            if ((starts_with('92', _call_punchedin_subnumber) && _call_punchedin_subnumber.length < 12)) {
                                //localnumber with 92 city code
                                _call_punchedin_subnumber = '92' + _call_punchedin_subnumber;
                            } else if (starts_with('00', _call_punchedin_subnumber)) {

                            } else if (starts_with('3', _call_punchedin_subnumber) && _call_punchedin_subnumber.length == 10) {
                                _call_punchedin_subnumber = '92' + _call_punchedin_subnumber;
                            } else {
                                if (starts_with('92', _call_punchedin_subnumber)) {

                                } else {
                                    if (starts_with('0', _call_punchedin_subnumber)) {
                                        _call_punchedin_subnumber = _call_punchedin_subnumber.substring(1);
                                    }
                                    _call_punchedin_subnumber = '92' + _call_punchedin_subnumber;
                                }
                            }
                        }
                        $("#div_cli_anis_bg").html('<span class="regular-body-12">Caller CLI: </span><span class="regular-body-white-bold">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                        $("#div_punchedin_subnumber_bg").html('<span class="regular-body-12">Punched In Number: </span><span class="regular-body-white-bold">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                        $("#_cli_anis_Span").html('0' + _call_cli_anis.substring(2, _call_cli_anis.length));
                        $("#_punchedin_subnumber_Span").html('0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length));

                        if (attachedData.length < 10) {
                            attachedData = attachedData[1].split(',');
                            DNIS = attachedData[2];
                            caller_msisdn = attachedData[0];
                            clientObj.setUnicaSessionID("unica_" + caller_msisdn + "_" + new Date().getTime());
                            if ((starts_with('92', caller_msisdn) && caller_msisdn.length < 12)) {
                                //localnumber with 92 city code
                                caller_msisdn = '92' + caller_msisdn;
                            } else if (starts_with('00', caller_msisdn)) {

                            } else {
                                if (starts_with('92', caller_msisdn)) {

                                } else {
                                    if (starts_with('0', caller_msisdn)) {
                                        caller_msisdn = caller_msisdn.substring(1);
                                    }
                                    caller_msisdn = '92' + caller_msisdn;
                                }
                            }
                            switchID = attachedData[3];
                            callType = null;
                            CDN = null;
                            customerType = null;
                            //clientObj.setCallSessionID("A" + clientObj.getAgentID() + "M" + caller_msisdn + "D" + new Date().getTime());
                            clientObj.setCallSessionID(switchID);
                        } else {
                            //DNIS = dataArray[42];
                            DNIS = dataArray[dataArray.length - 2];
                            caller_msisdn = $.trim(attachedData[1]);
                            if (caller_msisdn == 'NDF' || caller_msisdn.length < 5) {
                                caller_msisdn = $.trim(attachedData[2]);
                            }
                            clientObj.setUnicaSessionID("unica_" + caller_msisdn + "_" + new Date().getTime());
                            if ((starts_with('92', caller_msisdn) && caller_msisdn.length < 12)) {
                                //localnumber with 92 city code
                                caller_msisdn = '92' + caller_msisdn;
                            } else if (starts_with('00', caller_msisdn)) {

                            } else {
                                if (starts_with('92', caller_msisdn)) {

                                } else {
                                    if (starts_with('0', caller_msisdn)) {
                                        caller_msisdn = caller_msisdn.substring(1);
                                    }
                                    caller_msisdn = '92' + caller_msisdn;
                                }
                            }
                            //CDN = null;
                            CDN = dataArray[dataArray.length - 3];
                            customerType = attachedData[3];
                            callType = "";
                            switchID = dataArray[dataArray.length - 1];
                            //clientObj.setCallSessionID(attachedData[40]);
                            //clientObj.setCallSessionID("A" + clientObj.getAgentID() + "M" + caller_msisdn + "D" + new Date().getTime());
                            clientObj.setCallSessionID(switchID);
                        }

                        $('div.left-col div.body').show();
                        $('div.middle-col div.body').show();
                        $('div.right-col div.body').show();

                        $('div.customer-demo-lightbox div.body').show();
                        $('.map-controls-wrapper').show();
                        $('div.profit-sanc-lightbox div.body').show();
                        $('div.package-details-lightbox div.body').show();
                        $('div.latest-activities-lightbox div.body').show();

                        //if(ACW_idArray.length > 0)
                        {
                            //caresWorkCode(ACW_idArray);
                            $('#close-acw-lightbox').mousedown();
                            $('#acwSelectedName').html('');
                            $('#acwListCodeName').html('');
                            ACW_idArray = new Array();
                        }

                        $('#submitAWC_btn').css('display', 'none');
                        $('#close-acw-lightbox').css('display', '');
                        $('#minimize-acw-lightbox').css('display', 'none');

                        if (clientObj.isUnicaDown()) {
                            $("#unicaOfferLoadImage").parent().parent().find("div.body").hide();
                        }
                        if (!NDFCall) {
                            if (caller_msisdn.length < 12) {
                                NDFCall = true;
                            }
                        }
                        if (!NDFCall) {
                            if ($.trim(attachedData[1]).length == 0) {
                                NDFCall = true;
                            }
                        }

                        if (_cares_msisdn !== null) {
                            setCaresIntegration(_cares_msisdn);
                        } else {
                            setCaresIntegration(caller_msisdn);
                        }

//						setCaresIntegration(caller_msisdn);

                        getTDViewResult(caller_msisdn);

                        clearInterval(intervalUnicaHeartBeat);

                        unicaStartSession(caller_msisdn);

                        intervalUnicaHeartBeat = setInterval(function () {
                            unicaHeartBeat();
                        }, 3 * 60 * 1000);

                        if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Direct_Cares()) != -1) {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "DirectCares_DNIS=" + clientObj.getDNIS_Direct_Cares() + ";", 0);
                            getCustomerInfo(caller_msisdn);
                        } else if (attachedData.length < 10 || ($.trim(attachedData[1]).length == 0 && $.trim(attachedData[3]).length == 0 && $.trim(attachedData[4]).length == 0 && $.trim(attachedData[5]).length == 0 && $.trim(attachedData[6]).length == 0 && $.trim(attachedData[7]).length == 0 && $.trim(attachedData[8]).length == 0 && $.trim(attachedData[9]).length == 0 && $.trim(attachedData[10]).length == 0 && $.trim(attachedData[11]).length == 0 && $.trim(attachedData[12]).length == 0)) {
                            //getCustomerInfo(caller_msisdn);
                            if (starts_with('9292', caller_msisdn) || starts_with('00', caller_msisdn)) {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            } else if (starts_with('92', caller_msisdn)) {
                                $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
//								$('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
//								$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');

                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                            } else {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            }
                            getCustomerInfo(caller_msisdn);
                        } else {
                            //populateVAS();
                            //getCustomerInfo_VAS(caller_msisdn);
                            $('#splashName span').html(attachedData[7]);
                            var customer_name = attachedData[7];
                            if (customer_name.length > 19) {
                                customer_name = customer_name.substring(0, 15) + '...';
                            }
                            $('#divCustomerName').html('<span title="' + attachedData[7] + '" class="h1-lucida-bold-yellow">' + customer_name + '</span>');
                            $('#divCustomerName_bg').html('<span class="big-bold-lucida-white">' + attachedData[7] + '</span>');
                            $('#splashPackageDetails span').html(attachedData[6]);

                            if (starts_with('9292', caller_msisdn) || starts_with('00', caller_msisdn)) {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            } else {
                                if (attachedData[2].length < 5) {
                                    $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
                                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                    $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                } else {
                                    if ($.trim(attachedData[1]).length == 0) {
                                        $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
                                        $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                        $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                    } else {
                                        if (starts_with('0', attachedData[2]) && attachedData[2].length == 11) {
//											setCaresIntegration('92' + attachedData[2].substring(1, attachedData[2].length));
                                            if (_cares_msisdn !== null) {
                                                setCaresIntegration(_cares_msisdn);
                                            } else {
                                                setCaresIntegration('92' + attachedData[2].substring(1, attachedData[2].length));
                                            }
                                            $('#splashNumber span').html('' + attachedData[2]);
//											$('#divCustomerNumber').html('<span class="regular-lucida-italic">' + attachedData[2] + '</span>');
//											$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[1].substring(2, attachedData[1].length) + '</span>');

                                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                                        } else {
//											setCaresIntegration('92' + attachedData[2]);
                                            if (_cares_msisdn !== null) {
                                                setCaresIntegration(_cares_msisdn);
                                            } else {
                                                setCaresIntegration('92' + attachedData[2]);
                                            }
                                            $('#splashNumber span').html('0' + attachedData[2]);
//											$('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attachedData[2] + '</span>');
//											$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[1].substring(2, attachedData[1].length) + '</span>');

                                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                                        }
                                    }
                                }
                            }
                            getCustomerInfo_VAS(caller_msisdn);
                        }

                        if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Auto_Call_Pickup()) != -1) {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "AutoCallPickup_DNIS=" + clientObj.getDNIS_Auto_Call_Pickup() + ";", 0);
                            if (!clientObj.isSupervisor()) {
                                timeoutCallAnswerVariable = setTimeout(function () {
                                    clientObj.Answer_Call();
                                }, 1500);
                            }
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData AutoCallPickUp" + ";" + "AgentID=" + clientObj.getAgentID() + ";" + "IsSupervisor=" + clientObj.isSupervisor() + ";", 0);
                        } else {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS DOESNOT exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "AutoCallPickup_DNIS=" + clientObj.getDNIS_Auto_Call_Pickup() + ";", 0);
                        }

                        //splash_screen_show_hash();
                        if (splash_mutex)
                        {
                            splash_mutex = false;
                            //$("button").css('z-index', '0');
                            $("#lightbox-bg-blackout").css('display', 'block');
                            $("#lightbox-bg-blackout").css('z-index', '1000');
                            $("#lightbox-bg-blackout").css('opacity', '0.8');
                            /*
                             $("#lightbox-bg-blackout").animate({
                             opacity: 0.8
                             }, 10, 'easeOutCirc');
                             */
                            $(".splash-screen-lightbox").css('display', 'block');
                            $(".splash-screen-lightbox").css('z-index', '2000');
                            $(".splash-screen-lightbox").css('opacity', '1');
                            /*
                             $(".splash-screen-lightbox").animate({
                             opacity: 1
                             }, 10, 'easeOutCirc');
                             */
                            $(".splash-screen-lightbox").focus();
                            splash_mutex = true;
                        }
                        $(".telephony-bar-wrapper").mouseenter();

                        lightbox_black_hover = false;

                        getSessionHistory(caller_msisdn);

                        $("#search-msisdn").val(caller_msisdn);
                        getCustomerLocation(caller_msisdn);

                        $(".sop-pop-div").html("");
                        $("input.sop-search-field").val("");

                        var _number = $("#search-msisdn").val().trim();
                        if (starts_with("03", _number) && _number.length == 11) {
                            getCustomerComplaints("92" + _number.substring(1));
                        } else if (starts_with("923", _number) && _number.length == 12) {
                            getCustomerComplaints(_number);
                        }

                        var DNIS_desc = clientObj.getDNIS_Description_Text();
                        var dnis = clientObj.getDNIS();
                        for (var i = 0; i < DNIS_desc.length; i++) {
                            if (DNIS_desc[i].split('=')[0] == dnis) {
                                $('#DNISDescriptionDiv').html(DNIS_desc[i].split('=')[1]);
                                $('#DNISDescriptionSpan').html(DNIS_desc[i].split('=')[1]);
                            }
                        }

                        $('#CDNDescriptionDiv').parent().parent().hide();
                        $('#CDNDescriptionSpan').parent().hide();

                        /*
                         if (clientObj.getCallCDN() != null) {
                         var CDN_desc = clientObj.getCDN_Description();
                         var cdn = clientObj.getCallCDN();
                         for (var i = 0; i < CDN_desc.length; i++) {
                         if (CDN_desc[i].split('=')[0] == cdn) {
                         $('#CDNDescriptionDiv').html(CDN_desc[i].split('=')[1]);
                         $('#CDNDescriptionSpan').html(CDN_desc[i].split('=')[1]);		                        
                         }
                         }
                         }
                         */
                        break;
                    case "Cisco_KHI":
                        //$("#lightbox-bg-blackout").css('display', 'block');
                        //$(".telephony-bar-wrapper").mouseenter();

                        attachedData = null;

                        acw_ivr_transfered = false;
                        unicaEventsPosted = 0;
                        unicaResegEventsPosted = 0;

                        clientObj.setCallTransType(null);

                        if (dataArray.length < 10) {
                            attachedData = dataArray;
                            NDFCall = true;
                        } else {
                            attachedData = (dataArray);
                            NDFCall = false;
                        }
                        //dataArray[5] contains cnic i.e. 6110193683114
                        var _call_status = dataArray[5];
                        var _cares_msisdn = null;
                        //dataArray[2] contains number without 92 like 3365054022 
                        _call_cli_anis = dataArray[2];
                        //dataArray[1] contains number with 92 like 923365054022 
                        _call_punchedin_subnumber = dataArray[1];
                        // if  _call_punchedin_subnumber is null then  _call_punchedin_subnumber= cli_anis 
                        if (_call_punchedin_subnumber.length == "") {
                            _call_punchedin_subnumber = _call_cli_anis;
                        }

                        if (_call_status == "Transferred") {
                            _cares_msisdn = dataArray[1];
                        } else if (_call_status == "NotOMO") {
                            _cares_msisdn = dataArray[2];
                        } else if (_call_status == "Verified") {
                            _cares_msisdn = dataArray[1];
                        } else if (_call_status == "NotVerified") {
                            _cares_msisdn = dataArray[2];
                        } else {
                            //in our example this case will execute
                            _cares_msisdn = null;
                        }
                        $("#divCallStatus").html('<span class="regular-body-lucida-light-grey">Call Status </span><span class="regular-lucida-bold-11">' + _call_status + '</span>');
                        if (_cares_msisdn !== null) {
                            if ((starts_with('92', _cares_msisdn) && _cares_msisdn.length < 12)) {
                                //localnumber with 92 city code
                                _cares_msisdn = '92' + _cares_msisdn;
                            } else if (starts_with('00', _cares_msisdn)) {

                            } else if (starts_with('3', _cares_msisdn) && _cares_msisdn.length == 10) {
                                _cares_msisdn = '92' + _cares_msisdn;
                            } else {
                                if (starts_with('92', _cares_msisdn)) {

                                } else {
                                    if (starts_with('0', _cares_msisdn)) {
                                        _cares_msisdn = _cares_msisdn.substring(1);
                                    }
                                    _cares_msisdn = '92' + _cares_msisdn;
                                }
                            }
                        }
                        if (_call_cli_anis != "") {
                            if ((starts_with('92', _call_cli_anis) && _call_cli_anis.length < 12)) {
                                //localnumber with 92 city code
                                _call_cli_anis = '92' + _call_cli_anis;
                            } else if (starts_with('00', _call_cli_anis)) {

                            } else if (starts_with('3', _call_cli_anis) && _call_cli_anis.length == 10) {
                                //our example case
                                //92 will get add on the number
                                _call_cli_anis = '92' + _call_cli_anis;
                            } else {
                                if (starts_with('92', _call_cli_anis)) {

                                } else {
                                    if (starts_with('0', _call_cli_anis)) {
                                        _call_cli_anis = _call_cli_anis.substring(1);
                                    }
                                    _call_cli_anis = '92' + _call_cli_anis;
                                }
                            }
                        }
                        if (_call_punchedin_subnumber != "") {
                            if ((starts_with('92', _call_punchedin_subnumber) && _call_punchedin_subnumber.length < 12)) {
                                //localnumber with 92 city code
                                _call_punchedin_subnumber = '92' + _call_punchedin_subnumber;
                            } else if (starts_with('00', _call_punchedin_subnumber)) {

                            } else if (starts_with('3', _call_punchedin_subnumber) && _call_punchedin_subnumber.length == 10) {
                                _call_punchedin_subnumber = '92' + _call_punchedin_subnumber;
                            } else {
                                if (starts_with('92', _call_punchedin_subnumber)) {

                                } else {
                                    if (starts_with('0', _call_punchedin_subnumber)) {
                                        _call_punchedin_subnumber = _call_punchedin_subnumber.substring(1);
                                    }
                                    _call_punchedin_subnumber = '92' + _call_punchedin_subnumber;
                                }
                            }
                        }
                        $("#div_cli_anis_bg").html('<span class="regular-body-12">Caller CLI: </span><span class="regular-body-white-bold">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                        $("#div_punchedin_subnumber_bg").html('<span class="regular-body-12">Punched In Number: </span><span class="regular-body-white-bold">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                        $("#_cli_anis_Span").html('0' + _call_cli_anis.substring(2, _call_cli_anis.length));
                        $("#_punchedin_subnumber_Span").html('0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length));


                        if (attachedData.length < 10) {
                            attachedData = attachedData[1].split(',');
                            DNIS = attachedData[2];
                            caller_msisdn = attachedData[0];
                            clientObj.setUnicaSessionID("unica_" + caller_msisdn + "_" + new Date().getTime());
                            if ((starts_with('92', caller_msisdn) && caller_msisdn.length < 12)) {
                                //localnumber with 92 city code
                                caller_msisdn = '92' + caller_msisdn;
                            } else if (starts_with('00', caller_msisdn)) {

                            } else {
                                if (starts_with('92', caller_msisdn)) {

                                } else {
                                    if (starts_with('0', caller_msisdn)) {
                                        caller_msisdn = caller_msisdn.substring(1);
                                    }
                                    caller_msisdn = '92' + caller_msisdn;
                                }
                            }
                            switchID = attachedData[3];
                            callType = null;
                            CDN = null;
                            customerType = null;
                            //clientObj.setCallSessionID("A" + clientObj.getAgentID() + "M" + caller_msisdn + "D" + new Date().getTime());
                            clientObj.setCallSessionID(switchID);
                        } else {
                            if (isUnileverAgent == true) {
                                //attached data length>10 i.e. is 15....this is our case
                                WebLogs('WS:This is unliever case::when attached data is of length 16', 0);

                                //DNIS = dataArray[42];
                                //2nd last index e.g 25200

                                DNIS = dataArray[dataArray.length - 3];

                                ////DNIS = dataArray[dataArray.length - 2];

                                caller_msisdn = $.trim(attachedData[1]);
                                if (caller_msisdn == 'NDF' || caller_msisdn.length < 5) {
                                    caller_msisdn = $.trim(attachedData[2]);
                                }
                                clientObj.setUnicaSessionID("unica_" + caller_msisdn + "_" + new Date().getTime());
                                if ((starts_with('92', caller_msisdn) && caller_msisdn.length < 12)) {
                                    //localnumber with 92 city code
                                    caller_msisdn = '92' + caller_msisdn;
                                } else if (starts_with('00', caller_msisdn)) {

                                } else {
                                    if (starts_with('92', caller_msisdn)) {

                                    } else {
                                        if (starts_with('0', caller_msisdn)) {
                                            caller_msisdn = caller_msisdn.substring(1);
                                        }
                                        caller_msisdn = '92' + caller_msisdn;
                                    }
                                }
                                //CDN = null;
                                //CDN=Postpaid_LHR in our case
                                CDN = dataArray[dataArray.length - 4];

                                ////CDN = dataArray[dataArray.length - 3];

                                //customerType=POST-PAID
                                customerType = attachedData[3];
                                callType = "";
                                //switchID=icm.152779.27459
                                switchID = dataArray[dataArray.length - 2];

                                //// switchID = dataArray[dataArray.length - 1];

                                //clientObj.setCallSessionID(attachedData[40]);
                                //clientObj.setCallSessionID("A" + clientObj.getAgentID() + "M" + caller_msisdn + "D" + new Date().getTime());
                                clientObj.setCallSessionID(switchID);

                                ////////////change request///////////
                                //get this from db
                                //customerEngagementPreference = dataArray[dataArray.length - 1];
                                //this method will set the value of consnet after fetching it from db
                                WebLogs('WS:Customer Consent value before setCustomerConsent call ' + customerEngagementPreference + '', 0);

                                setCustomerConsent(caller_msisdn);
                                customerEngagementPreference = getCustomerEngagementPreference();
                                WebLogs('WS:Customer Consent value after setCustomerConsent call ' + customerEngagementPreference + '', 0);

                                if (customerEngagementPreference == 1) {
                                    $('#customerEngagementPreference').html('<span class="regular-lucida-bold-yellow" style="font-size:20;" >Yes</span>');
                                } else {
                                    $('#customerEngagementPreference').html('<span class="regular-lucida-bold-yellow" style="font-size:20;">No</span>');

                                }
                            } else {
                                ////// //not unilever////
                                WebLogs('WS:Not unilever case::when attached data is of length 15.', 0);

                                DNIS = dataArray[dataArray.length - 2];

                                caller_msisdn = $.trim(attachedData[1]);
                                if (caller_msisdn == 'NDF' || caller_msisdn.length < 5) {
                                    caller_msisdn = $.trim(attachedData[2]);
                                }
                                clientObj.setUnicaSessionID("unica_" + caller_msisdn + "_" + new Date().getTime());
                                if ((starts_with('92', caller_msisdn) && caller_msisdn.length < 12)) {
                                    //localnumber with 92 city code
                                    caller_msisdn = '92' + caller_msisdn;
                                } else if (starts_with('00', caller_msisdn)) {

                                } else {
                                    if (starts_with('92', caller_msisdn)) {

                                    } else {
                                        if (starts_with('0', caller_msisdn)) {
                                            caller_msisdn = caller_msisdn.substring(1);
                                        }
                                        caller_msisdn = '92' + caller_msisdn;
                                    }
                                }
                                //CDN = null;
                                //CDN=Postpaid_LHR in our case
                                //// CDN = dataArray[dataArray.length - 4];

                                CDN = dataArray[dataArray.length - 3];

                                //customerType=POST-PAID
                                customerType = attachedData[3];
                                callType = "";
                                //switchID=icm.152779.27459
                                ////  switchID = dataArray[dataArray.length - 2];

                                switchID = dataArray[dataArray.length - 1];

                                //clientObj.setCallSessionID(attachedData[40]);
                                //clientObj.setCallSessionID("A" + clientObj.getAgentID() + "M" + caller_msisdn + "D" + new Date().getTime());
                                clientObj.setCallSessionID(switchID);
//                            customerEngagementPreference = dataArray[dataArray.length - 1];
//                            if (customerEngagementPreference == 1) {
//                                $('#customerEngagementPreference').html('<span class="regular-lucida-bold-yellow" style="font-size:20;" >Yes</span>');
//                            } else {
//                                $('#customerEngagementPreference').html('<span class="regular-lucida-bold-yellow" style="font-size:20;">No</span>');
//
//                            }
                            }


                        }
                        //  customerEngagementPreference=dataArray[dataArray.length-1];
                        //change all above having dataArray.length-x
                        //DNIS,CDN,swithID
                        //customerEngagementPreference = 1;

                        $('div.left-col div.body').show();
                        $('div.middle-col div.body').show();
                        $('div.right-col div.body').show();

                        $('div.customer-demo-lightbox div.body').show();
                        $('.map-controls-wrapper').show();
                        $('div.profit-sanc-lightbox div.body').show();
                        $('div.package-details-lightbox div.body').show();
                        $('div.latest-activities-lightbox div.body').show();

                        //if(ACW_idArray.length > 0)
                        {
                            //caresWorkCode(ACW_idArray);
                            $('#close-acw-lightbox').mousedown();
                            $('#acwSelectedName').html('');
                            $('#acwListCodeName').html('');
                            ACW_idArray = new Array();
                        }

                        $('#submitAWC_btn').css('display', 'none');
                        $('#close-acw-lightbox').css('display', '');
                        $('#minimize-acw-lightbox').css('display', 'none');

                        if (clientObj.isUnicaDown()) {
                            $("#unicaOfferLoadImage").parent().parent().find("div.body").hide();
                        }
                        if (!NDFCall) {
                            if (caller_msisdn.length < 12) {
                                NDFCall = true;
                            }
                        }
                        if (!NDFCall) {
                            if ($.trim(attachedData[1]).length == 0) {
                                NDFCall = true;
                            }
                        }

                        if (_cares_msisdn !== null) {
                            setCaresIntegration(_cares_msisdn);
                        } else {
                            setCaresIntegration(caller_msisdn);
                        }

//						setCaresIntegration(caller_msisdn);

                        getTDViewResult(caller_msisdn);

                        clearInterval(intervalUnicaHeartBeat);

                        unicaStartSession(caller_msisdn);

                        intervalUnicaHeartBeat = setInterval(function () {
                            unicaHeartBeat();
                        }, 3 * 60 * 1000);

                        if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Direct_Cares()) != -1) {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "DirectCares_DNIS=" + clientObj.getDNIS_Direct_Cares() + ";", 0);
                            getCustomerInfo(caller_msisdn);
                        } else if (attachedData.length < 10) {
                            //getCustomerInfo(caller_msisdn);
                            if (starts_with('9292', caller_msisdn) || starts_with('00', caller_msisdn)) {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            } else if (starts_with('92', caller_msisdn)) {
                                $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
//								$('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
//								$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');

                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                            } else {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            }
                            getCustomerInfo(caller_msisdn);
                        } else {
                            //populateVAS();
                            //getCustomerInfo_VAS(caller_msisdn);
                            $('#splashName span').html(attachedData[7]);
                            var customer_name = attachedData[7];
                            if (customer_name.length > 19) {
                                customer_name = customer_name.substring(0, 15) + '...';
                            }
                            $('#divCustomerName').html('<span title="' + attachedData[7] + '" class="h1-lucida-bold-yellow">' + customer_name + '</span>');
                            $('#divCustomerName_bg').html('<span class="big-bold-lucida-white">' + attachedData[7] + '</span>');
                            $('#splashPackageDetails span').html(attachedData[6]);

                            if (starts_with('9292', caller_msisdn) || starts_with('00', caller_msisdn)) {
                                $('#splashNumber span').html(caller_msisdn);
                                $('#divCustomerNumber').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                                $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">' + caller_msisdn + '</span>');
                            } else {
                                if (attachedData[2].length < 5) {
                                    $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
                                    $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                    $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                } else {
                                    if ($.trim(attachedData[1]).length == 0) {
                                        $('#splashNumber span').html('0' + caller_msisdn.substring(2, caller_msisdn.length));
                                        $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                        $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + caller_msisdn.substring(2, caller_msisdn.length) + '</span>');
                                    } else {

                                        if (starts_with('0', attachedData[2]) && attachedData[2].length == 11) {
//											setCaresIntegration('92' + attachedData[2].substring(1, attachedData[2].length));
                                            if (_cares_msisdn !== null) {
                                                setCaresIntegration(_cares_msisdn);
                                            } else {
                                                setCaresIntegration('92' + attachedData[2].substring(1, attachedData[2].length));
                                            }
                                            $('#splashNumber span').html('' + attachedData[2]);
//											$('#divCustomerNumber').html('<span class="regular-lucida-italic">' + attachedData[2] + '</span>');
//											$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[1].substring(2, attachedData[1].length) + '</span>');

                                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                                        } else {
//											setCaresIntegration('92' + attachedData[2]);
                                            if (_cares_msisdn !== null) {
                                                setCaresIntegration(_cares_msisdn);
                                            } else {
                                                setCaresIntegration('92' + attachedData[2]);
                                            }
                                            $('#splashNumber span').html('0' + attachedData[2]);
//											$('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + attachedData[2] + '</span>');
//											$('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + attachedData[1].substring(2, attachedData[1].length) + '</span>');

                                            $('#divCustomerNumber').html('<span class="regular-lucida-italic">0' + _call_punchedin_subnumber.substring(2, _call_punchedin_subnumber.length) + '</span>');
                                            $('#divCustomerNumber_bg').html('<span class="regular-lucida-italic">0' + _call_cli_anis.substring(2, _call_cli_anis.length) + '</span>');
                                        }

                                    }
                                }
                            }
                            getCustomerInfo_VAS(caller_msisdn);
                        }

                        if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Auto_Call_Pickup()) != -1) {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "AutoCallPickup_DNIS=" + clientObj.getDNIS_Auto_Call_Pickup() + ";", 0);
                            if (!clientObj.isSupervisor()) {
                                timeoutCallAnswerVariable = setTimeout(function () {
                                    clientObj.Answer_Call();
                                }, 1500);
                            }
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData AutoCallPickUp" + ";" + "AgentID=" + clientObj.getAgentID() + ";" + "IsSupervisor=" + clientObj.isSupervisor() + ";", 0);
                        } else {
                            LogEvent("HashHandlerClass::MessageHandler::CallAttachData DNIS DOESNOT exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "AutoCallPickup_DNIS=" + clientObj.getDNIS_Auto_Call_Pickup() + ";", 0);
                        }

                        //splash_screen_show_hash();
                        if (splash_mutex)
                        {
                            splash_mutex = false;
                            //$("button").css('z-index', '0');
                            $("#lightbox-bg-blackout").css('display', 'block');
                            $("#lightbox-bg-blackout").css('z-index', '1000');
                            $("#lightbox-bg-blackout").css('opacity', '0.8');
                            /*
                             $("#lightbox-bg-blackout").animate({
                             opacity: 0.8
                             }, 10, 'easeOutCirc');
                             */
                            $(".splash-screen-lightbox").css('display', 'block');
                            $(".splash-screen-lightbox").css('z-index', '2000');
                            $(".splash-screen-lightbox").css('opacity', '1');
                            /*
                             $(".splash-screen-lightbox").animate({
                             opacity: 1
                             }, 10, 'easeOutCirc');
                             */
                            $(".splash-screen-lightbox").focus();
                            splash_mutex = true;
                        }
                        $(".telephony-bar-wrapper").mouseenter();

                        lightbox_black_hover = false;

                        getSessionHistory(caller_msisdn);

                        $("#search-msisdn").val(caller_msisdn);
                        getCustomerLocation(caller_msisdn);

                        $(".sop-pop-div").html("");
                        $("input.sop-search-field").val("");

                        var _number = $("#search-msisdn").val().trim();
                        if (starts_with("03", _number) && _number.length == 11) {
                            getCustomerComplaints("92" + _number.substring(1));
                        } else if (starts_with("923", _number) && _number.length == 12) {
                            getCustomerComplaints(_number);
                        }

                        var DNIS_desc = clientObj.getDNIS_Description_Text();
                        var dnis = clientObj.getDNIS();
                        for (var i = 0; i < DNIS_desc.length; i++) {
                            if (DNIS_desc[i].split('=')[0] == dnis) {
                                $('#DNISDescriptionDiv').html(DNIS_desc[i].split('=')[1]);
                                $('#DNISDescriptionSpan').html(DNIS_desc[i].split('=')[1]);
                            }
                        }

                        $('#CDNDescriptionDiv').parent().parent().hide();
                        $('#CDNDescriptionSpan').parent().hide();

                        /*
                         if (clientObj.getCallCDN() != null) {
                         var CDN_desc = clientObj.getCDN_Description();
                         var cdn = clientObj.getCallCDN();
                         for (var i = 0; i < CDN_desc.length; i++) {
                         if (CDN_desc[i].split('=')[0] == cdn) {
                         $('#CDNDescriptionDiv').html(CDN_desc[i].split('=')[1]);
                         $('#CDNDescriptionSpan').html(CDN_desc[i].split('=')[1]);		                        
                         }
                         }
                         }
                         */
                        break;
                }
                LogEvent("HashHandlerClass::MessageHandler::CallAttachData event recieved", 0);
                break;
            case 'GeneralFailure':
                var buttonstates = dataArray[1].split(',');
                switch (ipPhoneType) {
                    case "Nortel":
                        for (var i = 0; i < buttonstates.length; i++) {
                            if (buttonstates[i] == '1') {
                                buttonstates[i] = true;
                            } else if (buttonstates[i] == '0') {
                                buttonstates[i] = false;
                            } else {
                                buttonstates[i] = null;
                            }

                        }
                        break;
                    case "Cisco_LHR":
                        for (var i = 0; i < buttonstates.length; i++) {
                            if (buttonstates[i] == '1') {
                                buttonstates[i] = true;
                            } else if (buttonstates[i] == '0') {
                                buttonstates[i] = false;
                            } else {
                                buttonstates[i] = null;
                            }

                        }
                        break;
                    case "Cisco_ISB":
                        for (var i = 0; i < buttonstates.length; i++) {
                            if (buttonstates[i] == '1') {
                                buttonstates[i] = true;
                            } else if (buttonstates[i] == '0') {
                                buttonstates[i] = false;
                            } else {
                                buttonstates[i] = null;
                            }

                        }
                        break;
                    case "Cisco_KHI":
                        for (var i = 0; i < buttonstates.length; i++) {
                            if (buttonstates[i] == '1') {
                                buttonstates[i] = true;
                            } else if (buttonstates[i] == '0') {
                                buttonstates[i] = false;
                            } else {
                                buttonstates[i] = null;
                            }

                        }
                        break;
                }

                LogEvent("HashHandlerClass::MessageHandler::GeneralFailure event recieved, data :" + buttonstates, 0);
                SetButtonStates(buttonstates);
                break;
            case 'GeneralSuccess':
                var buttonstates = dataArray[1].split(',');
                switch (ipPhoneType) {
                    case "Nortel":
                        for (var i = 0; i < buttonstates.length; i++) {
                            if (buttonstates[i] == '1') {
                                buttonstates[i] = true;
                            } else if (buttonstates[i] == '0') {
                                buttonstates[i] = false;
                            } else {
                                buttonstates[i] = null;
                            }

                        }
                        break;
                    case "Cisco_LHR":
                        for (var i = 0; i < buttonstates.length; i++) {
                            if (buttonstates[i] == '1') {
                                buttonstates[i] = true;
                            } else if (buttonstates[i] == '0') {
                                buttonstates[i] = false;
                            } else {
                                buttonstates[i] = null;
                            }

                        }
                        break;
                    case "Cisco_ISB":
                        for (var i = 0; i < buttonstates.length; i++) {
                            if (buttonstates[i] == '1') {
                                buttonstates[i] = true;
                            } else if (buttonstates[i] == '0') {
                                buttonstates[i] = false;
                            } else {
                                buttonstates[i] = null;
                            }

                        }
                        break;
                    case "Cisco_KHI":
                        for (var i = 0; i < buttonstates.length; i++) {
                            if (buttonstates[i] == '1') {
                                buttonstates[i] = true;
                            } else if (buttonstates[i] == '0') {
                                buttonstates[i] = false;
                            } else {
                                buttonstates[i] = null;
                            }

                        }
                        break;
                }
                LogEvent("HashHandlerClass::MessageHandler::GeneralSuccess event recieved, data :" + buttonstates, 0);
                SetButtonStates(buttonstates);
                clientObj.System_Information();
                break;
            case 'NotReadyReasonCodes':
                switch (ipPhoneType) {
                    case "Nortel":
                        //alert('NotReadyReasonCodes Event');
                        break;
                    case "Cisco_LHR":
                        //alert('NotReadyReasonCodes Event');
                        var optionStr = '';
                        for (var i = 1; i < dataArray.length; i++) {
                            if (i == 1) {
                                optionStr += '<option selected="selected" value="' + dataArray[i].split('=')[0] + '" >' + dataArray[i].split('=')[1] + '</option>\n';
                            } else {
                                optionStr += '<option value="' + dataArray[i].split('=')[0] + '" >' + dataArray[i].split('=')[1] + '</option>\n';
                            }
                        }
                        $('#notReadyDropDownList').html(optionStr);
                        break;
                    case "Cisco_ISB":
                        //alert('NotReadyReasonCodes Event');
                        var optionStr = '';
                        for (var i = 1; i < dataArray.length; i++) {
                            if (i == 1) {
                                optionStr += '<option selected="selected" value="' + dataArray[i].split('=')[0] + '" >' + dataArray[i].split('=')[1] + '</option>\n';
                            } else {
                                optionStr += '<option value="' + dataArray[i].split('=')[0] + '" >' + dataArray[i].split('=')[1] + '</option>\n';
                            }
                        }
                        $('#notReadyDropDownList').html(optionStr);
                        break;
                    case "Cisco_KHI":
                        //alert('NotReadyReasonCodes Event');
                        var optionStr = '';
                        for (var i = 1; i < dataArray.length; i++) {
                            if (i == 1) {
                                optionStr += '<option selected="selected" value="' + dataArray[i].split('=')[0] + '" >' + dataArray[i].split('=')[1] + '</option>\n';
                            } else {
                                optionStr += '<option value="' + dataArray[i].split('=')[0] + '" >' + dataArray[i].split('=')[1] + '</option>\n';
                            }
                        }
                        $('#notReadyDropDownList').html(optionStr);
                        break;
                }
                break;
            case 'SystemInformation':
                //alert(data);
                clientObj.setSystemInfo(data);
                ipPhoneType = data.split('|')[4];
                ipAddr = data.split('|')[1];
                if (ipPhoneType == "Nortel") {
                    ajaxPage = "ajaxHandler_ISB.jsp";
                    ajaxPage = "HashServlet_ISB";
                } else if (ipPhoneType == "Cisco_LHR") {
                    ajaxPage = "ajaxHandler_LHR.jsp";
                    ajaxPage = "HashServlet_LHR";
                } else if (ipPhoneType == "Cisco_ISB") {
                    ajaxPage = "ajaxHandler_ISB_Cisco.jsp";
                    ajaxPage = "HashServlet_ISB_Cisco";
                } else if (ipPhoneType == "Cisco_KHI") {
                    ajaxPage = "ajaxHandler_KHI.jsp";
                    ajaxPage = "HashServlet_KHI";
                }

                getSystemConfigs(false);

                switch (ipPhoneType) {
                    case "Nortel":
                        getASNNotReadyCode();
                        updateNotReadyCodes();
                        setTimeout(function () {
                            getACWNotReadyCode();
                        }, 1000);
//						$('#btnSupv_Transfer').attr('disabled', true);
//						$('#btnSupv_Transfer').css('opacity', 0.1);

                        $('#btn_consult').attr('disabled', true);
                        $('#btn_consult').css('opacity', 0.1);

                        break;
                    case "Cisco_LHR":
                        clientObj.setAgentSetNotReadyCode("-1");
                        $('#btnSupv_Transfer').attr('disabled', false);
                        $('#btnSupv_Transfer').css('opacity', 1);

                        $('#CDNDescriptionDiv').parent().parent().hide();
                        $('#CDNDescriptionSpan').parent().hide();
                        break;
                    case "Cisco_ISB":
                        clientObj.setAgentSetNotReadyCode("-1");
                        $('#btnSupv_Transfer').attr('disabled', false);
                        $('#btnSupv_Transfer').css('opacity', 1);

                        $('#CDNDescriptionDiv').parent().parent().hide();
                        $('#CDNDescriptionSpan').parent().hide();
                        break;
                    case "Cisco_KHI":
                        clientObj.setAgentSetNotReadyCode("-1");
                        $('#btnSupv_Transfer').attr('disabled', false);
                        $('#btnSupv_Transfer').css('opacity', 1);

                        $('#CDNDescriptionDiv').parent().parent().hide();
                        $('#CDNDescriptionSpan').parent().hide();
                        break;
                }

//                getConsentInfo();
//                getFreqWC();
//                getAWCXML();                
//                getFreqIVR();
//                getIVRXML();
//                getUnicaWCXML();                
//                getComplaintWorkCodeList();                 
//                getSOPData();

                LogEvent("HashHandlerClass::MessageHandler::SystemInformation event recieved, data :" + data, 0);
                //alert(data);
                break;
            case 'AgentID':
                switch (ipPhoneType) {
                    case "Nortel":
                        agentID = data.split('|')[1];
                        getAgentSkillSet_Ajax(agentID);
                        //agentID = 7723;
                        getAgentStatsScreenData(agentID, "past");
                        break;
                    case "Cisco_LHR":
                        agentID = data.split('|')[1];
                        if (intervalVariable == null)
                        {
                            getAgentStatsScreenData(agentID, "past");
                            getAgentStats();
                            intervalVariable = setInterval(function () {
                                getAgentStats();
                                //alert('interval ' + intervalVariable);
                            }, 5 * 60 * 1000);
                        }
                        break;
                    case "Cisco_ISB":
                        agentID = data.split('|')[1];
                        if (intervalVariable == null)
                        {
                            getAgentStatsScreenData(agentID, "past");
                            getAgentStats();
                            getTimeOfstats();
                            intervalVariable = setInterval(function () {
                                getAgentStats();
                                getTimeOfstats();
                                //alert('interval ' + intervalVariable);
                            }, 5 * 60 * 1000);
                        }
                        break;
                    case "Cisco_KHI":
                        agentID = data.split('|')[1];
                        if (intervalVariable == null)
                        {
                            getAgentStatsScreenData(agentID, "past");
                            getAgentStats();
                            intervalVariable = setInterval(function () {
                                getAgentStats();
                                //alert('interval ' + intervalVariable);
                            }, 5 * 60 * 1000);
                        }
                        break;
                }
                break;
            case 'IsSupervisor':
                switch (ipPhoneType) {
                    case "Nortel":

                        break;
                    case "Cisco_LHR":
                        var status = data.split('|')[1];
                        clientObj.setIsSupervisor(false);
                        if (status == 't') {
                            clientObj.setIsSupervisor(true);
                        } else
                        {
                            clientObj.setIsSupervisor(false);
                        }
                        break;
                    case "Cisco_ISB":
                        var status = data.split('|')[1];
                        clientObj.setIsSupervisor(false);
                        if (status == 't') {
                            clientObj.setIsSupervisor(true);
                        } else
                        {
                            clientObj.setIsSupervisor(false);
                        }
                        break;
                    case "Cisco_KHI":
                        var status = data.split('|')[1];
                        clientObj.setIsSupervisor(false);
                        if (status == 't') {
                            clientObj.setIsSupervisor(true);
                        } else
                        {
                            clientObj.setIsSupervisor(false);
                        }
                        break;
                }
                break;
            case 'SkillInfo':
                switch (ipPhoneType) {
                    case "Nortel":

                        break;
                    case "Cisco_LHR":
                        clientObj.setAgentSkillSet(dataArray[1]);
                        //if(!clientObj.isUnicaDown())
                        {
                            var agentSkillSets = dataArray[1].split(',');
                            var unicaSkillSets = clientObj.getUnicaSkillSet();
                            if (!$.isArray(unicaSkillSets)) {
                                unicaSkillSets = new Array(clientObj.getUnicaSkillSet());
                            }
                            if (!_isUnicaDown)
                            {
                                clientObj.setIsUnicaDown(true);
                                for (var i = 0; i < agentSkillSets.length; i++) {
                                    if ($.trim(agentSkillSets[i]).length > 0) {
                                        if ($.inArray($.trim(agentSkillSets[i]).toLowerCase(), unicaSkillSets) != -1) {
                                            clientObj.setIsUnicaDown(false);
                                        }
                                    }
                                }
                            }
                            if (clientObj.isUnicaDown()) {
                                $("#unicaOfferLoadImage").parent().parent().find("div.body").hide();
                                resetUnicaCards();
                            }
                        }
                        break;
                    case "Cisco_ISB":

                        var skillArr = dataArray[1].split(',');
                        //LogEvent(skillArr,3);
                        isPIASystemAgent(skillArr);
                        clientObj.setAgentSkillSet(dataArray[1]);
                        //if(!clientObj.isUnicaDown())
                        {
                            var agentSkillSets = dataArray[1].split(',');
                            var unicaSkillSets = clientObj.getUnicaSkillSet();
                            if (!$.isArray(unicaSkillSets)) {
                                unicaSkillSets = new Array(clientObj.getUnicaSkillSet());
                            }
                            if (!_isUnicaDown)
                            {
                                clientObj.setIsUnicaDown(true);
                                for (var i = 0; i < agentSkillSets.length; i++) {
                                    if ($.trim(agentSkillSets[i]).length > 0) {
                                        if ($.inArray($.trim(agentSkillSets[i]).toLowerCase(), unicaSkillSets) != -1) {
                                            clientObj.setIsUnicaDown(false);
                                        }
                                    }
                                }
                            }
                            if (clientObj.isUnicaDown()) {
                                $("#unicaOfferLoadImage").parent().parent().find("div.body").hide();
                                resetUnicaCards();
                            }
                        }
                        break;
                    case "Cisco_KHI":
                        var skillArr = dataArray[1].split(',');
                        isPIASystemAgent(skillArr);
                        clientObj.setAgentSkillSet(dataArray[1]);
                        //if(!clientObj.isUnicaDown())
                        {
                            var agentSkillSets = dataArray[1].split(',');
                            var unicaSkillSets = clientObj.getUnicaSkillSet();
                            if (!$.isArray(unicaSkillSets)) {
                                unicaSkillSets = new Array(clientObj.getUnicaSkillSet());
                            }
                            if (!_isUnicaDown)
                            {
                                clientObj.setIsUnicaDown(true);
                                for (var i = 0; i < agentSkillSets.length; i++) {
                                    if ($.trim(agentSkillSets[i]).length > 0) {
                                        if ($.inArray($.trim(agentSkillSets[i]).toLowerCase(), unicaSkillSets) != -1) {
                                            clientObj.setIsUnicaDown(false);
                                        }
                                    }
                                }
                            }
                            if (clientObj.isUnicaDown()) {
                                $("#unicaOfferLoadImage").parent().parent().find("div.body").hide();
                                resetUnicaCards();
                            }
                        }
                        break;
                }
                break;
            case 'ErrorMessage':
                hashloginmutex = false;
                switch (ipPhoneType) {
                    case "Nortel":
                        Notifier.error('CTI ERROR: ' + data.split('|')[1]);
                        $('#txtUsername').val('');
                        $('#txtUserpass').val('');
                        $('#txtUsername').removeAttr('readonly');
                        $('#txtUserpass').removeAttr('readonly');
                        $('#txtUsername').focus();
                        break;
                    case "Cisco_LHR":
                        Notifier.error('CTI ERROR: ' + data.split('|')[1]);
                        $('#txtUsername').val('');
                        $('#txtUserpass').val('');
                        $('#txtUsername').removeAttr('readonly');
                        $('#txtUserpass').removeAttr('readonly');
                        $('#txtUsername').focus();
                        break;
                    case "Cisco_ISB":
                        Notifier.error('CTI ERROR: ' + data.split('|')[1]);
                        $('#txtUsername').val('');
                        $('#txtUserpass').val('');
                        $('#txtUsername').removeAttr('readonly');
                        $('#txtUserpass').removeAttr('readonly');
                        $('#txtUsername').focus();
                        break;
                    case "Cisco_KHI":
                        Notifier.error('CTI ERROR: ' + data.split('|')[1]);
                        $('#txtUsername').val('');
                        $('#txtUserpass').val('');
                        $('#txtUsername').removeAttr('readonly');
                        $('#txtUserpass').removeAttr('readonly');
                        $('#txtUsername').focus();
                        break;
                }
                break;
            case 'CallType':
                var _data = dataArray[1].toString().trim();
                if (_data == 0) {

                } else if (_data == "") {

                } else {
                    switch (ipPhoneType) {
                        case "Nortel":
                            $('#DNISDescriptionDiv').html(_data);
                            $('#DNISDescriptionSpan').html(_data);
                            break;
                        case "Cisco_LHR":
                            $('#DNISDescriptionDiv').html(_data);
                            $('#DNISDescriptionSpan').html(_data);
                            break;
                        case "Cisco_ISB":
                            $('#DNISDescriptionDiv').html(_data);
                            $('#DNISDescriptionSpan').html(_data);
                            break;
                        case "Cisco_KHI":
                            $('#DNISDescriptionDiv').html(_data);
                            $('#DNISDescriptionSpan').html(_data);
                            break;
                    }
                }
                break;
        }
    };

    var LogEvent = function (message, level) {
        //clientWSLog(message);
//		$.ajax({
//			//url: 'http://'+$('#hdnIP').val()+':8888/log?level=' + level + '&message=' + encodeURIComponent(message),
//			url: 'http://' + hdnIP + ':8888/log?level=' + level + '&message=' + encodeURIComponent(message),
//			success: function(data) {
//
//			}
//		});

//		Send('H_Log|' + 'eventLog -> [' + message + ']');
        setTimeout(function () {
//			Send('H_Log|' + 'eventLog -> [3000 ms delay][' + message + ']');
        }, 3000);
        try {
            console.log('' + 'eventLog -> [' + message + ']' + "Level: " + level);
        } catch (ex) {
        }
    };

    //Some unknown states or button....
    var SetButtonStates = function (statesArray) {
        var imgSrc = "Styles/img/";
        LogEvent("HashHandlerClass::SetButtonStates::Updating Button States", 0);
        if (statesArray[0] !== null) {
            $('#btnLogin').attr('disabled', !statesArray[0]);
            if (!statesArray[0]) {

            } else {

            }
        }
        if (statesArray[1] !== null) {
            $('#btnLogout').attr('disabled', !statesArray[1]);
            if (!statesArray[1]) {
                //$('#btnLogout img').attr('src', imgSrc + "d_logout-icon.png");
                $('#btnLogout').css('opacity', '0.3');
            } else {
                //$('#btnLogout img').attr('src', imgSrc + "logout-icon.png");
                $('#btnLogout').css('opacity', '1');
            }
        }
        if (statesArray[2] !== null) {
            $('#btnReady').attr('disabled', !statesArray[2]);
            $('#break-btn').attr('disabled', !statesArray[2]);
            if (!statesArray[2]) {
                //$('#btnReady img').attr('src', imgSrc + "d_ready-icon.png");
                $('#btnReady').css('opacity', '0.1');
                $('#break-btn').css('opacity', '0.1');
                //$().attr();id="btnNot_Ready" title="Not Ready" onclick="return false;" 
            } else {
                //$('#btnReady img').attr('src', imgSrc + "ready-icon.png");
                $('#btnReady').css('opacity', '1');
                $('#break-btn').css('opacity', '1');
            }
        }
        if (statesArray[3] !== null) {
            $('#btnNot_Ready').attr('disabled', !statesArray[3]);

            if (!statesArray[3]) {
                //$('#btnNot_Ready img').attr('src', imgSrc + "d_notready-icon.png");
                $('#btnNot_Ready').css('opacity', '0.1');

            } else {
                //$('#btnNot_Ready img').attr('src', imgSrc + "notready-icon.png");
                $('#btnNot_Ready').css('opacity', '1');

            }
        }
        if (statesArray[4] !== null) {
            $('#btnAnswer_Call').attr('disabled', !statesArray[4]);
            if (!statesArray[4]) {
                //$('#btnAnswer_Call img').attr('src', imgSrc + "d_answer-icon.png");
                $('#btnAnswer_Call').css('opacity', '0.1');
            } else {
                //$('#btnAnswer_Call img').attr('src', imgSrc + "answer-icon.png");
                $('#btnAnswer_Call').css('opacity', '1');
            }
        }
        if (statesArray[5] !== null) {
//			$('#btnSingle_Step_Transfer').attr('disabled', !statesArray[5]);
//			$('#btnEmDial').attr('disabled', !statesArray[5]);
//			$('#btnSupv_Transfer').attr('disabled', !statesArray[5]);
//			if (!statesArray[5]) {
//				$('#btnSingle_Step_Transfer').css('opacity', '0.1');
//				$('#btnEmDial').css('opacity', '0.1');
//				$('#btnSupv_Transfer').css('opacity', '0.1');
//			}
//			else {
//				$('#btnSingle_Step_Transfer').css('opacity', '1');
//				$('#btnEmDial').css('opacity', '1');
//				$('#btnSupv_Transfer').css('opacity', '1');
//			}
//			$('#btnSingle_Step_Transfer').attr('disabled', true);
//			$('#btnSingle_Step_Transfer').css('opacity', '0.1');

            $('#btn_transfer').attr('disabled', !statesArray[5]);
            $('#btn_transfer_complete').attr('disabled', !statesArray[5]);
//			$('#btnEmDial').attr('disabled', !statesArray[5]);
            $('#btnEmDial').attr('disabled', true);
            $('#btn_consult').attr('disabled', !statesArray[5]);
            if (!statesArray[5]) {
                $('#btn_transfer').css('opacity', '0.1');
                $('#btn_transfer_complete').css('opacity', '0.1');
//				$('#btnEmDial').css('opacity', '0.1');
                $('#btnEmDial').css('opacity', '1');
                $('#btn_consult').css('opacity', '0.1');
            } else {
                $('#btn_transfer').css('opacity', '1');
                $('#btn_transfer_complete').css('opacity', '1');
                $('#btnEmDial').css('opacity', '1');
                $('#btn_consult').css('opacity', '1');
            }
            $('#btn_transfer_complete').attr('disabled', true);
            $('#btn_transfer_complete').css('opacity', '0.1');
            $('#btn_transfer').attr('disabled', true);
            $('#btn_transfer').css('opacity', '0.1');


        }
        if (statesArray[6] !== null || statesArray[7] !== null) {
//			if (statesArray[6] === false && statesArray[7] === false) {
//				$('#btnTwo_Step_Transfer').html('Initialize Transfer');
//				$('#btnParentTransfer').attr('disabled', true);
//				$('#btnParentTransfer').css('opacity', '0.1');
//				$('#btnTwo_Step_Transfer').attr('disabled', true);
//				$('#btnTwo_Step_Transfer').css('opacity', '0.1');
//			}
//			else if (statesArray[6] === false && statesArray[7] === true) {
//				$('#btnTwo_Step_Transfer').html('Complete Transfer');
//				$('#btnParentTransfer').attr('disabled', false);
//				$('#btnParentTransfer').css('opacity', '1');
//				$('#btnTwo_Step_Transfer').attr('disabled', false);
//				$('#btnTwo_Step_Transfer').css('opacity', '1');
//			}
//			else if (statesArray[6] === true && statesArray[7] === false) {
//				$('#btnTwo_Step_Transfer').html('Initialize Transfer');
//				$('#btnParentTransfer').attr('disabled', false);
//				$('#btnParentTransfer').css('opacity', '1');
//				$('#btnTwo_Step_Transfer').attr('disabled', false);
//				$('#btnTwo_Step_Transfer').css('opacity', '1');
//			}

            if (statesArray[6] === false && statesArray[7] === false) {
                //$('#btnTwo_Step_Transfer').html('Initialize Transfer');
                $('#btn_transfer_complete').attr('disabled', true);
                $('#btn_transfer_complete').css('opacity', '0.1');

                $('#btnParentTransfer').attr('disabled', true);
                $('#btnParentTransfer').css('opacity', '0.1');
                $('#btn_transfer').attr('disabled', true);
                $('#btn_transfer').css('opacity', '0.1');
            } else if (statesArray[6] === false && statesArray[7] === true) {
                //$('#btnTwo_Step_Transfer').html('Complete Transfer');
                $('#btn_transfer_complete').attr('disabled', false);
                $('#btn_transfer_complete').css('opacity', '1');

                $('#btnParentTransfer').attr('disabled', false);
                $('#btnParentTransfer').css('opacity', '1');
                $('#btn_transfer').attr('disabled', false);
                $('#btn_transfer').css('opacity', '1');
            } else if (statesArray[6] === true && statesArray[7] === false) {
                //$('#btnTwo_Step_Transfer').html('Initialize Transfer');
                $('#btn_transfer_complete').attr('disabled', true);
                $('#btn_transfer_complete').css('opacity', '0.1');

                $('#btnParentTransfer').attr('disabled', false);
                $('#btnParentTransfer').css('opacity', '1');
                $('#btn_transfer').attr('disabled', false);
                $('#btn_transfer').css('opacity', '1');
            }
        }
        if (statesArray[8] !== null) {
            $('#btnIVR_Transfer').attr('disabled', !statesArray[8]);
            if (clientObj.getDNIS_Consent() != null && ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Consent()) != -1)) {
                if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Consent()) != -1) {
                    LogEvent("HashHandlerClass::SetButtonStates::statesArray[8] DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "Consent_DNIS=" + clientObj.getDNIS_Consent() + ";", 0);
                }
                $('#btnConsent_transfer').attr('disabled', !statesArray[8]);
            }
            if (!statesArray[8]) {
                //$('#btnIVR_Transfer img').attr('src', imgSrc + "d_transfer-icon.png");
                $('#btnIVR_Transfer').css('opacity', '0.1');
                if (clientObj.getDNIS_Consent() != null && ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Consent()) != -1)) {
                    if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Consent()) != -1) {
                        LogEvent("HashHandlerClass::SetButtonStates::statesArray[8] DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "Consent_DNIS=" + clientObj.getDNIS_Consent() + ";", 0);
                    }
                    $('#btnConsent_transfer').css('opacity', '0.1');
                }
            } else {
                //$('#btnIVR_Transfer img').attr('src', imgSrc + "transfer-icon.png");
                $('#btnIVR_Transfer').css('opacity', '1');
                if (clientObj.getDNIS_Consent() != null && ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Consent()) != -1)) {
                    if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Consent()) != -1) {
                        LogEvent("HashHandlerClass::SetButtonStates::statesArray[8] DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "Consent_DNIS=" + clientObj.getDNIS_Consent() + ";", 0);
                    }
                    $('#btnConsent_transfer').css('opacity', '1');
                }
            }
        }
        if (statesArray[9] !== null) {
            $('#btnIVR_Conference').attr('disabled', !statesArray[9]);
            if (!statesArray[9]) {

            } else {

            }
        }
        if (statesArray[10] !== null) {
            if ($.inArray($.trim(clientObj.getDNIS()), clientObj.getDNIS_Hangup_Disabled()) != -1) {
                LogEvent("HashHandlerClass::SetButtonStates::statesArray[10] DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "HangupDisabled_DNIS=" + clientObj.getDNIS_Hangup_Disabled() + ";", 0);
                $('#btnHang_up').attr('disabled', 'true');
                //$('#btnHang_up img').attr('src', imgSrc + "d_hangup-icon.png");
                $('#btnHang_up').css('opacity', '0.1');
            } else
            {
                $('#btnHang_up').attr('disabled', !statesArray[10]);
                if (!statesArray[10]) {
                    //$('#btnHang_up img').attr('src', imgSrc + "d_hangup-icon.png");
                    $('#btnHang_up').css('opacity', '0.1');
                } else {
                    //$('#btnHang_up img').attr('src', imgSrc + "hangup-icon.png");
                    $('#btnHang_up').css('opacity', '1');
                }
            }
            if (isCallTransfered) {
                $('#btnHang_up').attr('disabled', false);
                $('#btnHang_up').css('opacity', '1');
            }
        }
        if (statesArray[11] !== null) {
            $('#btnHold').attr('disabled', !statesArray[11]);
            if (!statesArray[11]) {
                //$('#btnHold img').attr('src', imgSrc + "d_hold-icon.png");
                $('#btnHold').css('opacity', '0.1');
            } else {
                //$('#btnHold img').attr('src', imgSrc + "hold-icon.png");
                $('#btnHold').css('opacity', '1');
            }
        }
        if (statesArray[12] !== null) {
            $('#btnUnHold').attr('disabled', !statesArray[12]);
            if (!statesArray[12]) {
                //$('#btnUnHold img').attr('src', imgSrc + "d_unhold-icon.png");
                $('#btnUnHold').css('opacity', '0.1');
            } else {
                //$('#btnUnHold img').attr('src', imgSrc + "unhold-icon.png");
                $('#btnUnHold').css('opacity', '1');
            }
        }
        if (statesArray[13] !== null) {
            if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Feedback_Disabled()) != -1) {
                LogEvent("HashHandlerClass::SetButtonStates::statesArray[13] DNIS exists" + ";" + "Call_DNIS=" + clientObj.getDNIS() + ";" + "FeedbackDisabled_DNIS=" + clientObj.getDNIS_Feedback_Disabled() + ";", 0);
                $('#btnFeedback').attr('disabled', true);
                //$('#btnFeedback img').attr('src', imgSrc + "d_feedback-icon.png");
                $('#btnFeedback').css('opacity', '0.1');
            } else
            {
                $('#btnFeedback').attr('disabled', !statesArray[13]);
                if (!statesArray[13]) {
                    //$('#btnFeedback img').attr('src', imgSrc + "d_feedback-icon.png");
                    $('#btnFeedback').css('opacity', '0.1');
                } else {
                    //$('#btnFeedback img').attr('src', imgSrc + "feedback-icon.png");
                    $('#btnFeedback').css('opacity', '1');
                }
            }
            if (isCallTransfered) {
                $('#btnFeedback').attr('disabled', false);
                $('#btnFeedback').css('opacity', '1');
            }
        }
        if (statesArray[14] !== null) {
            $('#btnInternalCallAnswer').attr('disabled', !statesArray[14]);
            if (!statesArray[14]) {
                //$('#btnFeedback').css('opacity', '0.1');
            } else {
                //$('#btnFeedback').css('opacity', '0.1');
            }
        }
        if (statesArray[15] !== null) {
//			$('#btnInternalCallHangup').attr('disabled', !statesArray[15]);
//			if (!statesArray[15]) {
//				$('#btnInternalCallHangup').css('opacity', '0.1');
//			}
//			else {
//				$('#btnInternalCallHangup').css('opacity', '1');
//			}


            $('#btn_consult_end').attr('disabled', !statesArray[15]);
            $('#btn_transfer_end').attr('disabled', !statesArray[15]);

            if (!statesArray[15]) {
                $('#btn_consult_end').css('opacity', '0.1');
                $('#btn_transfer_end').css('opacity', '0.1');
            } else {
                $('#btn_consult_end').css('opacity', '1');
                $('#btn_transfer_end').css('opacity', '1');
            }

            if (btn_consult_pressed) {
                $('#btn_consult_end').attr('disabled', false);
                $('#btn_transfer_complete').attr('disabled', true);
                $('#btn_transfer').attr('disabled', true);
                $('#btn_consult').attr('disabled', true);
                $('#btn_consult_end').css('opacity', '1');
                $('#btn_transfer_complete').css('opacity', '0.1');
                $('#btn_transfer').css('opacity', '0.1');
                $('#btn_consult').css('opacity', '0.1');
            } else if (btn_transfer_pressed) {
                $('#btn_consult_end').attr('disabled', true);
                $('#btn_transfer_complete').attr('disabled', false);
                $('#btn_transfer').attr('disabled', true);
                $('#btn_consult').attr('disabled', true);
                $('#btn_consult_end').css('opacity', '0.1');
                $('#btn_transfer_complete').css('opacity', '1');
                $('#btn_transfer').css('opacity', '0.1');
                $('#btn_consult').css('opacity', '0.1');
            }

        }
    };

    this.Login = function (login, password) {
        Send('Login' + '|' + login + '|' + password);
        LogEvent("HashHandlerClass::Login::Login Event", 0);
    };

    this.Logout = function () {
        Send('Logout');
        LogEvent("HashHandlerClass::Logout::Logout Event", 0);
    };

    this.Answer_Call = function () {
        Send('Answer Call');
        LogEvent("HashHandlerClass::Answer_Call::Answer Call Event", 0);
    };

    this.Hang_up = function () {
        Send('Hang-up');
        callTransType = 'HangedUp';
        LogEvent("HashHandlerClass::Hang_up::Hangup Event", 0);
    };

    this.Hold = function () {
        Send('Hold');
        LogEvent("HashHandlerClass::Hold::Hold Event", 0);
    };

    this.UnHold = function () {
        Send('UnHold');
        LogEvent("HashHandlerClass::UnHold::UnHold Event", 0);
    };

    this.IVR_Transfer = function (PromptID) {
        Send('IVR Transfer' + '|' + PromptID);
        callTransType = "IVR";
        callTransToIVRPrompt = PromptID;
        LogEvent("HashHandlerClass::IVR_Transfer::IVR Transfer Event: " + PromptID, 0);
    };

    this.IVR_Conference = function (PromptID) {
        Send('IVR Conference' + '|' + PromptID);
        LogEvent("HashHandlerClass::IVR_Conference::IVR Conference Event: " + PromptID, 0);
    };

    this.Single_Step_Transfer = function (TransferDN) {
        Send('Single Step Transfer' + '|' + TransferDN);
        callTransType = 'SingleStepTransfer';
        LogEvent("HashHandlerClass::Single_Step_Transfer::Single Step Transfer Event", 0);
    };

//	this.Two_Step_Transfer_Step1 = function(DN) {
//		Send('Two Step Transfer' + '|' + 'StartTransfer' + '|' + DN);
//		LogEvent("HashHandlerClass::Two_Step_Transfer_Step1::Two Step Transfer Event", 0);
//	};

    //two step???
    this.Two_Step_Transfer_Step1 = function (DN, tag) {
        Send('Two Step Transfer' + '|' + 'StartTransfer' + '|' + DN + '|' + tag);
        LogEvent("HashHandlerClass::Two_Step_Transfer_Step1::Two Step Transfer Event", 0);
    };

    this.Two_Step_Transfer_Step2 = function () {
        Send('Two Step Transfer' + '|' + 'CompleteTransfer');
        LogEvent("HashHandlerClass::Two_Step_Transfer_Step2::Two Step Transfer Event", 0);
    };

    //parameter issue
    this.Feedback = function () {
        switch (clientObj.getIpPhoneType()) {
            case "Nortel":
                Send("Feedback");
                break;
            case "Cisco_LHR":
                if (clientObj.getNDFCall()) {
                    Send('Feedback' + '|' + 0);
                } else {
                    if (clientObj.isCaresLoggedIn()) {
                        var cust_rating = clientObj.getCaresCustomerInfo();
                        if ($.isArray(cust_rating)) {
                            Send('Feedback' + '|' + cust_rating[7]);
                        } else {
                            Send('Feedback' + '|' + 0);
                        }

                    } else {
                        Send('Feedback' + '|' + 0);
                    }
                }
                break;
            case "Cisco_ISB":
                if (clientObj.getNDFCall()) {
                    Send('Feedback' + '|' + 0);
                } else {
                    if (clientObj.isCaresLoggedIn()) {
                        var cust_rating = clientObj.getCaresCustomerInfo();
                        if ($.isArray(cust_rating)) {
                            Send('Feedback' + '|' + cust_rating[7]);
                        } else {
                            Send('Feedback' + '|' + 0);
                        }

                    } else {
                        Send('Feedback' + '|' + 0);
                    }
                }
                break;
            case "Cisco_KHI":
                if (clientObj.getNDFCall()) {
                    Send('Feedback' + '|' + 0);
                } else {
                    if (clientObj.isCaresLoggedIn()) {
                        var cust_rating = clientObj.getCaresCustomerInfo();
                        if ($.isArray(cust_rating)) {
                            Send('Feedback' + '|' + cust_rating[7]);
                        } else {
                            Send('Feedback' + '|' + 0);
                        }
                    } else {
                        Send('Feedback' + '|' + 0);
                    }
                }
                break;
        }
        callTransType = "Feedback";
        LogEvent("HashHandlerClass::Feedback::Feedback Event", 0);
    };

    this.Ready = function () {
        Send('Ready');
        LogEvent("HashHandlerClass::Ready::Ready Event", 0);
    };

    this.Not_Ready = function (ReasonCode) {
        Send('Not Ready' + '|' + ReasonCode);
        LogEvent("HashHandlerClass::Not_Ready::Not Ready Event", 0);
    };

    this.Internal_Call_Answer = function () {
        Send('InternalCallAnswer');
        LogEvent("HashHandlerClass::Internal_Call_Answer::Internal Call Answer Event", 0);
    };

    this.Internal_Call_Hangup = function () {
        Send('InternalCallHangup');
        LogEvent("HashHandlerClass::Internal_Call_Hangup::Internal Call Hangup Event", 0);
    };

    this.System_Information = function () {
        Send('SystemInformation');
        LogEvent("HashHandlerClass::System_Information::System Information Event", 0);
    };

    this.Consent_Transfer = function (language) {
        switch (clientObj.getIpPhoneType()) {
            case "Nortel":
                Send('Consent' + '|' + language);
                break;
            case "Cisco_LHR":
                if (clientObj.isCaresLoggedIn()) {
                    var cust_rating = clientObj.getCaresCustomerInfo();
                    if ($.isArray(cust_rating)) {
                        Send('Consent' + '|' + language + "|" + cust_rating[7]);
                    } else {
                        Send('Consent' + '|' + language + "|" + 0);
                    }
                } else {
                    Send('Consent' + '|' + language + "|" + 0);
                }
                break;
            case "Cisco_ISB":
                if (clientObj.isCaresLoggedIn()) {
                    var cust_rating = clientObj.getCaresCustomerInfo();
                    if ($.isArray(cust_rating)) {
                        Send('Consent' + '|' + language + "|" + cust_rating[7]);
                    } else {
                        Send('Consent' + '|' + language + "|" + 0);
                    }
                } else {
                    Send('Consent' + '|' + language + "|" + 0);
                }
                break;
            case "Cisco_KHI":
                if (clientObj.isCaresLoggedIn()) {
                    var cust_rating = clientObj.getCaresCustomerInfo();
                    if ($.isArray(cust_rating)) {
                        Send('Consent' + '|' + language + "|" + cust_rating[7]);
                    } else {
                        Send('Consent' + '|' + language + "|" + 0);
                    }
                } else {
                    Send('Consent' + '|' + language + "|" + 0);
                }
                break;
        }
        callTransType = "Consent";
        LogEvent("HashHandlerClass::Consent_Transfer::Consent Transfer Event", 0);
    };

    this.Agent_ID = function () {
        Send('AgentID');
        LogEvent("HashHandlerClass::Agent_ID::Agent ID Event", 0);
    };

    this.getAttachedData = function () {
        return attachedData;
    };

    this.setAttachedData = function (data) {
        attachedData = data;
    };

    this.getUsername = function () {
        return username;
    };

    this.getPassword = function () {
        return password;
    };

    this.setUsername = function (usr) {
        username = usr;
    };

    this.getCardsToShow = function () {
        return unicaCardsNumber;
    };

    this.setCardsToShow = function (cards) {
        unicaCardsNumber = cards;
    };

    this.getAgentSetNotReadyCode = function () {
        return agentSetNotReadyCode;
    };

    this.setAgentSetNotReadyCode = function (code) {
        agentSetNotReadyCode = code;
    };

    this.getUnicaResegOfferCode = function () {
        return unicaResegOfferCode;
    };

    this.setUnicaResegOfferCode = function (code) {
        unicaResegOfferCode = code;
    };

    this.getCardsToFetch = function () {
        return unicaCardsToFetch;
    };

    this.setCardsToFetch = function (cards) {
        unicaCardsToFetch = cards;
    };

    this.setPassword = function (pwd) {
        password = pwd;
    };

    this.getCaresUserLogOnString = function () {
        return caresUserlogOnString;
    };

    this.setCaresUserLogOnString = function (str) {
        caresUserlogOnString = str;
    };

    this.getCommonPwdCCT = function () {
        return commonPwdCCT;
    };

    this.setCommonPwdCCT = function (pwd) {
        commonPwdCCT = pwd;
    };

    this.getUnicaSkillSet = function () {
        return unicaSkillSet;
    };

    this.setUnicaSkillSet = function (skill) {
        unicaSkillSet = skill;
    };

    this.getSystemInfo = function () {
        return systemInfo;
    };

    this.setSystemInfo = function (str) {
        systemInfo = str;
    };

    this.getCustomerCurrentHandset = function () {
        return customerCurrentHandset;
    };

    this.setCustomerCurrentHandset = function (handset) {
        customerCurrentHandset = handset;
    };

    this.getIP = function () {
        return ipAddr;
    };

    this.getDNIS = function () {
        return DNIS;
    };

    this.getDNIS_Auto_Call_Pickup = function () {
        return DNIS_Auto_Call_Pickup;
    };

    this.getDNIS_Charging = function () {
        return DNIS_Charging;
    };

    this.getDNIS_Description_Text = function () {
        return DNIS_Description_Text;
    };

    this.getDNIS_Direct_Cares = function () {
        return DNIS_Direct_Cares;
    };

    this.getDNIS_Feedback_Disabled = function () {
        return DNIS_Feedback_Disabled;
    };

    this.getDNIS_Hangup_Disabled = function () {
        return DNIS_Hangup_Disabled;
    };

    this.getDNIS_Consent = function () {
        return DNIS_Consent;
    };

    this.getCDN_Description = function () {
        return CDN_Description;
    };

    this.setDNIS_Auto_Call_Pickup = function (str) {
        DNIS_Auto_Call_Pickup = str;
    };

    this.setDNIS_Charging = function (str) {
        DNIS_Charging = str;
    };

    this.setDNIS_Description_Text = function (str) {
        DNIS_Description_Text = str;
    };

    this.setDNIS_Direct_Cares = function (str) {
        DNIS_Direct_Cares = str;
    };

    this.setDNIS_Feedback_Disabled = function (str) {
        DNIS_Feedback_Disabled = str;
    };

    this.setDNIS_Hangup_Disabled = function (str) {
        DNIS_Hangup_Disabled = str;
    };

    this.setDNIS_Consent = function (str) {
        DNIS_Consent = str;
    };

    this.setCDN_Description = function (str) {
        CDN_Description = str;
    };

    this.isCaresLoggedIn = function () {
        return caresLoggedIn;
    };

    this.isCCTLoggedIn = function () {
        return loggedIn;
    };

    this.setCaresLoggedIn = function (str) {
        caresLoggedIn = str;
    };

    this.getCallerMsisdn = function () {
        return caller_msisdn;
    };

    this.getCallCDN = function () {
        return CDN;
    };

    this.getIpPhoneType = function () {
        return ipPhoneType;
    };

    this.setCallStartTime = function () {
        callStartTime = new Date();
    };

    this.setCallEndTime = function () {
        callEndTime = new Date();
    };

    this.getCallStartTime = function () {
        return callStartTime;
    };

    this.getCallEndTime = function () {
        return callEndTime;
    };

    this.getUnicaSessionID = function () {
        return unica_sessionID;
    };

    this.setUnicaSessionID = function (sessionID) {
        unica_sessionID = sessionID;
    };

    this.setCallTransType = function (str) {
        callTransType = str;
    };

    this.getCallTransType = function () {
        return callTransType;
    };

    this.setCallTransToIVRPrompt = function (str) {
        callTransToIVRPrompt = str;
    };

    this.getCallTransToIVRPrompt = function () {
        return callTransToIVRPrompt;
    };

    this.setCaresCustomerInfo = function (arr) {
        caresCustomerInfo = arr;
    };

    this.getCaresCustomerInfo = function () {
        return caresCustomerInfo;
    };

    this.setAgentSkillSet = function (skill) {
        agentSkillSet = skill;
    };

    this.getAgentSkillSet = function () {
        return agentSkillSet;
    };

    this.setAfterCallInfo = function () {
        var attData = clientObj.getAttachedData();
        var systemInfoArr = clientObj.getSystemInfo().split('|');
        var customerInfo = null;
        var caresLogonArr = null;

        //if (data) 
        {
            customerInfo = clientObj.getCaresCustomerInfo();
            if (!clientObj.isCaresLoggedIn()) {
                if (clientObj.getNDFCall()) {
                    if ($.isArray(customerInfo)) {
                        var transfer_to_ivr_menu = null;
                        if (clientObj.getCallTransType() == "IVR") {
                            transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                        }
                        setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, null, clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), null, systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), customerInfo[3], customerInfo[1], customerInfo[8], customerInfo[7], customerInfo[11], customerInfo[12], customerInfo[10], customerInfo[17], null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                    } else {
                        var transfer_to_ivr_menu = null;
                        if (clientObj.getCallTransType() == "IVR") {
                            transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                        }
                        setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, null, clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), null, systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), null, null, null, null, null, null, null, null, null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                    }
                } else {
                    if ($.isArray(attData)) {
                        switch (clientObj.getIpPhoneType()) {
                            case "Nortel":
                                var transfer_to_ivr_menu = attData[33];
                                if (clientObj.getCallTransType() == "IVR") {
                                    transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                                }
                                setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), null, systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[3], attData[5], attData[4], attData[6], attData[7], attData[8], attData[9], attData[11], attData[12], clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                break;

                            case "Cisco_LHR":
                                var transfer_to_ivr_menu = attData[11];
                                if (clientObj.getCallTransType() == "IVR") {
                                    transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                                }
                                setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), null, systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[8], attData[7], attData[4], null, attData[6], null, attData[3], null, null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                break;

                            case "Cisco_ISB":
                                var transfer_to_ivr_menu = attData[11];
                                if (clientObj.getCallTransType() == "IVR") {
                                    transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                                }
                                setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), null, systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[8], attData[7], attData[4], null, attData[6], null, attData[3], null, null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                break;

                            case "Cisco_KHI":
                                var transfer_to_ivr_menu = attData[11];
                                if (clientObj.getCallTransType() == "IVR") {
                                    transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                                }
                                setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), null, systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[8], attData[7], attData[4], null, attData[6], null, attData[3], null, null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                break;

                        }
                    }
                }
            } else {
                if (clientObj.getNDFCall()) {
                    caresLogonArr = clientObj.getCaresUserLogOnString().split('|');
                    if ($.isArray(customerInfo)) {
                        var transfer_to_ivr_menu = null;
                        if (clientObj.getCallTransType() == "IVR") {
                            transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                        }
                        setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, null, clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), caresLogonArr[3], systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), customerInfo[3], customerInfo[1], customerInfo[8], customerInfo[7], customerInfo[11], customerInfo[12], customerInfo[10], customerInfo[17], null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                    } else {
                        var transfer_to_ivr_menu = null;
                        if (clientObj.getCallTransType() == "IVR") {
                            transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                        }
                        setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, null, clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), caresLogonArr[3], systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), null, null, null, null, null, null, null, null, null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                    }
                } else {
                    if ($.isArray(attData)) {
                        switch (clientObj.getIpPhoneType()) {
                            case "Nortel":
                                caresLogonArr = clientObj.getCaresUserLogOnString().split('|');
                                var transfer_to_ivr_menu = attData[33];
                                if (clientObj.getCallTransType() == "IVR") {
                                    transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                                }
                                setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), caresLogonArr[3], systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[3], attData[5], attData[4], attData[6], attData[7], attData[8], attData[9], attData[11], attData[12], clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                break;

                            case "Cisco_LHR":
                                caresLogonArr = clientObj.getCaresUserLogOnString().split('|');
                                var transfer_to_ivr_menu = attData[11];
                                if (clientObj.getCallTransType() == "IVR") {
                                    transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                                }
                                if ($.isArray(customerInfo)) {
                                    setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), caresLogonArr[3], systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[8], attData[7], attData[4], customerInfo[8], attData[6], customerInfo[12], attData[3], customerInfo[17], null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                } else {
                                    setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), caresLogonArr[3], systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[8], attData[7], attData[4], null, attData[6], null, attData[3], null, null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                }
                                break;

                            case "Cisco_ISB":
                                caresLogonArr = clientObj.getCaresUserLogOnString().split('|');
                                var transfer_to_ivr_menu = attData[11];
                                if (clientObj.getCallTransType() == "IVR") {
                                    transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                                }
                                if ($.isArray(customerInfo)) {
                                    setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), caresLogonArr[3], systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[8], attData[7], attData[4], customerInfo[8], attData[6], customerInfo[12], attData[3], customerInfo[17], null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                } else {
                                    setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), caresLogonArr[3], systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[8], attData[7], attData[4], null, attData[6], null, attData[3], null, null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                }
                                break;

                            case "Cisco_KHI":
                                caresLogonArr = clientObj.getCaresUserLogOnString().split('|');
                                var transfer_to_ivr_menu = attData[11];
                                if (clientObj.getCallTransType() == "IVR") {
                                    transfer_to_ivr_menu = clientObj.getCallTransToIVRPrompt();
                                }
                                if ($.isArray(customerInfo)) {
                                    setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), caresLogonArr[3], systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[8], attData[7], attData[4], customerInfo[8], attData[6], customerInfo[12], attData[3], customerInfo[17], null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                } else {
                                    setCallDetails(clientObj.getCallSessionID(), (clientObj.getCallEndTime().getTime() - clientObj.getCallStartTime().getTime()) / 1000, attData[10], clientObj.getCallTransType(), null, null, transfer_to_ivr_menu, null, null, clientObj.getUsername(), clientObj.getAgentID(), caresLogonArr[3], systemInfoArr[3], clientObj.getCallerMsisdn(), clientObj.getCallerMsisdn(), attData[8], attData[7], attData[4], null, attData[6], null, attData[3], null, null, clientObj.getCallCDN(), clientObj.getDNIS(), (clientObj.getCallStartTime().getTime()), (clientObj.getCallEndTime().getTime()), clientObj.getIP(), clientObj.getAgentSkillSet(), clientObj.getCustomerCurrentHandset());
                                }
                                break;
                        }
                    }
                }
            }
        }
        clientObj.setAttachedData(null);
    };

    this.caresSeparateOpen = function (caresResult) {
        if (!clientObj.isCaresDown() && clientObj.isCaresLoggedIn()) {
            Send("CareSeparteOpen" + "|" + caresResult);
            LogEvent("HashHandlerClass::caresSeparateOpen::caresSeparateOpen Event " + caresResult, 0);
        }
    };

    this.Mute = function () {
        Send("Mute");
    };

    this.UnMute = function () {
        Send("UnMute");
    };

    this.getAgentID = function () {
        return agentID;
    };

    this.setAgentID = function (id) {
        agentID = id;
    };

    this.getNDFCall = function () {
        return NDFCall;
    };

    this.setNDFCall = function (ndf) {
        NDFCall = ndf;
    };

    this.getCallSessionID = function () {
        return callSessionID;
    };

    this.setCallSessionID = function (id) {
        callSessionID = id;
    };

    this.getCaresSessionID = function () {
        return caresSessionID;
    };

    this.setCaresSessionID = function (id) {
        caresSessionID = id;
    };

    this.getChargingRate = function () {
        return chargingRate;
    };

    this.setChargingRate = function (rate) {
        chargingRate = rate;
    };

    this.getCustomertype = function () {
        return customerType;
    };

    this.setCustomertype = function (type) {
        customerType = type;
    };

    this.isCaresDown = function () {
        return isCaresDown;
    };

    this.setIsCaresDown = function (bool) {
        isCaresDown = bool;
    };

    this.isTDDown = function () {
        return isTDDown;
    };

    this.setIsTDDown = function (bool) {
        isTDDown = bool;
    };

    this.isUnicaDown = function () {
        return isUnicaDown;
    };

    this.setIsUnicaDown = function (bool) {
        isUnicaDown = bool;
        if (bool) {
            $("#nbaDiv-enabled").css("display", "none");
            $("#nbaDiv-disabled").css("display", "");
        } else {
            $("#nbaDiv-disabled").css("display", "none");
            $("#nbaDiv-enabled").css("display", "");
        }
    };

    this.getEM_CDN = function () {
        return EM_CDN;
    };

    this.setACWNRCode = function (code) {
        acwNRCode = code;
    };

    this.getACWNRCode = function () {
        return acwNRCode;
    };

    this.setEM_CDN = function (emcdn) {
        EM_CDN = emcdn;
    };

    this.getSwitchID = function () {
        return switchID;
    };

    this.setSwitchID = function (sid) {
        switchID = sid;
    };

    this.getCallType = function () {
        return callType;
    };

    this.setCallType = function (ct) {
        callType = ct;
    };

    this.getCaresUrl = function () {
        return caresUrl;
    };

    this.setCaresUrl = function (url) {
        caresUrl = url;
    };

    this.getUnicaResegDropDownLimit = function () {
        return unicaResegDropDownLimit;
    };

    this.setUnicaResegDropDownLimit = function (limit) {
        unicaResegDropDownLimit = limit;
    };


    this.getUnicaUrl = function () {
        return unicaUrl;
    };

    this.setUnicaUrl = function (url) {
        unicaUrl = url;
    };

    this.getTeraDataUrl = function () {
        return TDUrl;
    };

    this.setTeraDataUrl = function (url) {
        TDUrl = url;
    };

    this.getNRforNextCall = function () {
        return NRforNextCall;
    };

    this.setNRforNextCall = function (bool) {
        NRforNextCall = bool;
    };

    this.isSupervisor = function () {
        return isSupvsr;
    };

    this.setIsSupervisor = function (bool) {
        isSupvsr = bool;
    };

    this.isAgentOnCall = function () {
        return agentOnCall;
    };

    this.setAgentOnCall = function (bool) {
        agentOnCall = bool;
    };

    this.setSupVsrAcwDuration = function (dur) {
        supvsrAcwDuration = dur;
    };

    this.getSupVsrAcwDuration = function () {
        return supvsrAcwDuration;
    };

    this.reopenCares = function () {
        Send('CaresFlagClear');
        LogEvent("HashHandlerClass::Reopen Cares::reopenCares Event: ", 0);
    };

    this.setProductType = function (type) {
        callProductType = type;
    };

    this.getProductType = function () {
        return callProductType;
    };
}

var clientObj = new HashHandlerClass();

var ajaxTimeout = 30 * 1000;
var timeoutCallAnswerVariable = null;
var intervalUnicaHeartBeat = null;

$(document).ready(function () {
    if (!("WebSocket" in window)) {
        alert("This browser does not support websockets!");
        clientObj = null;
    }

    //document.getElementById('txtConnect').value = "ws://" + document.location.hostname + ":9999";
    //document.getElementById('hdnIP').value = document.location.hostname;

    document.body.oncontextmenu = function () {
        return false;
    };


    $.ajaxSetup({
        timeout: ajaxTimeout
    });

    $.ajaxSetup({
        cache: false
    });

    //clientObj.Connect($('#txtConnect').val());
    clientObj.Connect(txtConnect);

    $('div.left-col div.body').hide();
    $('div.middle-col div.body').hide();
    $('div.right-col div.body').hide();

    $('div.customer-demo-lightbox div.body').hide();
    $('.map-controls-wrapper').hide();
    $("#mapImage").attr("src", "images/error_location.png");
    $('div.profit-sanc-lightbox div.body').hide();
    $('div.package-details-lightbox div.body').hide();
    $('div.latest-activities-lightbox div.body').hide();

    $(".complaint-management-lightbox .small-complaints-panel-wrapper").html("");
    $(".cms-body-wrapper").html("");
    $(".complaint-management-lightbox .history-wrapper").css("display", "none");
    $(".complaint-management-lightbox .history-wrapper .work-code-name-wrapper .work-code-name span").html("");
    $(".complaint-management-lightbox .history-wrapper .work-code-name-wrapper .date-wrapper span").html("");
    $(".complaint-management-lightbox .history-wrapper .details-01-wrapper span").html("");
    $(".complaint-management-lightbox .history-wrapper .remarks-wrapper .remarks-details-wrapper span").html("");
    $(".complaint-management-lightbox .history-wrapper .action-remarks-wrapper .details span").html("");
    $(".complaint-management-lightbox .history-wrapper .last-action-remarks-wrapper .details span").html("");
    $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-color").css("background-color", "#5a8800");
    $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-label span").html("");
    $(".complaint-management-lightbox .history-wrapper .customer-complaint-details-wrapper").html("");
    $($(".complaint-management-lightbox .window-controls .search-wrapper")[1]).css("display", "none");

    $(".complaints-list-wrapper .list-left-wrapper .mCSB_container .list-item").css('border', 'none');
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container .list-item").css('border', 'none');
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container").html('');

    LocalTime_timerId = setInterval(function () {
        getTime();
    }, 1000);
});

var hashloginmutex = false;

function hashLogIn(evt) {
    if (!hashloginmutex)
    {
        if (evt.keyCode == 13) {
            hashloginmutex = true;
            if ($.trim($('#txtUsername').val()).length < 1 || $.trim($('#txtUserpass').val()).length < 1) {
                Notifier.error('Please Enter Your Correct Credentials for Authentication');
                $('#txtUsername').val('');
                $('#txtUserpass').val('');
                hashloginmutex = false;
            } else {
                clientObj.setUsername($.trim($('#txtUsername').val()));
                clientObj.setPassword($.trim($('#txtUserpass').val()));
                $('#txtUsername').attr('readonly', 'readonly');
                $('#txtUserpass').attr('readonly', 'readonly');
                $("div.logged-in-name h3").html(clientObj.getUsername());
                $("div.date span").html(new Date().toDateString());
                caresUserLogOn();
            }
            $('#txtUsername').focus();
            return false;
        }
    }
}

window.onerror = function (msg, url, linenumber) {
    var level = 0;
    var message = 'JS-ERROR :: Error message: ' + msg + ' ;; URL: ' + url + ' ;; Line Number: ' + linenumber;

//	$.ajax({
//		//url: 'http://'+$('#hdnIP').val()+':8888/log?level=' + level + '&message=' + encodeURIComponent(message),
//		url: 'http://' + hdnIP + ':8888/log?level=' + level + '&message=' + encodeURIComponent(message),
//		success: function(data) {
//
//		}
//	});

//	Send('H_Log|' + 'webError -> [' + message + ']');

    clientObj.logSend('H_Log|' + 'webError -> [' + message + ']');
};

var _timerVal = 0;
var _windowCloseInsertDateTime = 0;
var _timeOutTimer = null;

window.onbeforeunload = function (e) {

    if (clientObj.isCCTLoggedIn()) {
        windowBeforeCloseLog();
        var message = 'Are you Sure?';
        if (typeof e === 'undefined') {
            e = window.event;
        }
        _windowTimedCount();
        _timerVal++;
        if (e) {
            e.returnValue = message;
            return message;
        }
    }
};

function _windowTimedCount()
{
    _timeOutTimer = setTimeout("_windowTimedCount()", 10);
    if (_timerVal > 0)
    {
        _timerVal = 0;
        windowCloseCancelLog();
        clearTimeout(_timeOutTimer);
        _timeOutTimer = null;
    }
}

$(window).unload(function () {

    if (clientObj.isCCTLoggedIn()) {
        caresUserLogOut('2');
        WebLogs("Chrome window closed by username=" + clientObj.getUsername(), 0);
        windowCloseLog();
    }

});


function WebLogs(message, level) {
    //clientWSLog(message);
//	$.ajax({
//		//url: 'http://'+$('#hdnIP').val()+':8888/log?level=' + level + '&message=' + encodeURIComponent(message),
//		url: 'http://' + hdnIP + ':8888/log?level=' + level + '&message=' + encodeURIComponent(message),
//		success: function(data) {
//
//		}
//	});

//	Send('H_Log|' + 'webLog -> [' + message + ']');

    clientObj.logSend('H_Log|' + 'webLog -> [' + message + ']');
}

$('#btnConnect').live('click', function () {
    //clientObj.Connect($('#txtConnect').val());
    clientObj.Connect(txtConnect);
});

$('#btnClose').live('click', function () {
    clientObj.Close();
});

$('#mute-btn').live('click', function () {
    //$('#mute-btn').css('opacity', '0.1');
    $('#mute-btn').css('background-position-x', '-220px');
    $('#mute-btn').parent().find('div.label span').html('unmute');
    $('#mute-btn').attr('id', 'unmute-btn');
    clientObj.Mute();
});

$('#unmute-btn').live('click', function () {
    //$('#unmute-btn').css('opacity', '1');
    $('#unmute-btn').css('background-position-x', '0px');
    $('#unmute-btn').parent().find('div.label span').html('mute');
    $('#unmute-btn').attr('id', 'mute-btn');
    clientObj.UnMute();
});

$('#btnFeedback').live('click', function () {
    clientObj.Feedback();
    $('#btnFeedback').attr('disabled', true);
    $('#btnFeedback').css('opacity', '0.1');
});

$('#btnAnswer_Call').live('click', function () {
    clientObj.Answer_Call();
});

$('#btnHang_up').live('click', function () {
    clientObj.Hang_up();
    $('#btnHang_up').attr('disabled', true);
    $('#btnHang_up').css('opacity', '0.1');
});

$('#btnHold').live('click', function () {
    clientObj.Hold();
});

$('#btnUnHold').live('click', function () {
    clientObj.UnHold();
});

$('#btnIVR_Conference').live('click', function () {
    $('#divIVRoptions').show('fast');
    $('#btnDoTransfer').hide();
    $('#btnDoConference').show();

});

$('#btnIVR_Transfer').live('click', function () {

    getConsentInfo();

});

/*$('#btnLogin').live('click', function () {
 clientObj.setUsername($.trim($('#txtUsername').val()));
 clientObj.setPassword($.trim($('#txtUserpass').val()));
 caresUserLogOn();
 });*/

$('#btnLogout').live('click', function () {
    clientObj.Logout();
});

$('#btnNot_Ready').live('click', function () {
    clientObj.Not_Ready(clientObj.getAgentSetNotReadyCode());
});

$('#btnReady').live('click', function () {
    clientObj.Ready();
});

$('#btnSingle_Step_Transfer').live('click', function () {
    if ($.trim($('#txtAgentDN').val()).length > 1) {
        clientObj.Single_Step_Transfer($.trim($('#txtAgentDN').val()));
    }
});

//$('#btnTwo_Step_Transfer').live('click', function() {
//	var state = $('#btnTwo_Step_Transfer').html();
//	if ($.trim($('#txtAgentDN').val()).length > 1) {
//		if (state === 'Initialize Transfer') {
//			//$('#btnTwo_Step_Transfer').val('Complete Transfer');
//			clientObj.Two_Step_Transfer_Step1($.trim($('#txtAgentDN').val()));
//		}
//		else if (state === 'Complete Transfer') {
//			//$('#btnTwo_Step_Transfer').val('Initialize Transfer');
//			clientObj.Two_Step_Transfer_Step2();
//		}
//	}
//});

//$('#btn_consult').live('click', function() {
//	var state = $('#btnTwo_Step_Transfer').html();
//	if ($.trim($('#txtAgentDN').val()).length > 1) {
//		if (state === 'Consult') {
//			//$('#btnTwo_Step_Transfer').val('Complete Transfer');
//			clientObj.Two_Step_Transfer_Step1($.trim($('#txtAgentDN').val()));
//		}
////		else if (state === 'Complete Transfer') {
////			//$('#btnTwo_Step_Transfer').val('Initialize Transfer');
////			clientObj.Two_Step_Transfer_Step2();
////		}
//	}
//});

//$('#btnSupv_Transfer').live('click', function() {
//	clientObj.Two_Step_Transfer_Step1('Supervisor');
//	$('#btnInternalCallHangup').attr('disabled', false);
//	$('#btnInternalCallHangup').css('opacity', '1');
//});

$('#btn_consult').live('click', function () {
    if ($.trim($('#txtAgentDN').val()).length > 1) {
//		if (state === 'Consult') 
        {
            //$('#btnTwo_Step_Transfer').val('Complete Transfer');
//			clientObj.Two_Step_Transfer_Step1($.trim($('#txtAgentDN').val()));
            clientObj.Two_Step_Transfer_Step1($.trim($('#txtAgentDN').val()), "Consultation");
        }
//		else if (state === 'Complete Transfer') {
//			//$('#btnTwo_Step_Transfer').val('Initialize Transfer');
//			clientObj.Two_Step_Transfer_Step2();
//		}

        btn_consult_pressed = true;
        btn_transfer_pressed = false;

        $('#btn_consult_end').attr('disabled', false);
        $('#btn_consult_end').css('opacity', '1');

        $('#btn_transfer_end').attr('disabled', false);
        $('#btn_transfer_end').css('opacity', '1');
    }

//	$('#btnInternalCallHangup').attr('disabled', false);
//	$('#btnInternalCallHangup').css('opacity', '1');



});

$('#btn_transfer').live('click', function () {
    if ($.trim($('#txtAgentDN').val()).length > 1) {
//		if (state === 'Consult') 
        {
            //$('#btnTwo_Step_Transfer').val('Complete Transfer');
//			clientObj.Two_Step_Transfer_Step1($.trim($('#txtAgentDN').val()));
            clientObj.Two_Step_Transfer_Step1($.trim($('#txtAgentDN').val()), "Transferred");
        }
//		else if (state === 'Complete Transfer') {
//			//$('#btnTwo_Step_Transfer').val('Initialize Transfer');
//			clientObj.Two_Step_Transfer_Step2();
//		}

        btn_transfer_pressed = true;
        btn_consult_pressed = false;

        $('#btn_consult').attr('disabled', true);
        $('#btn_consult').css('opacity', '0.1');

        $('#btn_consult_end').attr('disabled', true);
        $('#btn_consult_end').css('opacity', '0.1');

        $('#btn_transfer_end').attr('disabled', true);
        $('#btn_transfer_end').css('opacity', '0.1');

        $('#btn_transfer_complete').attr('disabled', false);
        $('#btn_transfer_complete').css('opacity', '1');

    }

//	$('#btnInternalCallHangup').attr('disabled', false);
//	$('#btnInternalCallHangup').css('opacity', '1');

});

$('#btn_transfer_complete').live('click', function () {
    if ($.trim($('#txtAgentDN').val()).length > 1) {
//		if (state === 'Consult') 
        {
            //$('#btnTwo_Step_Transfer').val('Complete Transfer');
            clientObj.Two_Step_Transfer_Step2();
        }
//		else if (state === 'Complete Transfer') {
//			//$('#btnTwo_Step_Transfer').val('Initialize Transfer');
//			clientObj.Two_Step_Transfer_Step2();
//		}

        $('#btn_consult').attr('disabled', true);
        $('#btn_consult').css('opacity', '0.1');

        $('#btn_consult_end').attr('disabled', true);
        $('#btn_consult_end').css('opacity', '0.1');

        $('#btn_transfer_end').attr('disabled', true);
        $('#btn_transfer_end').css('opacity', '0.1');

        $('#btn_transfer').attr('disabled', false);
        $('#btn_transfer').css('opacity', '1');

    }

//	$('#btnInternalCallHangup').attr('disabled', false);
//	$('#btnInternalCallHangup').css('opacity', '1');

});

$('.splash-screen-lightbox').live('click', function () {
    clientObj.Answer_Call();
});

function keyPressSplashScreen(evt) {
    if (evt.keyCode == 13) {
        clientObj.Answer_Call();
    }
}

$('#btnReset').live('click', function () {
    $('#txtUsername').val('');
    $('#txtUserpass').val('');
});

$('#btnDoTransfer').live('click', function () {
    if ($('#IVRTransMenu').val() != null)
    {
        clientObj.IVR_Transfer($('#IVRTransMenu').val());
        $('#divIVRoptions').hide('fast');
    }
});

$('#btnEmDial').live('click', function () {
    clientObj.Single_Step_Transfer($.trim(clientObj.getEM_CDN()));
});

$('#IVRTransMenuValue').live('click', function () {
    if ($('#IVRTransMenuValue').val() != null)
    {
        if ($.trim($('#IVRTransMenuValue').val()).length > 0) {
            clientObj.IVR_Transfer($('#IVRTransMenuValue').val());
            $('#IVRTransSubCat').html('');
            $('#IVRTransMenu').html('');
            $('#close-ivr-lightbox').mousedown();
        }
    }
});


$('#btnConsentDoTransfer').live('click', function () {
    clientObj.Consent_Transfer($('#dropdownlanguage').val());
    //$('#divConsentLanguage').hide('fast');
    $("#close-consent-lightbox").mousedown();
});

$('#btnCancelConsentTransfer').live('click', function () {
    //$('#divConsentLanguage').hide('fast');
    $("#close-consent-lightbox").mousedown();
});

$('#btnDoConference').live('click', function () {
    clientObj.IVR_Conference($('#IVRTransMenu').val());
    $('#divIVRoptions').hide('fast');
});

$('#btnCancelTransfer').live('click', function () {
    $('#divIVRoptions').hide('fast');
});

$('#btnInternalCallAnswer').live('click', function () {
    clientObj.Internal_Call_Answer();
    $('#btnInternalCallAnswer').attr('disabled', true);
    $('#btnInternalCallHangup').attr('disabled', false);
});

$('#btnNRforNextCall').live('click', function () {
    //toggleButton($('#btnNRforNextCall'));
    if (clientObj.getNRforNextCall()) {
        resetButtonSelection($('#btnNRforNextCall'));
        clientObj.setNRforNextCall(false);
    } else {
        setButtonSelection($('#btnNRforNextCall'));
        clientObj.setNRforNextCall(true);
    }
});

//$('#btnInternalCallHangup').live('click', function() {
//	clientObj.Internal_Call_Hangup();
//});

$('#btn_consult_end').live('click', function () {
    clientObj.Internal_Call_Hangup();
//	$('#btn_consult_end').attr('disabled', true);
//	$('#btn_consult_end').css('opacity', "0.1");
});

$('#btn_transfer_end').live('click', function () {
    clientObj.Internal_Call_Hangup();
//	$('#btn_transfer_end').attr('disabled', true);
//	$('#btn_transfer_end').css('opacity', "0.1");
});

$('#btnInternalCallHangup2').live('click', function () {
    clientObj.Internal_Call_Hangup();
    try {
        $.fancybox.close();
    } catch (ex) {
    }
});

$('button#cares-btn').live('click', function () {
    //clientObj.reopenCares();
});

$('#btnEndBreak').live('click', function () {
    if ($('#txtBrkPwd').val() == clientObj.getPassword()) {
        //telephony_bar_show_status = true;
        brk_check = true;
        $('#spanBreakNotice').html('');
        $('#spanBreakTimer').countdown('destroy');
        $('#txtBrkPwd').val('');

        clearInterval(intervalBreakVariable);

        try {
            $.fancybox.close();
        } catch (ex) {
        }
    } else {
        $('#txtBrkPwd').val('');
        $('#spanBreakNotice').html('Invalid Password!!!');
    }
});

$('#divStatsScreen').live('click', function () {

    if (clientObj.isCCTLoggedIn()) {
        $('#divStatsScreen').fadeOut('slow', function () {
            $('#divDashboard').fadeIn('slow');
            $('#txtUsername').val('');
            $('#txtUserpass').val('');
        });
    } else {
        $('#divStatsScreen').fadeOut('slow', function () {
            $('#divLogin').fadeIn('slow');
        });
    }
});

function keyPressBreak(obj, evt) {
    if ($.trim($(obj).val()).length > 0) {
        if (evt.keyCode === 13) {
            if ($('#txtBrkPwd').val() == clientObj.getPassword()) {
                //telephony_bar_show_status = true;
                brk_check = true;
                $('#spanBreakNotice').html('');
                $('#spanBreakTimer').countdown('destroy');
                $('#txtBrkPwd').val('');

                clearInterval(intervalBreakVariable);

                try {
                    $.fancybox.close();
                } catch (ex) {
                }
                close_break_timer_lightbox();
                $('.break-timer-lightbox').blur();
            } else {
                $('#txtBrkPwd').val('');
                hide_break_timer_password();
                Notifier.error('Invalid Password!!!');
            }
        }
    }
}

$('#btnBreak').live('click', function () {
    if ($.trim($('#notReadyDropDownList option:selected').text()).length > 0) {
        clientObj.Not_Ready($('#notReadyDropDownList').val());
        //telephony_bar_show_status = false;
        //telephony_bar_hide();
        $("#lightbox-bg-blackout").mouseenter();
        setTimeout(function () {
            brk_check = false;
            $('#anchrBtn_brkDiv').mousedown();
            $('#spanBreakDesc').html($('#notReadyDropDownList option:selected').text());
            $('#spanBreakTimer').countdown({
                since: new Date(),
                compact: true,
                format: 'HMS',
                description: ''
            });
            intervalBreakVariable = setInterval(function () {
                $('#spanBreakTimer').countdown('resume');
            }, 1);
            $('.break-timer-lightbox').focus();
        }, 500);
    }
});


$("button#acw-btn").live('click', function () {
    if (clientObj.isAgentOnCall()) {
        $('#submitAWC_btn').css('display', 'none');
        $('#close-acw-lightbox').css('display', '');
        $('#minimize-acw-lightbox').css('display', 'none');

        $("button").css('z-index', '0');
        $("#lightbox-bg-blackout").css('display', 'block');
        $("#lightbox-bg-blackout").css('z-index', '1500');
        $("#lightbox-bg-blackout").css('opacity', '0.8');
        /*
         $("#lightbox-bg-blackout").animate({
         opacity: 0.8
         }, 10, 'easeOutCirc');
         */
        $(".acw-lightbox").css('z-index', '2000');
        $(".acw-lightbox").css('display', 'block');
        /*
         $(".acw-lightbox").animate({
         opacity: 1
         }, 10, 'easeOutCirc');
         */
        $(".acw-lightbox").css('opacity', '1');
    }
});

function resegConditionChange(obj) {
    var resegVal = $(obj).val();
    if (resegVal)
    {
        var id = resegVal.split('|')[0];
        var sms = resegVal.split('|')[3];
        var name = resegVal.split('|')[1];
        var selectedHeadName = resegVal.split('|')[2];
        var flowchartName = resegVal.split('|')[5];
        var acwSelectedNameDiv = $('#acwSelectedName').html() + '\n';
        if ($.inArray(id + ',' + sms + ',' + name + ',' + selectedHeadName, ACW_idArray) == -1 && id != '') {
            acwSelectedNameDiv += '<div class="selected-option-wrapper">';
            acwSelectedNameDiv += '<div class="label">';
            acwSelectedNameDiv += '<span class="small-10-body-lucida-white">' + name + '</span>';
            acwSelectedNameDiv += '<input name="acwSelectedNameChkBox" class="acwSelectedNameChkBox_class" type="hidden" value="' + id + ',' + sms + ',' + name + ',' + selectedHeadName + '" onchange="state_acwSelectedName(this)" >';
            acwSelectedNameDiv += '</div>';
            acwSelectedNameDiv += '<div class="remove-btn-wrapper" style="display:none">';
            acwSelectedNameDiv += '<button class="remove-btn" onclick="state_acwSelectedName(this)" style="z-index: 1500; "></button>';
            acwSelectedNameDiv += '</div>';
            acwSelectedNameDiv += '</div>';
            ACW_idArray.push(id + ',' + sms + ',' + name + ',' + selectedHeadName);
        }
        $('#acwSelectedName').html(acwSelectedNameDiv);

        unicaPostEvent("resegmentation", null + '|' + null + '|' + null + '|' + null + '|' + null + '|' + null + '|' + null + '|' + null + '|' + null + '|' + null + '|' + flowchartName + '|' + null);
        $("select#resegCondition option[value='" + resegVal + "']").attr("disabled", "disabled");
    }
}

var initial_zoom = 12;
var max_zoom = 16;
var zoom_step = 1;

function getLocationMap(lat, lon, imageSrc) {
    var image_src = imageSrc + '' + lat + '_' + lon + '_' + initial_zoom + '.png';
    $("#mapImage").attr("src", image_src);
}

$(".map-plus-button").live('click', function () {
    var zoom = parseInt($("#mapImage").attr("src").split("_")[2].split(".")[0]);
    if (zoom < max_zoom) {
        var lat = $("#mapImage").attr("src").split("_")[0].split("/")[$("#mapImage").attr("src").split("_")[0].split("/").length - 1];
        var lon = $("#mapImage").attr("src").split("_")[1];
        //var zoom = parseInt($("#mapImage").attr("src").split("_")[2].split(".")[0]);
        zoom = zoom + zoom_step;
        var image_src = location_url + lat + '_' + lon + '_' + zoom + '.png';
        $("#mapImage").attr("src", image_src);
    }
});

$(".map-minus-button").live('click', function () {
    var zoom = parseInt($("#mapImage").attr("src").split("_")[2].split(".")[0]);
    if (zoom > initial_zoom) {
        var lat = $("#mapImage").attr("src").split("_")[0].split("/")[$("#mapImage").attr("src").split("_")[0].split("/").length - 1];
        var lon = $("#mapImage").attr("src").split("_")[1];
        //var zoom = parseInt($("#mapImage").attr("src").split("_")[2].split(".")[0]);
        zoom = zoom - zoom_step;
        var image_src = location_url + lat + '_' + lon + '_' + zoom + '.png';
        $("#mapImage").attr("src", image_src);
    }
});

$(".cms-complaint-wrapper").live('click', function () {

    $(".cms-loader-center-div").css("display", "none");

    $(".cms-complaint-wrapper").removeClass("cms-complaint-wrapper-selected");
    $($(".complaint-management-lightbox .window-controls .search-wrapper")[1]).css("display", "none");

    $(".complaint-management-lightbox .large-complaints-panel-wrapper .complaints-list-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .dynamic-form-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .history-wrapper").css("display", "");

    $(this).addClass("cms-complaint-wrapper-selected");
    var jsonData = eval('(' + $(this).find(".cmsJsonData").first().val() + ')');
    $(".complaint-management-lightbox .history-wrapper .cmsJsonData").val($(this).find(".cmsJsonData").first().val());
    $(".complaint-management-lightbox .history-wrapper .work-code-name-wrapper .work-code-name span").html(jsonData.workCodeName);
    $(".complaint-management-lightbox .history-wrapper .work-code-name-wrapper .date-wrapper span").html(new Date(jsonData.complaintLogStartDate).toDateString());
    $(".complaint-management-lightbox .history-wrapper .work-code-name-wrapper .work-code-complaint-id-div").html(jsonData.complaintID);
    $(".complaint-management-lightbox .history-wrapper .details-01-wrapper span").html(jsonData.userInformation.toString().replace(/(?:\r\n|\r|\n)/g, '<br/>'));
//	$(".complaint-management-lightbox .history-wrapper .details-01-wrapper span").html(jsonData.userInformation.toString().replace(/(?:\r\n|\r|\n)/g, ''));
    $(".complaint-management-lightbox .history-wrapper .remarks-wrapper .remarks-details-wrapper span").html(jsonData.agentRemarks);
    $(".complaint-management-lightbox .history-wrapper .action-remarks-wrapper .details span").html(jsonData.actionRemarks);
    $(".complaint-management-lightbox .history-wrapper .last-action-remarks-wrapper .details span").html(jsonData.previousRemarks);
    if (jsonData.isComplaintOpen) {
        var workCodeStatus = jsonData.workCodeStatus.toLowerCase();
        if (workCodeStatus.indexOf("wip") != -1) {
            $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-color").css("background-color", "#4583B7");
            $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-label span").html("WIP");
        } else if (workCodeStatus.indexOf("fwd") != -1) {
            $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-color").css("background-color", "#BE2020");
            $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-label span").html("FWD");
        } else if (workCodeStatus.indexOf("open") != -1) {
            $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-color").css("background-color", "#5a8800");
            $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-label span").html("OPEN");
        } else {
            $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-color").css("background-color", "#5a8800");
            $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-label span").html("OPEN");
        }
        $(".complaint-management-lightbox .history-wrapper .declineTicketButton").css('display', '');
    } else {
        $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-color").css("background-color", "#E5B700");
        $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-label span").html(jsonData.workCodeStatus.toUpperCase());
        $(".complaint-management-lightbox .history-wrapper .declineTicketButton").css('display', 'none');
        $(".complaint-management-lightbox .history-wrapper .hotComplaintButton").css('display', 'none');
    }

    if (jsonData.hotComplaintEnabled) {
        $(".complaint-management-lightbox .history-wrapper .hotComplaintButton").css('display', '');
    } else {
        $(".complaint-management-lightbox .history-wrapper .hotComplaintButton").css('display', 'none');
    }

    $(".complaint-management-lightbox .history-wrapper .customer-complaint-details-wrapper").html("");

    var nameValueHtml = '';
    if (jsonData.fileData.length > 0) {
        nameValueHtml += populateComplaintNameValueHtml("Attachment", jsonData);
    }
    nameValueHtml += populateComplaintNameValueHtml("Quality Status", jsonData.wcQualityStatus);
    nameValueHtml += populateComplaintNameValueHtml("Complaint Log Start Date", new Date(jsonData.complaintLogStartDate).toDateString() + " " + new Date(jsonData.complaintLogStartDate).toLocaleTimeString());
    if (!jsonData.isComplaintOpen) {
        nameValueHtml += populateComplaintNameValueHtml("Complaint Log End Date", new Date(jsonData.complaintLogEndDate).toDateString() + " " + new Date(jsonData.complaintLogEndDate).toLocaleTimeString());
    }
    nameValueHtml += populateComplaintNameValueHtml("Logged Status", jsonData.loggedStatus);
    nameValueHtml += populateComplaintNameValueHtml("Logged by", jsonData.loggedBy);

    var customerNumberHtml = '';
    customerNumberHtml += '<div class="complaint-detail-label-wrapper">';
    customerNumberHtml += '<span class="h2-lucida-bold-white">' + $(this).find(".cmsJsonData").first().attr('msisdn') + '</span>';
    customerNumberHtml += '</div>';

    $(".complaint-management-lightbox .history-wrapper .customer-complaint-details-wrapper").html(nameValueHtml + customerNumberHtml);
    $(".complaint-management-lightbox .history-wrapper").css("display", "");
});

function populateComplaintNameValueHtml(name, value) {
    var nameValueHtml = "";
    if (name == "Attachment") {
        var data = base64ToArrayBuffer(value.fileData);
        var blob = new Blob([data], {type: "octet/stream"});
        var anchorTag = '<a href="' + window.URL.createObjectURL(blob) + '" download="' + value.complaintID + "." + value.fileExt + '" >' + value.complaintID + "." + value.fileExt + '</a>';
        nameValueHtml += '';
        nameValueHtml += '<div class="complaint-detail-label-wrapper">';
        nameValueHtml += '<span class="regular-lucida-yellow">' + name + ': </span>';
        nameValueHtml += '<span class="regular-lucida-bold">' + anchorTag + '</span>';
        nameValueHtml += '</div>';
    } else {
        nameValueHtml += '';
        nameValueHtml += '<div class="complaint-detail-label-wrapper">';
        nameValueHtml += '<span class="regular-lucida-yellow">' + name + ': </span>';
        nameValueHtml += '<span class="regular-lucida-bold">' + value + '</span>';
        nameValueHtml += '</div>';
    }
    return nameValueHtml;
}

$("#search-msisdn").live('keypress', function (evt) {
    if (evt.keyCode == 13) {
        var number = $("#search-msisdn").val().trim();
        if (starts_with("03", number) && number.length == 11) {
            getCustomerComplaints("92" + number.substring(1));
        } else if (starts_with("923", number) && number.length == 12) {
            getCustomerComplaints(number);
        }
    }
});

$(".declineTicketButton").live('click', function (evt) {
    var jsonData = eval('(' + $(this).parent().find(".cmsJsonData").first().val() + ')');
    var reason = prompt("Please state reason for ticket decline.");
    var complaintID = jsonData.complaintID;
    var msisdn = jsonData.customerMSISDN;
    if (reason != null) {
        $(this).css("display", "none");
        declineComplaint(complaintID, reason, msisdn);
    }
});

$(".hotComplaintButton").live('click', function (evt) {
    var jsonData = eval('(' + $(this).parent().find(".cmsJsonData").first().val() + ')');
    var parentComplaintID = jsonData.complaintID;
    var userInformation = jsonData.userInformation;
    var wcID = 0;
    for (var _i = 0; _i < complaintWorkCodeObjArray.length; _i++) {
        if (complaintWorkCodeObjArray[_i].wcName.toLowerCase().indexOf("hot complaint_" + jsonData.workCodeName.toLowerCase()) != -1) {
            wcID = complaintWorkCodeObjArray[_i].wcID;
        }
    }
    var complaintFieldObj = {
        agentRemarks: jsonData.agentRemarks,
        msisdn: jsonData.customerMSISDN,
        wcID: wcID,
        wcType: "Complaint"
    };

    var complaintNameValueArray = new Array();

    var _tmpArray = userInformation.split('\n');

    complaintNameValueArray.push({fieldName: "Parent Complaint ID", fieldValue: parentComplaintID});
    for (var _i = 0; _i < _tmpArray.length; _i++) {
        if (_tmpArray[_i].trim().length > 0) {
            complaintNameValueArray.push({fieldName: _tmpArray[_i].split(";")[0], fieldValue: _tmpArray[_i].split(";")[1]});
        }
    }

    var fileData = "";
    var fileName = "";

    if (jsonData.fileData.length > 0) {
        fileData = jsonData.fileData;
        fileName = parentComplaintID + "." + jsonData.fileExt;
    }

    var reason = prompt("Please state reason for hot-complaint.");
    if (reason != null) {
        $(this).css("display", "none");
        complaintFieldObj.agentRemarks = reason;
        logComplaint_Hot(complaintFieldObj, complaintNameValueArray, fileData, fileName);
    }

});

$(".complaint-management-lightbox .new-complaint-btn").live('click', function () {

    $(".cms-loader-center-div").css("display", "none");

    $(".cms-complaint-wrapper").removeClass("cms-complaint-wrapper-selected");

    $($(".complaint-management-lightbox .window-controls .search-wrapper")[1]).css("display", "");

    $(".complaint-management-lightbox .large-complaints-panel-wrapper .dynamic-form-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .history-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .complaints-list-wrapper").css("display", "");

});

$(".main-screen-panel-active .new-complaint-btn").live('click', function () {

    $(".cms-loader-center-div").css("display", "none");

    $(".cms-complaint-wrapper").removeClass("cms-complaint-wrapper-selected");

    $($(".complaint-management-lightbox .window-controls .search-wrapper")[1]).css("display", "");

    $(".complaint-management-lightbox .large-complaints-panel-wrapper .dynamic-form-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .history-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .complaints-list-wrapper").css("display", "");

    $("#cms-expand-btn").mousedown();

});

$(".complaints-list-wrapper .list-left-wrapper .mCSB_container .list-item").live('click', function () {
    $(".complaints-list-wrapper .list-left-wrapper .mCSB_container .list-item").css('border', 'none');
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container").html("");
    $(this).css('border', '1px solid #ffd200');
    var groupName = $(this).find("span").html();
    var complaintNameHtml = '';
    for (var i = 0; i < complaintWorkCodeObjArray.length; i++) {
        if (groupName == complaintWorkCodeObjArray[i].wcGroupName) {
            complaintNameHtml += '<div class="list-item" title="' + complaintWorkCodeObjArray[i].wcName + '">';
            complaintNameHtml += '<input class="complaintWCData" type="hidden" value=\'' + JSON.stringify(complaintWorkCodeObjArray[i]) + '\'>';
            complaintNameHtml += '<span class="small-10-body-lucida-white">' + complaintWorkCodeObjArray[i].wcName + '</span>';
            complaintNameHtml += '</div>';
        }
    }
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container").html(complaintNameHtml);
});

$("#search-complaints").live('keyup', function (evt) {
    $(".complaints-list-wrapper .list-left-wrapper .mCSB_container .list-item").css('border', 'none');
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container .list-item").css('border', 'none');

    var toSearch = $("#search-complaints").val().trim();
    var complaintNameHtml = '';
    if (toSearch.length <= 0) {
        $(".complaints-list-wrapper .list-right-wrapper .mCSB_container").html(complaintNameHtml);
    } else {
        for (var i = 0; i < complaintWorkCodeObjArray.length; i++) {
            if (complaintWorkCodeObjArray[i].wcName.toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
                if (starts_with("hot complaint_", complaintWorkCodeObjArray[i].wcName.toLowerCase())) {

                } else {
                    complaintNameHtml += '<div class="list-item" title="' + complaintWorkCodeObjArray[i].wcName + '">';
                    complaintNameHtml += '<input class="complaintWCData" type="hidden" value=\'' + JSON.stringify(complaintWorkCodeObjArray[i]) + '\'>';
                    complaintNameHtml += '<span class="small-10-body-lucida-white">' + complaintWorkCodeObjArray[i].wcName + '</span>';
                    complaintNameHtml += '</div>';
                }
            }
        }
        $(".complaints-list-wrapper .list-right-wrapper .mCSB_container").html(complaintNameHtml);
    }
});

$(".complaints-list-wrapper .list-right-wrapper .mCSB_container .list-item").live('click', function () {
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container .list-item").css('border', 'none');
    $(this).css('border', '1px solid #ffd200');
    var jsonObj = eval('(' + $(this).find(".complaintWCData").val() + ')');
    //console.log(jsonObj);
    var wcID = jsonObj.wcID;
    var msisdn = $("#search-msisdn").val().trim();
    if (msisdn.length > 0) {
        if (starts_with("923", msisdn) && msisdn.length == 12) {

        } else if (starts_with("03", msisdn) && msisdn.length == 11) {
            msisdn = "92" + msisdn.substring(1, msisdn.length);
        } else {
            msisdn = null;
        }
    } else {
        msisdn = clientObj.getCallerMsisdn();
    }
    if (msisdn != null) {
        if (starts_with("923", msisdn) && msisdn.length == 12) {

        } else if (starts_with("03", msisdn) && msisdn.length == 11) {
            msisdn = "92" + msisdn.substring(1, msisdn.length);
        } else {
            msisdn = null;
        }
    }
    //console.log(msisdn);
    if (msisdn != null && clientObj.isAgentOnCall()) {
        setTimeout(function () {
            getComplaintFields(wcID, msisdn);
        }, 500);
    }
});

function getComplaintFieldHtml(complaintWorkCodeFieldObj) {

//		Control_Name	Control_field_type	Autofilled_Control
//		Combo Box Control	Combo Box Field	1
//		Date Control	Date Field	1
//		File Uploader Control	File Uploader Field	0
//		Text Box Control	Address Field	1
//		Text Box Control	Balance Field	1
//		Text Box Control	CLI Field	1
//		Text Box Control	CNIC Field	1
//		Text Box Control	Connection Type Field	1
//		Text Box Control	Customer Package Field	1
//		Text Box Control	EMAIL Field	0
//		Text Box Control	MSISDN Field	1
//		Text Box Control	Numeric Field	0
//		Text Box Control	Remarks Field	0
//		Text Box Control	Scratch Card Sixteen Digit Field	0
//		Text Box Control	Text Area Field	0
//		Text Box Control	Text Field	0
//		Time Control	Time Field	1
//		Tree View Control	Tree View Field	1

    var wcID = complaintWorkCodeFieldObj.wcID;
    var wcTitle = complaintWorkCodeFieldObj.wcTitle;
    var script = complaintWorkCodeFieldObj.script;
    var controlName = complaintWorkCodeFieldObj.controlName;
    var fieldName = complaintWorkCodeFieldObj.fieldName;
    var fieldToolTip = complaintWorkCodeFieldObj.fieldToolTip;
    var isMandatory = complaintWorkCodeFieldObj.isMandatory;
    var seqNumber = complaintWorkCodeFieldObj.seqNumber;
    var controlFieldType = complaintWorkCodeFieldObj.controlFieldType;
    var autoFilledControl = complaintWorkCodeFieldObj.autoFilledControl;
    var batchLogging = complaintWorkCodeFieldObj.batchLogging;
    var wcType = complaintWorkCodeFieldObj.wcType;
    var lovIDForComboBox = complaintWorkCodeFieldObj.lovIDForComboBox;
    var lovNameForTreeView = complaintWorkCodeFieldObj.lovNameForTreeView;

    var autoFilledMSISDN = complaintWorkCodeFieldObj.autoFilledMSISDN;
    var autoFilledAddress = complaintWorkCodeFieldObj.autoFilledAddress;
    var autoFilledConnectionType = complaintWorkCodeFieldObj.autoFilledConnectionType;
    var autoFilledNIC = complaintWorkCodeFieldObj.autoFilledNIC;
    var autoFilledPackageName = complaintWorkCodeFieldObj.autoFilledPackageName;
    var autoFilledServiceCode = complaintWorkCodeFieldObj.autoFilledServiceCode;
    var autoFilledSIMNo = complaintWorkCodeFieldObj.autoFilledSIMNo;

    var balance = complaintWorkCodeFieldObj.balance;
    var expireTime = complaintWorkCodeFieldObj.expireTime;

    var tmpFieldID = fieldName.toLowerCase().replace(/[^\w\s]/gi, '').replace(/ /g, "").trim();
    var tmpFieldType = controlFieldType.toLowerCase().replace(/[^\w\s]/gi, '').replace(/ /g, "").trim();

    var fieldID = "complaint" + "_" + tmpFieldID + "_" + tmpFieldType + "_" + seqNumber;

    var msisdn = complaintWorkCodeFieldObj.msisdn;

    complaintWorkCodeFieldObj.fieldID = fieldID;

    if (autoFilledMSISDN === undefined) {
        autoFilledMSISDN = msisdn;
    }
    if (autoFilledAddress === undefined) {
        autoFilledAddress = "";
    }
    if (autoFilledConnectionType === undefined) {
        autoFilledConnectionType = "";
    }
    if (autoFilledNIC === undefined) {
        autoFilledNIC = "";
    }
    if (autoFilledPackageName === undefined) {
        autoFilledPackageName = "";
    }
    if (autoFilledServiceCode === undefined) {
        autoFilledServiceCode = "";
    }
    if (autoFilledSIMNo === undefined) {
        autoFilledSIMNo = "";
    }
    if (balance === undefined) {
        balance = "";
    } else {
        balance = "Rs. " + parseFloat(balance / 100).toString();
    }
    if (expireTime === undefined) {
        expireTime = "";
    }

    var fieldHtml = '';

    if (controlFieldType.toLowerCase() == "combo box field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<select id="' + fieldID + '" class="dropdown-form-element">';
        for (var i = 0; i < complaintWorkCodeFieldObj.lovArrayObj.length; i++) {
            fieldHtml += '<option value="' + complaintWorkCodeFieldObj.lovArrayObj[i].lovValue + '">' + complaintWorkCodeFieldObj.lovArrayObj[i].lovName + '</option>';
        }
        fieldHtml += '</select>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "date field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<div class="form-element-datepicker-wrapper">';
        fieldHtml += '<input id="' + fieldID + '" type="text" class="form-element-datepicker">';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "file uploader field") {

    } else if (controlFieldType.toLowerCase() == "address field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<span class="regular-lucida-bold">' + autoFilledAddress + '</span>';
        fieldHtml += '<input id="' + fieldID + '" type="hidden" value="' + autoFilledAddress + '" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "balance field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<span class="regular-lucida-bold">' + balance + '</span>';
        fieldHtml += '<input id="' + fieldID + '" type="hidden" value="' + balance + '" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "cli field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<div class="form-element-textfield-wrapper">';
        var _cliField = clientObj.getCallerMsisdn();
        if (clientObj.getAttachedData()[2].length > 0) {
            if (clientObj.getAttachedData()[2].length === 10) {
                _cliField = "92" + clientObj.getAttachedData()[2];
            } else if (clientObj.getAttachedData()[2].length > 4) {
                _cliField = "92" + clientObj.getAttachedData()[2];
            } else if (clientObj.getAttachedData()[2].length === 4) {
                _cliField = clientObj.getAttachedData()[1];
            } else {
                _cliField = clientObj.getAttachedData()[2];
            }
        }
        fieldHtml += '<input id="' + fieldID + '" disabled="disabled" type="text" class="form-element-textfield complaintFieldNumeric" value="' + _cliField + '" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "cnic field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<span class="regular-lucida-bold">' + autoFilledNIC + '</span>';
        fieldHtml += '<input id="' + fieldID + '" type="hidden" value="' + autoFilledNIC + '" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "connection type field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<span class="regular-lucida-bold">' + autoFilledConnectionType + '</span>';
        fieldHtml += '<input id="' + fieldID + '" type="hidden" value="' + autoFilledConnectionType + '" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "customer package field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<span class="regular-lucida-bold">' + autoFilledPackageName + '</span>';
        fieldHtml += '<input id="' + fieldID + '" type="hidden" value="' + autoFilledPackageName + '" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "email field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<div class="form-element-textfield-wrapper">';
        fieldHtml += '<input id="' + fieldID + '" type="text" class="form-element-textfield" value="" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "msisdn field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<div class="form-element-textfield-wrapper">';
        fieldHtml += '<input id="' + fieldID + '" type="text" class="form-element-textfield complaintFieldNumeric" value="' + autoFilledMSISDN + '" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "numeric field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<div class="form-element-textfield-wrapper">';
        fieldHtml += '<input id="' + fieldID + '" type="text" class="form-element-textfield complaintFieldNumeric" value="" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "remarks field") {
        fieldHtml += '<div class="complaint-desc-wrapper" style="display: inline-block;" title="' + fieldToolTip + '">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<textarea id="' + fieldID + '" placeholder="' + fieldName + '"></textarea>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "scratch card sixteen digit field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<div class="form-element-textfield-wrapper">';
        fieldHtml += '<input id="' + fieldID + '" type="text" class="form-element-textfield complaintFieldNumeric" maxlength="16" value="" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "text area field") {
        fieldHtml += '<div class="complaint-desc-wrapper" style="display: inline-block;" title="' + fieldToolTip + '">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<textarea id="' + fieldID + '" placeholder="' + fieldName + '"></textarea>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "text field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<div class="form-element-textfield-wrapper">';
        fieldHtml += '<input id="' + fieldID + '" type="text" class="form-element-textfield" value="" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "time field") {
        fieldHtml += '<div class="form-element-wrapper">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<div class="form-element-textfield-wrapper">';
        fieldHtml += '<input id="' + fieldID + '" type="time" class="form-element-textfield" />';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
    } else if (controlFieldType.toLowerCase() == "tree view field") {

        fieldHtml += '<div class="form-element-wrapper" style="min-height: 70px;">';
        fieldHtml += '<div class="form-element-heading">';
        fieldHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintWorkCodeFieldObj) + '\' />';
        fieldHtml += '<span class="regular-lucida-yellow">' + fieldName + '</span>';
        fieldHtml += '</div>';
        fieldHtml += '<div class="form-element-body" title="' + fieldToolTip + '">';
        fieldHtml += '<div class="form-element-textfield-wrapper tree-view-div" style="background:none;">';
        fieldHtml += '<input type="hidden" id="' + fieldID + '" value="" />';

        fieldHtml += '<select id="tree-view-select1" >';
        fieldHtml += '</select>';
        fieldHtml += '<select id="tree-view-select2" >';
        fieldHtml += '</select>';
        fieldHtml += '<select id="tree-view-select3" >';
        fieldHtml += '</select>';

        fieldHtml += '</div>';
        fieldHtml += '</div>';
        fieldHtml += '</div>';
        setTimeout(function () {
            getTreeData("Regions", "#tree-view-select1");
        }, 1000);
    } else {

    }
    return fieldHtml;
}

$(".complaint-management-lightbox .dynamic-form-wrapper .form-btns-wrapper .grey-btn").live('click', function () {
    $(".complaint-management-lightbox .new-complaint-btn").click();
});

$('.complaintFieldNumeric').live('keypress', function (evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
});

$(".form-element-datepicker").live('keydown', function (evt) {
    return false;
});


$(".complaint-management-lightbox .dynamic-form-wrapper .form-btns-wrapper .green-btn").live('click', function () {
    var complaintData = {
        msisdn: "",
        wcID: "",
        wcType: "",
        agentRemarks: "",
        customerEmail: "",
        fileupload: ""
    };
    var complaintNameValue = {
        fieldName: "",
        fieldValue: ""
    };
    var complaintNameValueArray = new Array();
    var isValidated = true;
    $('.complaint-management-lightbox .large-complaints-panel-wrapper .dynamic-form-wrapper .mCSB_container .complaint-form-elements-wrapper').children().each(function () {
        var complaintFieldObj = eval('(' + $(this).find('input.complaintWCFieldObj').val() + ')');
        var fieldID = "#" + complaintFieldObj.fieldID;
        if (complaintFieldObj.isMandatory && $(fieldID).val().trim().length > 0) {
            $(fieldID).parent().css("border", "none");
        } else if (!complaintFieldObj.isMandatory) {
            $(fieldID).parent().css("border", "none");
        } else {
            $(fieldID).parent().css("border", "1px solid #FF0000");
            isValidated = false;
            console.log("Check Validation: " + $(fieldID));
            console.log("Check Validation: " + $(fieldID).parent());
        }
        complaintData.msisdn = complaintFieldObj.msisdn.toString();
        complaintData.wcID = complaintFieldObj.wcID.toString();
        complaintData.wcType = complaintFieldObj.wcType.toString();
        complaintData.msisdn = $("#complaint_msisdn_msisdnfield_0").val();
        if (isNaN(complaintData.msisdn)) {
            complaintData.msisdn = complaintFieldObj.msisdn.toString();
        }
        if (complaintFieldObj.fieldID == "complaint_agent_remarks_0") {
            complaintData.agentRemarks = $("#" + complaintFieldObj.fieldID).val().trim();
//			complaintNameValue.fieldName = "Agent Remarks";
//			complaintNameValue.fieldValue = $("#" + complaintFieldObj.fieldID).val().trim();
            complaintNameValueArray.push({fieldName: "Agent Remarks", fieldValue: $("#" + complaintFieldObj.fieldID).val().trim()});
        } else if (complaintFieldObj.fieldID == "complaint_customer_email_0") {
            complaintData.customerEmail = $("#" + complaintFieldObj.fieldID).val().trim();
//			complaintNameValue.fieldName = "Agent Remarks";
//			complaintNameValue.fieldValue = $("#" + complaintFieldObj.fieldID).val().trim();
            complaintNameValueArray.push({fieldName: "Customer's Email", fieldValue: $("#" + complaintFieldObj.fieldID).val().trim()});
        } else if (complaintFieldObj.fieldID == "complaint_file_upload_0") {
            complaintData.fileupload = $("#" + complaintFieldObj.fieldID).val().trim();
//			complaintNameValue.fieldName = "Agent Remarks";
//			complaintNameValue.fieldValue = $("#" + complaintFieldObj.fieldID).val().trim();
            complaintNameValueArray.push({fieldName: "File Uploaded", fieldValue: $("#" + complaintFieldObj.fieldID).val().trim()});
        } else {
//			complaintNameValue.fieldName = complaintFieldObj.fieldName;
//			complaintNameValue.fieldValue = $("#" + complaintFieldObj.fieldID).val().trim();
            complaintNameValueArray.push({fieldName: complaintFieldObj.fieldName.toString(), fieldValue: $("#" + complaintFieldObj.fieldID).val().trim()});
        }
//		complaintNameValueArray.push(eval(complaintNameValue));
    });
    if (isValidated) {
        if (upload_files.length > 0) {
            var file = upload_files[0];
            if (upload_files && file) {
                var reader = new FileReader();
                reader.readAsBinaryString(file);
                reader.onload = function (readerEvt) {
                    var binaryString = readerEvt.target.result;
                    var file_data = btoa(binaryString);
                    var file_name = file.name;
                    logComplaint(complaintData, complaintNameValueArray, file_data, file_name);
                };
            } else {
                logComplaint(complaintData, complaintNameValueArray, "", "");
            }
        } else {
            logComplaint(complaintData, complaintNameValueArray, "", "");
        }
    }
});

$(".notification-bar-wrapper").live('click', function () {
    hide_notification_bar();
});

function base64ToArrayBuffer(base64) {
    var binaryString = atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

$("#acw-seg-selector").live('click', function () {
    var selectvalue = $(this).val();
    clientObj.setProductType(selectvalue.toString());
});

$(".sop-search-field").live('keyup', function (evt) {
    var toSearch = $(".sop-search-field").val().trim();
    var sopHtml = '';
    if (toSearch.length <= 0) {
        $(".sop-pop-div").html(sopHtml);
    } else if (toSearch.length > 1) {
        for (var i = 0; i < sopDataArray.length; i++) {
            if (sopDataArray[i].sopName.toLowerCase().indexOf(toSearch.toLowerCase()) != -1) {
                var sopName = sopDataArray[i].sopName;
                var sopDpt = sopDataArray[i].sopDpt;
                var sopURL = sopDataArray[i].sopURL + encodeURIComponent(sopDataArray[i].sopNumber) + ".pdf";
                sopHtml += '<ul>';
                sopHtml += '<li>';
                sopHtml += '<div class="heading sop-link-div" style="width: 307px; word-wrap: break-word; cursor: pointer; padding: 2px 5px;" onclick="window.open(\'' + sopURL + '\', \'' + sopName + '\', \'height=600,width=1100\');">';
//				sopHtml += '<span class="small-10-body-lucida-white">' + sopName + '(' + sopDpt + ')' + '</span>';
                sopHtml += '<span class="small-10-body-lucida-white">' + sopName + '' + '</span>';
                sopHtml += '</div>';
                sopHtml += '</li>';
                sopHtml += '</ul>';
            }
        }
        $(".sop-pop-div").html(sopHtml);
    }
});

$("#tree-view-select1, #tree-view-select2, #tree-view-select3").live('change', function () {
    var lov_name = $(this).find("option:selected").text();
    var lov_value = $(this).val();

    $(this).parent().find("input[type=hidden]").val(lov_value);

    var select_position = parseInt($(this).attr("id").substring($(this).attr("id").length - 1, $(this).attr("id").length));

    if (select_position == 1) {
        $("#tree-view-select2").html("");
        $("#tree-view-select3").html("");
    } else if (select_position == 2) {
        $("#tree-view-select3").html("");
    } else if (select_position == 3) {

    }

    var lov_value_1 = $("#tree-view-select1").val();
    var lov_value_2 = $("#tree-view-select2").val();
    var lov_value_3 = $("#tree-view-select3").val();

    var _tmp_lov_value = "";
    if (lov_value_1 !== null && lov_value_1 !== undefined) {
//		_tmp_lov_value += "[" + lov_value_1 + "]" + "-";
        _tmp_lov_value += lov_value_1 + "~";
    }
    if (lov_value_2 !== null && lov_value_2 !== undefined) {
//		_tmp_lov_value += "[" + lov_value_2 + "]" + "-";
        _tmp_lov_value += lov_value_2 + "~";
    }
    if (lov_value_3 !== null && lov_value_3 !== undefined) {
//		_tmp_lov_value += "[" + lov_value_3 + "]" + "-";
        _tmp_lov_value += lov_value_3 + "~";
    }
    _tmp_lov_value = _tmp_lov_value.substring(0, _tmp_lov_value.length - 1);

    $(this).parent().find("input[type=hidden]").val(_tmp_lov_value);

    select_position++;
    if (select_position < 4) {
        getTreeData(lov_name, "#tree-view-select" + select_position);
    }
});