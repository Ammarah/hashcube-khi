<%@page language="java" import="hash3.Main.IndexPageClass" contentType="text/html" pageEncoding="UTF-8"%>
<%response.setHeader("Cache-Control", "private, no-cache, no-store, must-revalidate, max-age=0");%>
<%response.setDateHeader("Expires", 0);%>
<%response.setHeader("Pragma", "no-cache");%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!--<!DOCTYPE html>-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<html>
    <head>
        <base href="<%=basePath%>">

        <title>HashCube</title>

        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">

        <link rel="shortcut icon" href="images/HashLogo.ico" />
        <link href="Styles/reset.css" rel="stylesheet" type="text/css" />
        <link href="Styles/hashcube-global.css" rel="stylesheet" type="text/css" />
        <link href="Styles/hashcube-links.css" rel="stylesheet" type="text/css" />
        <link href="Styles/hashcube-typography.css" rel="stylesheet" type="text/css" />
        <link href="Styles/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
        <link href="Styles/jquery.fancybox.css" rel="stylesheet" type="text/css" />
        <link href="Styles/telephonySlideOut.css" rel="stylesheet" type="text/css" />
        <link href="Styles/custom-css.css" rel="stylesheet" type="text/css" />
        <link href="Styles/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            var txtConnect = "ws://<%=request.getRemoteAddr()%>:9999";
            var hdnIP = "<%=request.getRemoteAddr()%>";

            txtConnect = "ws://localhost:9999";
            hdnIP = "localhost";

            //window.open("http://www.google.com.pk", "sample window", "height=600,width=800");
        </script>

        <script src="Scripts/jquery-1.7.2.min.js" type="text/javascript" ></script>
        <script src="Scripts/settings.js" type="text/javascript" ></script>
        <script src="Scripts/jquery.easing.1.3.js" type="text/javascript" ></script>
        <script src="Scripts/jquery.watermark.min.js" type="text/javascript" ></script>
        <script src="Scripts/jquery.mCustomScrollbar.concat.min.js" type="text/javascript" ></script>
        <script src="Scripts/animations.js" type="text/javascript" ></script>
        <script src="Scripts/jquery.countdown.js" type="text/javascript" ></script>
        <script src="Scripts/jquery.fancybox.pack.js" type="text/javascript" ></script>
        <!--<script src="Scripts/jquery-ui-personalized-1.6rc2.min.js" type="text/javascript" ></script> -->   
        <script src="Scripts/jquery-ui.min.js" type="text/javascript" ></script>
        <script src="Scripts/custom_form_elements.js" type="text/javascript" ></script>
        <script src="Scripts/kineticjs.js" type="text/javascript" ></script>
        <script src="Scripts/stats-bar-sidebar-graphs.js" type="text/javascript" ></script>
        <script src="Scripts/notifier.js" type="text/javascript" ></script>


        <!--<script data-main="Scripts/animations-sidebars.js" src="Scripts/require.js"></script>-->
        <script src="Scripts/HashClass.js" type="text/javascript"></script>
        <script src="Scripts/HashStatsFunction.js" type="text/javascript"></script>
        <script src="Scripts/animations-sidebars.js" type="text/javascript"></script>
        <!--<script src="Scripts/HashWebWorker.js" type="text/javascript" ></script>-->
        <script src="Scripts/AjaxJS.js" type="text/javascript" ></script>
        <script src="Scripts/HashClient.js" type="text/javascript" ></script>
        <script src="Scripts/HashStyleJS.js" type="text/javascript" ></script>
        <script src="Scripts/AWC_JS.js" type="text/javascript" ></script>
        <!--<script src="Scripts/AjaxJS.js" type="text/javascript" ></script>-->
        <script src="Scripts/HashWorkcode.js" type="text/javascript" ></script>
        <script src="Scripts/FWBL_JS.js" type="text/javascript"></script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                $('.dropdown').customStyle();
            });
            jQuery(document).ready(function () {
                $('.dropdown-offers').customStyle('offers');
            });
            jQuery(document).ready(function () {
                $('.dropdown-large').customStyle('large');
            });
            jQuery(document).ready(function () {
                $('.dropdown-without-icon').customStyle('withouticon');
            });
            jQuery(document).ready(function () {
                $('.dropdown-form-element').customStyle('formelement');
            });
            jQuery(document).ready(function () {
                $('.dropdown-acw-element').customStyle('formelement');
            });
            jQuery(document).ready(function () {
                $(".form-element-datepicker").datepicker({
                    dateFormat: 'MM dd, yy'
                });
            });
        </script>

    </head>

    <body>

        <div id="divWrapper" >

            <div id="divDashboard" style="display: none;">

                <div id="wrap">
                    <div id="main">
                        <div id="content-main">
                            <div class="main-screen-panel-row">
                                <div id="nbaDiv-disabled" class="left-col" >
                                    <div class="main-screen-panel-inactive"></div>
                                </div>
                                <div id="nbaDiv-enabled" class="left-col" style="display: none;">
                                    <div class="main-screen-panel-active">
                                        <img id="unicaOfferLoadImage" src="images/unica-loader.gif" style="display: none;position: absolute;z-index: 920" />
                                        <div class="header">
                                            <div class="heading"><h4><span class="drop-shadow">Offers</span></h4></div>
                                            <div class="pagination-expand-wrapper">
                                                <div class="expand-button-wrapper"><button class="expand-button" id="offers-expand-btn" style="display: none;"></button></div>
                                            </div> 
                                        </div>
                                        <div class="body">
                                            <div class="offers-details-body-wrapper">
                                                <!--
                                                commented div!!!!!
                                                <div class="holder">
                                                        <img src="images/offers_bottom_holder_bg.png" width="354" height="53" alt="" />
                                                </div>
                                                -->
                                                <div class="holder">
                                                    <div class="dropdown-wrapper">
                                                        <select id="resegCondition" class="dropdown-offers" onchange="resegConditionChange(this)">

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="show_all_cards_wrapper">
                                                    <button class="show_all_cards"></button>
                                                </div>
                                                <div class="no-more-offers-wrapper">
                                                    <div class="no-more-offers">
                                                        <span class="regular-lucida-italic">No More Offers</span>
                                                    </div>
                                                </div>
                                                <div class="cards-wrapper">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="middle-col">
                                    <div class="main-screen-panel-inactive" id="_customerEngagementPreferenceInactive" style="display:none"></div>
                                    <div class="main-screen-panel-active customerEngagementPreference-portlet-enable" id="_customerEngagementPreferenceActive">
                                        <div class="header">
                                            <div class="heading">
                                                <h4>
                                                    <span class="drop-shadow" id="_customerEngagementPreferenceHeader">
                                                        Customer Engagement Preference
                                                    </span>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="body" style="display: none;">
                                            <div id="customerEngagementPreference" style="padding: 10px 10px 10px 35px; text-align: center;">
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="right-col">
                                    <div class="main-screen-panel-inactive supercard-portlet-disable" id="_superCardInactive" style="display:none">
                                    </div>
                                    <div class="main-screen-panel-active supercard-portlet-enable" id="_superCardActive">
                                        <div class="header">
                                            <div class="heading">
                                                <h4>
                                                    <span class="drop-shadow" id="_superCardHeader">
                                                        Super card details
                                                    </span>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="body" style="display:none">
                                            <div id="superCardHolder" style="padding: 10px 10px 10px 35px;">
                                                <table style="color: #FFFFFF; width: 100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td class="regular-lucida-bold-yellow-10">Super Card User</td>
                                                            <td id="isSuperCardUser" style="font-size: 13px;">N/A</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="regular-lucida-bold-yellow-10">On-Net Minutes</td>
                                                            <td id="superCardOnMinutes" style="font-size: 13px;">N/A</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="regular-lucida-bold-yellow-10">Off-Net Minutes</td>
                                                            <td id="superCardOffMinutes" style="font-size: 13px;">N/A</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="regular-lucida-bold-yellow-10">Data</td>
                                                            <td id="superCardData" style="font-size: 13px;">N/A</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="regular-lucida-bold-yellow-10">SMS</td>
                                                            <td id="superCardSms" style="font-size: 13px;">N/A</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="main-screen-panel-row">
                                <div id="fwbl-left-col" class="left-col" style="display:none;">
                                    <div class="main-screen-panel-active">
                                        <div class="header">
                                            <div class="heading"><h4><span class="drop-shadow">Customer PIN change</span></h4></div>
                                        </div>
                                        <div class="body">
                                            <div style="padding-left: 10px; position: relative;">
                                                <div class="form-element-wrapper">
                                                    <div class="form-element-body" title="" style="width: 100%;float:none;">
                                                        <span class="regular-lucida-yellow" style="display: inline-block;width: 128px;">PAN Number</span>
                                                        <div class="form-element-textfield-wrapper" style="display: inline-block; border: none;">
                                                            <input id="fwbl-pan-input" type="text" class="form-element-textfield" readonly />
                                                        </div>
                                                    </div>
                                                    <br clear="both">
                                                </div>
                                                <div class="form-element-wrapper">
                                                    <div class="form-element-body" title="" style="width: 100%;float:none;">
                                                        <span class="regular-lucida-yellow" style="display: inline-block;width: 128px;">CNIC Number</span>
                                                        <div class="form-element-textfield-wrapper" style="display: inline-block; border: none;">
                                                            <input id="fwbl-cnic-input" type="text" class="form-element-textfield" readonly />
                                                            <!--                                                            <input id="fwbl-cnic-input" type="number" size="13" class="form-element-textfield" placeholder="E.g 0000012345678" onkeypress="if (this.value.length == 13) {
                                                                                                                                    return false;}" readonly />-->
                                                        </div>
                                                    </div>
                                                    <br clear="both">
                                                </div>
                                                <div class="form-element-wrapper">
                                                    <div class="form-element-body" title="" style="width: 100%;float:none;">
                                                        <span class="regular-lucida-yellow" style="display: inline-block;width: 128px;">Select Type</span>
                                                        <div class="form-element-textfield-wrapper" style="display: inline-block; border: none;">
                                                            <input id="fwbl-ivrflow-input" type="text" class="form-element-textfield" readonly />
                                                        </div>
                                                        <!--                                                        <div class="form-element-body" title="" style="display: inline-block; border: none; float: none;">
                                                            <select id="fwbl-ivrflow-option" name="fwbl-ivrflow" class="dropdown-form-element" style="width: 163px;">
                                                                <option value="" selected="selected" disabled="">Select Option</option>
                                                                <option value="GPIN">Generate ATM PIN</option>
                                                                <option value="GTPIN">Generate TPIN</option>
                                                                <option value="CPIN">Change ATM PIN</option>
                                                                <option value="CTPIN">Change TPIN</option>
                                                            </select>
                                                        </div>-->
                                                    </div>
                                                    <br clear="both">
                                                </div>
                                                <div class="checkbox-wrapper" style="padding: 8px 0 8px 0;">
                                                    <div class="checkbox-container">
                                                        <input id="fwbl-agent-verification" type="checkbox" class="checkbox-generic" checked />
                                                        <div id="fwbl-checkbox" class="checkbox-label">
                                                            <span class="regular-lucida">Above given information is verfied?</span>
                                                        </div>
                                                        <br clear="both" />
                                                    </div>
                                                </div>
                                                <button id="fwbl-transfer-btn">Transfer</button>
                                                <button id="fwbl-fetch-details-btn">Fetch Details</button>
                                                <div class="fwlb-fetch-loader">
                                                    <img src="images/cms-loading.gif" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="left-col"><!-- Profit Sanctuaries Portlet -->
                                    <div id="profitSanctuariesIdMain" class="main-screen-panel-active">
                                        <div class="header">
                                            <div class="heading"><h4><span class="drop-shadow">Profit Sanctuaries</span></h4></div>
                                            <div class="pagination-expand-wrapper">
                                                <div class="expand-button-wrapper"><button class="expand-button" id="profit-sanc-expand-btn"></button></div>
                                            </div>
                                        </div>
                                        <div class="body">
                                            <div class="profit-sanctuaries-body-wrapper">
                                                <div class="billing-tracks-wrapper">
                                                    <div class="profit-timeline-container">
                                                        <div class="label"><span class="regular-lucida">Total Bill</span></div>
                                                        <div class="profit-base-track">
                                                            <div class="profit-progress-track-1"></div>
                                                            <div class="profit-current-status-1"></div>
                                                        </div>
                                                        <div class="bill-value">
                                                            <span class="small-body-lucida-white-italic" ></span>
                                                        </div>
                                                    </div>
                                                    <div class="profit-timeline-container">
                                                        <div class="label"><span class="regular-lucida">Voice Bill</span></div>
                                                        <div class="profit-base-track">
                                                            <div class="profit-progress-track-2"></div>
                                                            <div class="profit-current-status-2"></div>
                                                        </div>
                                                        <div class="bill-value">
                                                            <span class="small-body-lucida-white-italic" ></span>
                                                        </div>
                                                    </div>
                                                    <div class="profit-timeline-container">
                                                        <div class="label"><span class="regular-lucida">Data Bill</span></div>
                                                        <div class="profit-base-track">
                                                            <div class="profit-progress-track-3"></div>
                                                            <div class="profit-current-status-3"></div>
                                                        </div>
                                                        <div class="bill-value">
                                                            <span class="small-body-lucida-white-italic" ></span>
                                                        </div>
                                                    </div>
                                                    <div class="profit-timeline-container-last">
                                                        <div class="label"><span class="regular-lucida">Messaging Bill</span></div>
                                                        <div class="profit-base-track">
                                                            <div class="profit-progress-track-4"></div>
                                                            <div class="profit-current-status-4"></div>
                                                        </div>
                                                        <div class="bill-value">
                                                            <span class="small-body-lucida-white-italic" ></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="current-billing-wrapper">
                                                    <div class="current-bill-bg">
                                                        <div class="current-bill-left-content-wrapper">
                                                            <table style="width:100%;" >
                                                                <tr >
                                                                    <td ><div class="label"><span id="spanCurrentBill" class="regular-lucida-bold-yellow">Current Bill</span></div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><div class="current-bill-value"><span id="spanCurrentBillValue" class="regular-lucida-bold"></span></div></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="current-bill-right-content-wrapper">
                                                            <div class="bill-label-wrapper">
                                                                <div class="average-bill-label"><span id="spanAverageBill" class="small-body-lucida-white-bold"></span></div>
                                                                <div class="last-bill-label"><span id="spanLastBill" class="small-body-lucida-white-bold"></span></div>
                                                            </div>
                                                            <div class="bill-value-wrapper">
                                                                <table style="width:100%">
                                                                    <tr>
                                                                        <td><div class="average-bill-value"><span id="spanAverageBillValue" class="small-body-lucida-light-grey-bold"></span></div></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><div class="last-bill-value"><span id="spanLastBillValue" class="small-body-lucida-light-grey-bold"></span></div></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="middle-col">
                                    <div class="main-screen-panel-active">
                                        <div class="header">
                                            <div class="heading"><h4><span class="drop-shadow">Customer Demographics</span></h4></div>
                                            <div class="pagination-expand-wrapper">
                                                <div class="expand-button-wrapper"><button class="expand-button" id="customer-demo-expand-btn"></button></div>
                                            </div>
                                        </div>
                                        <div class="body">
                                            <div class="customer-demographics-body-wrapper">
                                                <div class="customer-details-wrapper">
                                                    <div class="customer-name-number-wrapper">
                                                        <div id="divCustomerName" class="name"></div>
                                                        <div id="divCustomerNumber" class="number"></div>
                                                    </div>
                                                    <div id="divCustomerRatingStars" class="customer-stars-wrapper">

                                                    </div>
                                                    <div class="customer-name-sep"></div>
                                                    <div class="customer-other-details-wrapper">
                                                        <div id="divPackageName" class="package-name"></div>
                                                        <div id="divWithUfone" class="with-ufone"></div>
                                                        <div id="divBirthday" class="birthday"></div>
                                                        <div id="divCallStatus" class="birthday"></div>
                                                    </div>
                                                </div>
                                                <div class="handsets-wrapper">
                                                    <div class="handsets-content-bg">
                                                        <div class="handset-current-wrapper">
                                                            <div class="handset-pic"><img src="images/handsets/iphone-3g.png" width="37" height="70" /></div>
                                                            <div class="handset-desc">
                                                                <div class="handset-label"><span class="regular-body-lucida-light-grey">Current</span></div>
                                                                <div id="divCurrentHandset" class="handset-name"></div>
                                                            </div>
                                                        </div>
                                                        <div class="handset-previous-wrapper">
                                                            <div class="handset-pic"><img src="images/handsets/iphone-3g.png" width="37" height="70" /></div>
                                                            <div class="handset-desc">
                                                                <div class="handset-label"><span class="regular-body-lucida-light-grey">Previous</span></div>
                                                                <div id="divPreviousHandset" class="handset-name"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="right-col">
                                    <div class="main-screen-panel-inactive" id="lastactivities-portlet-disable" style="display: none"></div>
                                    <div class="main-screen-panel-active" id="lastactivities-portlet-enable">
                                        <div class="header">
                                            <div class="heading"><h4><span class="drop-shadow">Last Activities</span></h4></div>
                                            <div class="pagination-expand-wrapper">
                                                <div class="expand-button-wrapper"><button class="expand-button" id="latest-activities-expand-btn"></button></div>
                                            </div>
                                        </div>
                                        <div class="body">

                                            <!--<div class="last-acitivty-body-wrapper">
                                                    <div class="sep"></div>
                                                    <div class="activity-row">
                                                            <div class="icon"><div class="active-gprs-icon"></div></div>
                                                            <div class="desc"><span class="regular-lucida">21 Mb</span></div>
                                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                                    </div>
                                                    <div class="sep"></div>
                                                    <div class="activity-row">
                                                            <div class="icon"><div class="inactive-message-icon"></div></div>
                                                            <div class="desc"><span class="regular-lucida">+92 300 540 9876</span></div>
                                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                                    </div>
                                                    <div class="sep"></div>
                                                    <div class="activity-row">
                                                            <div class="icon"><div class="active-call-icon"></div></div>
                                                            <div class="desc"><span class="regular-lucida">+92 333 789 6753</span></div>
                                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                                    </div>
                                                    <div class="sep"></div>
                                                    <div class="activity-row">
                                                            <div class="icon"><div class="active-message-icon"></div></div>
                                                            <div class="desc"><span class="regular-lucida">+92 300 540 9876</span></div>
                                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                                    </div>
                                                    <div class="sep"></div>
                                                    <div class="activity-row">
                                                            <div class="icon"><div class="inactive-message-icon"></div></div>
                                                            <div class="desc"><span class="regular-lucida">+92 333 789 6753</span></div>
                                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                                    </div>
                                                    <div class="sep"></div>
                                            </div>-->

                                            <div id="divAgentHistory" class="last-acitivty-body-wrapper">

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="main-screen-panel-row">
                                <div class="left-col"><!-- Package Details -->
                                    <div class="main-screen-panel-inactive" id="package-portlet-disable" style="display: none"></div>
                                    <div class="main-screen-panel-active" id="package-portlet-enable">
                                        <div class="header">
                                            <div class="heading"><h4><span class="drop-shadow">Package Details</span></h4></div>
                                            <div class="pagination-expand-wrapper">
                                                <ul class="pagination-buttons">
                                                    <li class="button"><button class="inactive"></button></li>
                                                    <li class="button"><button id="vas_screen_active_btn" class="inactive"></button></li>
                                                </ul>
                                                <div class="sep"></div>
                                                <div class="expand-button-wrapper"><button class="expand-button" id="package-details-expand-btn"></button></div>
                                            </div> 
                                        </div>
                                        <div class="body">
                                            <div class="package-details-body-wrapper">
                                                <div class="row-wrapper">
                                                    <div class="dropdown-wrapper">
                                                        <select id="voiceDropDown" class="dropdown" disabled="disabled">

                                                        </select>
                                                        <div id="icon_1" class="voice_icn"></div>
                                                    </div>
                                                    <div class="services-wrapper">
                                                        <div class="inactive cli"><span class="small-body-lucida-white">CLI</span></div>
                                                        <div class="inactive waiting"><span class="small-body-lucida-white">Waiting</span></div>
                                                        <div class="inactive forward"><span class="small-body-lucida-white">Forward</span></div>
                                                        <div class="inactive restrict"><span class="small-body-lucida-white">Restrict</span></div>
                                                    </div>
                                                </div>
                                                <div class="row-wrapper">
                                                    <div class="dropdown-wrapper">
                                                        <select id="dataDropDown" class="dropdown" disabled="disabled">

                                                        </select>
                                                        <div id="icon_2" class="data_icn"></div>
                                                    </div>
                                                    <div class="services-wrapper">
                                                        <div class="inactive wap"><span class="small-body-lucida-white">WAP</span></div>
                                                        <div class="inactive gprs"><span class="small-body-lucida-white">GPRS</span></div>
                                                        <div class="inactive umail"><span class="small-body-lucida-white">Umail</span></div>
                                                        <div class="inactive fax"><span class="small-body-lucida-white">Fax</span></div>
                                                    </div>
                                                </div>
                                                <div class="row-wrapper">
                                                    <div class="dropdown-wrapper">
                                                        <select id="smsDropDown" class="dropdown" disabled="disabled">

                                                        </select>
                                                        <div id="icon_3" class="message_icn"></div>
                                                    </div>
                                                    <div class="services-wrapper">
                                                        <div class="inactive sms"><span class="small-body-lucida-white">SMS</span></div>
                                                        <div class="inactive mms"><span class="small-body-lucida-white">MMS</span></div>
                                                        <div class="inactive dr"><span class="small-body-lucida-white">DR</span></div>
                                                    </div>
                                                </div>
                                                <div class="row-wrapper">
                                                    <div class="dropdown-wrapper">
                                                        <select id="othersDropDown" class="dropdown" disabled="disabled">

                                                        </select>
                                                        <div id="icon_4" class="others_icn"></div>
                                                    </div>
                                                    <div class="services-wrapper">
                                                        <div class="inactive info"><span class="small-body-lucida-white">Info</span></div>
                                                        <div class="inactive cbrt"><span class="small-body-lucida-white">CBRT</span></div>
                                                        <div class="inactive mcn"><span class="small-body-lucida-white">MCN</span></div>
                                                        <div class="inactive sb"><span class="small-body-lucida-white">SB</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="middle-col">

                                    <div class="main-screen-panel-inactive" id="location-portlet-disable" style="display: none"></div>

                                    <div class="main-screen-panel-active" id="location-portlet-enable">
                                        <div class="map-controls-div">
                                            <div class="map-controls-wrapper" >
                                                <div class="map-plus-button" ></div>
                                                <div class="map-minus-button" ></div>
                                            </div>
                                        </div>
                                        <div class="header">
                                            <div class="heading"><h4><span class="drop-shadow">Location</span></h4></div>
                                            <div class="pagination-expand-wrapper" style="display: none;">
                                                <ul class="pagination-buttons">
                                                    <li class="button"><button class="inactive"></button></li>
                                                    <li class="button"><button id="ll" class="inactive"></button></li>
                                                </ul>
                                                <div class="sep"></div>
                                                <div class="expand-button-wrapper"><button class="expand-button" id=""></button></div>
                                            </div> 
                                        </div>
                                        <div class="body" style="margin: 5px 5px 5px 5px;">
                                            <img id="mapImage" src="images/error_location.png" onerror="this.src='images/error_location.png';" />

                                        </div>
                                    </div>
                                </div>

                                <div class="right-col">
                                    <div class="main-screen-panel-inactive" id="cms-portlet-disable" style="display: none"></div>
                                    <div class="right-col" id="cms-portlet-enable">
                                        <div class="main-screen-panel-active">
                                            <div class="header">
                                                <div class="heading">
                                                    <h4><span class="drop-shadow">Complaint Management System</span></h4>
                                                </div>
                                                <div class="pagination-expand-wrapper">
                                                    <div class="pagination-buttons">
                                                        <div class="button">
                                                            <button class="active"></button>
                                                            <button class="inactive"></button>
                                                        </div>
                                                        <div class="sep"></div>
                                                        <div class="new-complaint-btn-wrapper">
                                                            <button class="new-complaint-btn"></button>
                                                        </div>
                                                    </div>
                                                    <div class="expand-button-wrapper">
                                                        <button class="expand-button" id="cms-expand-btn"></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body">
                                                <div class="cms-body-wrapper">
                                                    <div class="complaint-wrapper">
                                                        <div class="icon-wrapper">
                                                            <div class="data-icon"></div>
                                                        </div>
                                                        <div class="complaint-body">
                                                            <div class="header">
                                                                <div class="heading-wrapper">
                                                                    <span class="regular-body-lucida-white">GPRS Working Issues</span>
                                                                </div>
                                                                <div class="actions">
                                                                    <div class="time-wrapper">
                                                                        <span class="small-lucida-italic-yellow">02 hrs 30 mins</span>
                                                                    </div>
                                                                    <div class="buttons-wrapper" style="display: none;">
                                                                        <button class="undo-btn"></button>
                                                                        <button class="send-btn"></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="complaint-details">
                                                                <span class="smallest-body-lucida-light-grey">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In gravida nulla id magna consectetur in tincidunt enim lacinia.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="complaint-wrapper">
                                                        <div class="icon-wrapper">
                                                            <div class="call-icon"></div>
                                                        </div>
                                                        <div class="complaint-body">
                                                            <div class="header">
                                                                <div class="heading-wrapper">
                                                                    <span class="regular-body-lucida-white">No Dial Tone</span>
                                                                </div>
                                                                <div class="actions">
                                                                    <div class="time-wrapper">
                                                                        <span class="small-lucida-italic-yellow">02 hrs 30 mins</span>
                                                                    </div>
                                                                    <div class="buttons-wrapper" style="display: none;">
                                                                        <button class="undo-btn"></button>
                                                                        <button class="send-btn"></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="complaint-details">
                                                                <span class="smallest-body-lucida-light-grey">Etiam vel ipsum in mi lobortis eleifend at non augue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="complaint-wrapper">
                                                        <div class="icon-wrapper">
                                                            <div class="messages-icon"></div>
                                                        </div>
                                                        <div class="complaint-body">
                                                            <div class="header">
                                                                <div class="heading-wrapper">
                                                                    <span class="regular-body-lucida-white">Message Blocking Issue</span>
                                                                </div>
                                                                <div class="actions">
                                                                    <div class="time-wrapper">
                                                                        <span class="small-lucida-italic-yellow">02 hrs 30 mins</span>
                                                                    </div>
                                                                    <div class="buttons-wrapper" style="display: none;">
                                                                        <button class="undo-btn"></button>
                                                                        <button class="send-btn"></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="complaint-details">
                                                                <span class="smallest-body-lucida-light-grey">Quisque tempus elit et mauris tincidunt ac vulputate orci facilisis. Praesent non facilisis nunc.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="total-complaints-wrapper">
                                                        <div class="all-services-wrapper" style="display: none;">
                                                            <div class="icon-details-wrapper">
                                                                <div class="call-icon"></div>
                                                                <div class="details">
                                                                    <span>3</span>
                                                                </div>
                                                            </div>
                                                            <div class="icon-details-wrapper">
                                                                <div class="messages-icon"></div>
                                                                <div class="details">
                                                                    <span>6</span>
                                                                </div>
                                                            </div>
                                                            <div class="icon-details-wrapper">
                                                                <div class="data-icon"></div>
                                                                <div class="details">
                                                                    <span>12</span>
                                                                </div>
                                                            </div>
                                                            <div class="icon-details-wrapper" style="margin: 0">
                                                                <div class="settings-icon"></div>
                                                                <div class="details">
                                                                    <span>24</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="notification-bar-wrapper">
                        <div class="notification-bar">
                            <div class="bell-icon">
                                <img src="images/bell-icon.png" width="9" alt="" />
                            </div>
                            <div class="notification-label">
                                <marquee behavior="scroll" direction="left" style="color: #FFFFFF;font-size: 12px;font-weight: bold;" scrollamount="3"></marquee>
                                <span style="display: none;" class="small-10-body-lucida-white">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec magna sapien.
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="stats-bar-wrapper">
                        <div class="stats-sidebar-bg-wrapper">
                            <div class="chart-block-wrapper">
                                <div class="stats-dial-wrapper">
                                    <div class="stats-bar-dial-bottom">
                                        <div class="chart-wrapper">
                                            <div id="chart01"></div>
                                        </div>
                                        <div class="stats-bar-dial-top"></div>
                                    </div>
                                </div>
                                <div class="legend-wrapper">
                                    <div class="row">
                                        <div class="legend-marker-1"></div>
                                        <div class="legend-label"><span id="LIT_span" title="Logged in time" class="small-10-body-lucida-grey"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="legend-marker-2-wrapper">
                                            <div class="legend-marker-2"></div>
                                            <div class="legend-label"><span id="IT_span" title="Idle time" class="small-10-body-lucida-grey"></span></div>
                                        </div>
                                        <div class="legend-marker-3-wrapper">
                                            <div class="legend-marker-3"></div>
                                            <div class="legend-label"><span id="BT_span" title="Break time" class="small-10-body-lucida-grey"></span></div>
                                        </div>
                                        <div class="legend-marker-4-wrapper">
                                            <div class="legend-marker-4"></div>
                                            <div class="legend-label"><span id="NRT_span" title="Not ready time" class="small-10-body-lucida-grey"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chart-block-wrapper">
                                <div class="stats-dial-wrapper">
                                    <div class="stats-bar-dial-bottom">
                                        <div class="chart-wrapper">
                                            <div id="chart02"></div>
                                        </div>
                                        <div class="stats-bar-dial-top"></div>
                                    </div>
                                </div>
                                <div class="legend-wrapper">
                                    <div class="row">
                                        <div class="legend-marker-1"></div>
                                        <div class="legend-label"><span id="PD_span" title="Productive duration" class="small-10-body-lucida-grey"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="legend-marker-2-wrapper">
                                            <div class="legend-marker-2"></div>
                                            <div class="legend-label"><span id="ACW_span" title="Acw time" class="small-10-body-lucida-grey"></span></div>
                                        </div>
                                        <div class="legend-marker-3-wrapper">
                                            <div class="legend-marker-3"></div>
                                            <div class="legend-label"><span id="HT_span" title="Hold time" class="small-10-body-lucida-grey"></span></div>
                                        </div>
                                        <div class="legend-marker-4-wrapper">
                                            <div class="legend-marker-4"></div>
                                            <div class="legend-label"><span id="AVG_span" title="Avg. talk time" class="small-10-body-lucida-grey"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chart-block-wrapper-last">
                                <div class="stats-dial-wrapper">
                                    <div class="stats-bar-dial-bottom">
                                        <div class="chart-wrapper">
                                            <div id="chart03"></div>
                                        </div>
                                        <div class="stats-bar-dial-top"></div>
                                    </div>
                                </div>
                                <div class="legend-wrapper">
                                    <div class="row">
                                        <div class="legend-marker-1"></div>
                                        <div class="legend-label"><span id="CO_span" title="Calls offered" class="small-10-body-lucida-grey"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="legend-marker-2-wrapper">
                                            <div class="legend-marker-2"></div>
                                            <div class="legend-label"><span id="CA_span" title="Calls answered" class="small-10-body-lucida-grey"></span></div>
                                        </div>
                                        <div class="legend-marker-3-wrapper">
                                            <div class="legend-marker-3"></div>
                                            <div class="legend-label"><span id="CRQ_span" title="Call REQ" class="small-10-body-lucida-grey"></span></div>
                                        </div>
                                        <div class="legend-marker-4-wrapper">
                                            <div class="legend-marker-4"></div>
                                            <div class="legend-label"><span id="SC_span" title="Short calls" class="small-10-body-lucida-grey"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="telephony-bar-wrapper">
                        <div class="tele-sidebar-bg-wrapper">
                            <div class="logout-area-wrapper">
                                <div class="logout-btn-wrapper"><button id="btnLogout" title ="Logout" onclick="return false;" class="logout-btn"></button></div>
                                <div class="label" style="margin: 5px auto;position: relative;bottom: 0px;"><span class="regular-lucida">logout</span></div>
                                <div style="margin: 0px 20px; margin-top: 20px;">
                                    <div style="float:left"><span id="callCounterSpan" title="Call Duration" class="regular-lucida" style="color: white;">00:00:00</span></div>
                                    <div style="float:right"><span id="holdCounterSpan" title="Hold Duration" class="regular-lucida" style="color: #FFD200;display: none;">00:00:00</span></div>                            
                                </div>
                            </div>
                            <div class="single-row">
                                <div class="telebar-btn-wrapper">
                                    <button class="mute-btn" id="mute-btn" ></button>
                                    <div class="label"><span class="regular-lucida">mute</span></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="left-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button id="btnNRforNextCall" class="NR-for-next-call-btn"></button>
                                        <div class="label"><span class="regular-lucida">NR for next call</span></div>
                                    </div>
                                </div>
                                <div class="right-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button id="btnAnswer_Call" title="Answer" class="answer-btn"></button>
                                        <div class="label"><span class="regular-lucida">answer</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="left-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button id="btnHold" title="Hold" class="hold-btn"></button>
                                        <div class="label"><span class="regular-lucida">hold</span></div>
                                    </div>
                                </div>
                                <div class="right-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button id="btnReady" title="Ready" class="ready-btn"></button>
                                        <div class="label"><span class="regular-lucida">ready</span></div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="left-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button id="btnParentTransfer" class="transfer-btn"></button>
                                        <div class="label"><span class="regular-lucida">transfer</span></div>
                                    </div>
                                </div>
                                <div class="right-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button id="btnConsent_transfer" disabled="disabled" style="opacity: 0.1" class="internal-call-btn"></button>
                                        <div class="label"><span class="regular-lucida">consent transfer</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="left-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button class="break-btn" id="break-btn"></button>
                                        <div class="label"><span class="regular-lucida">break</span></div>
                                    </div>
                                </div>
                                <div class="right-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button id="btnFeedback" title="FeedBack" onclick="return false;" class="feedback-btn"></button>
                                        <div class="label"><span class="regular-lucida">feedback</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="left-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button id="btnEmDial" class="emergency-dialing-btn"></button>
                                        <div class="label"><span class="regular-lucida">emergency dialing</span></div>
                                    </div>
                                </div>
                                <div class="right-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button disabled="disabled" style="opacity: 0.1" class="dialing-extension-btn"></button>
                                        <div class="label"><span class="regular-lucida">dialing extension</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-last">
                                <div class="left-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button disabled="disabled" style="opacity: 0.1" class="dial-btn" id="dial-btn"></button>
                                        <div class="label"><span class="regular-lucida">dial</span></div>
                                    </div>
                                </div>
                                <div class="right-btn-wrapper">
                                    <div class="telebar-btn-wrapper">
                                        <button id="btnIVR_Transfer" title="IVR Transfer" onclick="return false;" class="transfer-ivr-btn" ></button>
                                        <div class="label"><span class="regular-lucida">transfer IVR</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="transfer-pop-out-wrapper">
                        <div class="transfer-pop-out">
                            <div id="btn_transfer_end" style="position: absolute; right: 0; width: 17px; height: 17px; background: transparent url(./images/remove-btn.png) no-repeat; border: 0; cursor: pointer;margin: 10px 25px 5px 5px; z-index: 1500;">
                            </div>
                            <div class="label"><span class="h0-lucida-white">Transfer</span></div>
                            <div class="text-field-transfer-bg"><input type="text" id="txtAgentDN" name="txtAgentDN" class="text-field-transfer"/></div>
                            <div class="buttons">
                                <!--
                                                                <div class="button-1"><button id="btnSingle_Step_Transfer" disabled="disabled" style="opacity: 0.1" class="single-step-btn">Single Step</button></div>
                                <div class="button-2"><button id="btnTwo_Step_Transfer" class="double-step-btn">Double Step</button></div>
                                <div class="clearfix"></div>
                                <div class="button-1"><button id="btnSupv_Transfer" disabled="disabled" style="opacity:0.1" class="supv-transfer-btn">SUPV Transfer</button></div>
                                <div class="button-2"><button id="btnInternalCallHangup" class="end-internal-call-btn">End Internal Call</button></div>
                                -->
                                <div class="button-1"><button id="btn_consult" disabled="disabled" style="opacity: 0.1" class="single-step-btn">Consult</button></div>
                                <div class="button-2"><button id="btn_transfer" class="double-step-btn">Transfer</button></div>
                                <div class="clearfix"></div>
                                <div class="button-1"><button id="btn_consult_end" disabled="disabled" style="opacity:0.1" class="end-internal-call-btn">End Consult</button></div>
                                <div class="button-2"><button id="btn_transfer_complete" class="end-internal-call-btn">Complete Transfer</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="break-pop-out-wrapper">
                        <div class="break-pop-out">
                            <div class="label"><span class="h0-lucida-white">Break</span></div>
                            <div class="dropdown-wrapper">
                                <select id="notReadyDropDownList" class="dropdown-large">

                                </select>
                            </div>
                            <div class="buttons">
                                <div class="button-1"><button id="btnBreak" class="confirm-btn">Confirm</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="dialer-pop-out-wrapper">
                        <div class="dialer-pop-out">
                            <div class="dialed-number-display-wrapper">
                                <div class="dialed-number">
                                    <input type="text" id="dialed-number" class="dialed-number-text-field" name="dialed-number" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="number"><button class="dial-number-btn">1</button></div>
                                <div class="number"><div class="dial-alphabets"><span class="regular-body-12">ABC</span></div><button class="dial-number-btn">2</button></div>
                                <div class="number-last"><div class="dial-alphabets"><span class="regular-body-12">DEF</span></div><button class="dial-number-btn">3</button></div>
                            </div>
                            <div class="row">
                                <div class="number"><div class="dial-alphabets"><span class="regular-body-12">GHI</span></div><button class="dial-number-btn">4</button></div>
                                <div class="number"><div class="dial-alphabets"><span class="regular-body-12">JKL</span></div><button class="dial-number-btn">5</button></div>
                                <div class="number-last"><div class="dial-alphabets"><span class="regular-body-12">MNO</span></div><button class="dial-number-btn">6</button></div>
                            </div>
                            <div class="row">
                                <div class="number"><div class="dial-alphabets"><span class="regular-body-12">PQRS</span></div><button class="dial-number-btn">7</button></div>
                                <div class="number"><div class="dial-alphabets"><span class="regular-body-12">TUV</span></div><button class="dial-number-btn">8</button></div>
                                <div class="number-last"><div class="dial-alphabets"><span class="regular-body-12">WXYZ</span></div><button class="dial-number-btn">9</button></div>
                            </div>
                            <div class="row">
                                <div class="number"><button class="dial-star-btn">*</button></div>
                                <div class="number"><div class="dial-alphabets"><span class="regular-body-12">+</span></div><button class="dial-number-btn">0</button></div>
                                <div class="number-last"><button class="dial-number-btn">#</button></div>
                            </div>
                            <div class="row-last">
                                <div class="dial-button"><button class="call-btn"></button></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="lightbox-bg-blackout" style="z-index: 0"></div>

                <!-- <div class="acw-lightbox">
                               <div class="header">
                                       <div class="header-gradient">
                                               <div class="heading"><span class="h0-lucida-yellow-withshadow">After Call Works Screen</span></div>
                                               <div class="window-controls">
                                                       <div class="search-wrapper">
                                                               <input type="text" name="search" id="search_acw" class="search-box" placeholder="Search" value="" onkeyup="acwSearchKeyPress(this, event)">
                                                       </div>
                                                       <div class="sep"><img src="images/lightbox-search-sep.png" height="27" width="2"></div>
                                                       <div class="title-bar-buttons-wrapper">
                                                               <div class="minimize-button-wrapper"><button class="minimize-button" id="minimize-acw-lightbox" style="z-index: 1500; "></button></div>
                                                               <div class="close-button-wrapper" style="float: right;"><button class="close-button" id="close-acw-lightbox" style="z-index: 1500; "></button></div>
                                                       </div>
                                               </div>
                                       </div>
                               </div>
                               <div class="body">
                                       <div class="radio-checkbox-panel-wrapper">
                                               <div id="freqACWDiv" class="radio-checkbox-panel">

                                               </div>
                                       </div>
                                       <div style="width: 724px;height: 14px;padding: 0px 10px 10px;margin: auto;">
                                               <table style="text-align: center;width: 100%;font-size: 1.5em;color: white;">
                                                       <tr>
                                                               <td><span>Category</span></td>
                                                               <td><span>Subcategory</span></td>
                                                               <td><span>Selected Workcode</span></td>
                                                       </tr>
                                               </table>
                                       </div>
                                       <div class="select-box-panels-wrapper">
                                               <div class="select-box-panel">
                                                       <select id="acwListHeadName" onclick="populate_acwListCodeName()" onchange="populate_acwListCodeName()" onkeydown="acwKeyPress(this, event)" class="select-box-generic" size="2">

                                                       </select>
                                               </div>
                                               <div class="select-box-panel">
                                                       <select id="acwListCodeName" onclick="populate_acwSelectedName()" onkeydown="acwKeyPress(this, event)" class="select-box-generic" size="2">

                                                       </select>
                                               </div>
                                               <div id="acwSelectedName" class="selected-options-panel-wrapper">

                                               </div>
                                               <div class="clearfix"></div>
                                               <div class="submit-btn-wrapper">
                                                       <button id="submitAWC_btn" class="submit-btn" style="z-index: 1500; ">Submit</button>
                                               </div>
                                       </div>
                               </div>
                       </div> -->

                <div class="complaint-management-lightbox">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span class="h0-lucida-yellow-withshadow">Complaint Management System</span>
                            </div>
                            <div class="window-controls">
                                <div class="search-wrapper">
                                    <input type="text" name="search" id="search-msisdn" class="search-box" placeholder="Search MSISDN" maxlength="12" />
                                </div>
                                <div class="search-wrapper">
                                    <input type="text" name="search" id="search-complaints" class="search-box" placeholder="Search Complaints" />
                                </div>
                                <div class="new-complaint-btn-wrapper">
                                    <button class="new-complaint-btn"></button>
                                </div>
                                <div class="sep">
                                    <img src="images/lightbox-search-sep.png" height="27" width="2" alt="" />
                                </div>
                                <div class="title-bar-buttons-wrapper">
                                    <div class="close-button-wrapper">
                                        <button class="close-button" id="close-cms-lightbox"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="panel-container">
                            <div class="cms-loader-left-div" >
                                <img src="images/cms-loading.gif" />
                            </div>
                            <div class="small-complaints-panel-wrapper">
                                <div class="heading-wrapper">
                                    <span class="regular-lucida-yellow-caps-14">Complaints</span>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="data-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="call-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="messages-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="data-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="call-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="call-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-summary-wrapper">
                                    <div class="summary-label">
                                        <span class="small-10-body-lucida-white">Summary of complaints</span>
                                    </div>
                                    <div class="summary-number-wrapper">
                                        <div class="summary-number">
                                            <span class="big-lucida-white">3</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cms-loader-center-div" >
                                <img src="images/cms-loading.gif" />
                            </div>

                            <div class="large-complaints-panel-wrapper">
                                <div class="history-wrapper">
                                    <div class="declineTicketButton" title="Decline Ticket">Decline</div>
                                    <div class="hotComplaintButton" title="Hot Complaint">Hot Complaint</div>
                                    <input class="cmsJsonData" type="hidden" value="" />
                                    <div class="scrollable-form-wrapper">
                                        <div class="work-code-name-wrapper">
                                            <div class="work-code-name">
                                                <span class="h0-lucida-yellow-18">Work Code Name</span>
                                            </div>
                                            <div class="date-wrapper">
                                                <span class="regular-lucida-yellow">May 15, 2014</span>
                                            </div>
                                            <div class="work-code-complaint-id-div" style="padding-top: 5px;color: #ffd200;"></div>

                                        </div>
                                        <div class="details-01-wrapper">
                                            <span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel dapibus diam...</span>
                                        </div>
                                        <div class="remarks-wrapper">
                                            <div class="heading">
                                                <span class="regular-lucida-yellow">Agent Remarks</span>
                                            </div>
                                            <div class="remarks-details-wrapper">
                                                <span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel dapibus diam.</span>
                                            </div>
                                        </div>
                                        <div class="status-wrapper">
                                            <div class="status">
                                                <div class="status-color"></div>
                                                <div class="status-label">
                                                    <span class="small-10-body-lucida-white">OPEN</span>
                                                </div>
                                            </div>
                                            <div class="legend">
                                                <img src="images/complaint-status-legend.png" width="225" alt="" />
                                            </div>
                                        </div>
                                        <div class="action-last-action-remarks-wrapper">
                                            <div class="action-remarks-wrapper">
                                                <div class="heading">
                                                    <span class="regular-lucida-yellow">Action Remarks</span>
                                                </div>
                                                <div class="details">
                                                    <span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel dapibus diam.</span>
                                                </div>
                                            </div>
                                            <div class="last-action-remarks-wrapper">
                                                <div class="heading">
                                                    <span class="regular-lucida-yellow">Last Action Remarks</span>
                                                </div>
                                                <div class="details">
                                                    <span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel dapibus diam.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="customer-complaint-details-wrapper">
                                            <div class="complaint-detail-label-wrapper">
                                                <span class="regular-lucida-yellow">Quality Status: </span>
                                                <span class="regular-lucida-bold">Valid Complaint</span>
                                            </div>
                                            <div class="complaint-detail-label-wrapper">
                                                <span class="regular-lucida-yellow">Complaint Log End Date: </span>
                                                <span class="regular-lucida-bold">November 05, 2013</span>
                                            </div>
                                            <div class="complaint-detail-label-wrapper">
                                                <span class="regular-lucida-yellow">Logged Status: </span>
                                                <span class="regular-lucida-bold">Pending</span>
                                            </div>
                                            <div class="complaint-detail-label-wrapper">
                                                <span class="regular-lucida-yellow">Logged by: </span>
                                                <span class="regular-lucida-bold">Kamran Shahid</span>
                                            </div>
                                            <div class="complaint-detail-label-wrapper">
                                                <span class="h2-lucida-bold-white">0333 5123456</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="complaints-list-wrapper" style="display: none;">
                                    <div class="list-left-wrapper">
                                        <div class="scrollable-form-wrapper">
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-right-wrapper">
                                        <div class="scrollable-form-wrapper">
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Lorem ipsum dolor sit amet</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nulla eu luctus ligula</span>
                                            </div>
                                            <div class="list-item"><span class="small-10-body-lucida-white">Nullam ante mauris</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="dynamic-form-wrapper" style="display: none;">
                                    <div class="scrollable-form-wrapper">
                                        <div class="history-wrapper-1">
                                            <div class="work-code-name-wrapper">
                                                <div class="work-code-name">
                                                    <span class="h0-lucida-yellow-18">Work Code Name</span>
                                                </div>
                                                <div class="date-wrapper">
                                                    <span class="regular-lucida-yellow">May 15, 2014</span>
                                                </div>
                                                <div class="work-code-script-div" style="padding-top: 5px;color: #ffd200;"></div>
                                                <div class="customer-email-div form-element-textfield-wrapper" style="color: #ffd200;margin: auto;margin-top: 8px;">
                                                    <input type="hidden" class="complaintWCFieldObj" value="" />
                                                    <input id="complaint_customer_email_0" type="text" value="" class="form-element-textfield" style="padding: 0px;" placeholder="Customer's Email" />
                                                </div>
                                            </div>
                                        </div>
                                        <!--
                                        <div class="customer-email-wrapper">
                                                <div class="customer-email-label-wrapper">
                                                        <div class="regular-lucida-yellow">Customer's Email</div>
                                                        <div class="regular-lucida-italic-grey">Enter if necessary</div>
                                                </div>
                                                <div class="customer-email-textfield-wrapper">
                                                        <input type="text" class="textfield" />
                                                </div>
                                        </div>
                                        <div class="complaint-desc-wrapper">
                                                <textarea></textarea>
                                        </div>
                                        -->
                                        <div class="complaint-form-elements-wrapper">
                                            <div class="form-element-wrapper">
                                                <div class="form-element-heading">
                                                    <span class="regular-lucida-yellow">MSISDN</span>
                                                </div>
                                                <div class="form-element-body">
                                                    <span class="regular-lucida-bold">0333 5123456</span>
                                                </div>
                                            </div>
                                            <div class="form-element-wrapper">
                                                <div class="form-element-heading">
                                                    <span class="regular-lucida-yellow">Nature of Complaint</span>
                                                </div>
                                                <div class="form-element-body">
                                                    <select class="dropdown-form-element">
                                                        <option>Live Interaction</option>
                                                        <option>Data not working</option>
                                                        <option>Message delayed</option>
                                                        <option>Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-element-wrapper">
                                                <div class="form-element-heading">
                                                    <span class="regular-lucida-yellow">Date</span>
                                                </div>
                                                <div class="form-element-body">
                                                    <div class="form-element-datepicker-wrapper">
                                                        <input type="text" class="form-element-datepicker">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-element-wrapper">
                                                <div class="form-element-heading">
                                                    <span class="regular-lucida-yellow">Radio Buttons</span>
                                                </div>
                                                <div class="form-element-body">
                                                    <div class="form-element-radio">
                                                        <input id="option_1" type="radio" name="option" value="option 1">
                                                        <label for="option_1">Option 1</label>
                                                        <input id="option_2" type="radio" name="option" value="option 2">
                                                        <label for="option_2">Option 2</label>
                                                        <input id="option_3" type="radio" name="option" value="option 3">
                                                        <label for="option_3">Option 3</label>
                                                        <input id="option_4" type="radio" name="option" value="option 4">
                                                        <label for="option_4">Option 4</label>
                                                        <input id="option_5" type="radio" name="option" value="option 5">
                                                        <label for="option_5">Option 5</label>
                                                        <input id="option_6" type="radio" name="option" value="option 6">
                                                        <label for="option_6">Option 6</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-element-wrapper">
                                                <div class="form-element-heading">
                                                    <span class="regular-lucida-yellow">Radio Buttons</span>
                                                </div>
                                                <div class="form-element-body">
                                                    <div class="form-element-radio">
                                                        <input id="option_1" type="radio" name="option" value="option 1">
                                                        <label for="option_1">Option 1</label>
                                                        <input id="option_2" type="radio" name="option" value="option 2">
                                                        <label for="option_2">Option 2</label>
                                                        <input id="option_3" type="radio" name="option" value="option 3">
                                                        <label for="option_3">Option 3</label>
                                                        <input id="option_4" type="radio" name="option" value="option 4">
                                                        <label for="option_4">Option 4</label>
                                                        <input id="option_5" type="radio" name="option" value="option 5">
                                                        <label for="option_5">Option 5</label>
                                                        <input id="option_6" type="radio" name="option" value="option 6">
                                                        <label for="option_6">Option 6</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-element-wrapper">
                                                <div class="form-element-heading">
                                                    <span class="regular-lucida-yellow">Textfield</span>
                                                </div>
                                                <div class="form-element-body">
                                                    <div class="form-element-textfield-wrapper">
                                                        <input type="text" class="form-element-textfield" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-element-wrapper">
                                                <div class="form-element-heading">
                                                    <span class="regular-lucida-yellow">Checkboxes</span>
                                                </div>
                                                <div class="form-element-body">
                                                    <div class="form-element-checkbox">
                                                        <input id="check_1" type="checkbox" name="check" value="Check 1">
                                                        <label for="check_1">Check 1</label>
                                                        <input id="check_2" type="checkbox" name="check" value="Check 2">
                                                        <label for="check_2">Check 2</label>
                                                        <input id="check_3" type="checkbox" name="check" value="Check 3">
                                                        <label for="check_3">Check 3</label>
                                                        <input id="check_4" type="checkbox" name="check" value="Check 4">
                                                        <label for="check_4">Check 4</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-btns-wrapper">
                                        <button class="green-btn" style="z-index: 0;">Ok</button>
                                        <button class="grey-btn" style="z-index: 0;">Cancel</button>
                                    </div>
                                </div>
                            </div>
                            <div class="cms-loader-right-div" >
                                <img src="images/cms-loading.gif" />
                            </div>
                            <div class="small-complaints-panel-wrapper">
                                <div class="heading-wrapper">
                                    <span class="regular-lucida-yellow-caps-14">Closed Complaints</span>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="data-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="call-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="messages-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="data-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="call-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-wrapper">
                                    <div class="icon-wrapper">
                                        <div class="call-icon"></div>
                                    </div>
                                    <div class="complaint-details-wrapper">
                                        <div class="complaint-heading">
                                            <span class="regular-body-lucida-white">3G Working Issues</span>
                                        </div>
                                        <div class="complaint-time">
                                            <span class="small-lucida-italic-yellow">2hrs 35mins</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="cms-complaint-summary-wrapper">
                                    <div class="summary-label">
                                        <span class="small-10-body-lucida-white">Summary of closed complaints</span>
                                    </div>
                                    <div class="summary-number-wrapper">
                                        <div class="summary-number">
                                            <span class="big-lucida-white">3</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="acw-lightbox">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span class="h0-lucida-yellow-withshadow">After Call Works Screen</span></div>
                            <div class="window-controls">
                                <div class="form-element-body" style="float: left;margin-right: 10px;margin-top: 3px;">
                                    <select id="acw-seg-selector" class="dropdown-acw-element" style="width: 175px;">
                                        <option value="both" selected="selected">All</option>
                                        <option value="pre-paid">Pre-paid</option>
                                        <option value="post-paid">Post-paid</option>
                                    </select>
                                </div>
                                <div class="search-wrapper">
                                    <input type="text" name="search" id="search_acw" class="search-box" placeholder="Search" value="" onkeyup="acwSearchKeyPress(this, event)">
                                </div>
                                <div class="sep"><img src="images/lightbox-search-sep.png" height="27" width="2"></div>
                                <div class="title-bar-buttons-wrapper">
                                    <div class="minimize-button-wrapper"><button class="minimize-button" id="minimize-acw-lightbox" style="z-index: 1500; "></button></div>
                                    <div class="close-button-wrapper" style="float: right;"><button class="close-button" id="close-acw-lightbox" style="z-index: 1500; "></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="radio-checkbox-panel-wrapper">
                            <div id="freqACWDiv" class="radio-checkbox-panel">

                            </div>
                        </div>
                        <div style="width: 924px;height: 14px;padding: 0px 10px 10px;margin: auto;">
                            <table style="text-align: center;width: 100%;font-size: 1.5em;color: white;">
                                <tr>
                                    <td><span>Category</span></td>
                                    <td><span>Subcategory</span></td>
                                    <td><span>Selected Workcode</span></td>
                                    <td><span>Workcode Description</span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="select-box-panels-wrapper">
                            <div class="select-box-panel">
                                <select id="acwListHeadName" onclick="populate_acwListCodeName()" onchange="populate_acwListCodeName()" onkeydown="acwKeyPress(this, event)" class="select-box-generic" size="2">

                                </select>
                            </div>
                            <div class="select-box-panel">
                                <select id="acwListCodeName" onclick="populate_acwSelectedName()" onkeydown="acwKeyPress(this, event)" class="select-box-generic" size="2">

                                </select>
                            </div>
                            <div class="dragging">
                                <div id="acwSelectedName" style="margin: 0px 0px 5px 0px;" class="selected-options-panel-wrapper drag"></div>
                                <div style="font-weight: bold;color: #FFFFFF;margin: 0px 0px 5px 0px;">
                                    Online Resolution:
                                </div>
                                <div id="acwSelectedName1" class="selected-options-panel-wrapper dropable" style="margin-bottom: 0;"></div>
                            </div>
                            <div class="select-box-panel">
                                <div id="acwDesc" class="select-box-generic" style="width: 160px;height: 253px;word-wrap: break-word;">

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="submit-btn-wrapper">
                                <button id="submitAWC_btn" class="submit-btn" style="z-index: 1500; ">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="transfer-ivr-lightbox">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span class="h0-lucida-yellow-withshadow">Transfer IVR</span></div>
                            <div class="window-controls">
                                <div class="search-wrapper">
                                    <input type="text" name="search" id="search_ivr" class="search-box" placeholder="Search" value="" onkeyup="ivrSearchKeyPress(this, event)" />
                                </div>
                                <div class="sep"><img src="images/lightbox-search-sep.png" height="27" width="2" /></div>
                                <div class="title-bar-buttons-wrapper">
                                    <div class="close-button-wrapper"><button class="close-button" id="close-ivr-lightbox"></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="radio-checkbox-panel-wrapper">
                            <div id="freqIVRDiv" class="radio-checkbox-panel">

                            </div>
                        </div>
                        <div style="width: 724px;height: 14px;padding: 0px 10px 10px;margin: auto;">
                            <table style="text-align: center;width: 80%;font-size: 1.5em;color: white;">
                                <tr>
                                    <td><span>Category</span></td>
                                    <td><span>Subcategory</span></td>
                                    <td><span>IVR Menu</span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="select-box-panels-wrapper">
                            <div class="select-box-panel">
                                <select id="IVRTransCat" onclick="getIVRSubCategory()" onchange="getIVRSubCategory()" onkeydown="ivrKeyPress(this, event)" class="select-box-generic" size="2">

                                </select>
                            </div>
                            <div class="select-box-panel">
                                <select id="IVRTransSubCat" onclick="getIVRMenuCode()" onchange="getIVRMenuCode()" onkeydown="ivrKeyPress(this, event)" class="select-box-generic" size="2">

                                </select>
                            </div>
                            <div id="IVRTransMenu" >

                            </div>
                        </div>
                    </div>
                </div>

                <div class="consent-lightbox">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span class="h0-lucida-yellow-withshadow">Consent Transfer</span></div>
                            <div class="window-controls">
                                <div class="sep"><img src="images/lightbox-search-sep.png" height="27" width="2" /></div>
                                <div class="title-bar-buttons-wrapper">
                                    <div class="close-button-wrapper"><button class="close-button" id="close-consent-lightbox"></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">

                        <div id="divConsentLanguage" style="width:320px;margin:auto;">
                            <div style="margin-top: 50px;text-align: center;">
                                <select id="dropdownlanguage" style="width:135px">

                                </select>
                            </div>

                            <br />

                            <div >
                                <div class="submit-btn-wrapper">
                                    <button id="btnConsentDoTransfer" class="submit-btn" style="z-index: 1500; ">Transfer</button>
                                </div>
                                <div class="submit-btn-wrapper">
                                    <button id="btnCancelConsentTransfer" class="submit-btn" style="z-index: 1500; ">Cancel</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="customer-demo-lightbox">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span class="h0-lucida-yellow-withshadow">Customer Demographics</span></div>
                            <div class="window-controls">
                                <div class="title-bar-buttons-wrapper">
                                    <div class="close-button-wrapper"><button class="close-button" id="close-customer-demo-lightbox"></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="panel-bg">
                            <div id="divCustomerName_bg" class="customer-name-wrapper"></div>
                            <div id="divCustomerNumber_bg" class="customer-number-wrapper"></div>
                            <div id="divCustomerRatingStars_bg" class="customer-rating-wrapper">

                            </div>
                            <div><hr class="sep" noshade /></div>
                            <div id="divCustomerUserName" class="customer-username-wrapper"></div>
                            <div id="divPackageName_bg" class="customer-generic-margin"></div>
                            <div id="divWithUfone_bg" class="customer-generic-margin"></div>
                            <div id="divBirthday_bg" class="customer-generic-margin"></div>
                            <div id="div_cli_anis_bg" class="customer-generic-margin"></div>
                            <div id="div_punchedin_subnumber_bg" class="customer-generic-margin"></div>
                            <div id="divAddress" class="customer-17-top-margin"></div>
                        </div>
                        <div class="panel-bg">
                            <div id="divCustomerMotherName" class="customer-17-top-margin"></div>
                            <div id="divCustomerFatherName" class="customer-generic-margin"></div>
                            <div id="divCustomerCNIC" class="customer-generic-margin"></div>
                            <div id="divCustomerServiceType" class="customer-generic-margin"></div>
                            <div id="divSpecialInst" class="customer-17-top-margin"></div>
                        </div>
                        <div class="panel-bg">
                            <div id="divCustomerIVRlang" class="customer-17-top-margin"></div>
                            <div id="divCustomerPUK" class="customer-generic-margin"></div>
                            <div id="divCustomerStatus" class="customer-generic-margin"></div>
                            <div class="cellphones-wrapper">
                                <div class="current-cellphone-wrapper">
                                    <div class="cellphone-image"><img src="images/handsets/iphone-3g.png" width="37" height="70" /></div>
                                    <div class="label-wrapper">
                                        <div class="current-label"><span class="regular-body-12">Current</span></div>
                                        <div id="divCurrentHandset_bg" class="cellphone-name-label"></div>
                                    </div>
                                </div>
                                <div class="previous-cellphone-wrapper">
                                    <div class="cellphone-image"><img src="images/handsets/iphone-3g.png" width="37" height="70" /></div>
                                    <div class="label-wrapper">
                                        <div class="current-label"><span class="regular-body-12">Previous</span></div>
                                        <div id="divPreviousHandset_bg" class="cellphone-name-label"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profit-sanc-lightbox">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span class="h0-lucida-yellow-withshadow">Bill Details</span></div>
                            <div class="window-controls">
                                <div class="title-bar-buttons-wrapper">
                                    <div class="close-button-wrapper"><button class="close-button" id="close-profit-sanc-lightbox"></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="panel-bg">
                            <div class="left-col">
                                <div id="divSancTotalPayable" class="customer-generic-margin"></div>
                                <div id="divSancAverageBill" class="customer-generic-margin"></div>
                                <div id="divSancCustomerBalance" class="customer-generic-margin"></div>
                            </div>
                            <div class="right-col">
                                <div id="divUnBilledAmnt" class="customer-generic-margin"></div>
                                <div id="divUsedLimit" class="customer-generic-margin"></div>
                                <div id="divTotalLimit" class="customer-generic-margin"></div>
                            </div>
                        </div>
                        <div class="panel-bg">
                            <div class="left-col">
                                <div id="divSancCustomerDeposit" class="customer-generic-margin"></div>
                            </div>
                            <div class="right-col">
                                <div id="divSancLastPaidAmmnt" class="customer-generic-margin"></div>
                            </div>
                        </div>
                        <div class="panel-bg">
                            <div class="left-col">
                                <div id="divSancWHT" class="customer-generic-margin"></div>
                                <div id="divSancGST" class="customer-generic-margin"></div>
                            </div>
                            <div class="right-col">
                                <div id="divSancLineRent" class="customer-generic-margin"></div>
                                <div id="divSancLastPaymentdate" class="customer-generic-margin"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="offers-lightbox">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span class="h0-lucida-yellow-withshadow">Offers</span></div>
                            <div class="window-controls">
                                <div class="title-bar-buttons-wrapper">
                                    <div class="close-button-wrapper"><button class="close-button" id="close-offers-lightbox"></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="offers-wrapper">
                            <div class="primary-offer">
                                <div class="header">
                                    <span class="regular-lucida-yellow-14">Offer Heading</span>
                                </div>
                                <div class="search-wrapper">

                                </div>
                                <div class="desc-wrapper">
                                    <span class="regular-lucida-9">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in nunc nec dolor fermentum congue. Curabitur justo diam, porta eu mollis ut, luctus nec urna. Curabitur justo diam, porta eu mollis ut, luctus nec urna.
                                    </span>
                                </div>
                            </div>
                            <div class="second-offer">
                                <div class="header">
                                    <span class="regular-lucida-yellow-14">Offer Heading</span>
                                </div>
                                <div class="search-wrapper">
                                    <div class="search-input-bg">
                                        <div class="magniglass">
                                            <img src="images/offer-search-magniglass.png" width="12" height="11" alt="Search"/>
                                        </div>
                                        <input type="text" name="search_offer" id="search_offer" class="search-offer-input" />
                                    </div>
                                </div>
                                <div class="desc-wrapper">
                                    <span class="regular-lucida-9">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in nunc nec dolor fermentum congue. Curabitur justo diam, porta eu mollis ut, luctus nec urna. Curabitur justo diam, porta eu mollis ut, luctus nec urna.
                                    </span>
                                </div>
                            </div>
                            <div class="third-offer">
                                <div class="header">
                                    <span class="regular-lucida-yellow-14">Offer Heading</span>
                                </div>
                                <div class="search-wrapper">
                                    <div class="search-input-bg">
                                        <div class="magniglass">
                                            <img src="images/offer-search-magniglass.png" width="12" height="11" alt="Search"/>
                                        </div>
                                        <input type="text" name="search_offer" id="search_offer" class="search-offer-input" />
                                    </div>
                                </div>
                                <div class="desc-wrapper">
                                    <span class="regular-lucida-9">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in nunc nec dolor fermentum congue. Curabitur justo diam, porta eu mollis ut, luctus nec urna. Curabitur justo diam, porta eu mollis ut, luctus nec urna.
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="offers-values-wrapper">
                            <div class="row-light-bg">
                                <div class="offers-icon-expanded">
                                    <img src="images/offers-voice-icon-expanded.png" width="116" height="65" alt="Voice"/>
                                </div>
                                <div class="offer-values">
                                    <div class="col">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                    <div class="col">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                    <div class="col-last">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                </div>
                                <div class="offers-options-wrapper">
                                    <div class="services-wrapper">
                                        <div class="active" id="cli"><span class="small-body-lucida-white">CLI</span></div>
                                        <div class="active" id="waiting"><span class="small-body-lucida-white">Waiting</span></div>
                                        <div class="active" id="forward"><span class="small-body-lucida-white">Forward</span></div>
                                        <div class="active" id="restrict"><span class="small-body-lucida-white">Restrict</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-dark-bg">
                                <div class="offers-icon-expanded">
                                    <img src="images/offers-messaging-icon-expanded.png" width="116" height="65" alt="Messaging"/>
                                </div>
                                <div class="offer-values">
                                    <div class="col">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                    <div class="col">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                    <div class="col-last">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                </div>
                                <div class="offers-options-wrapper">
                                    <div class="services-wrapper">
                                        <div class="active"><span class="small-body-lucida-white">WAP</span></div>
                                        <div class="inactive"><span class="small-body-lucida-white">GPRS</span></div>
                                        <div class="active"><span class="small-body-lucida-white">Umail</span></div>
                                        <div class="inactive"><span class="small-body-lucida-white">Fax</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-light-bg">
                                <div class="offers-icon-expanded">
                                    <img src="images/offers-data-icon-expanded.png" width="116" height="65" alt="Voice"/>
                                </div>
                                <div class="offer-values">
                                    <div class="col">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                    <div class="col">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                    <div class="col-last">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                </div>
                                <div class="offers-options-wrapper">
                                    <div class="services-wrapper">
                                        <div class="active"><span class="small-body-lucida-white">SMS</span></div>
                                        <div class="inactive"><span class="small-body-lucida-white">MMS</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-dark-bg">
                                <div class="offers-icon-expanded">
                                    <img src="images/offers-others-icon-expanded.png" width="116" height="65" alt="Voice"/>
                                </div>
                                <div class="offer-values">
                                    <div class="col">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                    <div class="col">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                    <div class="col-last">
                                        <span class="h0-lucida-yellow-18">Rs. 2,008</span>
                                    </div>
                                </div>
                                <div class="offers-options-wrapper">
                                    <div class="services-wrapper">
                                        <div class="active"><span class="small-body-lucida-white">Info</span></div>
                                        <div class="active"><span class="small-body-lucida-white">CBRT</span></div>
                                        <div class="active"><span class="small-body-lucida-white">MCN</span></div>
                                        <div class="active"><span class="small-body-lucida-white">SB</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="package-details-lightbox">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span class="h0-lucida-yellow-withshadow">Package Details</span></div>
                            <div class="window-controls">
                                <div class="title-bar-buttons-wrapper">
                                    <div class="close-button-wrapper"><button class="close-button" id="close-package-details-lightbox"></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="panel-bg">
                            <div class="package-details-body-wrapper">
                                <div class="row-wrapper">
                                    <div class="dropdown-wrapper">
                                        <select id="voiceDropDown_bg" class="dropdown" disabled="disabled">

                                        </select>
                                        <div class="voice_icn"></div>
                                    </div>
                                    <div class="services-wrapper">
                                        <div class="inactive cli"><span class="small-body-lucida-white">CLI</span></div>
                                        <div class="inactive waiting"><span class="small-body-lucida-white">Waiting</span></div>
                                        <div class="inactive forward"><span class="small-body-lucida-white">Forward</span></div>
                                        <div class="inactive restrict"><span class="small-body-lucida-white">Restrict</span></div>
                                        <div class="inactive conference"><span class="small-body-lucida-white">Conference</span></div>
                                        <div class="inactive vmail"><span class="small-body-lucida-white">Voice Mail</span></div>
                                    </div>
                                </div>
                                <div class="row-wrapper">
                                    <div class="dropdown-wrapper">
                                        <select id="dataDropDown_bg" class="dropdown" disabled="disabled">

                                        </select>
                                        <div class="data_icn"></div>
                                    </div>
                                    <div class="services-wrapper">
                                        <div class="inactive wap"><span class="small-body-lucida-white">WAP</span></div>
                                        <div class="inactive gprs"><span class="small-body-lucida-white">GPRS</span></div>
                                        <div class="inactive umail"><span class="small-body-lucida-white">Umail</span></div>
                                        <div class="inactive fax"><span class="small-body-lucida-white">Fax</span></div>
                                        <div class="inactive bb"><span class="small-body-lucida-white">BB</span></div>
                                    </div>
                                </div>
                                <div class="row-wrapper">
                                    <div class="dropdown-wrapper">
                                        <select id="smsDropDown_bg" class="dropdown" disabled="disabled">

                                        </select>
                                        <div class="message_icn"></div>
                                    </div>
                                    <div class="services-wrapper">
                                        <div class="inactive sms"><span class="small-body-lucida-white">SMS</span></div>
                                        <div class="inactive mms"><span class="small-body-lucida-white">MMS</span></div>
                                        <div class="inactive dr"><span class="small-body-lucida-white">DR</span></div>
                                    </div>
                                </div>
                                <div class="row-wrapper">
                                    <div class="dropdown-wrapper">
                                        <select id="othersDropDown_bg" class="dropdown" disabled="disabled">

                                        </select>
                                        <div class="others_icn"></div>
                                    </div>
                                    <div class="services-wrapper">
                                        <div class="inactive info"><span class="small-body-lucida-white">Info</span></div>
                                        <div class="inactive cbrt"><span class="small-body-lucida-white">CBRT</span></div>
                                        <div class="inactive mcn"><span class="small-body-lucida-white">MCN</span></div>
                                        <div class="inactive sb"><span class="small-body-lucida-white">SB</span></div>
                                        <div class="inactive vb"><span class="small-body-lucida-white">VB</span></div>
                                        <div class="inactive utrack"><span class="small-body-lucida-white">UTrack</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="latest-activities-lightbox">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span class="h0-lucida-yellow-withshadow">Latest Activities</span></div>
                            <div class="window-controls">
                                <div class="title-bar-buttons-wrapper">
                                    <div class="close-button-wrapper"><button class="close-button" id="close-latest-activities-lightbox"></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="panel-bg">
                            <!-- <div class="last-acitivty-body-wrapper">
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="active-gprs-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">21 Mb</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="inactive-message-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">+92 300 540 9876</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="active-call-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">+92 333 789 6753</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="active-message-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">+92 300 540 9876</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="inactive-message-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">+92 333 789 6753</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="inactive-message-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">+92 333 789 6753</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="inactive-message-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">+92 333 789 6753</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="inactive-message-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">+92 333 789 6753</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="inactive-message-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">+92 333 789 6753</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                                    <div class="activity-row">
                                            <div class="icon"><div class="inactive-message-icon"></div></div>
                                            <div class="desc"><span class="regular-lucida">+92 333 789 6753</span></div>
                                            <div class="date"><span class="small-lucida-italic">01/24/01</span></div>
                                            <div class="time"><span class="small-lucida-italic">1430 hrs</span></div>
                                    </div>
                                    <div class="sep"></div>
                            </div>
                            -->

                            <div id="divAgentHistory_bg" class="last-acitivty-body-wrapper">

                            </div>

                        </div>
                    </div>
                </div>
                <div class="splash-screen-lightbox" onKeyPress="keyPressSplashScreen(event)" tabindex="0">
                    <div class="body">
                        <div class="content-container" >
                            <div id="splashName" class="name"><span class="h0-arial-yellow"></span></div>
                            <div id="splashNumber" class="number"><span class="h1-arial-white"></span></div>
                            <div id="splashPackageDetails" class="last-activity"><span class="h1-arial-white"></span></div>
                        </div>
                    </div>
                    <div style="text-align:center;margin-bottom: 11px;">
                        <div>
                            <span class="h0-arial-yellow" style="font-size: 24px;">DNIS:</span>
                            <span id="DNISDescriptionSpan" class="h1-arial-white"></span>
                        </div>
                        <div >
                            <span class="h0-arial-yellow" style="font-size: 24px;">CDN:</span>
                            <span id="CDNDescriptionSpan" class="h1-arial-white"></span>
                        </div>
                        <div >
                            <span class="h0-arial-yellow" style="font-size: 24px;">Caller CLI:</span>
                            <span id="_cli_anis_Span" class="h1-arial-white"></span>
                        </div>
                        <div >
                            <span class="h0-arial-yellow" style="font-size: 24px;">Punched In Number:</span>
                            <span id="_punchedin_subnumber_Span" class="h1-arial-white"></span>
                        </div>
                    </div>
                </div>

                <div class="break-timer-lightbox" onkeydown="$('.break-timer-lightbox').mousedown();
                        $('#txtBrkPwd').focus();" tabindex="0">
                    <div class="header">
                        <div class="header-gradient">
                            <div class="heading"><span id="spanBreakDesc" class="h0-lucida-yellow-withshadow"></span></div>
                            <div class="window-controls">
                                <div class="title-bar-buttons-wrapper">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div class="break-timer-body-wrapper">
                            <div class="break-timer-counter-wrapper"><span id="spanBreakTimer" class="drop-shadow" style="font-family: 'Lucida Grande', Lucida, Verdana, sans-serif; font-size: 350%; color: white;"></span></div>
                            <div class="break-timer-password-field-wrapper">
                                <div class="break-timer-password-field-bg"><input class="break-timer-password-field" type="password" name="break-timer-password-field" id="txtBrkPwd" onkeypress="keyPressBreak(this, event);" maxlength="20" /></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="exception-lightbox">
                    <div class="body">
                        <div class="exception-body-wrapper">
                            <div class="exception-icon-wrapper">
                                <div class="exception-icon"></div>
                            </div>
                            <div class="exception-label-wrapper">
                                <span class="regular-lucida"></span>
                            </div>
                            <div class="exception-btns-wrapper">
                                <button class="exception-generic-btn" id="exception-ok">Ok</button>
                                <button class="exception-generic-btn" id="exception-cancel">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="footer_mainscreen">
                    <div class="footer-content-wrapper">
                        <div class="terms-policy-links">
                            <div class="terms-link"><a href="#" class="footer-links">Terms of Use</a></div>
                            <div><a href="#" class="footer-links">Privacy Policy</a></div>
                        </div>
                        <div class="widget-bar-wrapper">
                            <div class="widget-bar-bg">
                                <div class="left-widgets-wrapper">
                                    <div class="rightside-btn-wrapper" style="display: none;">
                                        <div class="agent-info-container">
                                            <div class="agent-popup-sop-wrapper" style="text-align: left;">
                                                <div class="agent-popup-body">
                                                    <div class="heading"><span class="regular-lucida-bold-yellow-10">SOP Search</span>
                                                    </div>
                                                    <div class="all-details">
                                                        <input class="sop-search-field" type="text" value="" />
                                                        <div class="sop-pop-div">															

                                                        </div>
                                                        <!--
                                                        <ul>
                                                                <li>
                                                                        <div class="heading"><span class="small-10-body-lucida-white">DNIS</span>
                                                                        </div>
                                                                        <div class="details"><span class="small-10-body-lucida-grey">Lorem Ipsum</span>
                                                                        </div>
                                                                </li>
                                                                <li>
                                                                        <div class="heading"><span class="small-10-body-lucida-white">CDN Description</span>
                                                                        </div>
                                                                        <div class="details"><span class="small-10-body-lucida-grey">Lorem Ipsum</span>
                                                                        </div>
                                                                </li>
                                                                <li>
                                                                        <div class="heading"><span class="small-10-body-lucida-white">Agent Name</span>
                                                                        </div>
                                                                        <div class="details"><span class="small-10-body-lucida-grey">Lorem Ipsum</span>
                                                                        </div>
                                                                </li>
                                                                <li>
                                                                        <div class="heading"><span class="small-10-body-lucida-white">Agent Skillset</span>
                                                                        </div>
                                                                        <div class="details"><span class="small-10-body-lucida-grey">Lorem Ipsum</span>
                                                                        </div>
                                                                </li>
                                                        </ul>
                                                        -->
                                                    </div>
                                                </div>
                                                <div class="agent-popup-arrow"></div>
                                            </div>
                                            <button style="background: url(./images/notepad-selector-btn.png) no-repeat 0 center;" class="agent-info-ready" id="agent-sop-btn"></button>
                                        </div>
                                    </div>
                                    <button class="cares-btn" id="cares-btn"></button>
                                </div>
                                <div class="client-logo" ><img style="margin-top: 10px;" src="images/ufone-logo.png" width="64" height="22" alt="" /></div>
                                <div class="right-widgets-wrapper">
                                    <div class="rightside-btn-wrapper">
                                        <button class="acw-btn" id="acw-btn"></button>
                                    </div>
                                    <div class="rightside-btn-wrapper">
                                        <div class="agent-info-container">
                                            <div class="agent-popup-wrapper">
                                                <div class="agent-popup-body">
                                                    <div class="heading"><span class="regular-lucida-bold-yellow-10">Agent Info</span></div>
                                                    <div class="all-details">
                                                        <ul>
                                                            <li style="width: 217px;">
                                                                <div class="heading"><span class="small-10-body-lucida-white">DNIS Desc</span></div>
                                                                <div class="details"><span id="DNISDescriptionDiv" class="small-10-body-lucida-grey"></span></div>

                                                            </li>
                                                            <li style="width: 217px;">
                                                                <div class="heading"><span class="small-10-body-lucida-white">CDN Desc</span></div>
                                                                <div class="details"><span id="CDNDescriptionDiv" class="small-10-body-lucida-grey"></span></div>

                                                            </li>
                                                            <li style="width: 217px;">
                                                                <div class="heading"><span class="small-10-body-lucida-white">Agent ID</span></div>
                                                                <div class="details"><span id="agentIDSpan" class="small-10-body-lucida-grey"></span></div>

                                                            </li>
                                                            <li style="display: none;">
                                                                <div class="heading"><span class="small-10-body-lucida-white">Agent Skillset</span></div>
                                                                <div class="details"><span class="small-10-body-lucida-grey"></span></div>

                                                            </li>
                                                            <li style="width: 217px;">
                                                                <div class="heading"><span class="small-10-body-lucida-white">Stats Time</span></div>
                                                                <div class="details"><span id="statsTimeStampSpan" class="small-10-body-lucida-grey"></span></div>

                                                            </li>
                                                            <li style="width: 217px;">
                                                                <div class="heading"><span class="small-10-body-lucida-white">First Login</span></div>
                                                                <div class="details"><span id="firstLoginTimeStampSpan" class="small-10-body-lucida-grey"></span></div>

                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="agent-popup-arrow"></div>
                                            </div>
                                            <button class="agent-info-not-ready" id="agent-info-btn"></button>
                                        </div>
                                    </div>
                                    <div class="rightside-btn-wrapper">
                                        <div class="theme-selector-container">
                                            <div class="theme-selector-wrapper">
                                                <div class="theme-selector-popup-body">
                                                    <div class="heading"><span class="regular-lucida-bold-yellow-10">Themes</span></div>
                                                    <div class="all-details">
                                                        <div class="dropdown-wrapper">
                                                            <select class="dropdown-without-icon" id="theme-selector-dropdown">
                                                                <option>Carbon Grey</option>
                                                                <option>Eminent Blue</option>
                                                                <option>Windows Aqua</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="theme-selector-popup-arrow"></div>
                                            </div>
                                            <button class="theme-selector-btn" id="theme-selector-btn"></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="designed-by-footer"><span class="regular-body" id="designed-by">Designed by</span> <span class="regular-body-light-grey" id="apollo-telecom">APOLLO TELECOM</span></div>
                    </div>
                </div>

            </div>

            <a id="anchrBtn_brkDiv" style="display:none"></a>

            <!--
            <a id="anchrBtn_internalCallDiv" class="fancybox" href="#internalCallDiv" style="display:none"> </a>

            <div id="internalCallDiv" style="display:none;width:300px;font-size: 16px;font-weight:bold;">
                <input type="button" id="btnInternalCallAnswer" value="Answer Internal Call" />
                <br/>
                <input type="button" id="btnInternalCallHangUp2" value="Hangup Internal Call" />
                <br/>
            </div>

            <a id="anchrBtn_chngpwd" class="fancybox" href="#chngpwdDiv" style="display:none"> </a>
            -->

            <div id="chngpwdDiv" style="display:none;width:300px;">
                <span style="font-size: 16px;font-weight:bold;">Change your Password</span>
                <table>
                    <tr>
                        <td>User Name: </td>
                        <td><input type="text" id="chngpwd_username" /></td>
                    </tr>
                    <tr>
                        <td>Old Password: </td>
                        <td><input type="password" id="chngpwd_oldpwd" /></td>
                    </tr>
                    <tr>
                        <td>New Password </td>
                        <td><input type="password" id="chngpwd_newpwd" /></td>
                    </tr>
                </table>
                <input id="btnChangePwd_Confirm" type="button" value="Change Password" />
                <input id="btnChangePwd_Cancel" type="button" value="Cancel" />
            </div>



            <!--
                        
            <input type="hidden" id="txtConnect" value="ws://<%=request.getRemoteAddr()%>:9999" />

            <input type="hidden" id="hdnIP" value="<%=request.getRemoteAddr()%>" />
                        
                        
            <!--

<input type="hidden" id="txtConnect" value="ws://172.16.26.31:9999" />

<input type="hidden" id="hdnIP" value="172.16.26.31" />
            -->
            <!--  -->

            <div id="divLogin" style="margin:auto;" >
                <div id="wrap">
                    <div id="main">
                        <div id="content-index">
                            <div id="content-wrapper">
                                <div class="logo-wrapper">
                                    <div class="logo-hr"><img src="images/logo-hr.png" width="524" height="3" alt="" /></div>
                                    <div class="hashcube-logo-black"><a href="javascript:void(0);" id="open-login-area"><img src="images/hashcube-logo-black.png" width="123" height="122" alt=""/></a></div>
                                    <div class="hashcube-logo-yellow"><img src="images/hashcube-logo-yellow.png" width="167" height="168" alt=""/></div>
                                </div>
                                <div class="login-area-wrapper">
                                    <div class="hash-cube-text"><img src="images/hash-cube-text.png" width="162" height="25" alt="" /></div>
                                    <div class="hash-cube-tagline"><span class="regular-body">Smart Synformation</span></div>
                                    <div class="tagline-hr"><img src="images/tagline-hr.png" width="76" height="2" alt="" /></div>
                                    <div class="login-fields">
                                        <div class="username"><input name="txtUsername" id="txtUsername" placeholder="User Name" class="text-field" type="text" onKeyPress="hashLogIn(event)" maxlength="20" /></div>
                                        <div class="password"><input name="txtUserpass" id="txtUserpass" placeholder="Password" class="text-field" type="password" onKeyPress="hashLogIn(event)" maxlength="20" /></div>
                                    </div>
                                    <div class="keep-signed-in-wrapper">
                                        <div class="keep-signed-in-check" style="display: none;"><a href="#" class="keep-signed-in">Keep me signed in</a></div>
                                        <div class="keep-signed-in-text" style="display: none;"><div class="regular-body-12">Keep me signed in</div></div>
                                        <div class="forgot-password" style="text-align: center;display: none"><a href="mailto:admin@hashcube.com" class="forgot-password-link">Forgot Password</a></div>
                                    </div>
                                    <div class="keep-signed-in-hr"><img src="images/keep-signed-in-hr.png" width="236" height="2" alt="" /></div>
                                    <div class="contact-admin" style="display: none"><a href="mailto:admin@hashcube.com" class="contact-admin-link">Contact the administrator</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="footer_loginscreen">
                    <div class="footer-content-wrapper">
                        <div class="terms-policy-links">
                            <div class="terms-link"><a href="#" class="footer-links">Terms of Use</a></div>
                            <div><a href="#" class="footer-links">Privacy Policy</a></div>
                        </div>
                        <div class="ufone-logo-footer" ><img src="images/ufone-logo.png" width="64" height="22" alt="" /></div>
                        <div class="designed-by-footer"><span class="regular-body">Designed by</span> <span class="regular-body-light-grey">APOLLO TELECOM</span></div>
                    </div>
                </div>

            </div>

            <div id="divStatsScreen" style="margin:auto;display: none;">

                <div id="wrap">
                    <div id="header">
                        <div class="logo">
                            <div class="logo-image"><img src="images/hashcube-logo-header.png" width="247" height="83" alt="" /></div>
                            <div class="tagline"><span class="regular-body-light-grey">Smart Synformation</span></div>
                        </div>
                        <div class="logged-in-wrapper">
                            <div class="logged-in-name"><h3></h3></div>
                            <div class="date"><span class="regular-body-italic"></span></div>
                        </div>
                    </div>
                    <div id="main">
                        <div id="content-stats">
                            <div class="stats-panel">
                                <div class="long-timeline-wrapper">
                                    <div class="label"><h2>Logged in Time</h2></div>
                                    <div class="base-track">
                                        <div class="progress-track-1-1"></div>
                                        <div class="min-threshold-1-1"><span class="regular-body-white" id="min-threshold-1-1">0</span></div>
                                        <div class="current-status-1-1"><span class="regular-body-white" id="current-status-1-1">0</span></div>
                                        <div class="max-value-1-1"><span id="loggedInTimeLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                        <div class="min-threshold-value-1-1"><span id="loggedInTimeSLASpan" class="small-body-lucida"></span></div>
                                    </div>
                                </div>
                                <div class="small-timelines-wrapper">
                                    <div class="small-timeline-container">
                                        <div class="label"><h3>Idle Time</h3></div>
                                        <div class="base-track">
                                            <div class="progress-track-1-2"></div>
                                            <div class="min-threshold-1-2"><span class="regular-body-white" id="min-threshold-1-2">0</span></div>
                                            <div class="current-status-1-2"><span class="regular-body-white" id="current-status-1-2">0</span></div>
                                            <div class="max-value-small-timeline"><span id="idleTimeLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                            <div class="min-threshold-value-small-timeline"><span id="idleTimeSLASpan" class="small-body-lucida"></span></div>
                                        </div>
                                    </div>
                                    <div class="small-timeline-container">
                                        <div class="label"><h3>Break Time</h3></div>
                                        <div class="base-track">
                                            <div class="progress-track-1-3"></div>
                                            <div class="min-threshold-1-3"><span class="regular-body-white" id="min-threshold-1-3">0</span></div>
                                            <div class="current-status-1-3"><span class="regular-body-white" id="current-status-1-3">0</span></div>
                                            <div class="max-value-small-timeline"><span id="breakTimeLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                            <div class="min-threshold-value-small-timeline"><span id="breakTimeSLASpan" class="small-body-lucida"></span></div>
                                        </div>
                                    </div>
                                    <div class="small-timeline-container-last">
                                        <div class="label"><h3>Not Ready Time</h3></div>
                                        <div class="base-track">
                                            <div class="progress-track-1-4"></div>
                                            <div class="min-threshold-1-4"><span class="regular-body-white" id="min-threshold-1-4">0</span></div>
                                            <div class="current-status-1-4"><span class="regular-body-white" id="current-status-1-4">0</span></div>
                                            <div class="max-value-small-timeline"><span id="notReadyTimeLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                            <div class="min-threshold-value-small-timeline"><span id="notReadyTimeSLASpan" class="small-body-lucida"></span></div>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="stats-panel">
                                <div class="long-timeline-wrapper">
                                    <div class="label"><h2>Productive Duration</h2></div>
                                    <div class="base-track">
                                        <div class="progress-track-2-1"></div>
                                        <div class="min-threshold-2-1"><span class="regular-body-white" id="min-threshold-2-1">0</span></div>
                                        <div class="current-status-2-1"><span class="regular-body-white" id="current-status-2-1">0</span></div>
                                        <div class="max-value-2-1"><span id="productiveDurationLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                        <div class="min-threshold-value-2-1"><span id="productiveDurationSLASpan" class="small-body-lucida"></span></div>
                                    </div>
                                </div>
                                <div class="small-timelines-wrapper">
                                    <div class="small-timeline-container">
                                        <div class="label"><h3>ACW Time</h3></div>
                                        <div class="base-track">
                                            <div class="progress-track-2-2"></div>
                                            <div class="min-threshold-2-2"><span class="regular-body-white" id="min-threshold-2-2">0</span></div>
                                            <div class="current-status-2-2"><span class="regular-body-white" id="current-status-2-2">0</span></div>
                                            <div class="max-value-small-timeline"><span id="acwTimeLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                            <div class="min-threshold-value-small-timeline"><span id="acwTimeSLASpan" class="small-body-lucida"></span></div>
                                        </div>
                                    </div>
                                    <div class="small-timeline-container">
                                        <div class="label"><h3>Hold Time</h3></div>
                                        <div class="base-track">
                                            <div class="progress-track-2-3"></div>
                                            <div class="min-threshold-2-3"><span class="regular-body-white" id="min-threshold-2-3">0</span></div>
                                            <div class="current-status-2-3"><span class="regular-body-white" id="current-status-2-3">0</span></div>
                                            <div class="max-value-small-timeline"><span id="holdTimeLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                            <div class="min-threshold-value-small-timeline"><span id="holdTimeSLASpan" class="small-body-lucida"></span></div>
                                        </div>
                                    </div>
                                    <div class="small-timeline-container-last">
                                        <div class="label"><h3>Avg. Talk Time</h3></div>
                                        <div class="base-track">
                                            <div class="progress-track-2-4"></div>
                                            <div class="min-threshold-2-4"><span class="regular-body-white" id="min-threshold-2-4">0</span></div>
                                            <div class="current-status-2-4"><span class="regular-body-white" id="current-status-2-4">0</span></div>
                                            <div class="max-value-small-timeline"><span id="avgTalkTimeLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                            <div class="min-threshold-value-small-timeline"><span id="avgTalkTimeSLASpan" class="small-body-lucida"></span></div>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="stats-panel">
                                <div class="long-timeline-wrapper">
                                    <div class="label"><h2>Calls Offered</h2></div>
                                    <div class="base-track">
                                        <div class="progress-track-3-1"></div>
                                        <div class="min-threshold-3-1"><span class="regular-body-white" id="min-threshold-3-1">0</span></div>
                                        <div class="current-status-3-1"><span class="regular-body-white" id="current-status-3-1">0</span></div>
                                        <div class="max-value-3-1"><span id="callsOfferedLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                        <div class="min-threshold-value-3-1"><span id="callsOfferedSLASpan" class="small-body-lucida"></span></div>
                                    </div>
                                </div>
                                <div class="small-timelines-wrapper">
                                    <div class="small-timeline-container">
                                        <div class="label"><h3>Calls Answered</h3></div>
                                        <div class="base-track">
                                            <div class="progress-track-3-2"></div>
                                            <div class="min-threshold-3-2"><span class="regular-body-white" id="min-threshold-3-2">0</span></div>
                                            <div class="current-status-3-2"><span class="regular-body-white" id="current-status-3-2">0</span></div>
                                            <div class="max-value-small-timeline"><span id="callsAnsweredLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                            <div class="min-threshold-value-small-timeline"><span id="callsAnsweredSLASpan" class="small-body-lucida"></span></div>
                                        </div>
                                    </div>
                                    <div class="small-timeline-container">
                                        <div class="label"><h3>Calls REQ</h3></div>
                                        <div class="base-track">
                                            <div class="progress-track-3-3"></div>
                                            <div class="min-threshold-3-3"><span class="regular-body-white" id="min-threshold-3-3">0</span></div>
                                            <div class="current-status-3-3"><span class="regular-body-white" id="current-status-3-3">0</span></div>
                                            <div class="max-value-small-timeline"><span id="callsREQLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                            <div class="min-threshold-value-small-timeline"><span id="callsREQSLASpan" class="small-body-lucida"></span></div>
                                        </div>
                                    </div>
                                    <div class="small-timeline-container-last">
                                        <div class="label"><h3>Short Calls</h3></div>
                                        <div class="base-track">
                                            <div class="progress-track-3-4"></div>
                                            <div class="min-threshold-3-4"><span class="regular-body-white" id="min-threshold-3-4">0</span></div>
                                            <div class="current-status-3-4"><span class="regular-body-white" id="current-status-3-4">0</span></div>
                                            <div class="max-value-small-timeline"><span id="shortCallsLimitSpan" style="visibility: hidden;" class="small-body-lucida"></span></div>
                                            <div class="min-threshold-value-small-timeline"><span id="shortCallsSLASpan" class="small-body-lucida"></span></div>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="local-time-wrapper">
                                <div class="local-time-base-track">
                                    <div class="time-progress-track-wrapper">
                                        <div class="head"></div>
                                        <div class="time-progress-track"></div>
                                        <div class="tail"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="hour-label-wrapper">
                                    <div class="small-body-lucida" id="hour-label-first">12 A.M</div>
                                    <div class="small-body-lucida" id="hour-label-41">1</div>
                                    <div class="small-body-lucida" id="hour-label-39">2</div>
                                    <div class="small-body-lucida" id="hour-label-40">3</div>
                                    <div class="small-body-lucida" id="hour-label-39">4</div>
                                    <div class="small-body-lucida" id="hour-label-40">5</div>
                                    <div class="small-body-lucida" id="hour-label-39">6</div>
                                    <div class="small-body-lucida" id="hour-label-39">7</div>
                                    <div class="small-body-lucida" id="hour-label-39">8</div>
                                    <div class="small-body-lucida" id="hour-label-40">9</div>
                                    <div class="small-body-lucida" id="hour-label-34">10</div>
                                    <div class="small-body-lucida" id="hour-label-34">11</div>
                                    <div class="small-body-lucida" id="hour-label-12pm">12 P.M</div>
                                    <div class="small-body-lucida" id="hour-label-39">1</div>
                                    <div class="small-body-lucida" id="hour-label-40">2</div>
                                    <div class="small-body-lucida" id="hour-label-39">3</div>
                                    <div class="small-body-lucida" id="hour-label-40">4</div>
                                    <div class="small-body-lucida" id="hour-label-39">5</div>
                                    <div class="small-body-lucida" id="hour-label-40">6</div>
                                    <div class="small-body-lucida" id="hour-label-39">7</div>
                                    <div class="small-body-lucida" id="hour-label-39">8</div>
                                    <div class="small-body-lucida" id="hour-label-39">9</div>
                                    <div class="small-body-lucida" id="hour-label-34">10</div>
                                    <div class="small-body-lucida" id="hour-label-5">11</div>
                                    <div class="small-body-lucida" id="hour-label-last">12 A.M</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="footer_loginscreen">
                    <div class="footer-content-wrapper">
                        <div class="terms-policy-links">
                            <div class="terms-link"><a href="#" class="footer-links">Terms of Use</a></div>
                            <div><a href="#" class="footer-links">Privacy Policy</a></div>
                        </div>
                        <div class="ufone-logo-footer" ><img style="display:none;" src="images/ufone-logo.png" width="64" height="22" alt="" /></div>
                        <div class="designed-by-footer"><span class="regular-body">Designed by</span> <span class="regular-body-light-grey">APOLLO TELECOM</span></div>
                    </div>
                </div>
            </div>

        </div>

        <script type="text/javascript">
            (function ($) {
                $(window).load(function () {
                    $(".scrollable-form-wrapper").mCustomScrollbar({
                        advanced: {
                            updateOnBrowserResize: true,
                            updateOnContentResize: true
                        }
                    });
                });
            })(jQuery);
        </script>

        <div class="footerDiv" >
            <%IndexPageClass indexPageObj = new IndexPageClass();%>
            <%=indexPageObj.generateButtonStatesJS(request)%>
            <%indexPageObj = null;%>
        </div>

    </body>
</html>
