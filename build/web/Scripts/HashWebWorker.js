
var wsClient = null;
	
workerConnect = function(server){
	try {
		wsClient =  new WebSocket(server);
		//wsClient =  new WebSocket("ws://localhost:9999");
			
		//the events
		wsClient.onopen = workerOnOpen;
		wsClient.onmessage = workerOnMessage;
		wsClient.onclose = workerOnClose;
		wsClient.onerror = workerOnError;
			
	} catch (exception) {		
		postMessage({
			eventName:'Exception', 
			ex:exception
		});
			
	}
};
	
workerSend = function(data){
	wsClient.send(data);
};
	
workerClose = function(){
	try {
		wsClient.close();
			
	} catch (exception) {
		postMessage({
			eventName:'Exception', 
			ex:exception
		});
	}
};
	
workerOnOpen = function(){
	postMessage({
		eventName:'OnOpen'
	});
};
	
workerOnMessage = function(fromserver) {
	postMessage({
		eventName:'OnMessage', 
		data:fromserver.data
	});
};
	
workerOnClose = function(){
	postMessage({
		eventName:'OnClose'
	});
};
	
workerOnError = function() {
	postMessage({
		eventName:'OnError'
	});
};
	
onmessage = function(e) {
		
	var eventName = e.data.eventName;
	if(eventName === 'connect'){
		workerConnect(e.data.serverName);
	}
	else if(eventName === 'close'){
		workerClose();
	}
	else{
		workerSend(e.data);
	}
		
};