var isPIAWCEnabled = false;
var isPIAAgent = false;
var isFwblAgent = false;
var fwblCallGui = "";
var agentSkillGroup = "";
var isUnileverWCEnabled = false;
var isUnileverAgent = false;
var ajaxPage = null;
var LBServers = new Array();
var serverID = -1;
var LBMutex = false;
var LBServers_Unica = new Array();
var serverID_Unica = -1;
var LBMutex_Unica = false;
var LBServers_LocalDB = new Array();
var serverID_LocalDB = -1;
var LBMutex_LocalDB = false;
var LBServers_Cares = new Array();
var serverID_Cares = -1;
var LBMutex_Cares = false;
var ThirdPartyLBServers = new Array();
var ThirdPartyserverID = -1;
var ThirdPartyLBMutex = false;
var isUnicaStartSessionCalled = false;
var isUnicaEndSessionCalled = false;
var location_url = 'http://172.16.105.60:9991/LocationImages/images/';
var news_alert_timer = 15;
var _isUnicaDown = false;
var btn_consult_pressed = false;
var btn_transfer_pressed = false;
var btn_consult_end_pressed = false;
var btn_transfer_complete_pressed = false;

//customer conscent method 
function setCustomerConsent(caller_cli) {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL_Cares();
    //server ki value check krni h k atri ha k ni 
    WebLogs('WS:Going to call setCustomerConsent, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'number=' + '' + caller_cli + ']', 0);
    WebLogs('WS:Url called:'+ server + '' + ajaxPage + '?function=getCustomerConsent' + '&caller_cli=' + caller_cli+ '', 0);

    $.ajax({
//       url: server + '' + ajaxPage + '?function=setCustomerConsent' + '&caller_cli=' + '' + caller_cli,
//        url: server + '' + ajaxPage + '?function=getCustomerConsent' + '&caller_cli=' + Number(caller_cli),
        url: server + '' + ajaxPage + '?function=getCustomerConsent' + '&caller_cli=' + caller_cli,
        //pass this caller cli as number
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:setCustomerConsent, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'number=' + '' + caller_cli + '], Response: customerConsent' + data, 0);
            //From backend we are receiving only consent value
            if (data) {
                //data is in string, convert it to an integer
                clientobj.setCustomerEngagementPreference(Number(data));
            } else {
                ///if data is not received properly
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("setCustomerConsent -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("setCustomerConsent -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("setCustomerConsent -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("setCustomerConsent -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });

}

function getServerURL() {
    if (LBServers.length >= 1) {
        if (LBServers.length == 1) {
            return LBServers[0];
        } else {
            //while(LBMutex)
            {

            }
            LBMutex = true;
            var LBSlength = LBServers.length;
            if (serverID == -1) {
                serverID = Math.floor(Math.random() * (LBSlength - 1) + 1);
                serverID = serverID - 1;
            } else {
                serverID = (serverID + 1) % LBSlength;
            }
            LBMutex = false;
            return LBServers[serverID];
        }
    } else {
        return './';
    }
}

function getServerURL_Unica() {
    if (LBServers_Unica.length >= 1) {
        if (LBServers_Unica.length == 1) {
            return LBServers_Unica[0];
        } else {
            //while(LBMutex)
            {

            }
            LBMutex_Unica = true;
            var LBSlength = LBServers_Unica.length;
            if (serverID_Unica == -1) {
                serverID_Unica = Math.floor(Math.random() * (LBSlength - 1) + 1);
                serverID_Unica = serverID_Unica - 1;
            } else {
                serverID_Unica = (serverID_Unica + 1) % LBSlength;
            }
            LBMutex_Unica = false;
            return LBServers_Unica[serverID_Unica];
        }
    } else {
        return './';
    }
}

function getServerURL_LocalDB() {
    if (LBServers_LocalDB.length >= 1) {
        if (LBServers_LocalDB.length == 1) {
            return LBServers_LocalDB[0];
        } else {
//while(LBMutex)
            {

            }
            LBMutex_LocalDB = true;
            var LBSlength = LBServers_LocalDB.length;
            if (serverID_LocalDB == -1) {
                serverID_LocalDB = Math.floor(Math.random() * (LBSlength - 1) + 1);
                serverID_LocalDB = serverID_LocalDB - 1;
            } else {
                serverID_LocalDB = (serverID_LocalDB + 1) % LBSlength;
            }
            LBMutex_LocalDB = false;
            return LBServers_LocalDB[serverID_LocalDB];
        }
    } else {
        return './';
    }
}
function getServerURL_Cares() {
    if (LBServers_Cares.length >= 1) {
        if (LBServers_Cares.length == 1) {
            return LBServers_Cares[0];
        } else {
//while(LBMutex)
            {

            }
            LBMutex_Cares = true;
            var LBSlength = LBServers_Cares.length;
            if (serverID_Cares == -1) {
                serverID_Cares = Math.floor(Math.random() * (LBSlength - 1) + 1);
                serverID_Cares = serverID_Cares - 1;
            } else {
                serverID_Cares = (serverID_Cares + 1) % LBSlength;
            }
            LBMutex_Cares = false;
            return LBServers_Cares[serverID_Cares];
        }
    } else {
        return './';
    }
}

function getThirdPartyServerURL() {
    if (ThirdPartyLBServers.length >= 1) {
        if (ThirdPartyLBServers.length == 1) {
            return ThirdPartyLBServers[0];
        } else {
//while(LBMutex)
            {

            }
            ThirdPartyLBMutex = true;
            var ThirdPartyLBSlength = ThirdPartyLBServers.length;
            if (ThirdPartyserverID == -1) {
                ThirdPartyserverID = Math.floor(Math.random() * (ThirdPartyLBSlength - 1) + 1);
                ThirdPartyserverID = ThirdPartyserverID - 1;
            } else {
                ThirdPartyserverID = (ThirdPartyserverID + 1) % ThirdPartyLBSlength;
            }
            ThirdPartyLBMutex = false;
            return ThirdPartyLBServers[ThirdPartyserverID];
        }
    } else {
        return './';
    }
}

function caresUserLogOn() {
    var info = clientObj.getSystemInfo();
    var infoArray = new Array();
    infoArray = info.split('|');
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    //if(true)
    if (clientObj.isCaresDown())
    {
        Notifier.error('ERROR: Cares is currently down for maintainence');
        clientObj.setCaresLoggedIn(false);
        clientObj.Login(clientObj.getUsername(), clientObj.getCommonPwdCCT());
        $('#txtUsername').val('');
        $('#txtUserpass').val('');
        $('#txtUsername').removeAttr('readonly');
        $('#txtUserpass').removeAttr('readonly');
        $('#txtUsername').focus();
        hashloginmutex = false;
        var server = getServerURL();
        WebLogs('WS:Going to call caresUserLogOn, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'user_code=' + clientObj.getUsername() + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=caresUserLogOn' + '&user_code=' + clientObj.getUsername() + '&user_password=' + clientObj.getPassword() + '&user_ip=' + infoArray[1] + '&user_host=' + infoArray[3] + '&user_mac=' + infoArray[2] + '&os_user=' + clientObj.getUsername() + '&ipAddr=' + clientObj.getIP() + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID(),
            success: function (data) {
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("caresUserLogOn -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresUserLogOn -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("caresUserLogOn -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresUserLogOn -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    } else {
        var server = getServerURL();
        WebLogs('WS:Going to call caresUserLogOn, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'user_code=' + clientObj.getUsername() + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=caresUserLogOn' + '&user_code=' + clientObj.getUsername() + '&user_password=' + clientObj.getPassword() + '&user_ip=' + infoArray[1] + '&user_host=' + infoArray[3] + '&user_mac=' + infoArray[2] + '&os_user=' + clientObj.getUsername() + '&ipAddr=' + clientObj.getIP() + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID(),
            success: function (data) {
                data = $.trim(data);
                data = data.split(',');
                clientObj.setCaresUserLogOnString(data[1]);
                var date_now = new Date();
                WebLogs('WS:caresUserLogOn, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + "." + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'user_code=' + clientObj.getUsername() + '], Response:' + data, 0);
                //alert(clientObj.isCaresDown());
                if (data[0] == "T") {
                    clientObj.setCaresLoggedIn(true);
                    clientObj.setCaresSessionID(data[1].split('|')[0]);
                    clientObj.Login(clientObj.getUsername(), clientObj.getCommonPwdCCT());
                } else if (data[0] == "LDAP bypassed") {
                    clientObj.setCaresLoggedIn(true);
                    clientObj.setCaresSessionID(data[1].split('|')[0]);
                    clientObj.Login(clientObj.getUsername(), clientObj.getCommonPwdCCT());
                } else {
                    clientObj.setCaresLoggedIn(false);
                    if (data[1] == "timeout")
                    {
                        Notifier.error('ERROR: Cares Login request timed out');
                        clientObj.Login(clientObj.getUsername(), clientObj.getCommonPwdCCT());
                    } else {
                        Notifier.error('Please Enter Your Correct Credentials for Authentication');
                        hashloginmutex = false;
                    }
                    $('#txtUsername').val('');
                    $('#txtUserpass').val('');
                    $('#txtUsername').removeAttr('readonly');
                    $('#txtUserpass').removeAttr('readonly');
                    $('#txtUsername').focus();
                }
                //hashloginmutex = false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("caresUserLogOn -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresUserLogOn -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("caresUserLogOn -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresUserLogOn -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
}

function caresUserLogOut(logout_flag) {
//if (clientObj.isCaresLoggedIn()) 
    {
        var date_now = new Date();
        var a_sync = true;
        if (logout_flag == '2') {
            a_sync = false;
        }
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        var server = getServerURL();
        WebLogs('WS:Going to call caresUserLogOut, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'user_code=' + clientObj.getUsername() + ', logout_flag=' + logout_flag + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=caresUserLogOut' + '&user_code=' + clientObj.getUsername() + '&logout_flag=' + logout_flag + '&ipAddr=' + clientObj.getIP() + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID(),
            async: a_sync,
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:caresUserLogOut, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'user_code=' + clientObj.getUsername() + ', logout_flag=' + logout_flag + '], Response:' + data, 0);
                data = data.split(',');
                if (data[0] == "T") {
                    //clientObj.Login($.trim($('#txtUsername').val()), $.trim($('#txtUserpass').val()));
                } else {
//alert("Cares login failed!!!");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("caresUserLogOut -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresUserLogOut -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("caresUserLogOut -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresUserLogOut -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
}

function caresPostCallAdjustment(caller_cid) {
//if(clientObj.isCaresLoggedIn())
    {
        var attData = (clientObj.getAttachedData());
        var user_id = clientObj.getAgentID();
        var customerInfo = null;
        var callid = null;
        var clid = null;
        var calltype = null;
        var details = null;
        var rsl = null;
        var rdesc = null;
        var caresstatus = null;
        var caredt = null;
        var insertiondt = null;
        var switchid = null;
        var dnis = null;
        var mobno = null;
        var abal = null;
        var custtype = null;
        var prodtype = null;
        var amtcharged = null;
        var agentid = null;
        var retries = null;
        var lastretry = null;
        {
            customerInfo = clientObj.getCaresCustomerInfo();
            if (clientObj.getNDFCall()) {

                callid = clientObj.getCallSessionID();
                calltype = clientObj.getCallType();
                caresstatus = null;
                caredt = null;
                insertiondt = new Date().getTime();
                switchid = clientObj.getSwitchID();
                dnis = clientObj.getDNIS();
                mobno = caller_cid;
                if ($.isArray(customerInfo)) {
                    abal = customerInfo[17];
                    custtype = customerInfo[3];
                    prodtype = customerInfo[10];
                } else {
                    abal = null;
                    custtype = null;
                    prodtype = null;
                }
                amtcharged = clientObj.getChargingRate();
                agentid = clientObj.getAgentID();
                retries = "0";
                details = 101;
                lastretry = null;
                var date_now = new Date();
                var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//				var server = getThirdPartyServerURL();
                var server = getServerURL_Cares();
                WebLogs('WS:Going to call caresPostCallAdjustment, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'user_msisdn= ' + '' + caller_cid + ', call_id=' + callid + ']', 0);
                $.ajax({
                    type: "POST",
                    url: server + '' + ajaxPage + '?function=caresPostCallAdjustment',
                    data: {
                        user_msisdn: caller_cid,
                        agent_detail: user_id,
                        ipAddr: clientObj.getIP(),
                        _ndfStatus: true,
                        _details: details,
                        _callid: callid,
                        _calltype: calltype,
                        _caresstatus: caresstatus,
                        _caredt: caredt,
                        _insertiondt: insertiondt,
                        _switchid: switchid,
                        _dnis: dnis,
                        _mobno: mobno,
                        _abal: abal,
                        _custtype: custtype,
                        _prodtype: prodtype,
                        _amtcharged: amtcharged,
                        _agentid: agentid,
                        _retries: retries,
                        _lastretry: lastretry,
                        datetime: new Date().getTime(),
                        agent_id: clientObj.getUsername(),
                        amt_charged: clientObj.getChargingRate(),
                        conn_id: clientObj.getCallSessionID()

                    },
                    success: function (data) {
                        data = $.trim(data);
                        var date_now = new Date();
                        WebLogs('WS:caresPostCallAdjustment, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'user_msisdn= ' + '' + caller_cid + ', call_id=' + callid + '], Response:' + data, 0);
                        var isSuperCardUserResult = data.split(",");
                        var isSuperCardUser = isSuperCardUserResult[3].split(":")[1];
                        if (isSuperCardUser === 'N') {
                            $("#isSuperCardUser").text("No");
                            $("#superCardOnMinutes").text("N/A");
                        } else {
                            $("#isSuperCardUser").text("Yes");
                            const minutesInHour = 60;
                            var superCardUserOnMinutes = parseInt(isSuperCardUserResult[4].split(":")[1]) / minutesInHour;
                            $("#superCardOnMinutes").text(superCardUserOnMinutes);
                        }
                        if (data[0] == "T") {

                        } else {
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (textStatus == "timeout") {
                            console.log("caresPostCallAdjustment -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                            WebLogs("caresPostCallAdjustment -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                            //resetUnicaCards();
                        } else {
                            console.log("caresPostCallAdjustment -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                            WebLogs("caresPostCallAdjustment -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                        }
                    }
                });
            } else {
                callid = clientObj.getCallSessionID();
                calltype = clientObj.getCallType();
                caresstatus = null;
                caredt = null;
                insertiondt = new Date().getTime();
                switchid = clientObj.getSwitchID();
                dnis = clientObj.getDNIS();
                mobno = caller_cid;
                abal = null;
                switch (clientObj.getIpPhoneType()) {
                    case "Nortel":
                        abal = attData[11];
                        custtype = attData[3];
                        prodtype = attData[9];
                        break;
                    case "Cisco_LHR":
                        if ($.isArray(customerInfo)) {
                            abal = customerInfo[17];
                        }
                        custtype = attData[8];
                        prodtype = attData[3];
                        break;
                    case "Cisco_ISB":
                        if ($.isArray(customerInfo)) {
                            abal = customerInfo[17];
                        }
                        custtype = attData[8];
                        prodtype = attData[3];
                        break;
                    case "Cisco_KHI":
                        if ($.isArray(customerInfo)) {
                            abal = customerInfo[17];
                        }
                        custtype = attData[8];
                        prodtype = attData[3];
                        break;
                }
                amtcharged = clientObj.getChargingRate();
                agentid = clientObj.getAgentID();
                retries = "0";
                lastretry = null;
                details = 101;
                var date_now = new Date();
                var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//				var server = getThirdPartyServerURL();
                var server = getServerURL_Cares();
                WebLogs('WS:Going to call caresPostCallAdjustment, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'user_msisdn= ' + '' + caller_cid + ', call_id=' + callid + ']', 0);
                $.ajax({
                    type: "POST",
                    url: server + '' + ajaxPage + '?function=caresPostCallAdjustment',
                    data: {
                        user_msisdn: caller_cid,
                        agent_detail: user_id,
                        ipAddr: clientObj.getIP(),
                        _ndfStatus: false,
                        _details: details,
                        _callid: callid,
                        _calltype: calltype,
                        _caresstatus: caresstatus,
                        _caredt: caredt,
                        _insertiondt: insertiondt,
                        _switchid: switchid,
                        _dnis: dnis,
                        _mobno: mobno,
                        _abal: abal,
                        _custtype: custtype,
                        _prodtype: prodtype,
                        _amtcharged: amtcharged,
                        _agentid: agentid,
                        _retries: retries,
                        _lastretry: lastretry,
                        datetime: new Date().getTime(),
                        agent_id: clientObj.getUsername(),
                        amt_charged: clientObj.getChargingRate(),
                        conn_id: clientObj.getCallSessionID()

                    }, success: function (data) {
                        data = $.trim(data);
                        var date_now = new Date();
                        WebLogs('WS:caresPostCallAdjustment, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'user_msisdn= ' + '' + caller_cid + ', call_id=' + callid + '], Response:' + data, 0);
                        var isSuperCardUserResult = data.split(",");
                        var isSuperCardUser = isSuperCardUserResult[3].split(":")[1];
                        if (isSuperCardUser === 'N') {
                            $("#isSuperCardUser").text("No");
                            $("#superCardOnMinutes").text("N/A");
                        } else {
                            $("#isSuperCardUser").text("Yes");
                            const minutesInHour = 60;
                            var superCardUserOnMinutes = parseInt(isSuperCardUserResult[4].split(":")[1]) / minutesInHour;
                            $("#superCardOnMinutes").text(superCardUserOnMinutes);
                        }
                        if (data[0] == "T") {

                        } else {

                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (textStatus == "timeout") {
                            console.log("caresPostCallAdjustment -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                            WebLogs("caresPostCallAdjustment -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                            //resetUnicaCards();
                        } else {
                            console.log("caresPostCallAdjustment -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                            WebLogs("caresPostCallAdjustment -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                        }
                    }
                });
            }
        }

    }
}

function caresChangePassword(username, old_pwd, new_pwd) {
    if (clientObj.isCaresLoggedIn()) {
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        // 	var server = getThirdPartyServerURL();
        var server = getServerURL_Cares();
        WebLogs('WS:Going to call caresChangePassword, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'user_code=' + username + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=caresChangePassword' + '&user_code=' + username + '&old_password=' + old_pwd + '&new_password=' + new_pwd + '&ipAddr=' + clientObj.getIP() + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID(),
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:caresChangePassword, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'user_code=' + username + '], Response:' + data, 0);
                if (data[0] == "T") {
                    try {
                        $.fancybox.close();
                    } catch (ex) {
                    }
                } else {
                    alert("Password Change failed!!!");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("caresChangePassword -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresChangePassword -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("caresChangePassword -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresChangePassword -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    } else {
        try {
            $.fancybox.close();
        } catch (ex) {
        }
    }
}

function getCustomerInfo(caller_cli) {
//if(clientObj.isCaresLoggedIn())
    clientObj.setProductType('both');
    $("#acw-seg-selector").val('both');
    $("#acw-seg-selector").parent().find('.customStyleSelectBoxInner-form-element').html("All");
    if (caller_cli.length > 0)
    {
        var attData = (clientObj.getAttachedData());
        //alert(attData);
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        //		var server = getThirdPartyServerURL();
        var server = getServerURL_Cares();
        WebLogs('WS:Going to call getCustomerInfo, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'number=' + '' + caller_cli + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=getCustomerInfo' + '&number=' + '' + caller_cli + '&ipAddr=' + clientObj.getIP() + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID(),
            //async : false,

            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:getCustomerInfo, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'number=' + '' + caller_cli + '], Response:' + data, 0);
                if (data) {
                    var customerInfo = data.split('|');
                    if (customerInfo.length > 1) {
                        clientObj.setCaresCustomerInfo(customerInfo);
                        //PopulatePortletsCares(customerInfo);
                        clientObj.setCustomertype(customerInfo[10]);
                        if (clientObj.getCustomertype() == null) {
                            clientObj.setCustomertype(customerInfo[10]);
                        }
                        //clientObj.setProductType(customerInfo[10].toString().toLowerCase());
                        //$("#acw-seg-selector").val(customerInfo[10].toString().toLowerCase());
                        //$("#acw-seg-selector").parent().find('.customStyleSelectBoxInner-form-element').html(customerInfo[10].toString());
                        populateCustomerDemoCares(customerInfo);
                        populateVASCares(customerInfo);
                        populateProfitSancCares(customerInfo);
                    } else {
                        clientObj.setCaresCustomerInfo(new Array());
                        //PopulatePortletsCares(customerInfo);
                        clientObj.setCustomertype("unknown");
                    }
                } else {

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("getCustomerInfo -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getCustomerInfo -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("getCustomerInfo -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getCustomerInfo -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
}

function getCustomerInfo_VAS(caller_cli) {
//if(clientObj.isCaresLoggedIn())
    clientObj.setProductType('both');
    $("#acw-seg-selector").val('both');
    $("#acw-seg-selector").parent().find('.customStyleSelectBoxInner-form-element').html("All");
    if (caller_cli.length > 0)
    {
        var attData = (clientObj.getAttachedData());
        //alert(attData);
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        //		var server = getThirdPartyServerURL();
        var server = getServerURL_Cares();
        WebLogs('WS:Going to call getCustomerInfo_VAS, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'number=' + '' + caller_cli + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=getCustomerInfo' + '&number=' + '' + caller_cli + '&ipAddr=' + clientObj.getIP() + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID(),
            //async : false,

            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:getCustomerInfo_VAS, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'number=' + '' + caller_cli + '], Response:' + data, 0);
                if (data) {
                    var customerInfo = data.split('|');
                    if (customerInfo.length > 1) {
                        clientObj.setCaresCustomerInfo(customerInfo);
                        clientObj.setCustomertype(customerInfo[10]);
                        //						clientObj.setProductType(customerInfo[10].toString().toLowerCase());
                        //						$("#acw-seg-selector").val(customerInfo[10].toString().toLowerCase());
                        //						$("#acw-seg-selector").parent().find('.customStyleSelectBoxInner-form-element').html(customerInfo[10].toString());

//						populateVASCares(customerInfo);
                        populateCustomerDemoCares(customerInfo);
                        populateVASCares(customerInfo);
                        populateProfitSancCares(customerInfo);
                    } else {
                        clientObj.setCaresCustomerInfo(new Array());
                        clientObj.setCustomertype("unknown");
                    }
//					populateProfitSanc();
//					populateCustomerDemo();
                } else {
                    //alert("Password Change failed!!!");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("getCustomerInfo_VAS -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getCustomerInfo_VAS -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("getCustomerInfo_VAS -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getCustomerInfo_VAS -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
//else
    {
//populateProfitSanc();
//populateCustomerDemo();
    }
}

function getTDViewResult(number) {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    {
        $('#divPreviousHandset').html('<span class="regular-body-lucida-light-grey" style="font-weight:bold;color: white">N/A</span>');
        $('#divPreviousHandset_bg').html('<span class="regular-body-12" style="font-weight:bold;color: white"">N/A</span>');
    }

    {
        $('#spanAverageBillValue').html('N/A');
        $('#divSancAverageBill').html('<span class="regular-lucida-yellow">Average Bill: </span><span class="regular-body-white-bold">N/A</span>');
    }

    {
        $('.profit-progress-track-1').css('width', '0px');
        $('.profit-current-status-1').css('left', '0px');
        $('.profit-progress-track-2').css('width', '0px');
        $('.profit-current-status-2').css('left', '0px');
        $('.profit-progress-track-3').css('width', '0px');
        $('.profit-current-status-3').css('left', '0px');
        $('.profit-progress-track-4').css('width', '0px');
        $('.profit-current-status-4').css('left', '0px');
        $('.profit-progress-track-1').parent().parent().find('div.bill-value span').html('N/A');
        $('.profit-progress-track-2').parent().parent().find('div.bill-value span').html('N/A');
        $('.profit-progress-track-3').parent().parent().find('div.bill-value span').html('N/A');
        $('.profit-progress-track-4').parent().parent().find('div.bill-value span').html('N/A');
    }

//if(clientObj.getCustomertype() == 'Post-paid')
    {
        $('#voiceDropDown').html('<option>N/A</option>');
        $('#voiceDropDown').parent().find('.customStyleSelectBoxInner').html('N/A');
        $('#dataDropDown').html('<option>N/A</option>');
        $('#dataDropDown').parent().find('.customStyleSelectBoxInner').html('N/A');
        $('#smsDropDown').html('<option>N/A</option>');
        $('#smsDropDown').parent().find('.customStyleSelectBoxInner').html('N/A');
        $('#othersDropDown').html('<option>N/A</option>');
        $('#othersDropDown').parent().find('.customStyleSelectBoxInner').html('N/A');
        $('#voiceDropDown_bg').html('<option>N/A</option>');
        $('#voiceDropDown_bg').parent().find('.customStyleSelectBoxInner').html('N/A');
        $('#dataDropDown_bg').html('<option>N/A</option>');
        $('#dataDropDown_bg').parent().find('.customStyleSelectBoxInner').html('N/A');
        $('#smsDropDown_bg').html('<option>N/A</option>');
        $('#smsDropDown_bg').parent().find('.customStyleSelectBoxInner').html('N/A');
        $('#othersDropDown_bg').html('<option>N/A</option>');
        $('#othersDropDown_bg').parent().find('.customStyleSelectBoxInner').html('N/A');
        $('#divWithUfone').html('<span class="regular-body-lucida-light-grey">With Ufone Since </span><span class="regular-lucida-bold-11">N/A</span>');
        $('#divBirthday').html('<span class="regular-body-lucida-light-grey">Birthday </span><span class="regular-lucida-bold-11">N/A</span>');
        $('#divWithUfone_bg').html('<span class="regular-body-12">With Ufone since </span><span class="regular-body-white-bold">N/A</span>');
        $('#divBirthday_bg').html('<span class="regular-body-12">Birthday </span><span class="regular-body-white-bold">N/A</span>');
    }
    if (!clientObj.isTDDown()) {
        var server = getThirdPartyServerURL();
        WebLogs('WS:Going to call TDViewCall, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'number=' + '' + number + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=TDViewCall' + '&number=' + '' + number + '&ipAddr=' + clientObj.getIP() + '&cust_type=' + clientObj.getCustomertype() + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID(),
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:TDViewCall, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'number=' + '' + number + '], Response:' + data, 0);
                $(data).find('TDPrevHandSetInfo').each(function () {
                    var results = $(this).text().split('|');
                    if (results.length > 2) {
                        var handset = results[1];
                        if (handset.length > 10) {
                            //handset = results[1].substring(0,10);
//handset += '<br/>' + results[1].substring(11,results[1].length);
                            handset = results[1].split(' ')[0];
                            handset += "<br/>" + results[1].substring(results[1].indexOf(' '), results[1].length);
                        }
                        $('#divPreviousHandset').html('<span class="regular-body-lucida-light-grey" style="font-weight:bold;color: white">' + handset + '</span>');
                        handset = results[1];
                        if (handset.length > 18) {
                            //handset = results[1].substring(0,18);
                            //handset += '<br/>' + results[1].substring(19,results[1].length);
                            handset = results[1].split(' ')[0];
                            handset += "<br/>" + results[1].substring(results[1].indexOf(' '), results[1].length);
                        }
                        $('#divPreviousHandset_bg').html('<span class="regular-body-12" style="font-weight:bold;color:white;">' + handset + '</span>');
                    } else {
                        $('#divPreviousHandset').html('<span class="regular-body-lucida-light-grey" style="font-weight:bold;color:white;">' + $(this).text() + '</span>');
                        $('#divPreviousHandset_bg').html('<span class="regular-body-12" style="font-weight:bold;color:white;">' + $(this).text() + '</span>');
                    }
                });
                $(data).find('TDBundleInfo').each(function () {
                    /*
                     if(clientObj.getCustomertype() == 'Post-paid')
                     {}
                     else
                     */
                    {
                        var results = $(this).text().split('|');
                        $('#voiceDropDown').html('<option>' + results[2] + '</option>');
                        $('#voiceDropDown').parent().find('.customStyleSelectBoxInner').html(results[2]);
                        $('#dataDropDown').html('<option>' + results[4] + '</option>');
                        $('#dataDropDown').parent().find('.customStyleSelectBoxInner').html(results[4]);
                        $('#smsDropDown').html('<option>' + results[3] + '</option>');
                        $('#smsDropDown').parent().find('.customStyleSelectBoxInner').html(results[3]);
                        $('#othersDropDown').html('<option>' + results[1] + '</option>');
                        $('#othersDropDown').parent().find('.customStyleSelectBoxInner').html(results[1]);
                        $('#voiceDropDown_bg').html('<option>' + results[2] + '</option>');
                        $('#voiceDropDown_bg').parent().find('.customStyleSelectBoxInner').html(results[2]);
                        $('#dataDropDown_bg').html('<option>' + results[4] + '</option>');
                        $('#dataDropDown_bg').parent().find('.customStyleSelectBoxInner').html(results[4]);
                        $('#smsDropDown_bg').html('<option>' + results[3] + '</option>');
                        $('#smsDropDown_bg').parent().find('.customStyleSelectBoxInner').html(results[3]);
                        $('#othersDropDown_bg').html('<option>' + results[1] + '</option>');
                        $('#othersDropDown_bg').parent().find('.customStyleSelectBoxInner').html(results[1]);
                    }
                });
                $(data).find('TDDatesInfo').each(function () {
                    /*
                     if(clientObj.getCustomertype() == 'Post-paid')
                     {}
                     else
                     */
                    {
                        var results = $(this).text().split('|');
                        $('#divWithUfone').html('<span class="regular-body-lucida-light-grey">With Ufone Since </span><span class="regular-lucida-bold-11">' + results[1] + '</span>');
                        $('#divBirthday').html('<span class="regular-body-lucida-light-grey">Birthday </span><span class="regular-lucida-bold-11">N/A</span>');
                        $('#divWithUfone_bg').html('<span class="regular-body-12">With Ufone since </span><span class="regular-body-white-bold">' + results[1] + '</span>');
                        $('#divBirthday_bg').html('<span class="regular-body-12">Birthday </span><span class="regular-body-white-bold">N/A</span>');
                    }
                });
                $(data).find('TDSumInfo').each(function () {
                    var results = $(this).text().split('|');
                    var sumBill = (parseFloat(results[0]) + parseFloat(results[1]) + parseFloat(results[2]));
                    var voiceBill = Math.round((results[0] / sumBill) * 208);
                    var smsBill = Math.round((results[1] / sumBill) * 208);
                    var gprsBill = Math.round((results[2] / sumBill) * 208);
                    var sumBill_percent = Math.round((sumBill / sumBill) * 208);
                    $('.profit-progress-track-1').css('width', sumBill_percent + 'px');
                    $('.profit-current-status-1').css('left', sumBill_percent + 'px');
                    $('.profit-progress-track-2').css('width', voiceBill + 'px');
                    $('.profit-current-status-2').css('left', voiceBill + 'px');
                    $('.profit-progress-track-3').css('width', gprsBill + 'px');
                    $('.profit-current-status-3').css('left', gprsBill + 'px');
                    $('.profit-progress-track-4').css('width', smsBill + 'px');
                    $('.profit-current-status-4').css('left', smsBill + 'px');
                    $('.profit-progress-track-1').parent().parent().find('div.bill-value span').html('Rs. ' + Math.round(sumBill));
                    $('.profit-progress-track-2').parent().parent().find('div.bill-value span').html('Rs. ' + results[0]);
                    $('.profit-progress-track-3').parent().parent().find('div.bill-value span').html('Rs. ' + results[2]);
                    $('.profit-progress-track-4').parent().parent().find('div.bill-value span').html('Rs. ' + results[1]);
                });
                $(data).find('TDAvgInfo').each(function () {
                    var results = $(this).text().split(',');
                    $('#spanAverageBillValue').html("Rs. " + results[1]);
                    $('#divSancAverageBill').html('<span class="regular-lucida-yellow">Average Bill: </span><span class="regular-body-white-bold">Rs. ' + results[1] + '</span>');
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("TDViewCall -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("TDViewCall -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("TDViewCall -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("TDViewCall -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }

}

function getSupervisor() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call getSupervisor, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + ']', 0);
    $.ajax({
        url: server + '' + ajaxPage + '?function=getSupervisor' + '&ipAddr=' + clientObj.getIP() + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID(),
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getSupervisor, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '], Response:' + data, 0);
            if (data) {
                var agentName = clientObj.getUsername();
                var superNameArray = data.split("|");
                clientObj.setIsSupervisor(false);
                for (var i = 0; i < superNameArray.length - 1; i++) {
                    if (superNameArray[i] == agentName) {
                        clientObj.setIsSupervisor(true);
                    }
                }
            } else {
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getSupervisor -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getSupervisor -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getSupervisor -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getSupervisor -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function isPIASystemAgent(skillArr) {
    WebLogs("SKILL ARR => " + skillArr, 0);
    if (skillArr.length < 2) {
        return;
    }//&& skillArr.length == 3
    else if ($.inArray("NFWBank_KHI.SG", skillArr) != -1) {
        agentSkillGroup = "NFWBank_KHI.SG";
        isFwblAgent = true;
        isUnileverAgent = false;
        isPIAWCEnabled = false;
        isPIAAgent = false;
        initializeFwblHash();
        getConsentInfo();
    } else if (isUnileverWCEnabled && ($.inArray("Unilever_KHI.SG", skillArr) != -1 || $.inArray("ULFood_KHI.SG", skillArr) != -1) && $.inArray("FloorSupervisor_KHI.SG", skillArr) == -1) {
        isUnileverAgent = true;
        isPIAAgent = false;
        getConsentInfo();
        getUnileverAWCXML();
        getFreqIVR();
        //getIVRXML();
        $("#cms-expand-btn").attr("disabled", true);
        $(".notification-bar-wrapper").hide();

    } else if (isPIAWCEnabled && ($.inArray("PIA_KHI.SG", skillArr) != -1 || $.inArray("PIA_ISB.SG", skillArr) != -1)) {
        isPIAAgent = true;
        isUnileverAgent = false;
        getConsentInfo();
        getPIAAWCXML();
        getFreqIVR();
        getIVRXML();
        $("#cms-expand-btn").attr("disabled", true);
        $(".notification-bar-wrapper").hide();
    } else {
        isPIAAgent = false;
        isUnileverAgent = false;
        $("#cms-expand-btn").removeAttr("disabled");
        $(".notification-bar-wrapper").show();
        getConsentInfo();
        getFreqWC();
        getAWCXML();
        getFreqIVR();
        getIVRXML();
        getUnicaWCXML();
        getComplaintWorkCodeList();
        getSOPData();
    }
}
function getSystemConfigs(ajaxBool) {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = './';
    WebLogs('WS:Going to call getSystemConfigs, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + ']', 0);
    $.ajax({
        url: server + ajaxPage + '?function=getSystemConfigs' + '&ipAddr=' + clientObj.getIP(),
        async: ajaxBool,
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getSystemConfigs, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '], Response:' + data, 0);
            if (data) {
                var configValues = data.split('|');
                clientObj.setDNIS_Hangup_Disabled(configValues[0].split(','));
                clientObj.setDNIS_Direct_Cares(configValues[1].split(','));
                clientObj.setDNIS_Auto_Call_Pickup(configValues[2].split(','));
                clientObj.setDNIS_Description_Text(configValues[3].split(':'));
                clientObj.setDNIS_Feedback_Disabled(configValues[4].split(','));
                clientObj.setDNIS_Charging(configValues[5].split(','));
                clientObj.setCDN_Description(configValues[6].split(':'));
                clientObj.setDNIS_Consent(configValues[7].split(','));
                clientObj.setChargingRate(configValues[8]);
                clientObj.setEM_CDN(configValues[9]);
                clientObj.setSupVsrAcwDuration(configValues[10]);
                if (configValues[11] == "0") {
                    clientObj.setIsCaresDown(false);
                } else {
                    clientObj.setIsCaresDown(true);
                }
                clientObj.setCommonPwdCCT(configValues[12]);
                if (configValues[13] == "0") {
                    clientObj.setIsTDDown(false);
                } else {
                    clientObj.setIsTDDown(true);
                }
                if (configValues[14] == "0") {
                    clientObj.setIsUnicaDown(false);
                    _isUnicaDown = false;
                } else {
                    clientObj.setIsUnicaDown(true);
                    _isUnicaDown = true;
                }
//				clientObj.setIsUnicaDown(true);
                clientObj.setCardsToShow(configValues[15]);
                if (configValues[15] == "null" || configValues[15] < 1) {
                    clientObj.setCardsToShow(1);
                } else if (configValues[15] > 3) {
                    clientObj.setCardsToShow(4);
                }

                LBServers = configValues[16].split(',');
                ThirdPartyLBServers = configValues[17].split(',');
                clientObj.setCaresUrl("./");
                clientObj.setUnicaUrl("./");
                clientObj.setTeraDataUrl("./");
                clientObj.setCardsToFetch(configValues[18]);
                clientObj.setUnicaSkillSet(configValues[19].split(","));
                location_url = configValues[20];
                news_alert_timer = parseInt(configValues[21]);

                LBServers_Unica = configValues[22].split(',');
                LBServers_LocalDB = configValues[23].split(',');
                LBServers_Cares = configValues[24].split(',');
                if (configValues[25] == "0") {
                    console.log("----------------------------------------PIA Agent Workcodes not enabled \n");
                    isPIAWCEnabled = false;
                } else {
                    console.log("----------------------------------------PIA Agent Workcodes enabled \n");
                    isPIAWCEnabled = true;
                }
                if (configValues[26] == "0") {
                    console.log("----------------------------------------Unilever Agent Workcodes not enabled \n");
                    isUnileverWCEnabled = false;
                } else {
                    console.log("----------------------------------------Unilever Agent Workcodes enabled \n");
                    isUnileverWCEnabled = true;
                }

//				LBServers = new Array();
//				ThirdPartyLBServers = new Array();
//				LBServers_Unica = new Array();
//				LBServers_LocalDB = new Array();
//				LBServers_Cares = new Array();
            } else {

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getSystemConfigs -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getSystemConfigs -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getSystemConfigs -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getSystemConfigs -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }});
}
function getACWNotReadyCode() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var code = 'ACW';
    var server = getServerURL();
    WebLogs('WS:Going to call getNotReadyCode, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'notReadyCode=' + code + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + '?function=getNotReadyCode' + '&notReadyCode=' + code + '&ipAddr=' + clientObj.getIP(),
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getNotReadyCode, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'notReadyCode=' + code + '], Response:' + data, 0);
            if (data) {
                clientObj.setACWNRCode(data);
            } else {

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getNotReadyCode -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getNotReadyCode -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards(); 			
            } else {
                console.log("getNotReadyCode -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getNotReadyCode -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function getASNNotReadyCode() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var code = 'ASN';
    var server = getServerURL();
    WebLogs('WS:Going to call getNotReadyCode, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'notReadyCode=' + code + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + '?function=getNotReadyCode' + '&notReadyCode=' + code + '&ipAddr=' + clientObj.getIP(),
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getNotReadyCode, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'notReadyCode=' + code + '], Response:' + data, 0);
            if (data) {
                clientObj.setAgentSetNotReadyCode(data);
            } else {

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getNotReadyCode -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getNotReadyCode -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getNotReadyCode -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getNotReadyCode -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}
var fonixMutex = false;
var fonixMutexTimeout = null;
function setCaresIntegration(caller_cli) {
    if (!fonixMutex) {
        fonixMutex = true;
        fonixMutexTimeout = setTimeout(function () {
            fonixMutex = false;
        }, 10000);
        var attData = (clientObj.getAttachedData());
        var insert_datetime = new Date().getTime();
        var cares_id = clientObj.getCaresSessionID();
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        // var server = getServerURL();
        var server = getServerURL_LocalDB();
        WebLogs('WS:Going to call setCaresIntegration, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'callerCli=' + caller_cli + ', ipAddr=' + clientObj.getIP() + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=setCaresIntegration' + '&callerCli=' + caller_cli + '&ipAddr=' + clientObj.getIP() + '&insert_datetime=' + insert_datetime + '&cares_id=' + cares_id + '&dnis=' + clientObj.getDNIS(),
            success: function (data) {
                fonixMutex = false;
                clearTimeout(fonixMutexTimeout);
                fonixMutexTimeout = null;
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:setCaresIntegration, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'callerCli=' + caller_cli + ', ipAddr=' + clientObj.getIP() + '], Response:' + data, 0);
                if (data) {

                } else {
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                fonixMutex = false;
                clearTimeout(fonixMutexTimeout);
                fonixMutexTimeout = null;
                if (textStatus == "timeout") {
                    console.log("setCaresIntegration -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("setCaresIntegration -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0); //resetUnicaCards();
                } else {
                    console.log("setCaresIntegration -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("setCaresIntegration -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
}

function getConsentInfo() {
//if ($.inArray(clientObj.getDNIS(), clientObj.getDNIS_Consent()) != -1) 
    {
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        var server = getServerURL();
        WebLogs('WS:Going to call getConsentInfo, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'ipPhoneType=' + clientObj.getIpPhoneType() + ', ipAddr=' + clientObj.getIP() + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=getConsentInfo' + '&ipPhoneType=' + clientObj.getIpPhoneType() + '&ipAddr=' + clientObj.getIP(),
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:getConsentInfo, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'ipPhoneType=' + clientObj.getIpPhoneType() + ', ipAddr=' + clientObj.getIP() + '], Response:' + data, 0);
                data = data.split('|');
                var consentOptionTxt = '';
                for (var i = 0; i < data.length - 1; i++) {
                    consentOptionTxt += "<option value='" + data[i].split('=')[1] + "' >" + data[i].split('=')[0] + "</option>" + '\n';
                }
                $('#dropdownlanguage').html(consentOptionTxt);
                $('#divConsentLanguage').show('fast');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("getConsentInfo -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getConsentInfo -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("getConsentInfo -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getConsentInfo -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
    /*else {
     $('#divIVRoptions').show('fast');
     $('#btnDoTransfer').show();
     $('#btnDoConference').hide();
     }*/
}

function getAgentSkillSet_Ajax(agentID)
{
    {
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        var server = getServerURL();
        WebLogs('WS:Going to call getAgentSkillSet_Ajax, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'agentID=' + agentID + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=getAgentSkillSet_Ajax' + '&agentID=' + agentID,
            success: function (data) {
                var date_now = new Date();
                data = $.trim(data);
                WebLogs('WS:getAgentSkillSet_Ajax, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'agentID=' + agentID + '], Response:' + data, 0);
                clientObj.setAgentSkillSet(data);
                if (!_isUnicaDown) {
                    var agentSkillSets = data.split(',');
                    var unicaSkillSets = clientObj.getUnicaSkillSet();
                    if (!$.isArray(unicaSkillSets)) {
                        unicaSkillSets = new Array(clientObj.getUnicaSkillSet());
                    }
                    clientObj.setIsUnicaDown(true);
                    for (var i = 0; i < agentSkillSets.length; i++) {
                        if ($.trim(agentSkillSets[i]).length > 0) {
                            if ($.inArray($.trim(agentSkillSets[i]).toLowerCase(), unicaSkillSets) != -1) {
                                clientObj.setIsUnicaDown(false);
                            }
                        }
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("getAgentSkillSet_Ajax -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getAgentSkillSet_Ajax -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("getAgentSkillSet_Ajax -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getAgentSkillSet_Ajax -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
}

function setCallDetails(call_con_id, call_duration, call_time_in_ivr, call_trans_type, call_dn_in,
        call_dn_out, call_trans_ivr_menu, call_apps, call_cms_flag, agent_login_name, agent_login_id,
        agent_name, agent_workstation, cust_mobile, cust_cli, cust_type, cust_name, cust_language, cust_rating,
        cust_package, cust_access_level, cust_product, cust_balance_amt, cust_prepaid_expiry, skillset, dnis,
        call_start_date, call_end_date, ip_address, agent_skillset, cust_mobile_model) {

    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    if (cust_language == '1') {
        cust_language = "English";
    } else if (cust_language == '2') {
        cust_language = "Urdu";
    } else if (cust_language == '4') {
        cust_language = "Pushto";
    } else if (cust_language == '5') {
        cust_language = "Sindhi";
    } else if (cust_language == '7') {
        cust_language = "Saraiki";
    }
//	var server = getServerURL(); 	
    var server = getServerURL_LocalDB();
    WebLogs('WS:Going to call setCallDetails, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'call_con_id= ' + '' + call_con_id + ', cust_cli= ' + cust_cli + ']', 0);
    $.ajax({
        type: "POST",
        url: server + '' + ajaxPage + '?function=setCallDetails',
        data: {
            _call_con_id: call_con_id,
            _call_duration: call_duration,
            _call_time_in_ivr: call_time_in_ivr,
            _call_trans_type: call_trans_type,
            _call_dn_in: call_dn_in,
            _call_dn_out: call_dn_out,
            _call_trans_ivr_menu: call_trans_ivr_menu, _call_apps: call_apps,
            _call_cms_flag: call_cms_flag,
            _agent_login_name: agent_login_name, _agent_login_id: agent_login_id,
            _agent_name: agent_name,
            _agent_workstation: agent_workstation,
            _cust_mobile: cust_mobile,
            _cust_cli: cust_cli,
            _cust_type: cust_type,
            _cust_name: cust_name,
            _cust_language: cust_language,
            _cust_rating: cust_rating,
            _cust_package: cust_package,
            _cust_access_level: cust_access_level,
            _cust_product: cust_product,
            _cust_balance_amt: cust_balance_amt,
            _cust_prepaid_expiry: cust_prepaid_expiry,
            _skillset: skillset,
            _dnis: dnis,
            _call_start_date: call_start_date,
            _call_end_date: call_end_date,
            _agent_skillset: agent_skillset,
            _cust_mobile_model: cust_mobile_model, ipAddr: ip_address
        },
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:setCallDetails, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'call_con_id= ' + '' + call_con_id + ', cust_cli= ' + cust_cli + '], Response:' + data, 0);
            //alert(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("setCallDetails -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("setCallDetails -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("setCallDetails -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("setCallDetails -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function caresHandSetInfo(number) {
//if(clientObj.isCaresLoggedIn())
    {
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        var server = getThirdPartyServerURL();
        WebLogs('WS:Going to call caresHandSetInfo, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'number=' + '' + number + ']', 0);
        $.ajax({
            url: server + '' + ajaxPage + '?function=caresHandSetInfo' + '&number=' + '' + number + '&ipAddr=' + clientObj.getIP() + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID(),
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:caresHandSetInfo, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'number=' + '' + number + '], Response:' + data, 0);
                var results = data.split('|');
                if (results.length > 2)
                {
                    var _2G_3G = '';
                    if (results[5].toLowerCase() == "y") {
                        _2G_3G += '2G';
                    }
                    if (results[6].toLowerCase() == "y") {
                        _2G_3G += ', ';
                        _2G_3G += '3G';
                    }
                    if (_2G_3G.length > 0) {
                        _2G_3G = '(' + _2G_3G + ')';
                    }
                    var handset_temp = results[0] + ' ' + results[1] + _2G_3G;
                    clientObj.setCustomerCurrentHandset(handset_temp);
                    var handset = results[0] + ' ' + results[1] + _2G_3G;
                    if (handset.length > 10) {
                        //handset = handset_temp.substring(0,10);
                        //handset += '<br/>' + handset_temp.substring(11, handset_temp.length);
                        handset = results[0];
                        handset += '<br/>' + results[1];
                        handset += '<br/>' + _2G_3G;
                    }
                    $('#divCurrentHandset').html('<span class="regular-body-lucida-light-grey" style="font-weight:bold;color:white;">' + handset + '</span>');
                    handset_temp = results[0] + ' ' + results[1] + _2G_3G;
                    handset = results[0] + ' ' + results[1] + _2G_3G;
                    if (handset.length > 18) {
//handset = handset_temp.substring(0,18);
                        //handset += "<br/>" + handset_temp.substring(19,handset_temp.length);
                        handset = results[0];
                        handset += '<br/>' + results[1];
                        handset += '<br/>' + _2G_3G;
                    }
                    $('#divCurrentHandset_bg').html('<span class="regular-body-12" style="font-weight:bold;color:white;">' + handset + '</span>');
                } else {
                    clientObj.setCustomerCurrentHandset(data);
                    $('#divCurrentHandset').html('<span class="regular-body-lucida-light-grey" style="font-weight:bold;color:white;">' + data + '</span>');
                    $('#divCurrentHandset_bg').html('<span class="regular-body-12" style="font-weight:bold;color:white;">' + data + '</span>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("caresHandSetInfo -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresHandSetInfo -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("caresHandSetInfo -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresHandSetInfo -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }

        });
    }
}

function getSessionHistory(number) {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getThirdPartyServerURL();
    WebLogs('WS:Going to call getSessionHistory, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'number=' + '' + number + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        data: {
            "function": "getSessionHistory",
            number: number,
            ipAddr: clientObj.getIP(),
            datetime: new Date().getTime(),
            cli: clientObj.getCallerMsisdn(),
            username: clientObj.getUsername(),
            amt_charged: clientObj.getChargingRate(),
            conn_id: clientObj.getCallSessionID()
        }, success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getSessionHistory, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'number=' + '' + number + '], Response:' + data, 0);
            var agentHistoryTable = '';
            if (data.length > 10) {
                var agentHistoryArray = data.split('|');
                agentHistoryTable += '<table style="width: 100%;font-size: 1.2em;color: #FFFFFF;line-height: 1.25em">';
                agentHistoryTable += '<thead style="font-weight: bold;color: #FFD200"><tr><th>Call Date</th><th>Work Code</th></tr></thead>';
                var minLength = 11;
                if (agentHistoryArray.length - 1 < minLength) {
                    minLength = agentHistoryArray.length - 1;
                }
                for (var i = 0; i < minLength; i++) {
                    var call_date = agentHistoryArray[i].split(',')[0].split(' ')[0];
                    call_date = call_date.split('-')[2] + '/' + call_date.split('-')[1] + '/' + call_date.split('-')[0];
                    var agent_name = agentHistoryArray[i].split(',')[1];
                    var work_code = agentHistoryArray[i].split(',')[2];
                    if (agent_name == "null") {
                        agent_name = "N/A";
                    }
                    agentHistoryTable += '<tr>';
                    agentHistoryTable += '<td>' + call_date + '</td><td>' + work_code + '</td>';
                    agentHistoryTable += '</tr>';
                }
                agentHistoryTable += '</table>';
                //$('li#liSessionHistory div.widget-content').html(agentHistoryTable);
                $('#divAgentHistory').html(agentHistoryTable);
                agentHistoryTable = '';
                var agentHistoryArray = data.split('|');
                agentHistoryTable += '<table style="width: 100%;font-size: 1.2em;color: #FFFFFF;line-height: 1.25em">';
                agentHistoryTable += '<thead style="font-weight: bold;color: #FFD200;"><tr><th>Call Date</th><th>Agent Name</th><th>Work Code</th></tr></thead>';
                for (var i = 0; i < agentHistoryArray.length - 1; i++) {
                    var call_date = new Date(agentHistoryArray[i].split(',')[3]).toLocaleString();
                    //var call_date = agentHistoryArray[i].split(',')[3];
                    //call_date = call_date.split('-')[2] + '/' + call_date.split('-')[1] + '/' + call_date.split('-')[0];
                    var agent_name = agentHistoryArray[i].split(',')[1];
                    var work_code = agentHistoryArray[i].split(',')[2];
                    if (agent_name == "null") {
                        agent_name = "N/A";
                    }
                    agentHistoryTable += '<tr>';
                    agentHistoryTable += '<td>' + call_date + '</td><td>' + agent_name + '</td><td>' + work_code + '</td>';
                    agentHistoryTable += '</tr>';
                }
                agentHistoryTable += '</table>';
            }
            $('#divAgentHistory_bg').html(agentHistoryTable);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getSessionHistory -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getSessionHistory -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getSessionHistory -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getSessionHistory -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

var agentStatsReqTimer = 17;
function getAgentStats() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    //	var server = getServerURL();
    var server = getThirdPartyServerURL();
    WebLogs('WS:Going to call getAgentStats, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'agent_id=' + clientObj.getAgentID() + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + '?function=getAgentStats' + '&agent_id=' + clientObj.getAgentID(),
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getAgentStats, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'agent_id=' + clientObj.getAgentID() + '], Response:' + data, 0);
            if (data.length > 10) {
                var agentStatsArray = data.split('|');
                setData_Top(agentStatsArray[0].split('=')[1], agentStatsArray[5].split('=')[1], agentStatsArray[3].split('=')[1], agentStatsArray[1].split('=')[1]);
                setData_Middle(agentStatsArray[2].split('=')[1], agentStatsArray[4].split('=')[1], agentStatsArray[6].split('=')[1], agentStatsArray[7].split('=')[1]);
                setData_Bottom(agentStatsArray[8].split('=')[1], agentStatsArray[9].split('=')[1], agentStatsArray[11].split('=')[1], agentStatsArray[10].split('=')[1]);
                try {
                    initialize_stats_charts();
                } catch (exception) {
                    WebLogs("KinectJS exception = " + exception, 0);
                    console.log("KinectJS exception" + exception);
                }
                $('#LIT_span').html(agentStatsArray[0].split('=')[2]);
                $('#IT_span').html(agentStatsArray[5].split('=')[2]);
                $('#BT_span').html(agentStatsArray[3].split('=')[2]);
                $('#NRT_span').html(agentStatsArray[1].split('=')[2]);
                $('#PD_span').html(agentStatsArray[2].split('=')[2]);
                $('#ACW_span').html(agentStatsArray[4].split('=')[2]);
                $('#HT_span').html(agentStatsArray[6].split('=')[2]);
                $('#AVG_span').html(agentStatsArray[7].split('=')[2]);
                $('#CO_span').html(agentStatsArray[8].split('=')[1]);
                $('#CA_span').html(agentStatsArray[9].split('=')[1]);
                $('#CRQ_span').html(agentStatsArray[11].split('=')[1]);
                $('#SC_span').html(agentStatsArray[10].split('=')[1]);
                agentStatsReqTimer = 17;
            } else {
                $('div#chart01').html('');
                $('div#chart02').html('');
                $('div#chart03').html('');
                $('#LIT_span').html('');
                $('#IT_span').html('');
                $('#BT_span').html('');
                $('#NRT_span').html('');
                $('#PD_span').html('');
                $('#ACW_span').html('');
                $('#HT_span').html('');
                $('#AVG_span').html('');
                $('#CO_span').html('');
                $('#CA_span').html('');
                $('#CRQ_span').html('');
                $('#SC_span').html('');
                agentStatsReqTimer = 2;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getAgentStats -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getAgentStats -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getAgentStats -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getAgentStats -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function getIVRXML() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call getIVRXML, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + "?function=getIVRXML",
        success: function (xml) {
            xml = $.trim(xml);
            var date_now = new Date();
            WebLogs('WS:getIVRXML, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '], Response:' + xml, 0);
            if (xml.length > 20) {

                IVR_XML = xml;
                getIVRCategory();
            } else {
                getIVRXML();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getIVRXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getIVRXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getIVRXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getIVRXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }});
}

function getIVRCategory() {
    var title_txt = '';
    var dropdown = '';
    $(IVR_XML).find('ivrcatname').each(function () {
        if ($(this).text().length > 15) {
            title_txt = 'title="' + $(this).text() + '"';
        } else {
            title_txt = '';
        }
        dropdown += '<option value="' + $(this).text() + '" ' + title_txt + ' >' + $(this).text() + '</option>\n';
    });
    $('select#IVRTransCat').html(dropdown);
}

var IVR_tmpArray = new Array();
var ivr_click_state = 3;
var awc_click_state = 1;
function getIVRSubCategory() {
    var category = $('select#IVRTransCat').val();
    $('#IVRTransMenu').html('');
    IVR_tmpArray = new Array();
    var dropdown = '';
    var title_txt = '';
    $(IVR_XML).find('ivrcatname').each(function () {
        if ($(this).text() == category) {
            $(this).parent().find('ivrsubcat ivrsubcatname').each(function () {
                if ($(this).text().length > 15) {
                    title_txt = 'title="' + $(this).text() + '"';
                } else {
                    title_txt = '';
                }
                dropdown += '<option value="' + $(this).text() + '" ' + title_txt + ' >' + $(this).text() + '</option>\n';
                IVR_tmpArray.push($(this).text());
            });
        }
    });
    //$('#search_ivr').focus();
    //ivr_click_state = 1;
    ivr_click_state = 3;
    $('select#IVRTransSubCat').html(dropdown);
}

var IVR_tmpArray2 = new Array();
function getIVRMenuCode() {
    IVR_tmpArray2 = new Array();
    var subcategory = $('select#IVRTransSubCat').val();
    var title_txt = '';
    var dropdown = '';
    var menu_desc = '';
    var menu_code = '';
    dropdown += '<select id="IVRTransMenuValue" onkeydown="ivrKeyPress(this, event)" class="select-box-generic" style="width: 335px;" size="2" >\n';
    $(IVR_XML).find('ivrsubcat ivrsubcatname').each(function () {
        if ($(this).text() == subcategory) {
            $(this).parent().find('ivrname').each(function () {
                $(this).find('menucode').each(function () {
                    menu_code = $(this).text();
                });
                $(this).find('menudesc').each(function () {
                    menu_desc = $(this).text();
                });
                if (menu_desc.length > 15) {
                    title_txt = 'title="' + menu_desc + '"';
                } else {
                    title_txt = '';
                }
                dropdown += '<option value="' + menu_code + '" ' + title_txt + ' >' + menu_desc + '</option>\n';
                IVR_tmpArray2.push(menu_code + "," + menu_desc);
            });
        }
    });
    dropdown += '</select>\n';
    //ivr_click_state = 2; 	ivr_click_state = 3;
    //$('#search_ivr').focus();
    $('#IVRTransMenu').html(dropdown);
}

function updateNotReadyCodes() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call updateNotReadyCodes, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + '?function=updateNotReadyCodes',
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:updateNotReadyCodes, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '], Response:' + data, 0);
            var dataArray = data.split('|');
            var optionStr = '';
            for (var i = 0; i < dataArray.length - 1; i++) {
                if (i == 0) {
                    optionStr += '<option selected="selected" value="' + dataArray[i].split('=')[0] + '" >' + dataArray[i].split('=')[1] + '</option>\n';
                } else {
                    optionStr += '<option value="' + dataArray[i].split('=')[0] + '" >' + dataArray[i].split('=')[1] + '</option>\n';
                }
            }
            $('#notReadyDropDownList').html(optionStr);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("updateNotReadyCodes -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("updateNotReadyCodes -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("updateNotReadyCodes -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("updateNotReadyCodes -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function getPIAAWCXML() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call getPIAAWCXML, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + "?function=getPIAAWCXML",
        success: function (xml) {
            xml = $.trim(xml);
            var date_now = new Date();
            WebLogs('WS:getPIAAWCXML, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '], Response:' + xml, 0);
            if (xml.length > 20)
            {
                ACW_XML = xml;
                populate_acwListHeadName();
            } else {
                getPIAAWCXML();
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getPIAAWCXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getPIAAWCXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0); //resetUnicaCards();
            } else {
                console.log("getPIAAWCXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getPIAAWCXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}
function getUnileverAWCXML() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call getUnileverAWCXML, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + "?function=getUnileverAWCXML",
        success: function (xml) {
            xml = $.trim(xml);
            var date_now = new Date();
            WebLogs('WS:getUnileverAWCXML, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '], Response:' + xml, 0);
            if (xml.length > 20)
            {
                ACW_XML = xml;
                populate_acwListHeadName();
            } else {
                getUnileverAWCXML();
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getUnileverAWCXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getUnileverAWCXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0); //resetUnicaCards();
            } else {
                console.log("getUnileverAWCXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getUnileverAWCXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}
function getAWCXML() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call getAWCXML, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + "?function=getAWCXML",
        success: function (xml) {
            xml = $.trim(xml);
            var date_now = new Date();
            WebLogs('WS:getAWCXML, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '], Response:' + xml, 0);
            if (xml.length > 20)
            {
                ACW_XML = xml;
                populate_acwListHeadName();
            } else {
                getAWCXML();
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getAWCXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getAWCXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0); //resetUnicaCards();
            } else {
                console.log("getAWCXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getAWCXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function getFreqWC() {
    var id = '';
    var name = '';
    var sms = '';
    var head = '';
    var ivrcode = '';
    var smsstring = '';
    var sop = '';
    var wctype = '';
    var count = 0;
    var freqUsedCodes_txt = '';
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call getFreqWC, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + "?function=getFreqWC",
        success: function (xml) {
            xml = $.trim(xml);
            var date_now = new Date();
            WebLogs('WS:getFreqWC, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '], Response:' + xml, 0);
            freqCodesXML = xml;
            $(freqCodesXML).find('wname').each(function () {
                id = $(this).find('id').text();
                name = $(this).find('name').text();
                sms = $(this).find('sms').text();
                head = $(this).find('wHead').text();
                ivrcode = $(this).find('ivrcode').text();
                smsstring = $(this).find('smsstring').text();
                sop = $(this).find('sop').text();
                wctype = $(this).find('wctype').text();
                freqUsedCodes_txt += '<div class="checkbox-wrapper">\n';
                freqUsedCodes_txt += '<div class="checkbox-container"><a href="#" class="checkbox-generic" onclick="freqCodeBtnPress(this,' + id + ',' + sms + ',\'' + name + '\',\'' + head + '\',\'' + ivrcode + '\',\'' + smsstring + '\',\'' + sop + '\',\'' + wctype + '\')">' + name + '</a></div>' + '\n';
                freqUsedCodes_txt += '<div class="checkbox-label"><span class="regular-lucida">' + name + '</span></div>\n';
                freqUsedCodes_txt += '</div>\n';
            });
            $('#freqACWDiv').html(freqUsedCodes_txt);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getFreqWC -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getFreqWC -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getFreqWC -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getFreqWC -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}
function getFreqIVR() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call getFreqIVR, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage + "?function=getFreqIVR",
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getFreqIVR, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '], Response:' + data, 0);
            var dataArray = data.split("|");
            var freqIVRDiv_innerHtml = "";
            for (var i = 0; i < dataArray.length - 1; i++) {
                freqIVRDiv_innerHtml += '<div class="radio-btn-wrapper">\n';
                freqIVRDiv_innerHtml += '<div class="radio-btn-container"><a href="#" class="radio-btn-generic" onclick="clientObj.IVR_Transfer($(this).html());" >' + dataArray[i].split('=')[0] + '</a></div>\n';
                freqIVRDiv_innerHtml += '<div class="radio-btn-label"><span class="regular-lucida">' + dataArray[i].split('=')[1] + '</span></div>\n';
                freqIVRDiv_innerHtml += '</div>\n';
            }
            $('#freqIVRDiv').html(freqIVRDiv_innerHtml);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getFreqIVR -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getFreqIVR -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getFreqIVR -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getFreqIVR -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function caresWorkCode(workcodes_array) {
//if(clientObj.isCaresLoggedIn())
    {
        var _workcodes = '';
        var _user_msisdn = clientObj.getCallerMsisdn();
        var _user_code = clientObj.getUsername();
        var _location_id = null;
        if (clientObj.isCaresLoggedIn()) {
            _location_id = clientObj.getCaresUserLogOnString().split('|')[1];
        }
        var _user_id = clientObj.getAgentID();
        if (clientObj.isCaresLoggedIn()) {
//_user_id = clientObj.getCaresUserLogOnString().split('|')[2];
            _user_id = clientObj.getAgentID();
        }
        var _date = getFormatedDate(new Date());
        for (var i = 0; i < workcodes_array.length; i++) {
            _workcodes += workcodes_array[i] + "|";
        }
        var resolvedArray = new Array();
        var _resolvedArray = "";
        $("#acwSelectedName1").find(".selected-option-wrapper").each(function () {
            resolvedArray.push($(this).find("input.acwSelectedNameChkBox_class").first().val());
        });
        var smsSendArray = new Array();
        var _smsSendArray = "";
        $(".acw-lightbox .dragging").find(".selected-option-wrapper").each(function () {
            if ($(this).hasClass("bgClick")) {
                smsSendArray.push($(this).find("input.acwSelectedNameChkBox_class").first().val());
            }
        });
        var ivrSendArray = new Array();
        var _ivrSendArray = "";
        $(".acw-lightbox .dragging").find(".selected-option-wrapper").each(function () {
            if ($(this).hasClass("bgdblClick")) {
                ivrSendArray.push($(this).find("input.acwSelectedNameChkBox_class").first().val());
            }
        });
        for (var i = 0; i < resolvedArray.length; i++) {
            _resolvedArray += resolvedArray[i] + "|";
        }
        for (var i = 0; i < smsSendArray.length; i++) {
            _smsSendArray += smsSendArray[i] + "|";
        }
        for (var i = 0; i < ivrSendArray.length; i++) {
            _ivrSendArray += ivrSendArray[i] + "|";
        }
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        //		var server = getThirdPartyServerURL();
        var server = getServerURL_Cares();
        WebLogs('WS:Going to call caresWorkCode, Time:[' + req_time + '], Server:[' + server + '], Request:[' + "number=" + _user_msisdn + ", workcodes=" + _workcodes + ", location_id=" + _location_id + ", user_id=" + _user_id + ", user_code=" + _user_code + ", date=" + _date + ', datetime=' + new Date().getTime() + ', agent_id=' + clientObj.getUsername() + ', amt_charged=' + clientObj.getChargingRate() + ', conn_id=' + clientObj.getCallSessionID() + ", ipAddr=" + clientObj.getIP() + ']', 0);
        $.ajax({
            //type: "GET",
            //url: getServerURL()+''+ajaxPage+"?function=caresWorkCode" + "&number=" + user_msisdn + "&workcodes=" + workcodes+ "&location_id=" + location_id + "&user_id=" + user_id + "&user_code=" + user_code+ "&date=" + date + '&datetime=' + new Date().getTime() + '&agent_id=' + clientObj.getUsername() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID() + "&ipAddr=" + clientObj.getIP(),
            type: "POST",
            url: server + '' + ajaxPage + "?function=caresWorkCode",
            data: {
                number: _user_msisdn,
                workcodes: _workcodes,
                location_id: _location_id,
                user_id: _user_id,
                user_code: _user_code,
                date: _date, datetime: new Date().getTime(),
                agent_id: clientObj.getUsername(),
                amt_charged: clientObj.getChargingRate(),
                conn_id: clientObj.getCallSessionID(), ipAddr: clientObj.getIP(),
                resolvedArray: _resolvedArray,
                smsSendArray: _smsSendArray,
                ivrSendArray: _ivrSendArray
            },
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:caresWorkCode, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + "number=" + _user_msisdn + ", workcodes=" + _workcodes + ", location_id=" + _location_id + ", user_id=" + _user_id + ", user_code=" + _user_code + ", date=" + _date + ', datetime=' + new Date().getTime() + ', agent_id=' + clientObj.getUsername() + ', amt_charged=' + clientObj.getChargingRate() + ', conn_id=' + clientObj.getCallSessionID() + ", ipAddr=" + clientObj.getIP() + '], Response:' + data, 0);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("caresWorkCode -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresWorkCode -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("caresWorkCode -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("caresWorkCode -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }});
    }
}
var unicaOfferXML = null;
var unicaOfferArray = new Array();
var unicaOfferArrayIndex = null;
var unicaEventsPosted = 0;
var unicaResegEventsPosted = 0;
var isUnicaDropDownEventCalled = false;
function getUnicaWCXML() {
    if (!clientObj.isUnicaDown()) {
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds(); //		var server = getThirdPartyServerURL();
        var server = getServerURL();
        WebLogs('WS:Going to call getUnicaWCXML, Time:[' + req_time + '], Server:[' + server + '], Request:[]', 0);
        $.ajax({
            type: "GET",
            url: server + '' + ajaxPage + "?function=getUnicaWCXML",
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:getUnicaWCXML, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '], Response:' + data, 0);
                var dropdownArray = "";
                dropdownArray = data.split(',');
                if (data.length < 1) {
                    dropdownArray = data;
                }
                var dropdownhtml = '<option value=""></option>' + "\n";
                for (var i = 0; i < dropdownArray.length; i++) {
                    var title = dropdownArray[i].split("|")[1];
                    dropdownhtml += '<option value="' + dropdownArray[i] + '">' + title + '</option>' + "\n";
                }
                $("select#resegCondition").html(dropdownhtml);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("getUnicaWCXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getUnicaWCXML -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("getUnicaWCXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getUnicaWCXML -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
}

function unicaStartSession(msisdn) {
    if (!clientObj.isUnicaDown()) {
        if (!isUnicaStartSessionCalled) {
            var date_now = new Date();
            var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
            //	 	var server = getThirdPartyServerURL();
            var server = getServerURL_Unica();
            WebLogs('WS:Going to call unicaStartSession, Time:[' + req_time + '], Server:[' + server + '], Request:[' + "sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + msisdn + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + ']', 0);
            $('#unicaOfferLoadImage').css('display', '');
            isUnicaStartSessionCalled = true;
            isUnicaEndSessionCalled = false;
            $.ajax({
                type: "GET",
                url: server + '' + ajaxPage + "?function=unicaStartSession" + "&sessionID=" + clientObj.getUnicaSessionID() + "&msisdn=" + msisdn + "&username=" + clientObj.getUsername() + "&agent_id=" + clientObj.getAgentID() + '&datetime=' + new Date().getTime() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID() + "&ipAddr=" + clientObj.getIP(), success: function (data) {
                    data = $.trim(data);
                    var date_now = new Date();
                    WebLogs('WS:unicaStartSession, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + "sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + msisdn + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + '], Response:' + data, 0);
                    if (data.split(',')[0] == "TTT" || data.split(',')[0] == "TT" || data.split(',')[0] == "T") {
                        clientObj.setUnicaResegOfferCode(null);
                        unicaResegEventsPosted = 0;
                        unicaEventsPosted = 0;
                        isUnicaDropDownEventCalled = false;
                        unicaGetOffers(false);
                    } else if ($.trim(data.split(',')[1]).length == 0) {
                        console.log("Empty result returned from tomcat");
                        WebLogs("Empty result returned from tomcat", 0);
                        resetUnicaCards();
                    } else {
                        Notifier.error('UNICA ERROR: ' + $.trim(data.split(',')[1]));
                        resetUnicaCards();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        console.log("unicaStartSession -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                        WebLogs("unicaStartSession -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                        resetUnicaCards();
                    } else {
                        console.log("unicaStartSession -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                        WebLogs("unicaStartSession -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    }
                }
            });
        }
    }
}

function unicaHeartBeat() {
    if (!clientObj.isUnicaDown()) {
        var msisdn = clientObj.getCallerMsisdn();
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//		var server = getThirdPartyServerURL();
        var server = getServerURL_Unica();
        WebLogs('WS:Going to call unicaHeartBeat, Time:[' + req_time + '], Server:[' + server + '], Request:[' + "sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + msisdn + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + ']', 0);
        $.ajax({
            type: "GET",
            url: server + '' + ajaxPage + "?function=unicaStartSession" + "&sessionID=" + clientObj.getUnicaSessionID() + "&msisdn=" + msisdn + "&username=" + clientObj.getUsername() + "&agent_id=" + clientObj.getAgentID() + '&datetime=' + new Date().getTime() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID() + "&ipAddr=" + clientObj.getIP(),
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:unicaHeartBeat, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + "sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + msisdn + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + '], Response:' + data, 0);
                if (data.split(',')[0] == "TTT" || data.split(',')[0] == "TT" || data.split(',')[0] == "T") {

                } else {
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("unicaHeartBeat -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("unicaHeartBeat -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("unicaHeartBeat -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("unicaHeartBeat -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
}

function unicaGetOffers(callPostEvent) {
    if (!clientObj.isUnicaDown()) {
        if (!isUnicaEndSessionCalled) {
            var date_now = new Date();
            var req_time1 = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
            //			var server = getThirdPartyServerURL();
            var server = getServerURL_Unica();
            WebLogs('WS:Going to call unicaGetOffers, Time:[' + req_time1 + '], Server:[' + server + '], Request:[' + "sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + clientObj.getCallerMsisdn() + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + ']', 0);
            $.ajax({
                type: "GET",
                url: server + '' + ajaxPage + "?function=unicaGetOffers" + "&sessionID=" + clientObj.getUnicaSessionID() + "&msisdn=" + clientObj.getCallerMsisdn() + "&username=" + clientObj.getUsername() + "&agent_id=" + clientObj.getAgentID() + '&datetime=' + new Date().getTime() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID() + "&ipAddr=" + clientObj.getIP(), success: function (data1) {
                    data1 = $.trim(data1);
                    var date_now1 = new Date();
                    WebLogs('WS:unicaGetOffers, Time:[' + req_time1 + ', ' + date_now1.toLocaleTimeString() + '.' + date_now1.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + "sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + clientObj.getCallerMsisdn() + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + '], Response:' + data1, 0);
                    if (data1.split(',')[0] == "TTT" || data1.split(',')[0] == "TT" || data1.split(',')[0] == "T") {
                        unicaOfferXML = data1;
                        unicaOfferArray = new Array();
                        var i = 0;
                        $(unicaOfferXML).find('offer').each(function () {
                            unicaOfferArray[i] = $(this).find('offername').text() + "|" +
                                    $(this).find('offerdesc').text() + "|" +
                                    $(this).find('offerscore').text() + "|" +
                                    $(this).find('offertreatmentcode').text() + "|" +
                                    $(this).find('offercodes offercode').first().text() + "|" + getOfferAttributesByName(this, "Offer_type") + "|" +
                                    getOfferAttributesByName(this, "AP_ID") + "|" +
                                    getOfferAttributesByName(this, "offer_min_sms") + "|" +
                                    getOfferAttributesByName(this, "OFFER_VALIDITY") + "|" +
                                    getOfferAttributesByName(this, "color") + "|" +
                                    getOfferAttributesByName(this, "VALIDITY") + "|" + getOfferAttributesByName(this, "OFFER_CHARGES") + "|" +
                                    getOfferAttributesByName(this, "Int_Reseg_Flag") + "," + getOfferAttributesByName(this, "Acp_Reseg_Flag") + "," + getOfferAttributesByName(this, "Rjt_Reseg_Flag") + "|" +
                                    getOfferAttributesByName(this, "Int_FC_Name") + "," + getOfferAttributesByName(this, "Acp_FC_Name") + "," + getOfferAttributesByName(this, "Rjt_FC_Name") + "|" +
                                    getOfferAttributesByName(this, "EVENT_NAME");
                            i++;
                        });
                        unicaOfferArrayIndex = 0;
                        last_card_clicked = 0;
                        last_card_clicked_position = 0;
                        console.log(unicaOfferArray);
                        if (callPostEvent) {
                            //unicaPostEvent('pageLoad', null+'|'+null+'|'+null+'|'+null+'|'+null+'|'+null+'|'+null+'|'+null+'|'+null+'|'+null+'|'+null+'|'+null);
                        }
                        populateUnicaCards();
                    } else if ($.trim(data1.split(',')[1]).length == 0) {
                        console.log("Empty result returned from tomcat");
                        WebLogs("Empty result returned from tomcat", 0);
                        resetUnicaCards();
                    } else {
                        Notifier.error('UNICA ERROR: ' + $.trim(data1.split(',')[1]));
                        resetUnicaCards();
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        console.log("unicaGetOffers -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                        WebLogs("unicaGetOffers -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                        resetUnicaCards();
                    } else {
                        console.log("unicaGetOffers -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                        WebLogs("unicaGetOffers -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    }
                }
            });
        }
    }
}
function getOfferAttributesByName(xmlObj, offerName) {
    var toReturn = "";
    $(xmlObj).find('offerattributes').each(function () {
        if ($(this).find('name').text().toLowerCase() == offerName.toLowerCase()) {
            toReturn = $(this).find('value').text();
        }
    });
    return toReturn;
}

function unicaPostEvent(eventName, unicaCode) {
    if (!clientObj.isUnicaDown()) {
        if (!isUnicaEndSessionCalled) {

            if (unicaEventsPosted >= clientObj.getCardsToFetch() && eventName != 'resegmentation') {
                if (!isUnicaDropDownEventCalled) {
                    resetUnicaCards();
                    return false;
                }
            }
            if (eventName == 'resegmentation') {
                if (unicaResegEventsPosted >= clientObj.getUnicaResegDropDownLimit()) {
                    $("select#resegCondition option").attr("disabled", "disabled");
                    return false;
                }
                unicaResegEventsPosted += 1;
            } else {
                unicaEventsPosted += 1;
            }

            var date_now = new Date();
            var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
            if (eventName == 'accept') {
//unicaPostEvent('pageLoad', unicaCode);
            }
            if (eventName == 'reject') {
//unicaPostEvent('pageLoad', unicaCode);
            }
            if (eventName == 'interest') {
//unicaPostEvent('pageLoad', unicaCode);
            }

            if (eventName == 'accept' && unicaCode.split("|")[9].split(",")[1] == "1.0") {
                $('#unicaOfferLoadImage').css('display', '');
            }
            if (eventName == 'reject' && unicaCode.split("|")[9].split(",")[2] == "1.0") {
                $('#unicaOfferLoadImage').css('display', '');
            }
            if (eventName == 'interest' && unicaCode.split("|")[9].split(",")[0] == "1.0") {
                $('#unicaOfferLoadImage').css('display', '');
            }
            if (eventName == 'resegmentation') {
                $('#unicaOfferLoadImage').css('display', '');
                $('select#resegCondition option:first-child').attr("selected", "selected");
            }
//			var server = getThirdPartyServerURL();
            var server = getServerURL_Unica();
            WebLogs('WS:Going to call unicaPostEvent, Time:[' + req_time + '], Server:[' + server + '], Request:[' + "eventName=" + eventName + ",unicaCode=" + unicaCode + ",sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + clientObj.getCallerMsisdn() + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + ']', 0);
            $.ajax({
                type: "GET",
                url: server + '' + ajaxPage + "?function=unicaPostEvent" + "&eventName=" + eventName + "&unicaCode=" + unicaCode + "&unicaResegOfferCode=" + clientObj.getUnicaResegOfferCode() + "&sessionID=" + clientObj.getUnicaSessionID() + "&msisdn=" + clientObj.getCallerMsisdn() + "&username=" + clientObj.getUsername() + "&agent_id=" + clientObj.getAgentID() + '&datetime=' + new Date().getTime() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID() + "&ipAddr=" + clientObj.getIP(),
                success: function (data) {
                    data = $.trim(data);
                    var date_now = new Date();
                    WebLogs('WS:unicaPostEvent, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + "eventName=" + eventName + ",unicaCode=" + unicaCode + "&unicaResegOfferCode=" + clientObj.getUnicaResegOfferCode() + ",sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + clientObj.getCallerMsisdn() + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + '], Response:' + data, 0);
                    if (unicaEventsPosted >= clientObj.getCardsToFetch() && eventName != 'resegmentation') {
                        resetUnicaCards();
                    } else if (isUnicaDropDownEventCalled) {
                        resetUnicaCards();
                    } else if (data.split(',')[0] == "TTT" || data.split(',')[0] == "TT" || data.split(',')[0] == "T") {
                        if (eventName == 'accept' && unicaCode.split("|")[9].split(",")[1] == "1.0") {
                            clientObj.setUnicaResegOfferCode(unicaCode.split("|")[1]);
                            unicaGetOffers(false);
                        }
                        if (eventName == 'reject' && unicaCode.split("|")[9].split(",")[2] == "1.0") {
                            clientObj.setUnicaResegOfferCode(unicaCode.split("|")[1]);
                            unicaGetOffers(false);
                        }
                        if (eventName == 'interest' && unicaCode.split("|")[9].split(",")[0] == "1.0") {
                            clientObj.setUnicaResegOfferCode(unicaCode.split("|")[1]);
                            unicaGetOffers(false);
                        }
                        if (eventName == 'resegmentation') {
                            if (unicaResegEventsPosted >= clientObj.getUnicaResegDropDownLimit()) {
                                $("select#resegCondition option").attr("disabled", "disabled");
                            }
                            isUnicaDropDownEventCalled = true;
                            unicaGetOffers(false);
                        }
                    } else if ($.trim(data.split(',')[1]).length == 0) {
                        console.log("Empty result returned from tomcat");
                        WebLogs("Empty result returned from tomcat", 0);
                        //						resetUnicaCards();
                    } else {
                        Notifier.error('UNICA ERROR: ' + $.trim(data.split(',')[1]));
                        //						resetUnicaCards();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        console.log("unicaPostEvent -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                        WebLogs("unicaPostEvent -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
//						resetUnicaCards();
                    } else {
                        console.log("unicaPostEvent -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                        WebLogs("unicaPostEvent -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    }
                }
            });
        }
    }
}

function unicaEndSession() {
    if (!clientObj.isUnicaDown()) {
        if (!isUnicaEndSessionCalled) {
            var date_now = new Date();
            var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
            //			var server = getThirdPartyServerURL();
            var server = getServerURL_Unica();
            WebLogs('WS:Going to call unicaEndSession, Time:[' + req_time + '], Server:[' + server + '], Request:[' + "sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + clientObj.getCallerMsisdn() + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + ']', 0);
            isUnicaEndSessionCalled = true;
            isUnicaStartSessionCalled = false;
            $.ajax({
                type: "GET",
                url: server + '' + ajaxPage + "?function=unicaEndSession" + "&sessionID=" + clientObj.getUnicaSessionID() + "&msisdn=" + clientObj.getCallerMsisdn() + "&username=" + clientObj.getUsername() + "&agent_id=" + clientObj.getAgentID() + '&datetime=' + new Date().getTime() + '&amt_charged=' + clientObj.getChargingRate() + '&conn_id=' + clientObj.getCallSessionID() + "&ipAddr=" + clientObj.getIP(),
                success: function (data) {
                    data = $.trim(data);
                    var date_now = new Date();
                    WebLogs('WS:unicaEndSession, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + "sessionID=" + clientObj.getUnicaSessionID() + ",msisdn=" + clientObj.getCallerMsisdn() + ",username=" + clientObj.getUsername() + ",agent_id=" + clientObj.getAgentID() + ',datetime=' + new Date().getTime() + ',amt_charged=' + clientObj.getChargingRate() + ',conn_id=' + clientObj.getCallSessionID() + ",ipAddr=" + clientObj.getIP() + '], Response:' + data, 0);
                    if (data.split(',')[0] == "TTT" || data.split(',')[0] == "TT" || data.split(',')[0] == "T") {
                        clientObj.setUnicaResegOfferCode(null);
                    } else if ($.trim(data.split(',')[1]).length == 0) {
                        console.log("Empty result returned from tomcat");
                        WebLogs("Empty result returned from tomcat", 0);
                        resetUnicaCards();
                    } else {
                        Notifier.error('UNICA ERROR: ' + $.trim(data.split(',')[1]));
                        resetUnicaCards();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        console.log("unicaEndSession -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                        WebLogs("unicaEndSession -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                        //resetUnicaCards();
                    } else {
                        console.log("unicaEndSession -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                        WebLogs("unicaEndSession -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    }
                }});
        }
    }
}
function getAgentStatsScreenData(agentID, time) {
    statsScreenObj.destroy();
    populateStatsScreen();
    var server = getServerURL();
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    WebLogs('WS:Going to call getAgentStatsScreenData, Time:[' + req_time + '], Server:[' + server + '], Request:[ server=' + server + "agentID=" + agentID + ",time=" + time + ']', 0);
    $.ajax({
        type: "GET",
        async: true,
        url: server + '' + ajaxPage,
        data: {
            "function": "getAgentStatsScreenData",
            agentID: agentID,
            time: time
        },
        success: function (data) {

            data = eval('(' + $.trim(data) + ')');
            var date_now = new Date();
            var current = null;
            var max = null;
            if (time == "present") {
                current = (data.presentCurrent);
                max = (data.presentMax);
            } else if (time == "past") {
                current = (data.pastCurrent);
                max = (data.pastMax);
            }
            statsScreenObj.destroy();
            statsScreenObj.acwTimeCurrent_seconds = current.acwtime;
            statsScreenObj.avgTalkTimeCurrent_seconds = current.avgtalktime;
            statsScreenObj.breakTimeCurrent_seconds = current.breaktime;
            statsScreenObj.callsAnsweredCurrent = current.callsanswered;
            statsScreenObj.callsOfferedCurrent = current.callsoffered;
            statsScreenObj.callsREQCurrent = current.callsreq;
            statsScreenObj.holdTimeCurrent_seconds = current.holdtime;
            statsScreenObj.idleTimeCurrent_seconds = current.idleduration;
            statsScreenObj.loggedInTimeCurrent_seconds = current.loggedintime;
            statsScreenObj.notReadyTimeCurrent_seconds = current.notreadytime;
            statsScreenObj.productiveDurationCurrent_seconds = current.prodduration;
            statsScreenObj.shortCallsCurrent = current.shortcalls;
            statsScreenObj.acwTimeMax_seconds = max.acwtime;
            statsScreenObj.avgTalkTimeMax_seconds = max.avgtalktime;
            statsScreenObj.breakTimeMax_seconds = max.breaktime;
            statsScreenObj.callsAnsweredMax = max.callsanswered;
            statsScreenObj.callsOfferedMax = max.callsoffered;
            statsScreenObj.callsREQMax = max.callsreq;
            statsScreenObj.holdTimeMax_seconds = max.holdtime;
            statsScreenObj.idleTimeMax_seconds = max.idleduration;
            statsScreenObj.loggedInTimeMax_seconds = max.loggedintime;
            statsScreenObj.notReadyTimeMax_seconds = max.notreadytime;
            statsScreenObj.productiveDurationMax_seconds = max.prodduration;
            statsScreenObj.shortCallsMax = max.shortcalls;
            populateStatsScreen();
            WebLogs('WS:getAgentStatsScreenData, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[ server=' + server + "agentID=" + agentID + ",time=" + time + '], Response:' + JSON.stringify(data), 0);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getAgentStatsScreenData -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getAgentStatsScreenData -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getAgentStatsScreenData -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getAgentStatsScreenData -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function getCustomerLocation(msisdn) {
    //if(clientObj.isCaresLoggedIn()) 	if (msisdn.length > 0)
    {
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//		var server = getThirdPartyServerURL();
        var server = getServerURL_Cares();
        WebLogs('WS:Going to call getCustomerLocation, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'number=' + '' + msisdn + ']', 0);
        $.ajax({
            type: "GET",
            url: server + '' + ajaxPage,
            data: {
                "function": "getCustomerLocation",
                number: msisdn,
                ipAddr: clientObj.getIP(),
                datetime: new Date().getTime(),
                agent_id: clientObj.getUsername()
            },
            async: true,
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                data = eval('(' + $.trim(data) + ')');
                WebLogs('WS:getCustomerLocation, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'number=' + '' + msisdn + '], Response:' + data.result, 0);
                if (data) {
                    if (data.result == 0) {
                        getLocationMap(data.lat, data.lon, location_url);
                        $(".map-controls-wrapper").css("display", "block");
                    }
                } else {
                    //alert("Password Change failed!!!");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("getCustomerLocation -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getCustomerLocation -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("getCustomerLocation -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getCustomerLocation -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
//else
    {
        //populateProfitSanc();
//populateCustomerDemo();
    }
}

function windowBeforeCloseLog() {
    var info = clientObj.getSystemInfo();
    var infoArray = new Array();
    infoArray = info.split('|');
    var server = getServerURL();
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    WebLogs('WS:Going to call windowBeforeCloseLog, Time:[' + req_time + '], Server:[' + server + '], Request:[ server=' + server + "ipAddr=" + clientObj.getIP() + ",agent_id=" + clientObj.getUsername() + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        data: {
            "function": "windowBeforeCloseLog",
            workstation: infoArray[3],
            ipAddr: clientObj.getIP(),
            datetime: new Date().getTime(),
            agent_id: clientObj.getUsername(),
            amt_charged: clientObj.getChargingRate(),
            conn_id: clientObj.getCallSessionID()
        },
        async: false,
        success: function (data) {
            data = $.trim(data);
            if (data) {
                _windowCloseInsertDateTime = new Date(data).getTime();
                var date_now = new Date();
                WebLogs('WS:windowBeforeCloseLog, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '],  Request:[ server=' + server + "ipAddr=" + clientObj.getIP() + ",agent_id=" + clientObj.getUsername() + '], Response:' + data, 0);
            } else {

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("windowBeforeCloseLog -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("windowBeforeCloseLog -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0); //resetUnicaCards();
            } else {
                console.log("windowBeforeCloseLog -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("windowBeforeCloseLog -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function windowCloseCancelLog() {
    var info = clientObj.getSystemInfo();
    var infoArray = new Array();
    infoArray = info.split('|');
    var server = getServerURL();
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    WebLogs('WS:Going to call windowCloseCancelLog, Time:[' + req_time + '], Server:[' + server + '], Request:[ server=' + server + "ipAddr=" + clientObj.getIP() + ",agent_id=" + clientObj.getUsername() + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        data: {
            "function": "windowCloseCancelLog",
            workstation: infoArray[3],
            ipAddr: clientObj.getIP(),
            datetime: _windowCloseInsertDateTime,
            agent_id: clientObj.getUsername(),
            amt_charged: clientObj.getChargingRate(),
            conn_id: clientObj.getCallSessionID()
        },
        async: false,
        success: function (data) {
            data = $.trim(data);
            if (data) {
                var date_now = new Date();
                WebLogs('WS:windowCloseCancelLog, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '],  Request:[ server=' + server + "ipAddr=" + clientObj.getIP() + ",agent_id=" + clientObj.getUsername() + '], Response:' + data, 0);
            } else {

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("windowCloseCancelLog -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("windowCloseCancelLog -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("windowCloseCancelLog -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("windowCloseCancelLog -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function windowCloseLog() {
    var info = clientObj.getSystemInfo();
    var infoArray = new Array();
    infoArray = info.split('|');
    var server = getServerURL();
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        data: {
            "function": "windowCloseLog",
            workstation: infoArray[3],
            ipAddr: clientObj.getIP(),
            datetime: _windowCloseInsertDateTime,
            agent_id: clientObj.getUsername(),
            amt_charged: clientObj.getChargingRate(),
            conn_id: clientObj.getCallSessionID()
        },
        async: false,
        success: function (data) {
            data = $.trim(data);
            if (data) {

            } else {

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("windowCloseLog -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("windowCloseLog -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("windowCloseLog -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("windowCloseLog -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}
function getCustomerComplaints(msisdn) {
//if(clientObj.isCaresLoggedIn())
    if (msisdn.length > 0)
    {
        $(".complaint-management-lightbox .small-complaints-panel-wrapper").html("");
        $(".cms-body-wrapper").html("");
        $(".complaint-management-lightbox .history-wrapper").css("display", "none");
        $(".complaint-management-lightbox .history-wrapper .work-code-name-wrapper .work-code-name span").html("");
        $(".complaint-management-lightbox .history-wrapper .work-code-name-wrapper .date-wrapper span").html("");
        $(".complaint-management-lightbox .history-wrapper .details-01-wrapper span").html("");
        $(".complaint-management-lightbox .history-wrapper .remarks-wrapper .remarks-details-wrapper span").html("");
        $(".complaint-management-lightbox .history-wrapper .action-remarks-wrapper .details span").html("");
        $(".complaint-management-lightbox .history-wrapper .last-action-remarks-wrapper .details span").html("");
        $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-color").css("background-color", "#5a8800");
        $(".complaint-management-lightbox .history-wrapper .status-wrapper .status-label span").html("");
        $(".complaint-management-lightbox .history-wrapper .customer-complaint-details-wrapper").html("");
        $(".cms-loader-left-div").css("display", "block");
        $(".cms-loader-right-div").css("display", "block");
        $($(".complaint-management-lightbox .window-controls .search-wrapper")[1]).css("display", "none");
        $(".complaint-management-lightbox .large-complaints-panel-wrapper .complaints-list-wrapper").css("display", "none");
        $(".complaint-management-lightbox .large-complaints-panel-wrapper .dynamic-form-wrapper").css("display", "none");
        $(".complaint-management-lightbox .large-complaints-panel-wrapper .history-wrapper").css("display", "none");
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        var server = getThirdPartyServerURL();
        WebLogs('WS:Going to call getCustomerComplaints, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'number=' + '' + msisdn + ']', 0);
        $.ajax({
            type: "GET",
            url: server + '' + ajaxPage,
            timeout: 0,
            data: {
                "function": "getCustomerComplaints",
                number: msisdn,
                cli: clientObj.getCallerMsisdn(),
                ipAddr: clientObj.getIP(),
                datetime: new Date().getTime(),
                agent_id: clientObj.getAgentID(),
                username: clientObj.getUsername(),
                amt_charged: clientObj.getChargingRate(),
                conn_id: clientObj.getCallSessionID()
            },
            async: true,
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:getCustomerComplaints, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'number=' + '' + msisdn + '], Response:' + data.substring(0, 1000), 0);
                $(".cms-loader-left-div").css("display", "none");
                $(".cms-loader-right-div").css("display", "none");
                data = eval('(' + $.trim(data) + ')');
                if (data) {
                    if (data.returnCode == 0) {
                        var openComplaintsMinPortal = '';
                        var isOpenComplaintsMinPortalLess = true;
                        var openComplaintMinPortalCount = 0;
                        for (var i = 0; i < data.taskArray.length && isOpenComplaintsMinPortalLess; i++) {
                            if (data.taskArray[i].isComplaintOpen) {
                                openComplaintsMinPortal += '<div class="complaint-wrapper" title="' + data.taskArray[i].workCodeName + '">';
                                openComplaintsMinPortal += '<div class="icon-wrapper">';
                                openComplaintsMinPortal += '<div class="messages-icon"></div>';
                                openComplaintsMinPortal += '</div>';
                                openComplaintsMinPortal += '<div class="complaint-body">';
                                openComplaintsMinPortal += '<div class="header">';
                                openComplaintsMinPortal += '<div class="heading-wrapper">';
                                var tmpDots = '';
                                if (data.taskArray[i].workCodeName.length > 20) {
                                    tmpDots = ' ...';
                                }
                                var tmpDots1 = '';
                                if (data.taskArray[i].userInformation.length > 150) {
                                    tmpDots1 = ' ...';
                                }
                                openComplaintsMinPortal += '<span class="regular-body-lucida-white">' + data.taskArray[i].workCodeName.substring(0, 20) + tmpDots + '</span>';
                                openComplaintsMinPortal += '</div>';
                                openComplaintsMinPortal += '<div class="actions">';
                                openComplaintsMinPortal += '<div class="time-wrapper">';
                                openComplaintsMinPortal += '<span class="small-lucida-italic-yellow">' + data.taskArray[i].resolutionTime + '</span>';
                                openComplaintsMinPortal += '</div>';
                                openComplaintsMinPortal += '<div class="buttons-wrapper" style="display: none;">';
                                openComplaintsMinPortal += '<button class="undo-btn"></button>';
                                openComplaintsMinPortal += '<button class="send-btn"></button>';
                                openComplaintsMinPortal += '</div>';
                                openComplaintsMinPortal += '</div>';
                                openComplaintsMinPortal += '</div>';
                                openComplaintsMinPortal += '<div class="complaint-details">';
                                openComplaintsMinPortal += '<span class="smallest-body-lucida-light-grey">' + data.taskArray[i].userInformation.substring(0, 150) + tmpDots1 + '</span>';
                                openComplaintsMinPortal += '</div>';
                                openComplaintsMinPortal += '</div>';
                                openComplaintsMinPortal += '</div>';
                                openComplaintMinPortalCount++;
                                if (openComplaintMinPortalCount >= 3) {
                                    isOpenComplaintsMinPortalLess = false;
                                } else {
                                    isOpenComplaintsMinPortalLess = true;
                                }
                            }
                        }

                        var openComplaintsMinPortalFooter = '';
                        openComplaintsMinPortalFooter += '<div class="total-complaints-wrapper">';
                        openComplaintsMinPortalFooter += '<div class="all-services-wrapper" style="display: none;">';
                        openComplaintsMinPortalFooter += '<div class="icon-details-wrapper">';
                        openComplaintsMinPortalFooter += '<div class="call-icon"></div>';
                        openComplaintsMinPortalFooter += '<div class="details">';
                        openComplaintsMinPortalFooter += '<span>3</span>';
                        openComplaintsMinPortalFooter += '</div>';
                        openComplaintsMinPortalFooter += '</div>';
                        openComplaintsMinPortalFooter += '<div class="icon-details-wrapper">';
                        openComplaintsMinPortalFooter += '<div class="messages-icon"></div>';
                        openComplaintsMinPortalFooter += '<div class="details">';
                        openComplaintsMinPortalFooter += '<span>6</span>';
                        openComplaintsMinPortalFooter += '</div>';
                        openComplaintsMinPortalFooter += '</div>';
                        openComplaintsMinPortalFooter += '<div class="icon-details-wrapper">';
                        openComplaintsMinPortalFooter += '<div class="data-icon"></div>';
                        openComplaintsMinPortalFooter += '<div class="details">';
                        openComplaintsMinPortalFooter += '<span>12</span>';
                        openComplaintsMinPortalFooter += '</div>';
                        openComplaintsMinPortalFooter += '</div>';
                        openComplaintsMinPortalFooter += '<div class="icon-details-wrapper" style="margin: 0">';
                        openComplaintsMinPortalFooter += '<div class="settings-icon"></div>';
                        openComplaintsMinPortalFooter += '<div class="details">';
                        openComplaintsMinPortalFooter += '<span>24</span>';
                        openComplaintsMinPortalFooter += '</div>';
                        openComplaintsMinPortalFooter += '</div>';
                        openComplaintsMinPortalFooter += '</div>';
                        openComplaintsMinPortalFooter += '</div>';
                        if (openComplaintMinPortalCount >= 3) {
                            $(".cms-body-wrapper").html(openComplaintsMinPortal + openComplaintsMinPortalFooter);
                        } else {
                            $(".cms-body-wrapper").html(openComplaintsMinPortal);
                        }

                        var openComplaintsHeader = '';
                        openComplaintsHeader += '<div class="heading-wrapper">';
                        openComplaintsHeader += '<span class="regular-lucida-yellow-caps-14">Open/WIP Complaints</span>';
                        openComplaintsHeader += '</div>';
                        var openComplaintsList = '';
                        var isOpenComplaintsLess = true;
                        var openComplaintCount = 0;
                        for (var i = 0; i < data.taskArray.length; i++) {
                            var hotComplaintFound = false;
                            for (var _i = 0; _i < complaintWorkCodeObjArray.length; _i++) {
                                if (complaintWorkCodeObjArray[_i].wcName.toLowerCase().indexOf("hot complaint_" + data.taskArray[i].workCodeName.toLowerCase()) != -1) {
                                    hotComplaintFound = true;
                                }
                            }
                            if (data.taskArray[i].hotComplaintEnabled && hotComplaintFound) {
                                data.taskArray[i].hotComplaintEnabled = true;
                            } else {
                                data.taskArray[i].hotComplaintEnabled = false;
                            }
                        }
                        openComplaintsList += '<div style="overflow-y: auto; overflow-x: hidden; height: 350px; width: 248px">';
                        for (var i = 0; i < data.taskArray.length && isOpenComplaintsLess; i++) {
                            if (data.taskArray[i].isComplaintOpen) {
                                openComplaintsList += '<div class="cms-complaint-wrapper" title="' + data.taskArray[i].workCodeName + '">';
                                openComplaintsList += '<input class="cmsJsonData" type="hidden" msisdn="' + data.customerMSISDN + '" value=\'' + JSON.stringify(data.taskArray[i]) + '\'>';
                                openComplaintsList += '<div class="icon-wrapper">';
                                openComplaintsList += '<div class="messages-icon"></div>';
                                openComplaintsList += '</div>';
                                openComplaintsList += '<div class="complaint-details-wrapper">';
                                openComplaintsList += '<div class="complaint-heading">';
                                var tmpDots = '';
                                if (data.taskArray[i].workCodeName.length > 20) {
                                    tmpDots = ' ...';
                                }
                                openComplaintsList += '<span class="regular-body-lucida-white">' + data.taskArray[i].workCodeName.substring(0, 20) + tmpDots + '</span>';
                                openComplaintsList += '</div>';
                                openComplaintsList += '<div class="complaint-time">';
                                openComplaintsList += '<span class="small-lucida-italic-yellow">' + data.taskArray[i].resolutionTime + '</span>';
                                openComplaintsList += '</div>';
                                if (data.taskArray[i].complaintID.trim().length > 20) {
                                    openComplaintsList += '<div style="margin: 3px 0px 0px 0px;color: #ffd200;">' + data.taskArray[i].workCodeStatus + '</div>';
                                } else if (data.taskArray[i].complaintID.trim().length <= 0) {
                                    openComplaintsList += '<div style="margin: 3px 0px 0px 0px;color: #ffd200;">' + data.taskArray[i].workCodeStatus + '</div>';
                                } else {
                                    openComplaintsList += '<div style="margin: 3px 0px 0px 0px;color: #ffd200;">' + data.taskArray[i].workCodeStatus + ', ' + data.taskArray[i].complaintID + '</div>';
                                }
                                openComplaintsList += '</div>';
                                openComplaintsList += '</div>';
                                openComplaintCount++;
                                if (openComplaintCount >= 99) {
                                    //isOpenComplaintsLess = false;
                                } else {
                                    isOpenComplaintsLess = true;
                                }
                            }
                        }
                        openComplaintsList += '</div>';
                        var openComplaintsSummary = '';
                        openComplaintsSummary += '<div class="cms-complaint-summary-wrapper">';
                        openComplaintsSummary += '<div class="summary-label">';
                        openComplaintsSummary += '<span style="font-size: 9px;" class="small-10-body-lucida-white">Summary of Open/WIP complaints</span>';
                        openComplaintsSummary += '</div>';
                        openComplaintsSummary += '<div class="summary-number-wrapper">';
                        openComplaintsSummary += '<div class="summary-number">';
                        openComplaintsSummary += '<span class="big-lucida-white">' + openComplaintCount + '</span>';
                        openComplaintsSummary += '</div>';
                        openComplaintsSummary += '</div>';
                        openComplaintsSummary += '</div>';
                        var closeComplaintsHeader = '';
                        closeComplaintsHeader += '<div class="heading-wrapper">';
                        closeComplaintsHeader += '<span class="regular-lucida-yellow-caps-14">Closed Complaints</span>';
                        closeComplaintsHeader += '</div>';
                        var closeComplaintsList = '';
                        var isCloseComplaintsLess = true;
                        var closeComplaintCount = 0;
                        closeComplaintsList += '<div style="overflow-y: auto; overflow-x: hidden; height: 350px; width: 248px">';
                        for (var i = 0; i < data.taskArray.length && isCloseComplaintsLess; i++) {
                            if (!data.taskArray[i].isComplaintOpen) {
                                closeComplaintsList += '<div class="cms-complaint-wrapper" title="' + data.taskArray[i].workCodeName + '">';
                                closeComplaintsList += '<input class="cmsJsonData" type="hidden" msisdn="' + data.customerMSISDN + '" value=\'' + JSON.stringify(data.taskArray[i]) + '\'>';
                                closeComplaintsList += '<div class="icon-wrapper">';
                                closeComplaintsList += '<div class="messages-icon"></div>';
                                closeComplaintsList += '</div>';
                                closeComplaintsList += '<div class="complaint-details-wrapper">';
                                closeComplaintsList += '<div class="complaint-heading">';
                                var tmpDots = '';
                                if (data.taskArray[i].workCodeName.length > 20) {
                                    tmpDots = ' ...';
                                }
                                closeComplaintsList += '<span class="regular-body-lucida-white">' + data.taskArray[i].workCodeName.substring(0, 20) + tmpDots + '</span>';
                                closeComplaintsList += '</div>';
                                closeComplaintsList += '<div class="complaint-time">';
                                closeComplaintsList += '<span class="small-lucida-italic-yellow">' + data.taskArray[i].resolutionTime + '</span>';
                                closeComplaintsList += '</div>';
                                if (data.taskArray[i].complaintID.trim().length > 20) {
                                    closeComplaintsList += '<div style="margin: 3px 0px 0px 0px;color: #ffd200;">' + data.taskArray[i].workCodeStatus + '</div>';
                                } else if (data.taskArray[i].complaintID.trim().length <= 0) {
                                    closeComplaintsList += '<div style="margin: 3px 0px 0px 0px;color: #ffd200;">' + data.taskArray[i].workCodeStatus + '</div>';
                                } else {
                                    closeComplaintsList += '<div style="margin: 3px 0px 0px 0px;color: #ffd200;">' + data.taskArray[i].workCodeStatus + ', ' + data.taskArray[i].complaintID + '</div>';
                                }
                                closeComplaintsList += '</div>';
                                closeComplaintsList += '</div>';
                                closeComplaintCount++;
                                if (closeComplaintCount >= 99) {
                                    //isCloseComplaintsLess = false;
                                } else {
                                    isCloseComplaintsLess = true;
                                }
                            }
                        }
                        closeComplaintsList += '</div>';
                        var closeComplaintsSummary = '';
                        closeComplaintsSummary += '<div class="cms-complaint-summary-wrapper">';
                        closeComplaintsSummary += '<div class="summary-label">';
                        closeComplaintsSummary += '<span style="font-size: 9px;" class="small-10-body-lucida-white">Summary of closed complaints</span>';
                        closeComplaintsSummary += '</div>';
                        closeComplaintsSummary += '<div class="summary-number-wrapper">';
                        closeComplaintsSummary += '<div class="summary-number">';
                        closeComplaintsSummary += '<span class="big-lucida-white">' + closeComplaintCount + '</span>';
                        closeComplaintsSummary += '</div>';
                        closeComplaintsSummary += '</div>';
                        closeComplaintsSummary += '</div>';
                        $($(".complaint-management-lightbox .small-complaints-panel-wrapper")[0]).html(openComplaintsHeader + openComplaintsList + openComplaintsSummary);
                        $($(".complaint-management-lightbox .small-complaints-panel-wrapper")[1]).html(closeComplaintsHeader + closeComplaintsList + closeComplaintsSummary);
                    }
                } else {
                    $(".cms-loader-left-div").css("display", "none");
                    $(".cms-loader-right-div").css("display", "none");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".cms-loader-left-div").css("display", "none");
                $(".cms-loader-right-div").css("display", "none");
                if (textStatus == "timeout") {
                    console.log("getCustomerComplaints -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getCustomerComplaints -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("getCustomerComplaints -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("getCustomerComplaints -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }

        });
    }
}

function declineComplaint(complaintID, remarks, msisdn) {
    {
        var date_now = new Date();
        var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
        var server = getThirdPartyServerURL();
        WebLogs('WS:Going to call declineComplaint, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'complaintID=' + '' + complaintID + ']', 0);
        $.ajax({
            type: "GET",
            url: server + '' + ajaxPage,
            timeout: 0,
            data: {
                "function": "declineComplaint",
                complaintID: complaintID,
                remarks: remarks,
                cli: clientObj.getCallerMsisdn(),
                ipAddr: clientObj.getIP(),
                agent_id: clientObj.getAgentID(),
                number: msisdn,
                datetime: new Date().getTime(),
                username: clientObj.getUsername(),
                amt_charged: clientObj.getChargingRate(),
                conn_id: clientObj.getCallSessionID()
            },
            async: true,
            success: function (data) {
                data = $.trim(data);
                var date_now = new Date();
                WebLogs('WS:declineComplaint, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'complaintID=' + '' + complaintID + '], Response:' + data, 0);
                data = eval('(' + $.trim(data) + ')');
                if (data.returnCode == 0) {
                    Notifier.info("CARES : " + data.returnDesc);
                    getCustomerComplaints(msisdn);
                } else {
                    Notifier.error("CARES : " + data.returnDesc);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (textStatus == "timeout") {
                    console.log("declineComplaint -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("declineComplaint -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                    //resetUnicaCards();
                } else {
                    console.log("declineComplaint -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                    WebLogs("declineComplaint -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
                }
            }
        });
    }
}

var complaintWorkCodeObjArray = new Array();
function getComplaintWorkCodeList() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call getComplaintWorkCodeList, Time:[' + req_time + '], Server:[' + server + '], Request:[' + '' + '' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        data: {
            "function": "getComplaintWorkCodeList"
        },
        async: true,
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getComplaintWorkCodeList, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + '' + '' + '], Response:' + data, 0);
            data = eval('(' + $.trim(data) + ')');
            if (data.length > 0) {
                complaintWorkCodeObjArray = eval(data);
                $(".complaints-list-wrapper .list-left-wrapper .mCSB_container").html("");
                $(".complaints-list-wrapper .list-right-wrapper .mCSB_container").html("");
                var complaintGroupNameHtml = '';
                var groupName = '';
                for (var i = 0; i < complaintWorkCodeObjArray.length; i++) {
                    if (groupName != complaintWorkCodeObjArray[i].wcGroupName) {
                        groupName = complaintWorkCodeObjArray[i].wcGroupName;
                        complaintGroupNameHtml += '<div class="list-item" title="' + groupName + '">';
                        complaintGroupNameHtml += '<span class="small-10-body-lucida-white">' + groupName + '</span>';
                        complaintGroupNameHtml += '</div>';
                    }
                }
                $(".complaints-list-wrapper .list-left-wrapper .mCSB_container").html(complaintGroupNameHtml);
            } else {

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getComplaintWorkCodeList -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getComplaintWorkCodeList -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getComplaintWorkCodeList -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getComplaintWorkCodeList -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

var upload_files = new Array();
function getComplaintFields(wcID, msisdn) {

    $(".complaint-management-lightbox .dynamic-form-wrapper .mCSB_container .work-code-name-wrapper").css("display", "none");
    $(".complaint-management-lightbox .dynamic-form-wrapper .form-btns-wrapper").css("display", "none");
    $(".complaint-management-lightbox .dynamic-form-wrapper .mCSB_container .work-code-name span").html("");
    $(".complaint-management-lightbox .dynamic-form-wrapper .mCSB_container .date-wrapper span").html("");
    $(".complaint-management-lightbox .dynamic-form-wrapper .mCSB_container .complaint-form-elements-wrapper").html("");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .history-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .complaints-list-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .dynamic-form-wrapper").css("display", "");
    $($(".complaint-management-lightbox .window-controls .search-wrapper")[1]).css("display", "none");
    $(".cms-loader-center-div").css("display", "block");
    $("#complaint_customer_email_0").parent().find(".complaintWCFieldObj").first().val("");
    $("#complaint_customer_email_0").val("");
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getThirdPartyServerURL();
    WebLogs('WS:Going to call getComplaintFields, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'wcID' + ' ' + wcID + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        timeout: 0,
        data: {
            "function": "getComplaintFields", wcID: wcID,
            //number: clientObj.getCallerMsisdn(),
            number: msisdn,
            ipAddr: clientObj.getIP()
        },
        async: true,
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getComplaintFields, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'wcID' + ' ' + wcID + '], Response:' + data, 0);
            data = eval('(' + $.trim(data) + ')');
            if (data.length > 0) {
                var complaintWorkCodeFieldObjArray = eval(data);
                console.log(complaintWorkCodeFieldObjArray);
                var fieldsHtml = '';
                for (var i = 0; i < complaintWorkCodeFieldObjArray.length; i++) {
                    //		 	 var wcID = complaintWorkCodeFieldObjArray[i].wcID;
                    //					var wcTitle = complaintWorkCodeFieldObjArray[i].wcTitle;
                    //					var script = complaintWorkCodeFieldObjArray[i].script;
                    // 			 var controlName = complaintWorkCodeFieldObjArray[i].controlName;
//				 var fieldName = complaintWorkCodeFieldObjArray[i].fieldName;
                    //					var fieldToolTip = complaintWorkCodeFieldObjArray[i].fieldToolTip;
                    //					var isMandatory = complaintWorkCodeFieldObjArray[i].isMandatory;
                    //			 	var seqNumber = complaintWorkCodeFieldObjArray[i].seqNumber;
                    //					var controlFieldType = complaintWorkCodeFieldObjArray[i].controlFieldType;
                    //					var autoFilledControl = complaintWorkCodeFieldObjArray[i].autoFilledControl; //					var batchLogging = complaintWorkCodeFieldObjArray[i].batchLogging;
                    //	 	 	var wcType = complaintWorkCodeFieldObjArray[i].wcType;
                    //					var lovIDForComboBox = complaintWorkCodeFieldObjArray[i].lovIDForComboBox;
                    //					var lovNameForTreeView = complaintWorkCodeFieldObjArray[i].lovNameForTreeView;
//
                    //					var autoFilledMSISDN = complaintWorkCodeFieldObjArray[i].autoFilledMSISDN;
                    //					var autoFilledAddress = complaintWorkCodeFieldObjArray[i].autoFilledAddress;
                    //		 	 var autoFilledConnectionType = complaintWorkCodeFieldObjArray[i].autoFilledConnectionType;
                    // 	 		var autoFilledNIC = complaintWorkCodeFieldObjArray[i].autoFilledNIC;
                    //					var autoFilledPackageName = complaintWorkCodeFieldObjArray[i].autoFilledPackageName;
                    //	 			var autoFilledServiceCode = complaintWorkCodeFieldObjArray[i].autoFilledServiceCode;
                    //					var autoFilledSIMNo = complaintWorkCodeFieldObjArray[i].autoFilledSIMNo;
//
//					var balance = complaintWorkCodeFieldObjArray[i].balance;
                    //					var expireTime = complaintWorkCodeFieldObjArray[i].expireTime;

                    complaintWorkCodeFieldObjArray[i].msisdn = msisdn;
                    fieldsHtml += getComplaintFieldHtml(complaintWorkCodeFieldObjArray[i]);
                }

                //				fieldsHtml += '<div style="border-bottom: 1px solid #555555;margin: 5px 15px 15px 15px;"></div>';

                var complaintFieldTmpObj_customeremail = eval(complaintWorkCodeFieldObjArray[0]);
                complaintFieldTmpObj_customeremail.fieldID = "complaint_customer_email_0";
                complaintFieldTmpObj_customeremail.isMandatory = false;
                $("#complaint_customer_email_0").parent().find(".complaintWCFieldObj").first().val(JSON.stringify(complaintFieldTmpObj_customeremail));
//				fieldsHtml += '<div class="form-element-wrapper">';
                //				fieldsHtml += '<div class="form-element-heading">';
                //	 	 fieldsHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintFieldTmpObj_customeremail) + '\' />';
                //				fieldsHtml += '<span class="regular-lucida-yellow">' + 'Customer\'s Email' + '</span>';
                //				fieldsHtml += '</div>';
                //				fieldsHtml += '<div class="form-element-body" title="' + 'Customer\'s Email' + '">';
                //				fieldsHtml += '<div class="form-element-textfield-wrapper">';
                //	 		fieldsHtml += '<input id="' + 'complaint_customer_email_0' + '" type="text" class="form-element-textfield" value="" />';
                //				fieldsHtml += '</div>';
                //				fieldsHtml += '</div>';
                //				fieldsHtml += '</div>'; 
                var complaintFieldTmpObj_fileupload = eval(complaintWorkCodeFieldObjArray[0]);
                complaintFieldTmpObj_fileupload.fieldID = "complaint_file_upload_0";
                complaintFieldTmpObj_fileupload.isMandatory = false;
                fieldsHtml += '<div class="form-element-wrapper" style="width: 300px">';
                fieldsHtml += '<div class="form-element-heading">';
                fieldsHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintFieldTmpObj_fileupload) + '\' />';
                fieldsHtml += '<span class="regular-lucida-yellow">' + 'File' + '</span>';
                fieldsHtml += '</div>';
                fieldsHtml += '<div class="form-element-body" title="' + 'File Upload' + '">';
                fieldsHtml += '<div class="form-element-textfield-wrapper" style="background: none;">';
                fieldsHtml += '<input id="' + 'complaint_file_upload_0' + '" type="file" />';
                fieldsHtml += '</div>';
                fieldsHtml += '</div>';
                fieldsHtml += '</div>';
                //				var complaintFieldTmpObj_agentremarks = eval(complaintWorkCodeFieldObjArray[0]);
//
                // 	 	complaintFieldTmpObj_agentremarks.fieldID = "complaint_agent_remarks_0";
                //				complaintFieldTmpObj_agentremarks.isMandatory = false;
//
                //				fieldsHtml += '<div class="complaint-desc-wrapper" style="display: inline-block;" title="' + "Agent Remarks" + '">'; //	 		fieldsHtml += '<input type="hidden" class="complaintWCFieldObj" value=\'' + JSON.stringify(complaintFieldTmpObj_agentremarks) + '\' />';
                // 	 	fieldsHtml += '<textarea id="' + "complaint_agent_remarks_0" + '" placeholder="' + "Agent Remarks" + '"></textarea>';
                // 	 	fieldsHtml += '</div>';
                $(".complaint-management-lightbox .dynamic-form-wrapper .mCSB_container .work-code-name-wrapper").css("display", "");
                $(".complaint-management-lightbox .dynamic-form-wrapper .form-btns-wrapper").css("display", "");
                $(".complaint-management-lightbox .dynamic-form-wrapper .mCSB_container .work-code-name span").html(complaintWorkCodeFieldObjArray[0].wcTitle);
                $(".complaint-management-lightbox .dynamic-form-wrapper .mCSB_container .date-wrapper span").html(new Date().toDateString());
                $(".complaint-management-lightbox .dynamic-form-wrapper .mCSB_container .work-code-script-div").html(complaintWorkCodeFieldObjArray[0].script);
                $(".complaint-management-lightbox .dynamic-form-wrapper .mCSB_container .complaint-form-elements-wrapper").html(fieldsHtml);
                $(".form-element-datepicker").datepicker({
                    dateFormat: 'MM dd, yy'});
                $('.dropdown-form-element').customStyle('formelement');
                $(".cms-loader-center-div").css("display", "none");
                $("#complaint_file_upload_0").live('change', function (event) {
                    upload_files = event.target.files;
                });
            } else {
                $(".cms-loader-center-div").css("display", "none");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".cms-loader-center-div").css("display", "none");
            if (textStatus == "timeout") {
                console.log("getComplaintFields -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getComplaintFields -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getComplaintFields -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getComplaintFields -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}
function logComplaint(complaintFieldObj, complaintNameValueArray, file_data, file_name) {

    $(".complaint-management-lightbox .large-complaints-panel-wrapper .history-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .dynamic-form-wrapper").css("display", "none");
    $(".complaint-management-lightbox .large-complaints-panel-wrapper .complaints-list-wrapper").css("display", "");
    $($(".complaint-management-lightbox .window-controls .search-wrapper")[1]).css("display", "");
    $(".complaints-list-wrapper .list-left-wrapper .mCSB_container .list-item").css('border', 'none');
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container .list-item").css('border', 'none');
    $(".complaints-list-wrapper .list-right-wrapper .mCSB_container").html("");
    upload_files = new Array();
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
// var server = getThirdPartyServerURL();
    var server = getServerURL_Cares();
    WebLogs('WS:Going to call logComplaint, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'complaintFieldObj' + '' + JSON.stringify(complaintFieldObj) + ', complaintNameValueArray' + '' + JSON.stringify(complaintNameValueArray) + ']', 0);
    $.ajax({
        type: "POST",
        url: server + '' + ajaxPage,
        timeout: 0,
        data: {
            "function": "logComplaint",
            agentRemarks: complaintFieldObj.agentRemarks,
            cli: clientObj.getCallerMsisdn(),
            controlsDataObj: JSON.stringify(complaintNameValueArray),
            //customerEmail: complaintFieldObj.customerEmail
            customerEmail: $("#complaint_customer_email_0").val(),
            logAgentName: clientObj.getUsername(),
            msisdn: complaintFieldObj.msisdn,
            wcId: complaintFieldObj.wcID,
            wcStatus: "New",
            wcType: complaintFieldObj.wcType,
            file_data: file_data,
            file_name: file_name,
            ipAddr: clientObj.getIP(),
            number: complaintFieldObj.msisdn,
            datetime: new Date().getTime(),
            agent_id: clientObj.getAgentID(),
            username: clientObj.getUsername(),
            amt_charged: clientObj.getChargingRate(),
            conn_id: clientObj.getCallSessionID()
        },
        async: true,
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:logComplaint, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'complaintFieldObj' + '' + JSON.stringify(complaintFieldObj) + ', complaintNameValueArray' + '' + JSON.stringify(complaintNameValueArray) + '], Response:' + data, 0);
            data = eval('(' + $.trim(data) + ')');
            if (data.returnCode == 0) {
                Notifier.info("CARES : " + "Issue successfully logged, Complaint ID : " + data.complaintId);
                var id = complaintFieldObj.wcID;
                var sms = 0;
                var name = '';
                var selectedHeadName = '';
                for (var i = 0; i < complaintWorkCodeObjArray.length; i++) {
                    if (complaintFieldObj.wcID == complaintWorkCodeObjArray[i].wcID) {
                        name = complaintWorkCodeObjArray[i].wcName;
                        selectedHeadName = complaintWorkCodeObjArray[i].wcGroupName;
                    }
                }
                var ivrcode = 'null';
                var smsstring = 'null';
                var sop = 'null';
                var wctype = 'both';
                var acwSelectedNameDiv = $('#acwSelectedName').html() + '\n';
                if ($.inArray(id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype, ACW_idArray) == -1 && id != '') {
                    acwSelectedNameDiv += '<div class="selected-option-wrapper dragable">';
                    acwSelectedNameDiv += '<div class="label">';
                    acwSelectedNameDiv += '<span class="small-10-body-lucida-white">' + name + '</span>';
                    acwSelectedNameDiv += '<input name="acwSelectedNameChkBox" class="acwSelectedNameChkBox_class" type="hidden" value="' + id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype + '" onchange="state_acwSelectedName(this)" >';
                    acwSelectedNameDiv += '</div>';
                    acwSelectedNameDiv += '<div class="remove-btn-wrapper">';
                    acwSelectedNameDiv += '<button style="display: none;" class="remove-btn" onclick="state_acwSelectedName(this)" style="z-index: 1500; "></button>';
                    acwSelectedNameDiv += '</div>';
                    acwSelectedNameDiv += '</div>';
                    ACW_idArray.push(id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype);
                }

                $('#acwSelectedName').html(acwSelectedNameDiv);
                $('.dragable').draggable({cursor: "crosshair", revert: "invalid",
                    drag: function () {
                        $(this).parent().parent().find('.selected-options-panel-wrapper').css('overflow', 'overlay');
                    },
                    stop: function () {
                        $(this).parent().parent().find('.selected-options-panel-wrapper').css('overflow-y', 'scroll');
                    }});
                $('.dropable').droppable({accept: ".dragable", drop: function (event, ui) {
                        var dropped = ui.draggable;
                        var droppedOn = $(this);
                        $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
                    }});
                $('.dropable').sortable();
                $('.drag').droppable({accept: ".dragable",
                    drop: function (event, ui) {
                        var dropped = ui.draggable;
                        var droppedOn = $(this);
                        $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
                    }});
                $('.drag').sortable(); //				alert("CARES : " + "Issue successfully logged, Complaint ID : " + data.complaintId);
            } else {
                Notifier.error("CARES : " + data.returnDesc);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("logComplaint -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("logComplaint -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("logComplaint -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("logComplaint -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function logComplaint_Hot(complaintFieldObj, complaintNameValueArray, file_data, file_name) {

    upload_files = new Array();
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    //	var server = getThirdPartyServerURL();
    var server = getServerURL_Cares();
    WebLogs('WS:Going to call logComplaint_Hot, Time:[' + req_time + '], Server:[' + server + '], Request:[' + 'complaintFieldObj' + '' + JSON.stringify(complaintFieldObj) + ', complaintNameValueArray' + '' + JSON.stringify(complaintNameValueArray) + ']', 0);
    $.ajax({
        type: "POST",
        url: server + '' + ajaxPage,
        timeout: 0,
        data: {
            "function": "logComplaint",
            agentRemarks: complaintFieldObj.agentRemarks,
            cli: clientObj.getCallerMsisdn(),
            controlsDataObj: JSON.stringify(complaintNameValueArray),
            customerEmail: "",
            logAgentName: clientObj.getUsername(),
            msisdn: complaintFieldObj.msisdn,
            wcId: complaintFieldObj.wcID,
            wcStatus: "Hot Complaint",
            wcType: complaintFieldObj.wcType,
            file_data: file_data,
            file_name: file_name,
            ipAddr: clientObj.getIP(),
            number: complaintFieldObj.msisdn,
            datetime: new Date().getTime(),
            agent_id: clientObj.getAgentID(),
            username: clientObj.getUsername(),
            amt_charged: clientObj.getChargingRate(),
            conn_id: clientObj.getCallSessionID()
        },
        async: true,
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:logComplaint_Hot, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + ',' + 'complaintFieldObj' + '' + JSON.stringify(complaintFieldObj) + ', complaintNameValueArray' + '' + JSON.stringify(complaintNameValueArray) + '], Response:' + data, 0);
            data = eval('(' + $.trim(data) + ')');
            if (data.returnCode == 0) {
                Notifier.info("CARES : " + "Hot Complaint successfully logged, Complaint ID : " + data.complaintId);
                var id = complaintFieldObj.wcID;
                var sms = 0;
                var name = '';
                var selectedHeadName = '';
                for (var i = 0; i < complaintWorkCodeObjArray.length; i++) {
                    if (complaintFieldObj.wcID == complaintWorkCodeObjArray[i].wcID) {
                        name = complaintWorkCodeObjArray[i].wcName;
                        selectedHeadName = complaintWorkCodeObjArray[i].wcGroupName;
                    }
                }
                var ivrcode = 'null';
                var smsstring = 'null';
                var sop = 'null';
                var wctype = 'both';
                var acwSelectedNameDiv = $('#acwSelectedName').html() + '\n';
                if ($.inArray(id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype, ACW_idArray) == -1 && id != '') {
                    acwSelectedNameDiv += '<div class="selected-option-wrapper dragable">';
                    acwSelectedNameDiv += '<div class="label">';
                    acwSelectedNameDiv += '<span class="small-10-body-lucida-white">' + name + '</span>';
                    acwSelectedNameDiv += '<input name="acwSelectedNameChkBox" class="acwSelectedNameChkBox_class" type="hidden" value="' + id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype + '" onchange="state_acwSelectedName(this)" >';
                    acwSelectedNameDiv += '</div>';
                    acwSelectedNameDiv += '<div class="remove-btn-wrapper">';
                    acwSelectedNameDiv += '<button style="display: none;" class="remove-btn" onclick="state_acwSelectedName(this)" style="z-index: 1500; "></button>';
                    acwSelectedNameDiv += '</div>';
                    acwSelectedNameDiv += '</div>';
                    ACW_idArray.push(id + ',' + sms + ',' + name + ',' + selectedHeadName + ',' + ivrcode + ',' + smsstring + ',' + sop + ',' + wctype);
                }

                $('#acwSelectedName').html(acwSelectedNameDiv);
                $('.dragable').draggable({cursor: "crosshair", revert: "invalid",
                    drag: function () {
                        $(this).parent().parent().find('.selected-options-panel-wrapper').css('overflow', 'overlay');
                    },
                    stop: function () {
                        $(this).parent().parent().find('.selected-options-panel-wrapper').css('overflow-y', 'scroll');
                    }});
                $('.dropable').droppable({accept: ".dragable",
                    drop: function (event, ui) {
                        var dropped = ui.draggable;
                        var droppedOn = $(this);
                        $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
                    }});
                $('.dropable').sortable();
                $('.drag').droppable({accept: ".dragable",
                    drop: function (event, ui) {
                        var dropped = ui.draggable;
                        var droppedOn = $(this);
                        $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
                    }});
                $('.drag').sortable();
//				alert("CARES : " + "Hot Complaint successfully logged, Complaint ID : " + data.complaintId);
                getCustomerComplaints(complaintFieldObj.msisdn);
            } else {
                Notifier.error("CARES : " + data.returnDesc);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("logComplaint_Hot -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("logComplaint_Hot -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("logComplaint_Hot -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("logComplaint_Hot -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function getNewsUpdate() {
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
    var server = getServerURL();
    WebLogs('WS:Going to call getNewsUpdate, Time:[' + req_time + '], Server:[' + server + '], Request:[' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        data: {
            "function": "getNewsUpdate"
        },
        async: true,
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getNewsUpdate, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + '], Response:' + data, 0);
            data = eval('(' + $.trim(data) + ')');
            if (data.result == 1) {
//				$(".notification-bar-wrapper .notification-label span").html(data.newsDesc);
                $(".notification-bar-wrapper .notification-label marquee").html(data.newsDesc);
                show_notification_bar();
                setTimeout(function () {
//					$(".notification-bar-wrapper .notification-label span").html("");
                    $(".notification-bar-wrapper .notification-label marquee").html("");
                    getNewsUpdate();
                }, news_alert_timer * 60 * 1000);
            } else {
//				$(".notification-bar-wrapper .notification-label span").html("");
                $(".notification-bar-wrapper .notification-label marquee").html("");
                hide_notification_bar();
                setTimeout(function () {
//					$(".notification-bar-wrapper .notification-label span").html("");
                    $(".notification-bar-wrapper .notification-label marquee").html("");
                    getNewsUpdate();
                }, news_alert_timer * 60 * 1000);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getNewsUpdate -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getNewsUpdate -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getNewsUpdate -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getNewsUpdate -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

var sopDataArray = new Array();
function getSOPData() {

    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//	var server = getServerURL();
    var server = getThirdPartyServerURL();
    WebLogs('WS:Going to call getSOPData, Time:[' + req_time + '], Server:[' + server + '], Request:[' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        data: {
            "function": "getSOPData"
        },
        async: true,
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getSOPData, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + '], Response:' + data, 0);
            sopDataArray = eval('(' + $.trim(data) + ')');
            $(".sop-pop-div").html('');
            $(".sop-search-field").val('');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getSOPData -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getSOPData -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getSOPData -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getSOPData -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function getTreeData(treeNode, selectObj) {

    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//	var server = getServerURL();
    var server = getThirdPartyServerURL();
    WebLogs('WS:Going to call getTreeData, Time:[' + req_time + '], Server:[' + server + '], Request:[' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        data: {
            "function": "getTreeData",
            treeHeadName: treeNode.trim()
        },
        async: true,
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getTreeData, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + '], Response:' + data, 0);
            var treeArray = (eval('(' + $.trim(data) + ')'));
            var selectInnerHtml = "";
            if (treeArray.length > 0) {
                selectInnerHtml += '<option selected="true" disabled="disabled">Choose Option</option>';
                for (var i = 0; i < treeArray.length; i++) {
                    selectInnerHtml += '<option value="' + treeArray[i].lov_value + '">' + treeArray[i].lov_name + '</option>';
                }
            }
            $(selectObj).html(selectInnerHtml);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getTreeData -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getTreeData -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getTreeData -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getTreeData -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}

function getTimeOfstats() {
    $("#statsTimeStampSpan").html("");
    $("#firstLoginTimeStampSpan").html("");
    var date_now = new Date();
    var req_time = date_now.toLocaleTimeString() + "." + date_now.getMilliseconds();
//	var server = getServerURL();
    var server = getThirdPartyServerURL();
    WebLogs('WS:Going to call getTimeOfstats, Time:[' + req_time + '], Server:[' + server + '], Request:[' + ']', 0);
    $.ajax({
        type: "GET",
        url: server + '' + ajaxPage,
        data: {
            "function": "getTimeOfstats",
            agentName: clientObj.getUsername()
        },
        async: true,
        success: function (data) {
            data = $.trim(data);
            var date_now = new Date();
            WebLogs('WS:getTimeOfstats, Time:[' + req_time + ', ' + date_now.toLocaleTimeString() + '.' + date_now.getMilliseconds() + '], Request:[' + 'server=' + server + '], Response:' + data, 0);
            if (data.length > 0) {
                var statsObj = eval('(' + data + ')');
                $("#statsTimeStampSpan").html(statsObj.timeOfStats);
                $("#firstLoginTimeStampSpan").html(statsObj.firstLogin);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus == "timeout") {
                console.log("getTimeOfstats -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getTimeOfstats -> Request timeout, Server -> " + server + ", Detail -> " + errorThrown, 0);
                //resetUnicaCards();
            } else {
                console.log("getTimeOfstats -> server not responding, Server -> " + server + ", Detail -> " + errorThrown);
                WebLogs("getTimeOfstats -> server not responding, Server -> " + server + ", Detail -> " + errorThrown, 0);
            }
        }
    });
}