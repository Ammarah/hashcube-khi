
package com.fwbl.ws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fwbl.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fwbl.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WsFeedback }
     * 
     */
    public WsFeedback createWsFeedback() {
        return new WsFeedback();
    }

    /**
     * Create an instance of {@link WsCallStatus }
     * 
     */
    public WsCallStatus createWsCallStatus() {
        return new WsCallStatus();
    }

    /**
     * Create an instance of {@link WsCallEnd }
     * 
     */
    public WsCallEnd createWsCallEnd() {
        return new WsCallEnd();
    }

    /**
     * Create an instance of {@link WsAcknowledgmentResponse }
     * 
     */
    public WsAcknowledgmentResponse createWsAcknowledgmentResponse() {
        return new WsAcknowledgmentResponse();
    }

    /**
     * Create an instance of {@link WsCallEndResponse }
     * 
     */
    public WsCallEndResponse createWsCallEndResponse() {
        return new WsCallEndResponse();
    }

    /**
     * Create an instance of {@link WsFeedbackResponse }
     * 
     */
    public WsFeedbackResponse createWsFeedbackResponse() {
        return new WsFeedbackResponse();
    }

    /**
     * Create an instance of {@link WsCallStatusResponse }
     * 
     */
    public WsCallStatusResponse createWsCallStatusResponse() {
        return new WsCallStatusResponse();
    }

    /**
     * Create an instance of {@link WsAcknowledgment }
     * 
     */
    public WsAcknowledgment createWsAcknowledgment() {
        return new WsAcknowledgment();
    }

}
