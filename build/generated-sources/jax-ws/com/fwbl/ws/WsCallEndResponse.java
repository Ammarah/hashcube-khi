
package com.fwbl.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ws_call_endResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsCallEndResult"
})
@XmlRootElement(name = "ws_call_endResponse")
public class WsCallEndResponse {

    @XmlElement(name = "ws_call_endResult")
    protected String wsCallEndResult;

    /**
     * Gets the value of the wsCallEndResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsCallEndResult() {
        return wsCallEndResult;
    }

    /**
     * Sets the value of the wsCallEndResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsCallEndResult(String value) {
        this.wsCallEndResult = value;
    }

}
