
package cms.dynamics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReturnCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ReturnDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WorkCodesList" type="{http://www.ufone.com/CMS/}WorkCodesListType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "returnCode",
    "returnDesc",
    "workCodesList"
})
@XmlRootElement(name = "RetrieveWorkCodesResponse")
public class RetrieveWorkCodesResponse {

    @XmlElement(name = "ReturnCode", required = true)
    protected String returnCode;
    @XmlElement(name = "ReturnDesc", required = true)
    protected String returnDesc;
    @XmlElement(name = "WorkCodesList")
    protected WorkCodesListType workCodesList;

    /**
     * Gets the value of the returnCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnCode(String value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the returnDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnDesc() {
        return returnDesc;
    }

    /**
     * Sets the value of the returnDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnDesc(String value) {
        this.returnDesc = value;
    }

    /**
     * Gets the value of the workCodesList property.
     * 
     * @return
     *     possible object is
     *     {@link WorkCodesListType }
     *     
     */
    public WorkCodesListType getWorkCodesList() {
        return workCodesList;
    }

    /**
     * Sets the value of the workCodesList property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkCodesListType }
     *     
     */
    public void setWorkCodesList(WorkCodesListType value) {
        this.workCodesList = value;
    }

}
