
package cares.WCMS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LovDataListBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LovDataListBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lovDataList" type="{http://Ufone-WCMS-Integration}LovDataBO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LovDataListBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "lovDataList"
})
public class LovDataListBO {

    protected List<LovDataBO> lovDataList;

    /**
     * Gets the value of the lovDataList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lovDataList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLovDataList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LovDataBO }
     * 
     * 
     */
    public List<LovDataBO> getLovDataList() {
        if (lovDataList == null) {
            lovDataList = new ArrayList<LovDataBO>();
        }
        return this.lovDataList;
    }

}
