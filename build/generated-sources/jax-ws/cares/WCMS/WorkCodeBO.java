
package cares.WCMS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WorkCodeBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkCodeBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wc_id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="wc_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wc_group_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wc_revision" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkCodeBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "wcId",
    "wcName",
    "wcGroupName",
    "wcRevision"
})
public class WorkCodeBO {

    @XmlElement(name = "wc_id")
    protected Integer wcId;
    @XmlElement(name = "wc_name")
    protected String wcName;
    @XmlElement(name = "wc_group_name")
    protected String wcGroupName;
    @XmlElement(name = "wc_revision")
    protected Integer wcRevision;

    /**
     * Gets the value of the wcId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWcId() {
        return wcId;
    }

    /**
     * Sets the value of the wcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWcId(Integer value) {
        this.wcId = value;
    }

    /**
     * Gets the value of the wcName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWcName() {
        return wcName;
    }

    /**
     * Sets the value of the wcName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWcName(String value) {
        this.wcName = value;
    }

    /**
     * Gets the value of the wcGroupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWcGroupName() {
        return wcGroupName;
    }

    /**
     * Sets the value of the wcGroupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWcGroupName(String value) {
        this.wcGroupName = value;
    }

    /**
     * Gets the value of the wcRevision property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWcRevision() {
        return wcRevision;
    }

    /**
     * Sets the value of the wcRevision property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWcRevision(Integer value) {
        this.wcRevision = value;
    }

}
