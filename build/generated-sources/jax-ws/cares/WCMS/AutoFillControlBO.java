
package cares.WCMS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AutoFillControlBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutoFillControlBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="p_msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_connection_type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_package_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_sim_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_nic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_service_code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoFillControlBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "pMsisdn",
    "pConnectionType",
    "pPackageName",
    "pSimNo",
    "pNic",
    "pAddress",
    "pServiceCode"
})
public class AutoFillControlBO {

    @XmlElementRef(name = "p_msisdn", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pMsisdn;
    @XmlElementRef(name = "p_connection_type", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pConnectionType;
    @XmlElementRef(name = "p_package_name", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pPackageName;
    @XmlElementRef(name = "p_sim_no", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pSimNo;
    @XmlElementRef(name = "p_nic", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pNic;
    @XmlElementRef(name = "p_address", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pAddress;
    @XmlElementRef(name = "p_service_code", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pServiceCode;

    /**
     * Gets the value of the pMsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPMsisdn() {
        return pMsisdn;
    }

    /**
     * Sets the value of the pMsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPMsisdn(JAXBElement<String> value) {
        this.pMsisdn = value;
    }

    /**
     * Gets the value of the pConnectionType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPConnectionType() {
        return pConnectionType;
    }

    /**
     * Sets the value of the pConnectionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPConnectionType(JAXBElement<String> value) {
        this.pConnectionType = value;
    }

    /**
     * Gets the value of the pPackageName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPPackageName() {
        return pPackageName;
    }

    /**
     * Sets the value of the pPackageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPPackageName(JAXBElement<String> value) {
        this.pPackageName = value;
    }

    /**
     * Gets the value of the pSimNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPSimNo() {
        return pSimNo;
    }

    /**
     * Sets the value of the pSimNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPSimNo(JAXBElement<String> value) {
        this.pSimNo = value;
    }

    /**
     * Gets the value of the pNic property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPNic() {
        return pNic;
    }

    /**
     * Sets the value of the pNic property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPNic(JAXBElement<String> value) {
        this.pNic = value;
    }

    /**
     * Gets the value of the pAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPAddress() {
        return pAddress;
    }

    /**
     * Sets the value of the pAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPAddress(JAXBElement<String> value) {
        this.pAddress = value;
    }

    /**
     * Gets the value of the pServiceCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPServiceCode() {
        return pServiceCode;
    }

    /**
     * Sets the value of the pServiceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPServiceCode(JAXBElement<String> value) {
        this.pServiceCode = value;
    }

}
