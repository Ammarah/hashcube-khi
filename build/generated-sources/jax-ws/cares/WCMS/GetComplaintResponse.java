
package cares.WCMS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="complaintInfo" type="{http://Ufone-WCMS-Integration}ComplaintInfo"/>
 *         &lt;element name="returnCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="returnDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "complaintInfo",
    "returnCode",
    "returnDesc"
})
@XmlRootElement(name = "getComplaintResponse")
public class GetComplaintResponse {

    @XmlElement(required = true, nillable = true)
    protected ComplaintInfo complaintInfo;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer returnCode;
    @XmlElement(required = true, nillable = true)
    protected String returnDesc;

    /**
     * Gets the value of the complaintInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComplaintInfo }
     *     
     */
    public ComplaintInfo getComplaintInfo() {
        return complaintInfo;
    }

    /**
     * Sets the value of the complaintInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplaintInfo }
     *     
     */
    public void setComplaintInfo(ComplaintInfo value) {
        this.complaintInfo = value;
    }

    /**
     * Gets the value of the returnCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReturnCode(Integer value) {
        this.returnCode = value;
    }

    /**
     * Gets the value of the returnDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnDesc() {
        return returnDesc;
    }

    /**
     * Sets the value of the returnDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnDesc(String value) {
        this.returnDesc = value;
    }

}
