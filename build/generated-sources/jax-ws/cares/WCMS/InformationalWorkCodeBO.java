
package cares.WCMS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InformationalWorkCodeBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InformationalWorkCodeBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workcodename" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complaintlogstartdate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loggedlocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loggedby" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InformationalWorkCodeBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "workcodename",
    "complaintlogstartdate",
    "loggedlocation",
    "loggedby"
})
public class InformationalWorkCodeBO {

    @XmlElementRef(name = "workcodename", type = JAXBElement.class, required = false)
    protected JAXBElement<String> workcodename;
    @XmlElementRef(name = "complaintlogstartdate", type = JAXBElement.class, required = false)
    protected JAXBElement<String> complaintlogstartdate;
    @XmlElementRef(name = "loggedlocation", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loggedlocation;
    @XmlElementRef(name = "loggedby", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loggedby;

    /**
     * Gets the value of the workcodename property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWorkcodename() {
        return workcodename;
    }

    /**
     * Sets the value of the workcodename property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWorkcodename(JAXBElement<String> value) {
        this.workcodename = value;
    }

    /**
     * Gets the value of the complaintlogstartdate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComplaintlogstartdate() {
        return complaintlogstartdate;
    }

    /**
     * Sets the value of the complaintlogstartdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComplaintlogstartdate(JAXBElement<String> value) {
        this.complaintlogstartdate = value;
    }

    /**
     * Gets the value of the loggedlocation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoggedlocation() {
        return loggedlocation;
    }

    /**
     * Sets the value of the loggedlocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoggedlocation(JAXBElement<String> value) {
        this.loggedlocation = value;
    }

    /**
     * Gets the value of the loggedby property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoggedby() {
        return loggedby;
    }

    /**
     * Sets the value of the loggedby property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoggedby(JAXBElement<String> value) {
        this.loggedby = value;
    }

}
