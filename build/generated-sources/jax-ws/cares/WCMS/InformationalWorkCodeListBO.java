
package cares.WCMS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InformationalWorkCodeListBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InformationalWorkCodeListBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InformationalWorkCodeBOList" type="{http://Ufone-WCMS-Integration}InformationalWorkCodeBO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InformationalWorkCodeListBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "informationalWorkCodeBOList"
})
public class InformationalWorkCodeListBO {

    @XmlElement(name = "InformationalWorkCodeBOList")
    protected List<InformationalWorkCodeBO> informationalWorkCodeBOList;

    /**
     * Gets the value of the informationalWorkCodeBOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the informationalWorkCodeBOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInformationalWorkCodeBOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InformationalWorkCodeBO }
     * 
     * 
     */
    public List<InformationalWorkCodeBO> getInformationalWorkCodeBOList() {
        if (informationalWorkCodeBOList == null) {
            informationalWorkCodeBOList = new ArrayList<InformationalWorkCodeBO>();
        }
        return this.informationalWorkCodeBOList;
    }

}
