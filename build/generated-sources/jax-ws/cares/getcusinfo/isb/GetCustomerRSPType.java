
package cares.getcusinfo.isb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCustomerRSPType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomerRSPType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerInfo" type="{http://www.ufone.com/GetCustomerInfo/}CustomerInfoType" minOccurs="0"/>
 *         &lt;element name="MSISDNINFO" type="{http://www.ufone.com/GetCustomerInfo/}MSISDNINFOType" minOccurs="0"/>
 *         &lt;element name="SIMINFO" type="{http://www.ufone.com/GetCustomerInfo/}SIMINFOType" minOccurs="0"/>
 *         &lt;element name="Connection" type="{http://www.ufone.com/GetCustomerInfo/}ConnectionType" minOccurs="0"/>
 *         &lt;element name="VAS" type="{http://www.ufone.com/GetCustomerInfo/}VASType" minOccurs="0"/>
 *         &lt;element name="CurrentVAS" type="{http://www.ufone.com/GetCustomerInfo/}CurrentVASType" minOccurs="0"/>
 *         &lt;element name="VPN" type="{http://www.ufone.com/GetCustomerInfo/}VPNType" minOccurs="0"/>
 *         &lt;element name="PaymentInfo" type="{http://www.ufone.com/GetCustomerInfo/}PaymentInfoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomerRSPType", propOrder = {
    "customerInfo",
    "msisdninfo",
    "siminfo",
    "connection",
    "vas",
    "currentVAS",
    "vpn",
    "paymentInfo"
})
public class GetCustomerRSPType {

    @XmlElement(name = "CustomerInfo")
    protected CustomerInfoType customerInfo;
    @XmlElement(name = "MSISDNINFO")
    protected MSISDNINFOType msisdninfo;
    @XmlElement(name = "SIMINFO")
    protected SIMINFOType siminfo;
    @XmlElement(name = "Connection")
    protected ConnectionType connection;
    @XmlElement(name = "VAS")
    protected VASType vas;
    @XmlElement(name = "CurrentVAS")
    protected CurrentVASType currentVAS;
    @XmlElement(name = "VPN")
    protected VPNType vpn;
    @XmlElement(name = "PaymentInfo")
    protected PaymentInfoType paymentInfo;

    /**
     * Gets the value of the customerInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerInfoType }
     *     
     */
    public CustomerInfoType getCustomerInfo() {
        return customerInfo;
    }

    /**
     * Sets the value of the customerInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerInfoType }
     *     
     */
    public void setCustomerInfo(CustomerInfoType value) {
        this.customerInfo = value;
    }

    /**
     * Gets the value of the msisdninfo property.
     * 
     * @return
     *     possible object is
     *     {@link MSISDNINFOType }
     *     
     */
    public MSISDNINFOType getMSISDNINFO() {
        return msisdninfo;
    }

    /**
     * Sets the value of the msisdninfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MSISDNINFOType }
     *     
     */
    public void setMSISDNINFO(MSISDNINFOType value) {
        this.msisdninfo = value;
    }

    /**
     * Gets the value of the siminfo property.
     * 
     * @return
     *     possible object is
     *     {@link SIMINFOType }
     *     
     */
    public SIMINFOType getSIMINFO() {
        return siminfo;
    }

    /**
     * Sets the value of the siminfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SIMINFOType }
     *     
     */
    public void setSIMINFO(SIMINFOType value) {
        this.siminfo = value;
    }

    /**
     * Gets the value of the connection property.
     * 
     * @return
     *     possible object is
     *     {@link ConnectionType }
     *     
     */
    public ConnectionType getConnection() {
        return connection;
    }

    /**
     * Sets the value of the connection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConnectionType }
     *     
     */
    public void setConnection(ConnectionType value) {
        this.connection = value;
    }

    /**
     * Gets the value of the vas property.
     * 
     * @return
     *     possible object is
     *     {@link VASType }
     *     
     */
    public VASType getVAS() {
        return vas;
    }

    /**
     * Sets the value of the vas property.
     * 
     * @param value
     *     allowed object is
     *     {@link VASType }
     *     
     */
    public void setVAS(VASType value) {
        this.vas = value;
    }

    /**
     * Gets the value of the currentVAS property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentVASType }
     *     
     */
    public CurrentVASType getCurrentVAS() {
        return currentVAS;
    }

    /**
     * Sets the value of the currentVAS property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentVASType }
     *     
     */
    public void setCurrentVAS(CurrentVASType value) {
        this.currentVAS = value;
    }

    /**
     * Gets the value of the vpn property.
     * 
     * @return
     *     possible object is
     *     {@link VPNType }
     *     
     */
    public VPNType getVPN() {
        return vpn;
    }

    /**
     * Sets the value of the vpn property.
     * 
     * @param value
     *     allowed object is
     *     {@link VPNType }
     *     
     */
    public void setVPN(VPNType value) {
        this.vpn = value;
    }

    /**
     * Gets the value of the paymentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentInfoType }
     *     
     */
    public PaymentInfoType getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * Sets the value of the paymentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentInfoType }
     *     
     */
    public void setPaymentInfo(PaymentInfoType value) {
        this.paymentInfo = value;
    }

}
