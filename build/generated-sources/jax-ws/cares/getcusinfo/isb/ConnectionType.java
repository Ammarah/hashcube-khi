
package cares.getcusinfo.isb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConnectionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConnectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentPackageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccessLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentAccessLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentServiceStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BarStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentBarStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FNFPlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrentFNFPlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConnectionType", propOrder = {
    "type",
    "currentType",
    "packageName",
    "currentPackageName",
    "accessLevel",
    "currentAccessLevel",
    "serviceStatus",
    "currentServiceStatus",
    "barStatus",
    "currentBarStatus",
    "fnfPlan",
    "currentFNFPlan"
})
public class ConnectionType {

    @XmlElement(name = "Type")
    protected String type;
    @XmlElement(name = "CurrentType")
    protected String currentType;
    @XmlElement(name = "PackageName")
    protected String packageName;
    @XmlElement(name = "CurrentPackageName")
    protected String currentPackageName;
    @XmlElement(name = "AccessLevel")
    protected String accessLevel;
    @XmlElement(name = "CurrentAccessLevel")
    protected String currentAccessLevel;
    @XmlElement(name = "ServiceStatus")
    protected String serviceStatus;
    @XmlElement(name = "CurrentServiceStatus")
    protected String currentServiceStatus;
    @XmlElement(name = "BarStatus")
    protected String barStatus;
    @XmlElement(name = "CurrentBarStatus")
    protected String currentBarStatus;
    @XmlElement(name = "FNFPlan")
    protected String fnfPlan;
    @XmlElement(name = "CurrentFNFPlan")
    protected String currentFNFPlan;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the currentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentType() {
        return currentType;
    }

    /**
     * Sets the value of the currentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentType(String value) {
        this.currentType = value;
    }

    /**
     * Gets the value of the packageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * Sets the value of the packageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageName(String value) {
        this.packageName = value;
    }

    /**
     * Gets the value of the currentPackageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentPackageName() {
        return currentPackageName;
    }

    /**
     * Sets the value of the currentPackageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentPackageName(String value) {
        this.currentPackageName = value;
    }

    /**
     * Gets the value of the accessLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessLevel() {
        return accessLevel;
    }

    /**
     * Sets the value of the accessLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessLevel(String value) {
        this.accessLevel = value;
    }

    /**
     * Gets the value of the currentAccessLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentAccessLevel() {
        return currentAccessLevel;
    }

    /**
     * Sets the value of the currentAccessLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentAccessLevel(String value) {
        this.currentAccessLevel = value;
    }

    /**
     * Gets the value of the serviceStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceStatus() {
        return serviceStatus;
    }

    /**
     * Sets the value of the serviceStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceStatus(String value) {
        this.serviceStatus = value;
    }

    /**
     * Gets the value of the currentServiceStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentServiceStatus() {
        return currentServiceStatus;
    }

    /**
     * Sets the value of the currentServiceStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentServiceStatus(String value) {
        this.currentServiceStatus = value;
    }

    /**
     * Gets the value of the barStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarStatus() {
        return barStatus;
    }

    /**
     * Sets the value of the barStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarStatus(String value) {
        this.barStatus = value;
    }

    /**
     * Gets the value of the currentBarStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentBarStatus() {
        return currentBarStatus;
    }

    /**
     * Sets the value of the currentBarStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentBarStatus(String value) {
        this.currentBarStatus = value;
    }

    /**
     * Gets the value of the fnfPlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFNFPlan() {
        return fnfPlan;
    }

    /**
     * Sets the value of the fnfPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFNFPlan(String value) {
        this.fnfPlan = value;
    }

    /**
     * Gets the value of the currentFNFPlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentFNFPlan() {
        return currentFNFPlan;
    }

    /**
     * Sets the value of the currentFNFPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentFNFPlan(String value) {
        this.currentFNFPlan = value;
    }

}
