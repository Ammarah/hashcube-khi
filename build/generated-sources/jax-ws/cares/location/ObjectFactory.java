
package cares.location;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cares.location package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _QueryResponseMsg_QNAME = new QName("http://www.ufone.org/GetIMSILocation/", "QueryResponseMsg");
    private final static QName _GetIMSILocationRequestMsg_QNAME = new QName("http://www.ufone.org/GetIMSILocation/", "GetIMSILocationRequestMsg");
    private final static QName _GetIMSILocationResponseMsg_QNAME = new QName("http://www.ufone.org/GetIMSILocation/", "GetIMSILocationResponseMsg");
    private final static QName _QueryRequestMsg_QNAME = new QName("http://www.ufone.org/GetIMSILocation/", "QueryRequestMsg");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cares.location
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QueryResponseMsgType }
     * 
     */
    public QueryResponseMsgType createQueryResponseMsgType() {
        return new QueryResponseMsgType();
    }

    /**
     * Create an instance of {@link GetIMSILocationRequestMsgType }
     * 
     */
    public GetIMSILocationRequestMsgType createGetIMSILocationRequestMsgType() {
        return new GetIMSILocationRequestMsgType();
    }

    /**
     * Create an instance of {@link GetIMSILocationResultMsgType }
     * 
     */
    public GetIMSILocationResultMsgType createGetIMSILocationResultMsgType() {
        return new GetIMSILocationResultMsgType();
    }

    /**
     * Create an instance of {@link QueryRequestMsgType }
     * 
     */
    public QueryRequestMsgType createQueryRequestMsgType() {
        return new QueryRequestMsgType();
    }

    /**
     * Create an instance of {@link QueryResponseType }
     * 
     */
    public QueryResponseType createQueryResponseType() {
        return new QueryResponseType();
    }

    /**
     * Create an instance of {@link GetIMSILocationRequestType }
     * 
     */
    public GetIMSILocationRequestType createGetIMSILocationRequestType() {
        return new GetIMSILocationRequestType();
    }

    /**
     * Create an instance of {@link QueryRequestType }
     * 
     */
    public QueryRequestType createQueryRequestType() {
        return new QueryRequestType();
    }

    /**
     * Create an instance of {@link GetIMSILocationResultType }
     * 
     */
    public GetIMSILocationResultType createGetIMSILocationResultType() {
        return new GetIMSILocationResultType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryResponseMsgType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ufone.org/GetIMSILocation/", name = "QueryResponseMsg")
    public JAXBElement<QueryResponseMsgType> createQueryResponseMsg(QueryResponseMsgType value) {
        return new JAXBElement<QueryResponseMsgType>(_QueryResponseMsg_QNAME, QueryResponseMsgType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIMSILocationRequestMsgType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ufone.org/GetIMSILocation/", name = "GetIMSILocationRequestMsg")
    public JAXBElement<GetIMSILocationRequestMsgType> createGetIMSILocationRequestMsg(GetIMSILocationRequestMsgType value) {
        return new JAXBElement<GetIMSILocationRequestMsgType>(_GetIMSILocationRequestMsg_QNAME, GetIMSILocationRequestMsgType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIMSILocationResultMsgType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ufone.org/GetIMSILocation/", name = "GetIMSILocationResponseMsg")
    public JAXBElement<GetIMSILocationResultMsgType> createGetIMSILocationResponseMsg(GetIMSILocationResultMsgType value) {
        return new JAXBElement<GetIMSILocationResultMsgType>(_GetIMSILocationResponseMsg_QNAME, GetIMSILocationResultMsgType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryRequestMsgType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ufone.org/GetIMSILocation/", name = "QueryRequestMsg")
    public JAXBElement<QueryRequestMsgType> createQueryRequestMsg(QueryRequestMsgType value) {
        return new JAXBElement<QueryRequestMsgType>(_QueryRequestMsg_QNAME, QueryRequestMsgType.class, null, value);
    }

}
