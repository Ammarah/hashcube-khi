
package cares.WS6;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Query_CustomerResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Res" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Result_Desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "queryCustomerResult",
    "res",
    "resultDesc"
})
@XmlRootElement(name = "Query_CustomerResponse")
public class QueryCustomerResponse {

    @XmlElement(name = "Query_CustomerResult")
    protected int queryCustomerResult;
    @XmlElement(name = "Res")
    protected String res;
    @XmlElement(name = "Result_Desc")
    protected String resultDesc;

    /**
     * Gets the value of the queryCustomerResult property.
     * 
     */
    public int getQueryCustomerResult() {
        return queryCustomerResult;
    }

    /**
     * Sets the value of the queryCustomerResult property.
     * 
     */
    public void setQueryCustomerResult(int value) {
        this.queryCustomerResult = value;
    }

    /**
     * Gets the value of the res property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRes() {
        return res;
    }

    /**
     * Sets the value of the res property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRes(String value) {
        this.res = value;
    }

    /**
     * Gets the value of the resultDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultDesc() {
        return resultDesc;
    }

    /**
     * Sets the value of the resultDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultDesc(String value) {
        this.resultDesc = value;
    }

}
