
package cares.WCMS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="areaName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CallingSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CallingIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "areaName",
    "callingSystem",
    "callingIP"
})
@XmlRootElement(name = "getCityAreaCluster")
public class GetCityAreaCluster {

    @XmlElement(required = true, nillable = true)
    protected String areaName;
    @XmlElement(name = "CallingSystem", required = true, nillable = true)
    protected String callingSystem;
    @XmlElement(name = "CallingIP", required = true, nillable = true)
    protected String callingIP;

    /**
     * Gets the value of the areaName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * Sets the value of the areaName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaName(String value) {
        this.areaName = value;
    }

    /**
     * Gets the value of the callingSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingSystem() {
        return callingSystem;
    }

    /**
     * Sets the value of the callingSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingSystem(String value) {
        this.callingSystem = value;
    }

    /**
     * Gets the value of the callingIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingIP() {
        return callingIP;
    }

    /**
     * Sets the value of the callingIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingIP(String value) {
        this.callingIP = value;
    }

}
