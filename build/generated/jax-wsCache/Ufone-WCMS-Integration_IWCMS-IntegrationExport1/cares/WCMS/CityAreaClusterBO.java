
package cares.WCMS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CityAreaClusterBO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CityAreaClusterBO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="p_area_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_city_area_cluster" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CityAreaClusterBO", namespace = "http://Ufone-WCMS-Integration", propOrder = {
    "pAreaName",
    "pCityAreaCluster"
})
public class CityAreaClusterBO {

    @XmlElementRef(name = "p_area_name", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pAreaName;
    @XmlElementRef(name = "p_city_area_cluster", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pCityAreaCluster;

    /**
     * Gets the value of the pAreaName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPAreaName() {
        return pAreaName;
    }

    /**
     * Sets the value of the pAreaName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPAreaName(JAXBElement<String> value) {
        this.pAreaName = value;
    }

    /**
     * Gets the value of the pCityAreaCluster property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPCityAreaCluster() {
        return pCityAreaCluster;
    }

    /**
     * Sets the value of the pCityAreaCluster property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPCityAreaCluster(JAXBElement<String> value) {
        this.pCityAreaCluster = value;
    }

}
