
package cares.WCMS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CallingSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CallingIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ComplaintId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "callingSystem",
    "callingIP",
    "complaintId",
    "msisdn"
})
@XmlRootElement(name = "getComplaint")
public class GetComplaint {

    @XmlElement(name = "CallingSystem", required = true, nillable = true)
    protected String callingSystem;
    @XmlElement(name = "CallingIP", required = true, nillable = true)
    protected String callingIP;
    @XmlElement(name = "ComplaintId", required = true, nillable = true)
    protected String complaintId;
    @XmlElement(name = "MSISDN", required = true, nillable = true)
    protected String msisdn;

    /**
     * Gets the value of the callingSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingSystem() {
        return callingSystem;
    }

    /**
     * Sets the value of the callingSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingSystem(String value) {
        this.callingSystem = value;
    }

    /**
     * Gets the value of the callingIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingIP() {
        return callingIP;
    }

    /**
     * Sets the value of the callingIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingIP(String value) {
        this.callingIP = value;
    }

    /**
     * Gets the value of the complaintId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplaintId() {
        return complaintId;
    }

    /**
     * Sets the value of the complaintId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplaintId(String value) {
        this.complaintId = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSISDN() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSISDN(String value) {
        this.msisdn = value;
    }

}
