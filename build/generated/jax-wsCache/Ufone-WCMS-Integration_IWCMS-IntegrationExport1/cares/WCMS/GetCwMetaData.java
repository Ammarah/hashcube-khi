
package cares.WCMS;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workCodeId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CallingSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CallingIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "workCodeId",
    "callingSystem",
    "callingIP"
})
@XmlRootElement(name = "getCwMetaData")
public class GetCwMetaData {

    @XmlElement(required = true, nillable = true)
    protected BigDecimal workCodeId;
    @XmlElement(name = "CallingSystem", required = true, nillable = true)
    protected String callingSystem;
    @XmlElement(name = "CallingIP", required = true, nillable = true)
    protected String callingIP;

    /**
     * Gets the value of the workCodeId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWorkCodeId() {
        return workCodeId;
    }

    /**
     * Sets the value of the workCodeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWorkCodeId(BigDecimal value) {
        this.workCodeId = value;
    }

    /**
     * Gets the value of the callingSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingSystem() {
        return callingSystem;
    }

    /**
     * Sets the value of the callingSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingSystem(String value) {
        this.callingSystem = value;
    }

    /**
     * Gets the value of the callingIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingIP() {
        return callingIP;
    }

    /**
     * Sets the value of the callingIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingIP(String value) {
        this.callingIP = value;
    }

}
