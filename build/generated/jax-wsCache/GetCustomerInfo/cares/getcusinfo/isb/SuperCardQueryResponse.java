
package cares.getcusinfo.isb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReturnParams" type="{http://www.ufone.com/GetCustomerInfo/}ReturnParams"/>
 *         &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "returnParams",
    "balance"
})
@XmlRootElement(name = "SuperCardQueryResponse")
public class SuperCardQueryResponse {

    @XmlElement(name = "ReturnParams", required = true)
    protected ReturnParams returnParams;
    @XmlElement(name = "Balance")
    protected String balance;

    /**
     * Gets the value of the returnParams property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnParams }
     *     
     */
    public ReturnParams getReturnParams() {
        return returnParams;
    }

    /**
     * Sets the value of the returnParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnParams }
     *     
     */
    public void setReturnParams(ReturnParams value) {
        this.returnParams = value;
    }

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalance(String value) {
        this.balance = value;
    }

}
