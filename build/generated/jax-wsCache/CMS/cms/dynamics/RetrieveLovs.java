
package cms.dynamics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SecurityParams" type="{http://www.ufone.com/CMS/}SecurityParamsType"/>
 *         &lt;element name="AgentDomainId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LovTypeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParentLovId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "securityParams",
    "agentDomainId",
    "lovTypeId",
    "parentLovId"
})
@XmlRootElement(name = "RetrieveLovs")
public class RetrieveLovs {

    @XmlElement(name = "SecurityParams", required = true)
    protected SecurityParamsType securityParams;
    @XmlElement(name = "AgentDomainId")
    protected String agentDomainId;
    @XmlElement(name = "LovTypeId")
    protected String lovTypeId;
    @XmlElement(name = "ParentLovId")
    protected String parentLovId;

    /**
     * Gets the value of the securityParams property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityParamsType }
     *     
     */
    public SecurityParamsType getSecurityParams() {
        return securityParams;
    }

    /**
     * Sets the value of the securityParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityParamsType }
     *     
     */
    public void setSecurityParams(SecurityParamsType value) {
        this.securityParams = value;
    }

    /**
     * Gets the value of the agentDomainId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentDomainId() {
        return agentDomainId;
    }

    /**
     * Sets the value of the agentDomainId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentDomainId(String value) {
        this.agentDomainId = value;
    }

    /**
     * Gets the value of the lovTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLovTypeId() {
        return lovTypeId;
    }

    /**
     * Sets the value of the lovTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLovTypeId(String value) {
        this.lovTypeId = value;
    }

    /**
     * Gets the value of the parentLovId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentLovId() {
        return parentLovId;
    }

    /**
     * Sets the value of the parentLovId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentLovId(String value) {
        this.parentLovId = value;
    }

}
