
package cms.dynamics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgentProfileListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgentProfileListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgentProfileList" type="{http://www.ufone.com/CMS/}AgentProfileItemType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgentProfileListType", propOrder = {
    "agentProfileList"
})
public class AgentProfileListType {

    @XmlElement(name = "AgentProfileList")
    protected AgentProfileItemType agentProfileList;

    /**
     * Gets the value of the agentProfileList property.
     * 
     * @return
     *     possible object is
     *     {@link AgentProfileItemType }
     *     
     */
    public AgentProfileItemType getAgentProfileList() {
        return agentProfileList;
    }

    /**
     * Sets the value of the agentProfileList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentProfileItemType }
     *     
     */
    public void setAgentProfileList(AgentProfileItemType value) {
        this.agentProfileList = value;
    }

}
