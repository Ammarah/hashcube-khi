
package com.fwbl.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ws_feedbackResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsFeedbackResult"
})
@XmlRootElement(name = "ws_feedbackResponse")
public class WsFeedbackResponse {

    @XmlElement(name = "ws_feedbackResult")
    protected String wsFeedbackResult;

    /**
     * Gets the value of the wsFeedbackResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsFeedbackResult() {
        return wsFeedbackResult;
    }

    /**
     * Sets the value of the wsFeedbackResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsFeedbackResult(String value) {
        this.wsFeedbackResult = value;
    }

}
