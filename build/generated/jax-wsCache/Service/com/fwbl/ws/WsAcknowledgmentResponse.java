
package com.fwbl.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ws_acknowledgmentResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsAcknowledgmentResult"
})
@XmlRootElement(name = "ws_acknowledgmentResponse")
public class WsAcknowledgmentResponse {

    @XmlElement(name = "ws_acknowledgmentResult")
    protected String wsAcknowledgmentResult;

    /**
     * Gets the value of the wsAcknowledgmentResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsAcknowledgmentResult() {
        return wsAcknowledgmentResult;
    }

    /**
     * Sets the value of the wsAcknowledgmentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsAcknowledgmentResult(String value) {
        this.wsAcknowledgmentResult = value;
    }

}
