
package cares.WS2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operusername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operpassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="User_Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="User_Pwd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="User_Ip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="User_host" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="User_Mac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Os_user" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rsl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Result_Desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "operusername",
    "operpassword",
    "userCode",
    "userPwd",
    "userIp",
    "userHost",
    "userMac",
    "osUser",
    "rsl",
    "resultDesc"
})
@XmlRootElement(name = "User_log_on")
public class UserLogOn {

    protected String operusername;
    protected String operpassword;
    @XmlElement(name = "User_Code")
    protected String userCode;
    @XmlElement(name = "User_Pwd")
    protected String userPwd;
    @XmlElement(name = "User_Ip")
    protected String userIp;
    @XmlElement(name = "User_host")
    protected String userHost;
    @XmlElement(name = "User_Mac")
    protected String userMac;
    @XmlElement(name = "Os_user")
    protected String osUser;
    protected String rsl;
    @XmlElement(name = "Result_Desc")
    protected String resultDesc;

    /**
     * Gets the value of the operusername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperusername() {
        return operusername;
    }

    /**
     * Sets the value of the operusername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperusername(String value) {
        this.operusername = value;
    }

    /**
     * Gets the value of the operpassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperpassword() {
        return operpassword;
    }

    /**
     * Sets the value of the operpassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperpassword(String value) {
        this.operpassword = value;
    }

    /**
     * Gets the value of the userCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserCode() {
        return userCode;
    }

    /**
     * Sets the value of the userCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserCode(String value) {
        this.userCode = value;
    }

    /**
     * Gets the value of the userPwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserPwd() {
        return userPwd;
    }

    /**
     * Sets the value of the userPwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserPwd(String value) {
        this.userPwd = value;
    }

    /**
     * Gets the value of the userIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserIp() {
        return userIp;
    }

    /**
     * Sets the value of the userIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserIp(String value) {
        this.userIp = value;
    }

    /**
     * Gets the value of the userHost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserHost() {
        return userHost;
    }

    /**
     * Sets the value of the userHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserHost(String value) {
        this.userHost = value;
    }

    /**
     * Gets the value of the userMac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserMac() {
        return userMac;
    }

    /**
     * Sets the value of the userMac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserMac(String value) {
        this.userMac = value;
    }

    /**
     * Gets the value of the osUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOsUser() {
        return osUser;
    }

    /**
     * Sets the value of the osUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOsUser(String value) {
        this.osUser = value;
    }

    /**
     * Gets the value of the rsl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsl() {
        return rsl;
    }

    /**
     * Sets the value of the rsl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsl(String value) {
        this.rsl = value;
    }

    /**
     * Gets the value of the resultDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultDesc() {
        return resultDesc;
    }

    /**
     * Sets the value of the resultDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultDesc(String value) {
        this.resultDesc = value;
    }

}
