
package cares.location;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIMSILocationRequestMsgType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIMSILocationRequestMsgType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIMSILocationRequest" type="{http://www.ufone.org/GetIMSILocation/}GetIMSILocationRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIMSILocationRequestMsgType", propOrder = {
    "getIMSILocationRequest"
})
public class GetIMSILocationRequestMsgType {

    @XmlElement(name = "GetIMSILocationRequest", required = true)
    protected GetIMSILocationRequestType getIMSILocationRequest;

    /**
     * Gets the value of the getIMSILocationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetIMSILocationRequestType }
     *     
     */
    public GetIMSILocationRequestType getGetIMSILocationRequest() {
        return getIMSILocationRequest;
    }

    /**
     * Sets the value of the getIMSILocationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetIMSILocationRequestType }
     *     
     */
    public void setGetIMSILocationRequest(GetIMSILocationRequestType value) {
        this.getIMSILocationRequest = value;
    }

}
