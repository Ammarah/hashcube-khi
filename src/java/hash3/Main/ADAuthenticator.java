/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hash3.Main;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

/**
 *
 * @author Haris
 */
public class ADAuthenticator {

	private String domain;
	private String ldapHost;
	private String searchBase;

	public ADAuthenticator() {
		/*
		 this.domain = "isbcc.com";
		 this.ldapHost = "ldap://172.16.41.101";
		 this.searchBase = "dc=isbcc,dc=com";
		 */
		this.domain = "ptml.pk";
		this.ldapHost = "ldap://172.16.12.144";
		this.searchBase = "dc=ptml,dc=pk";

	}

	public ADAuthenticator(String domain, String host, String dn) {
		this.domain = domain;
		this.ldapHost = host;
		this.searchBase = dn;
	}

	public Boolean authenticate_new(String user, String pass) {
		String returnedAtts[] = {"sn", "givenName"};
		String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";

		//Create the search controls
		SearchControls searchCtls = new SearchControls();
		//searchCtls.setReturningAttributes(returnedAtts);

		//Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		Hashtable<String, Object> env = new Hashtable<String, Object>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, ldapHost);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, user + "@" + domain);
		env.put(Context.SECURITY_CREDENTIALS, pass);
		//env.put("com.sun.jndi.ldap.trace.ber", System.out);

		LdapContext ctxGC = null;
		SearchResult searchResult = null;
		Boolean isAuth = false;

		try {
			//ctxGC = new InitialLdapContext();
			ctxGC = new InitialLdapContext(env, null);
			NamingEnumeration<SearchResult> answer = ctxGC.search(searchBase, searchFilter, searchCtls);
			if (answer.hasMoreElements()) {
				searchResult = (SearchResult) answer.nextElement();
				isAuth = true;
				if (answer.hasMoreElements()) {
					System.out.println("Matched multiple users for the accountName: " + user);
					return false;
				}
			}
			return isAuth;
		} catch (Exception ex) {
			Logger.getLogger(ADAuthenticator.class.getName()).log(Level.SEVERE, null, ex);
		}
		return isAuth;
	}

	public Map<Object, Object> authenticate(String user, String pass) {
		String returnedAtts[] = {"sn", "givenName"};
		String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";

		//Create the search controls
		SearchControls searchCtls = new SearchControls();
		searchCtls.setReturningAttributes(returnedAtts);

		//Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, ldapHost);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, user + "@" + domain);
		env.put(Context.SECURITY_CREDENTIALS, pass);

		LdapContext ctxGC = null;

		try {
			ctxGC = new InitialLdapContext(env, null);
			//Search objects in GC using filters
			NamingEnumeration<SearchResult> answer = ctxGC.search(searchBase, searchFilter, searchCtls);
			while (answer.hasMoreElements()) {
				SearchResult sr = answer.next();
				Attributes attrs = sr.getAttributes();
				Map<Object, Object> amap = null;
				if (attrs != null) {
					amap = new HashMap<Object, Object>();
					NamingEnumeration ne = attrs.getAll();
					while (ne.hasMore()) {
						Attribute attr = (Attribute) ne.next();
						amap.put(attr.getID(), attr.get());
					}
					ne.close();
				}
				return amap;
			}
		} catch (Exception ex) {
			Logger.getLogger(ADAuthenticator.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public static void main(String args[]) {
		String user_code = "farhan.m";
		String user_pwd = "laru143fani&*^";
		String result = "0|0|0|" + user_code + "|01|23|45|67|89";
		ADAuthenticator auth = new ADAuthenticator();
		Map<Object, Object> authenticate = null;
		try {
			authenticate = auth.authenticate(user_code, user_pwd);
			System.out.println(authenticate);
			if (!authenticate.isEmpty()) {
				result = "T," + result;
			} else {
				result = "F," + result;
			}
		} catch (Exception ex) {
			Logger.getLogger(ADAuthenticator.class.getName()).log(Level.SEVERE, null, ex);
			result = "F," + result;
		}
		System.out.println(result);
		System.out.println("_________________________________");
		System.out.println(auth.authenticate_new(user_code, user_pwd));
	}

}
