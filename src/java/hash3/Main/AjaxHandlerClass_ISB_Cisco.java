package hash3.Main;

import cares.getcusinfo.isb.ReturnParams;
import cares.getcusinfo.isb.SecurityParamType;
import com.sun.xml.ws.client.BindingProviderProperties;
import com.unicacorp.interact.api.AdvisoryMessage;
import com.unicacorp.interact.api.AdvisoryMessageCodes;
import com.unicacorp.interact.api.NameValuePair;
import com.unicacorp.interact.api.NameValuePairImpl;
import com.unicacorp.interact.api.Offer;
import com.unicacorp.interact.api.OfferList;
import com.unicacorp.interact.api.Response;
import com.unicacorp.interact.api.jsoverhttp.InteractAPI;
import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.BindingProvider;
import jdk.nashorn.internal.objects.Global;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class AjaxHandlerClass_ISB_Cisco {

    Connection conn = null;
    Statement stat = null;
    ResultSet rs = null;
    static int WSTimeoutSec = 5;
    int TDTimeoutSec = 5;
    int UTimeoutSec = 5;
    int DBTimeoutSec = 30;
    InteractAPI unica_api = null;
    String InteractAPIURL = null;
    boolean unica_relyOnExistingSession;
    boolean unica_initialDebugFlag;
    String unica_audienceLevel = null;
    String unica_interactiveChannel = null;
    String unica_interactionPoint = null;
    int unica_numberRequested;
    String unica_CustomLoggerTableName_Interest = null;
    int unica_Score_Interest;
    int unica_OverRideTypeID_Interest;
    String unica_Predicate_Interest = null;
    int unica_FinalScore_Interest;
    String unica_CellCode_Interest = null;
    String unica_Zone_Interest = null;
    int unica_EnableStateID_Interest;
    String unica_EventName_Interest = null;
    String unica_CustomLoggerTableName_Accept = null;
    String unica_Product_ID_Accept = null;
    String unica_EventName_Accept = null;
    String unica_EventName_PageLoad = null;
    String unica_CustomLoggerTableName_Reject = null;
    String unica_Product_ID_Reject = null;
    String unica_EventName_Reject = null;
    String unica_EventName_ReSeg = null;
    String unica_ExecuteFlowchartByName_ReSeg = null;
    Connection conn_live = null;
    Connection conn_live_2 = null;
    Connection conn_teradata = null;
    String dbUrl = "jdbc:jtds:sqlserver://localhost/Hash3";
    String userId = "sa";
    String pass = "apollopc";
//	String dbUrl_live = "jdbc:jtds:sqlserver://172.18.32.117/new_cct_live";
//	String userId_live = "sa";
//	String pass_live = "ptml@123";
//	String dbUrl_live = "jdbc:jtds:sqlserver://172.16.105.50/new_cct_live_cisco;instance=ccinstdb;";
//	String userId_live = "apollo";
//	String pass_live = "apollo@123";
    // Replace if testing local
    String dbUrl_live = "jdbc:jtds:sqlserver://172.16.12.69/new_cct_live_cisco;instance=HASHISBMAINDB;";
    String userId_live = "hashcube_cc";
    String pass_live = "ufone@123";
    //Start of localdb testing
//    String dbUrl_live = "jdbc:jtds:sqlserver://localhost/new_cct_live_cisco";
//    String userId_live = "sa";
//    String pass_live = "sa@123";
    //End of localdb Testing
//	String dbUrl_live_2 = "jdbc:jtds:sqlserver://172.16.105.195/new_cct_live";
//	String userId_live_2 = "sa";
//	String pass_live_2 = "ptml@123";
    String dbUrl_live_2 = "jdbc:jtds:sqlserver://172.16.12.19/new_cct_live;instance=ALLIEDSERVICES";
    String userId_live_2 = "hashcube_cc";
    String pass_live_2 = "ptml@123";
//	String dbUrl_live_2 = "jdbc:jtds:sqlserver://172.16.105.196/new_cct_live";
//	String userId_live_2 = "sa";
//	String pass_live_2 = "ptml@123";
    String TDurl = "jdbc:teradata://172.16.15.20/LOGMECH=TD2,CHARSET=ASCII";
    String TDuserID = "CCIT_USR";
    String TDpassword = "ptml@12345";
    String cares_username = "USSD_CALL";
    String cares_password = "call123";
    String adt_username = "ADTSERV";
    String adt_password = "ADT^SERV#135*kijwew_RTG";
    String AD_domain = "ptml.pk";
//	String AD_host = "ldap://ptml.pk";
    String AD_host = "ldap://172.16.12.144";
    String AD_dn = "dc=ptml,dc=pk";
    StatsScreenClassISBCisco statsScreenObjIsbCisco = null;

    final String ACCOUNTTYPE = "5585";//Ufone super card 333
//	StatsScreenClassLHR statsScreenObjLhr = null;

    public AjaxHandlerClass_ISB_Cisco() {
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            //Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //conn = java.sql.DriverManager.getConnection(dbUrl, userId, pass);
            conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);
            conn_live = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);
            conn_live_2 = java.sql.DriverManager.getConnection(dbUrl_live_2, userId_live_2, pass_live_2);

        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String DebugFunction(String number) {
        String toReturn = "";
        String result = "";

        try {
            //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            //conn_teradata = java.sql.DriverManager.getConnection("jdbc:odbc:TD U");
            Class.forName("com.teradata.jdbc.TeraDriver");
            conn_teradata = java.sql.DriverManager.getConnection(TDurl, TDuserID, TDpassword);
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += ex.getLocalizedMessage().trim();
            result += "<br/>" + ex.toString();
            result += "<br/>" + ex.getLocalizedMessage().trim();
            result += "<br/>";
        }

        String sql = "select * from MP_BVEW_CCIT.HASH_CUBE where msisdn=?";
        try {
            PreparedStatement pStat = conn_teradata.prepareStatement(sql);
            pStat.setString(1, number);
            rs = pStat.executeQuery(sql);
            int tmp = 0;
            while (rs.next()) {
                for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                    if (tmp == 0) {
                        result += "<b>" + rs.getMetaData().getColumnName(i + 1) + "</b>" + ";";
                    }
                    //result += rs.getString(i+1) + ";";
                }
                result += "<br/>";
                for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                    result += rs.getString(i + 1) + ";";
                }
                tmp++;
                result += "<br/>";
            }
            rs.close();
            pStat.close();
            conn_teradata.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += ex.getLocalizedMessage().trim();
            result += "<br/>" + ex.toString();
            result += "<br/>" + ex.getLocalizedMessage().trim();
            result += "<br/>";

        }
        toReturn = result;

        return toReturn;
    }

    public String HandleAjax(HttpServletRequest req) {
        String toReturn = "";
        String function = "";

        if (req.getParameter("function") != null) {
            function = req.getParameter("function");
        }

        String user_code = "";
        String user_msisdn = "";
        String ipAddr = req.getParameter("ipAddr");
        String response = "";
        String number = "";

        if (function.equals("getSystemConfigs")) {
            response = getSystemConfigs();
            toReturn = response;
        } else if (function.equals("setUnicaConfigs")) {

            response = (setUnicaConfigs1());
            toReturn = response;

        } else if (function.equals("getUnicaWCXML")) {
            response = getUnicaWCXML();
            toReturn = response;
        } else if (function.equals("DebugFunction")) {
            number = req.getParameter("number");
            response = DebugFunction(number);
            toReturn = response;

        } else if (function.equals("getAgentSkillSet_Ajax")) {
            String agentID = req.getParameter("agentID");
            response = getAgentSkillSet_Ajax(agentID);
            toReturn = response;
        } else if (function.equals("getSupervisor")) {
            response = getSupervisor();
            toReturn = response;
        } else if (function.equals("TDViewCall")) {
            number = req.getParameter("number");
            String cust_type = req.getParameter("cust_type");
            response = TDViewCall(number, cust_type);
            toReturn = response;
        } else if (function.equals("unicaStartSession")) {

            String sessionID = req.getParameter("sessionID");
            String msisdn = req.getParameter("msisdn");
            String datetime = req.getParameter("datetime");
            String username = req.getParameter("username");
            String agent_id = req.getParameter("agent_id");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");

            response = unicaStartSession(sessionID, msisdn, datetime, username, agent_id, amt_charged, conn_id, ipAddr);

            toReturn = response;
        } else if (function.equals("unicaGetOffers")) {
            String sessionID = req.getParameter("sessionID");
            String msisdn = req.getParameter("msisdn");
            String datetime = req.getParameter("datetime");
            String agent_id = req.getParameter("agent_id");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");
            String username = req.getParameter("username");

            response = unicaGetOffers(sessionID, username, msisdn, datetime, agent_id, amt_charged, conn_id, ipAddr);

            toReturn = response.replace("\n", "").replace("\r", "");

        } else if (function.equals("unicaPostEvent")) {
            String sessionID = req.getParameter("sessionID");
            String msisdn = req.getParameter("msisdn");
            String datetime = req.getParameter("datetime");
            String username = req.getParameter("username");
            String agent_id = req.getParameter("agent_id");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");
            String eventName = req.getParameter("eventName");
            String unicaCode = req.getParameter("unicaCode");
            String unicaResegOfferCode = req.getParameter("unicaResegOfferCode");

            response = unicaPostEvent(eventName, unicaCode, unicaResegOfferCode, sessionID, msisdn, datetime, agent_id, username, amt_charged, conn_id, ipAddr);

            toReturn = response;
        } else if (function.equals("unicaEndSession")) {

            String sessionID = req.getParameter("sessionID");
            String msisdn = req.getParameter("msisdn");
            String datetime = req.getParameter("datetime");
            String agent_id = req.getParameter("agent_id");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");
            String username = req.getParameter("username");

            response = unicaEndSession(sessionID, username, msisdn, datetime, agent_id, amt_charged, conn_id, ipAddr);

            toReturn = response;
        } else if (function.equals("getAgentStats")) {
            String agent_id = req.getParameter("agent_id");
            response = getAgentStats(agent_id);
            toReturn = response;
        } else if (function.equals("getSessionHistory")) {
            number = req.getParameter("number");
            String ipAddress = req.getParameter("ipAddr");
            String datetime = req.getParameter("datetime");
            String cli = req.getParameter("cli");
            String username = req.getParameter("username");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");

            response = getSessionHistory(number, datetime, username, cli, amt_charged, conn_id, ipAddress);
            toReturn = response;
        } else if (function.equals("caresHandSetInfo")) {
            number = req.getParameter("number");

            String datetime = req.getParameter("datetime");
            String agent_id = req.getParameter("agent_id");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");

            response = caresHandSetInfo(number, datetime, agent_id, amt_charged, conn_id, ipAddr);
            toReturn = response;
        } else if (function.equals("getCustomerInfo")) {
            //CARES WS Required!!!
            number = req.getParameter("number");

            String datetime = req.getParameter("datetime");
            String agent_id = req.getParameter("agent_id");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");

            response = getCustomerInfo(number, datetime, agent_id, amt_charged, conn_id, ipAddr);
            toReturn = response;
        } else if (function.equals("caresUserLogOn")) {
            //CARES WS Required!!!
            user_code = req.getParameter("user_code");
            String user_password = req.getParameter("user_password");
            String user_ip = req.getParameter("user_ip");
            String user_host = req.getParameter("user_host");
            String user_mac = req.getParameter("user_mac");
            String os_user = req.getParameter("os_user");

            String datetime = req.getParameter("datetime");
            String agent_id = user_code;
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = "null";

            response = caresUserLogOn(user_code, user_password, user_ip, user_host, user_mac, os_user,
                    datetime, agent_id, amt_charged, conn_id, ipAddr);
            toReturn = response;
        } else if (function.equals("caresUserLogOut")) {
            //CARES WS Required!!!
            user_code = req.getParameter("user_code");
            String logout_flag = req.getParameter("logout_flag");

            String datetime = req.getParameter("datetime");
            String agent_id = user_code;
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = "null";

            response = caresUserLogOut(user_code, Integer.parseInt(logout_flag), datetime, agent_id, amt_charged, conn_id, ipAddr);

            toReturn = response;
        } else if (function.equals("caresWorkCode")) {
            //CARES WS Required!!!
            user_msisdn = req.getParameter("number");
            String workcodes = req.getParameter("workcodes");
            user_code = req.getParameter("user_code");
            String date = req.getParameter("date");
            int location_id = -1;
            int user_id = -1;
            if (req.getParameter("location_id") == null) {
                location_id = -1;
            } else {
                if (req.getParameter("location_id").equals("null")) {
                    location_id = -1;
                } else {
                    try {
                        location_id = Integer.parseInt(req.getParameter("location_id"));
                    } catch (Exception ex) {
                        Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                        location_id = -1;
                    }
                }
            }
            try {
                user_id = Integer.parseInt(req.getParameter("user_id"));
            } catch (Exception ex) {
                Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                user_id = -1;
            }
            String datetime = req.getParameter("datetime");
            String agent_id = user_code;
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");

            String resolvedArray = req.getParameter("resolvedArray");
            String smsSendArray = req.getParameter("smsSendArray");
            String ivrSendArray = req.getParameter("ivrSendArray");

            response = caresWorkCode(user_msisdn, workcodes, user_code, location_id, user_id, date,
                    datetime, agent_id, amt_charged, conn_id, ipAddr, resolvedArray, smsSendArray, ivrSendArray);
            toReturn = response;
        } else if (function.equals("caresPostCallAdjustment")) {
            //CARES WS Required!!!
            user_msisdn = req.getParameter("user_msisdn");
            String agent_detail = req.getParameter("agent_detail");

            String _ndfStatus = req.getParameter("_ndfStatus");
            String _callid = req.getParameter("_callid");
            String _clid = user_msisdn;
            String _calltype = req.getParameter("_calltype");
            String _details = agent_detail;
            String _caresstatus = req.getParameter("_caresstatus");
            String _caredt = req.getParameter("_caredt");
            String _insertiondt = req.getParameter("_insertiondt");
            String _switchid = req.getParameter("_switchid");
            String _dnis = req.getParameter("_dnis");
            String _mobno = req.getParameter("_mobno");
            String _abal = req.getParameter("_abal");
            String _custtype = req.getParameter("_custtype");
            String _prodtype = req.getParameter("_prodtype");
            String _amtcharged = req.getParameter("_amtcharged");
            String _agentid = req.getParameter("_agentid");
            String _retries = req.getParameter("_retries");
            String _lastretry = req.getParameter("_lastretry");

            String datetime = _insertiondt;
            String agent_id = req.getParameter("agent_id");
            String amt_charged = _amtcharged;
            String conn_id = _callid;

            response = caresPostCallAdjustment(user_msisdn, agent_detail, _callid, _clid, _calltype, _details,
                    _caresstatus, _caredt, _insertiondt, _switchid, _dnis, _mobno, _abal, _custtype, _prodtype,
                    _amtcharged, _agentid, _retries, _lastretry, datetime, agent_id, amt_charged, conn_id, ipAddr, _ndfStatus);
            toReturn = response;
        } else if (function.equals("caresChangePassword")) {
            //CARES WS Required!!!
            user_code = req.getParameter("user_code");
            String old_password = req.getParameter("old_password");
            String new_password = req.getParameter("new_password");

            String datetime = req.getParameter("datetime");
            String agent_id = user_code;
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = "null";

            response = caresChangePassword(user_code, old_password, new_password, datetime,
                    agent_id, amt_charged, conn_id, ipAddr);
            toReturn = response;
        } else if (function.equals("getNotReadyCode")) {
            String code = req.getParameter("notReadyCode");
            response = getNotReadyCode(code);
            toReturn = response;
        } else if (function.equals("setCaresIntegration")) {
            //SP Required!!!
            String caller_cli = req.getParameter("callerCli");
            String insert_datetime = req.getParameter("insert_datetime");
            String cares_id = req.getParameter("cares_id");
            response = setCaresIntegration(caller_cli, ipAddr, insert_datetime, cares_id);
            toReturn = response;
        } else if (function.equals("getConsentInfo")) {
            String ipPhoneType = req.getParameter("ipPhoneType");
            response = getConsentInfo(ipPhoneType);
            toReturn = response;
        } else if (function.equals("setCallDetails")) {
            //SP Required!!!
            String _call_con_id = req.getParameter("_call_con_id");
            String _call_duration = req.getParameter("_call_duration");
            String _call_time_in_ivr = req.getParameter("_call_time_in_ivr");
            String _call_trans_type = req.getParameter("_call_trans_type");
            String _call_dn_in = req.getParameter("_call_dn_in");
            String _call_dn_out = req.getParameter("_call_dn_out");
            String _call_trans_ivr_menu = req.getParameter("_call_trans_ivr_menu");
            String _call_apps = req.getParameter("_call_apps");
            String _call_cms_flag = req.getParameter("_call_cms_flag");
            String _agent_login_name = req.getParameter("_agent_login_name");
            String _agent_login_id = req.getParameter("_agent_login_id");
            String _agent_name = req.getParameter("_agent_name");
            String _agent_workstation = req.getParameter("_agent_workstation");
            String _cust_mobile = req.getParameter("_cust_mobile");
            String _cust_cli = req.getParameter("_cust_cli");
            String _cust_type = req.getParameter("_cust_type");
            String _cust_name = req.getParameter("_cust_name");
            String _cust_language = req.getParameter("_cust_language");
            String _cust_rating = req.getParameter("_cust_rating");
            String _cust_package = req.getParameter("_cust_package");
            String _cust_access_level = req.getParameter("_cust_access_level");
            String _cust_product = req.getParameter("_cust_product");
            String _cust_balance_amt = req.getParameter("_cust_balance_amt");
            String _cust_prepaid_expiry = req.getParameter("_cust_prepaid_expiry");
            String _skillset = req.getParameter("_skillset");
            String _dnis = req.getParameter("_dnis");
            String _call_start_date = req.getParameter("_call_start_date");
            String _call_end_date = req.getParameter("_call_end_date");
            String _agent_skillset = req.getParameter("_agent_skillset");
            String _cust_mobile_model = req.getParameter("_cust_mobile_model");

            response = setCallDetails(_call_con_id, _call_duration, _call_time_in_ivr, _call_trans_type, _call_dn_in, _call_dn_out,
                    _call_trans_ivr_menu, _call_apps, _call_cms_flag, _agent_login_name, _agent_login_id, _agent_name, _agent_workstation,
                    _cust_mobile, _cust_cli, _cust_type, _cust_name, _cust_language, _cust_rating, _cust_package, _cust_access_level,
                    _cust_product, _cust_balance_amt, _cust_prepaid_expiry, _skillset, _dnis, _call_start_date, _call_end_date, ipAddr, _agent_skillset, _cust_mobile_model);

            toReturn = response;
        } else if (function.equals("updateNotReadyCodes")) {
            response = updateNotReadyCodes();
            toReturn = response;
        } else if (function.equals("getIVRXML")) {
            response = getIVRXML();
            toReturn = response;
        } else if (function.equals("getFreqIVR")) {

            response = getFreqIVR();
            toReturn = response;

        } else if (function.equals("getPIAAWCXML")) {
            response = getPIAAWCXML();
            toReturn = response;
        } else if (function.equals("getAWCXML")) {
            response = getAWCXML();
            toReturn = response;
        } else if (function.equals("getFreqWC")) {
            response = getFreqWC();
            toReturn = response;
        } else if (function.equals("insertUnicaOffersPresented")) {
            response = insertUnicaOffersPresented(userId, pass, userId, user_code, userId, TDurl);
            toReturn = response;
        } else if (function.equals("insertUnicaOfferAction")) {
            response = insertUnicaOfferAction(userId, pass, userId, TDurl);
            toReturn = response;
        } else if (function.equals("getAgentStatsScreenData")) {
            String agentID = req.getParameter("agentID");
            String time = req.getParameter("time");
            statsScreenObjIsbCisco = new StatsScreenClassISBCisco(dbUrl_live_2, userId_live_2, pass_live_2);
            if (time.equals("present")) {
                toReturn = statsScreenObjIsbCisco.getAgentStatsScreenDataPresent(agentID);
            } else if (time.equals("past")) {
                toReturn = statsScreenObjIsbCisco.getAgentStatsScreenDataPast(agentID);
            } else {
                toReturn = "";
            }
            statsScreenObjIsbCisco.closeDBConnections();
            statsScreenObjIsbCisco = null;
//			statsScreenObjLhr = new StatsScreenClassLHR(dbUrl_live_2, userId_live_2, pass_live_2);
//			if (time.equals("present")) {
//				toReturn = "";
//				toReturn = statsScreenObjLhr.getAgentStatsScreenDataPresent(agentID);
//			} else if (time.equals("past")) {
//				toReturn = "";
//				toReturn = statsScreenObjLhr.getAgentStatsScreenDataPast(agentID);
//			} else {
//				toReturn = "";
//			}
//			statsScreenObjLhr = null;
        } else if (function.equals("windowBeforeCloseLog")) {

            String datetime = req.getParameter("datetime");
            String agent_id = req.getParameter("agent_id");
            String workstation = req.getParameter("workstation");
            String ipaddress = req.getParameter("ipAddr");

            response = windowBeforeCloseLog(datetime, agent_id, workstation, ipaddress);
            toReturn = response;
        } else if (function.equals("windowCloseCancelLog")) {

            String datetime = req.getParameter("datetime");
            String agent_id = req.getParameter("agent_id");
            String workstation = req.getParameter("workstation");
            String ipaddress = req.getParameter("ipAddr");

            response = windowCloseCancelLog(datetime, agent_id, workstation, ipaddress);
            toReturn = response;
        } else if (function.equals("windowCloseLog")) {

            String datetime = req.getParameter("datetime");
            String agent_id = req.getParameter("agent_id");
            String workstation = req.getParameter("workstation");
            String ipaddress = req.getParameter("ipAddr");

            response = windowCloseLog(datetime, agent_id, workstation, ipaddress);
            toReturn = response;
        } else if (function.equals("getCustomerLocation")) {
            number = req.getParameter("number");
            LocationClass locationObj = new LocationClass();
            response = locationObj.getLocationCoordinates(number);
            toReturn = response;
            locationObj = null;
        } else if (function.equals("getCustomerComplaints")) {
            number = req.getParameter("number");
            String datetime = req.getParameter("datetime");
            String agent_id = req.getParameter("agent_id");
            String ipaddress = req.getParameter("ipAddr");
            String username = req.getParameter("username");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");
            String cli = req.getParameter("cli");
            WCMSClassCiscoISB wcmsClassISB = new WCMSClassCiscoISB();
            response = wcmsClassISB.getTasks(number, ipaddress, datetime, agent_id, username, amt_charged, conn_id, cli);
            toReturn = response;
            wcmsClassISB = null;
        } else if (function.equals("declineComplaint")) {
            number = req.getParameter("number");
            String remarks = req.getParameter("remarks");
            String complaintID = req.getParameter("complaintID");
            String agent_id = req.getParameter("agent_id");
            String ipaddress = req.getParameter("ipAddr");
            String datetime = req.getParameter("datetime");
            String username = req.getParameter("username");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");
            String cli = req.getParameter("cli");
            WCMSClassCiscoISB wcmsClassISB = new WCMSClassCiscoISB();
            response = wcmsClassISB.declineTicket(complaintID, remarks, agent_id, ipaddress, number,
                    datetime, username, amt_charged, conn_id, cli);
            toReturn = response;
            wcmsClassISB = null;
        } else if (function.equals("getComplaintWorkCodeList")) {
            WCMSClassCiscoISB wcmsClassISB = new WCMSClassCiscoISB();
            response = wcmsClassISB.getComplaintWorkCodeList();
            toReturn = response;
            wcmsClassISB = null;
        } else if (function.equals("getComplaintFields")) {
            String wcID = req.getParameter("wcID");
            String ipaddress = req.getParameter("ipAddr");
            number = req.getParameter("number");
            WCMSClassCiscoISB wcmsClassISB = new WCMSClassCiscoISB();
            response = wcmsClassISB.getComplaintFields(wcID, number, ipaddress);
            toReturn = response;
            wcmsClassISB = null;
        } else if (function.equals("logComplaint")) {
            String agentRemarks = req.getParameter("agentRemarks");
            String cli = req.getParameter("cli");
            String controlsDataObjArray = req.getParameter("controlsDataObj");
            String customerEmail = req.getParameter("customerEmail");
            String logAgentName = req.getParameter("logAgentName");
            String msisdn = req.getParameter("msisdn");
            String wcId = req.getParameter("wcId");
            String wcStatus = req.getParameter("wcStatus");
            String wcType = req.getParameter("wcType");
            String ipaddress = req.getParameter("ipAddr");

            String datetime = req.getParameter("datetime");
            String agent_id = req.getParameter("agent_id");
            String username = req.getParameter("username");
            String amt_charged = req.getParameter("amt_charged");
            String conn_id = req.getParameter("conn_id");

            String file_data = req.getParameter("file_data");
            String file_name = req.getParameter("file_name");

            JSONArray _controlsDataObjArray = new JSONArray();

            JSONParser parser = new JSONParser();
            try {
                _controlsDataObjArray = (JSONArray) parser.parse(controlsDataObjArray);
            } catch (ParseException ex) {
                Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            }
            WCMSClassCiscoISB wcmsClassISB = new WCMSClassCiscoISB();
            response = wcmsClassISB.logComplaint(agentRemarks, cli, _controlsDataObjArray,
                    customerEmail, logAgentName, msisdn, wcId, wcStatus, wcType, ipaddress,
                    datetime, agent_id, username, amt_charged, conn_id, file_data, file_name);
            toReturn = response;
            wcmsClassISB = null;
        } else if (function.equals("getNewsUpdate")) {
            response = getNewsUpdate();
            toReturn = response;
        } else if (function.equals("getTreeData")) {
            String treeHeadName = req.getParameter("treeHeadName");
            WCMSClassCiscoISB wcmsClassISB = new WCMSClassCiscoISB();
            response = wcmsClassISB.getTreeData(treeHeadName);
            toReturn = response;
            wcmsClassISB = null;
        } else if (function.equals("getSOPData")) {
            SOPLinksClass sopObj = new SOPLinksClass();
            response = sopObj.getAllSOPs();
            toReturn = response;
            sopObj.closeDBConnections();
            sopObj = null;
        } else if (function.equals("getTimeOfstats")) {
            String agentName = req.getParameter("agentName");
            statsScreenObjIsbCisco = new StatsScreenClassISBCisco(dbUrl_live_2, userId_live_2, pass_live_2);
            toReturn = statsScreenObjIsbCisco.getTimeOfstats(agentName);
            statsScreenObjIsbCisco.closeDBConnections();
            statsScreenObjIsbCisco = null;
        }
        try {
            rs.close();
        } catch (Exception ex) {
        }
        try {
            stat.close();
        } catch (Exception ex) {
        }
        try {
            conn.close();
        } catch (Exception ex) {
        }
        try {
            conn_live.close();
        } catch (Exception ex) {
        }
        try {
            conn_live_2.close();
        } catch (Exception ex) {
        }
        try {
            conn_teradata.close();
        } catch (Exception ex) {
        }

        rs = null;
        stat = null;
        conn = null;
        conn_live = null;
        conn_live_2 = null;
        conn_teradata = null;
        statsScreenObjIsbCisco = null;
//		statsScreenObjLhr = null;

        return toReturn;
    }

    private String setCallDetails(String _call_con_id, String _call_duration,
            String _call_time_in_ivr, String _call_trans_type,
            String _call_dn_in, String _call_dn_out,
            String _call_trans_ivr_menu, String _call_apps,
            String _call_cms_flag, String _agent_login_name,
            String _agent_login_id, String _agent_name,
            String _agent_workstation, String _cust_mobile, String _cust_cli,
            String _cust_type, String _cust_name, String _cust_language,
            String _cust_rating, String _cust_package,
            String _cust_access_level, String _cust_product,
            String _cust_balance_amt, String _cust_prepaid_expiry,
            String _skillset, String _dnis, String _call_start_date, String _call_end_date,
            String _ip_address, String _agent_skillset, String _cust_mobile_model) {
        String toReturn = "";
        java.sql.Timestamp start_date = null;
        java.sql.Timestamp end_date = null;
        try {
            CallableStatement proc = conn_live.prepareCall("{ call sp_InsertIntoCobweb_v1(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");

            proc.setString("@para_callconnectionid", _call_con_id);
            start_date = new java.sql.Timestamp(Long.parseLong(_call_start_date));
            proc.setTimestamp("@para_startdatetime", start_date);
            end_date = new java.sql.Timestamp(Long.parseLong(_call_end_date));
            proc.setTimestamp("@para_enddatetime", end_date);
            proc.setInt("@para_month", end_date.getMonth() + 1);
            try {
                if (_call_duration.equals("null") || _call_duration == null) {
                    proc.setNull("@para_duration", java.sql.Types.NULL);
                } else {
                    proc.setInt("@para_duration", new Float(Float.parseFloat(_call_duration)).intValue());
                }
            } catch (Exception ex) {
                proc.setNull("@para_duration", java.sql.Types.NULL);
            }
            try {
                if (_call_time_in_ivr.equals("null") || _call_time_in_ivr == null) {
                    proc.setNull("@para_timeinivr", java.sql.Types.NULL);
                } else {
                    proc.setInt("@para_timeinivr", Integer.parseInt(_call_time_in_ivr));
                }
            } catch (Exception ex) {
                proc.setNull("@para_timeinivr", java.sql.Types.NULL);
            }
            try {
                if (_call_trans_type.equals("null") || _call_trans_type == null) {
                    proc.setNull("@para_calltranstype", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_calltranstype", _call_trans_type);
                }
            } catch (Exception ex) {
                proc.setNull("@para_calltranstype", java.sql.Types.NULL);
            }
            try {

                if (_call_dn_in.equals("null") || _call_dn_in == null) {
                    proc.setNull("@para_dnin", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_dnin", _call_dn_in);
                }
            } catch (Exception ex) {
                proc.setNull("@para_dnin", java.sql.Types.NULL);
            }
            try {

                if (_call_dn_out.equals("null") || _call_dn_out == null) {
                    proc.setNull("@para_dnout", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_dnout", _call_dn_out);
                }
            } catch (Exception ex) {
                proc.setNull("@para_dnout", java.sql.Types.NULL);
            }
            try {

                if (_call_trans_ivr_menu.equals("null") || _call_trans_ivr_menu == null) {
                    proc.setNull("@para_calltransivrmenu", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_calltransivrmenu", _call_trans_ivr_menu);
                }
            } catch (Exception ex) {
                proc.setNull("@para_calltransivrmenu", java.sql.Types.NULL);
            }
            try {

                if (_call_apps.equals("null") || _call_apps == null) {
                    proc.setNull("@para_apps", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_apps", _call_apps);
                }
            } catch (Exception ex) {
                proc.setNull("@para_apps", java.sql.Types.NULL);
            }
            try {

                if (_call_cms_flag.equals("null") || _call_cms_flag == null) {
                    proc.setNull("@para_cmsflag", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_cmsflag", _call_cms_flag);
                }
            } catch (Exception ex) {
                proc.setNull("@para_cmsflag", java.sql.Types.NULL);
            }
            try {

                if (_agent_login_name.equals("null") || _agent_login_name == null) {
                    proc.setNull("@para_loginname", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_loginname", _agent_login_name);
                }
            } catch (Exception ex) {
                proc.setNull("@para_loginname", java.sql.Types.NULL);
            }
            try {

                if (_agent_login_id.equals("null") || _agent_login_id == null) {
                    proc.setNull("@para_loginid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_loginid", _agent_login_id);
                }
            } catch (Exception ex) {
                proc.setNull("@para_loginid", java.sql.Types.NULL);
            }
            try {

                if (_agent_name.equals("null") || _agent_name == null) {
                    proc.setNull("@para_agentname", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_agentname", _agent_name);
                }
            } catch (Exception ex) {
                proc.setNull("@para_agentname", java.sql.Types.NULL);
            }
            try {

                if (_agent_workstation.equals("null") || _agent_workstation == null) {
                    proc.setNull("@para_workstation", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_workstation", _agent_workstation);
                }
            } catch (Exception ex) {
                proc.setNull("@para_workstation", java.sql.Types.NULL);
            }

            try {

                if (_cust_mobile.equals("null") || _cust_mobile == null) {
                    proc.setNull("@para_custmobno", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custmobno", _cust_mobile);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custmobno", java.sql.Types.NULL);
            }
            try {

                if (_cust_cli.equals("null") || _cust_cli == null) {
                    proc.setNull("@para_custcli", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custcli", _cust_cli);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custcli", java.sql.Types.NULL);
            }
            try {

                if (_cust_type.equals("null") || _cust_type == null) {
                    proc.setNull("@para_custtype", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custtype", _cust_type);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custtype", java.sql.Types.NULL);
            }
            try {

                if (_cust_name.equals("null") || _cust_name == null) {
                    proc.setNull("@para_custname", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custname", _cust_name);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custname", java.sql.Types.NULL);
            }
            try {

                if (_cust_language.equals("null") || _cust_language == null) {
                    proc.setNull("@para_custlanguage", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custlanguage", _cust_language);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custlanguage", java.sql.Types.NULL);
            }
            try {

                if (_cust_rating.equals("null") || _cust_rating == null) {
                    proc.setNull("@para_custrating", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custrating", _cust_rating);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custrating", java.sql.Types.NULL);
            }
            try {

                if (_cust_package.equals("null") || _cust_package == null) {
                    proc.setNull("@para_custpackage", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custpackage", _cust_package);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custpackage", java.sql.Types.NULL);
            }
            try {

                if (_cust_access_level.equals("null") || _cust_access_level == null) {
                    proc.setNull("@para_custaccesslevel", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custaccesslevel", _cust_access_level);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custaccesslevel", java.sql.Types.NULL);
            }
            try {

                if (_cust_product.equals("null") || _cust_product == null) {
                    proc.setNull("@para_custproduct", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custproduct", _cust_product);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custproduct", java.sql.Types.NULL);
            }
            try {

                if (_cust_balance_amt.equals("null") || _cust_balance_amt == null) {
                    proc.setNull("@para_custbalamnt", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custbalamnt", _cust_balance_amt);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custbalamnt", java.sql.Types.NULL);
            }
            try {

                if (_cust_prepaid_expiry.equals("NDF") || _cust_prepaid_expiry.equals("null") || _cust_prepaid_expiry == null) {
                    proc.setNull("@para_custprepaidexpiry", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custprepaidexpiry", _cust_prepaid_expiry);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custprepaidexpiry", java.sql.Types.NULL);
            }
            try {

                if (_skillset.equals("null") || _skillset == null) {
                    proc.setNull("@para_skillset", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_skillset", _skillset);
                }
            } catch (Exception ex) {
                proc.setNull("@para_skillset", java.sql.Types.NULL);
            }
            try {

                if (_dnis.equals("null") || _dnis == null) {
                    proc.setNull("@para_dnis", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_dnis", _dnis);
                }
            } catch (Exception ex) {
                proc.setNull("@para_dnis", java.sql.Types.NULL);
            }
            try {

                if (_ip_address.equals("null") || _ip_address == null) {
                    proc.setNull("@para_ipadress", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_ipadress", _ip_address);
                }

            } catch (Exception ex) {
                proc.setNull("@para_ipadress", java.sql.Types.NULL);
            }
            try {

                if (_agent_skillset.equals("null") || _agent_skillset == null) {
                    proc.setNull("@para_SkillSetGroup", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_SkillSetGroup", _agent_skillset);
                }
            } catch (Exception ex) {
                proc.setNull("@para_SkillSetGroup", java.sql.Types.NULL);
            }
            try {

                if (_cust_mobile_model.equals("null") || _cust_mobile_model == null) {
                    proc.setNull("@para_CUST_MOBSET", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_CUST_MOBSET", _cust_mobile_model);
                }
            } catch (Exception ex) {
                proc.setNull("@para_CUST_MOBSET", java.sql.Types.NULL);
            }
            boolean execute = proc.execute();
            proc.close();
            toReturn = new Boolean(execute).toString();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }

        String deleteQuery = "DELETE FROM [new_cct_live_cisco].[dbo].[Fonix] WHERE [new_cct_live_cisco].[dbo].[Fonix].[Fonix2] = ?";

        try {
            PreparedStatement pStat = conn_live.prepareStatement(deleteQuery);
            pStat.setString(1, _ip_address);
            toReturn += "," + Integer.toString(pStat.executeUpdate(deleteQuery));
            pStat.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }

        return toReturn;
    }

    private String setCaresIntegration(String caller_cli, String ipAddr, String insert_datetime, String cares_id) {
        String toReturn = "";
        try {
            CallableStatement proc = conn_live.prepareCall("{ call sp_InsertIntoFonix(?, ?, ?, ?) }");

            try {

                if (insert_datetime.equals("null") || insert_datetime == null) {
                    proc.setNull("@para_INSDATETIME", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp insert_timestamp = new java.sql.Timestamp(Long.parseLong(insert_datetime));
                    proc.setTimestamp("@para_INSDATETIME", insert_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@para_INSDATETIME", java.sql.Types.NULL);
            }
            try {

                if (ipAddr.equals("null") || ipAddr == null) {
                    proc.setNull("@para_agentpcip", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_agentpcip", ipAddr);
                }
            } catch (Exception ex) {
                proc.setNull("@para_agentpcip", java.sql.Types.NULL);
            }

            try {

                if (caller_cli.equals("null") || caller_cli == null) {
                    proc.setNull("@para_callercli", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_callercli", caller_cli);
                }
            } catch (Exception ex) {
                proc.setNull("@para_callercli", java.sql.Types.NULL);
            }
            try {

                if (cares_id.equals("null") || cares_id == null) {
                    proc.setNull("@para_caresid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_caresid", cares_id);
                }
            } catch (Exception ex) {
                proc.setNull("@para_caresid", java.sql.Types.NULL);
            }
            boolean execute = proc.execute();
            proc.close();
            toReturn = new Boolean(execute).toString();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }
        return toReturn;
    }

    private String windowBeforeCloseLog(String datetime, String agent_id, String workstation, String ipAddress) {
        String toReturn = "";
        try {
            CallableStatement proc = conn_live.prepareCall("{ call sp_Altf4press(?, ?, ?, ?) }");

            try {
                if (datetime.equals("null") || datetime == null) {
                    proc.setNull("@timestamp", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp insert_timestamp = new java.sql.Timestamp(Long.parseLong(datetime));
                    proc.setTimestamp("@timestamp", insert_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@timestamp", java.sql.Types.NULL);
            }
            try {
                if (agent_id.equals("null") || agent_id == null) {
                    proc.setNull("@agentid", java.sql.Types.NULL);
                } else {
                    proc.setString("@agentid", agent_id);
                }
            } catch (Exception ex) {
                proc.setNull("@agentid", java.sql.Types.NULL);
            }
            try {
                if (ipAddress.equals("null") || ipAddress == null) {
                    proc.setNull("@ipadress", java.sql.Types.NULL);
                } else {
                    proc.setString("@ipadress", ipAddress);
                }
            } catch (Exception ex) {
                proc.setNull("@ipadress", java.sql.Types.NULL);
            }
            try {

                if (workstation.equals("null") || workstation == null) {
                    proc.setNull("@ws_name", java.sql.Types.NULL);
                } else {
                    proc.setString("@ws_name", workstation);
                }
            } catch (Exception ex) {
                proc.setNull("@ws_name", java.sql.Types.NULL);
            }
            ResultSet executeQuery = proc.executeQuery();
            while (executeQuery.next()) {
                toReturn = executeQuery.getString("datetime");
            }
            //boolean execute = proc.execute();
            proc.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }
        return toReturn;
    }

    private String windowCloseCancelLog(String datetime, String agent_id, String workstation, String ipAddress) {
        String toReturn = "";
        try {
            CallableStatement proc = conn_live.prepareCall("{ call sp_Altf4cancel(?, ?, ?, ?) }");

            try {
                if (datetime.equals("null") || datetime == null) {
                    proc.setNull("@datetime", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp insert_timestamp = new java.sql.Timestamp(Long.parseLong(datetime));
                    proc.setTimestamp("@datetime", insert_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@datetime", java.sql.Types.NULL);
            }
            try {
                if (agent_id.equals("null") || agent_id == null) {
                    proc.setNull("@agentid", java.sql.Types.NULL);
                } else {
                    proc.setString("@agentid", agent_id);
                }
            } catch (Exception ex) {
                proc.setNull("@agentid", java.sql.Types.NULL);
            }
            try {
                if (ipAddress.equals("null") || ipAddress == null) {
                    proc.setNull("@ipadress", java.sql.Types.NULL);
                } else {
                    proc.setString("@ipadress", ipAddress);
                }
            } catch (Exception ex) {
                proc.setNull("@ipadress", java.sql.Types.NULL);
            }
            try {

                if (workstation.equals("null") || workstation == null) {
                    proc.setNull("@ws_name", java.sql.Types.NULL);
                } else {
                    proc.setString("@ws_name", workstation);
                }
            } catch (Exception ex) {
                proc.setNull("@ws_name", java.sql.Types.NULL);
            }
            boolean execute = proc.execute();
            toReturn = new Boolean(execute).toString();
            proc.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }
        return toReturn;
    }

    private String windowCloseLog(String datetime, String agent_id, String workstation, String ipAddress) {
        String toReturn = "";
        try {
            CallableStatement proc = conn_live.prepareCall("{ call sp_Altf4shutdown(?, ?, ?, ?) }");

            try {
                if (datetime.equals("null") || datetime == null) {
                    proc.setNull("@datetime", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp insert_timestamp = new java.sql.Timestamp(Long.parseLong(datetime));
                    proc.setTimestamp("@datetime", insert_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@datetime", java.sql.Types.NULL);
            }
            try {
                if (agent_id.equals("null") || agent_id == null) {
                    proc.setNull("@agentid", java.sql.Types.NULL);
                } else {
                    proc.setString("@agentid", agent_id);
                }
            } catch (Exception ex) {
                proc.setNull("@agentid", java.sql.Types.NULL);
            }
            try {
                if (ipAddress.equals("null") || ipAddress == null) {
                    proc.setNull("@ipadress", java.sql.Types.NULL);
                } else {
                    proc.setString("@ipadress", ipAddress);
                }
            } catch (Exception ex) {
                proc.setNull("@ipadress", java.sql.Types.NULL);
            }
            try {

                if (workstation.equals("null") || workstation == null) {
                    proc.setNull("@ws_name", java.sql.Types.NULL);
                } else {
                    proc.setString("@ws_name", workstation);
                }
            } catch (Exception ex) {
                proc.setNull("@ws_name", java.sql.Types.NULL);
            }
            boolean execute = proc.execute();
            toReturn = new Boolean(execute).toString();
            proc.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }
        return toReturn;
    }

    private String setWCDetail(String conn_id, String cust_cli, String wc_id, String wc,
            String category, String ws_result, String rsl, String datetime,
            boolean smsFlag, boolean smsSend, boolean ivrEnabled, boolean ivrSend, String ivrMenuCode,
            boolean onlineResolved) {
        String toReturn = "";
        try {
            CallableStatement proc = conn_live.prepareCall("{ call sp_InsertIntoChomosV1(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
            //CallableStatement proc = conn_live.prepareCall("{ call sp_InsertIntoChomos(?, ?, ?, ?, ?, ?, ?, ?) }");

            try {

                if (conn_id.equals("null") || conn_id == null) {
                    proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_callconnectionid", conn_id);
                }
            } catch (Exception ex) {
                proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
            }
            try {

                if (cust_cli.equals("null") || cust_cli == null) {
                    proc.setNull("@para_custcli", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custcli", cust_cli);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custcli", java.sql.Types.NULL);
            }
            try {

                if (wc_id.equals("null") || wc_id == null) {
                    proc.setNull("@para_wcid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_wcid", wc_id);
                }
            } catch (Exception ex) {
                proc.setNull("@para_wcid", java.sql.Types.NULL);
            }
            try {

                if (wc.equals("null") || wc == null) {
                    proc.setNull("@para_wc", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_wc", wc);
                }
            } catch (Exception ex) {
                proc.setNull("@para_wc", java.sql.Types.NULL);
            }
            try {

                if (category.equals("null") || category == null) {
                    proc.setNull("@para_category", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_category", category);
                }
            } catch (Exception ex) {
                proc.setNull("@para_category", java.sql.Types.NULL);
            }
            try {

                if (ws_result.equals("null") || ws_result == null) {
                    proc.setNull("@para_wsresult", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_wsresult", ws_result);
                }
            } catch (Exception ex) {
                proc.setNull("@para_wsresult", java.sql.Types.NULL);
            }
            try {

                if (rsl.equals("null") || rsl == null) {
                    proc.setNull("@para_rsl", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_rsl", rsl);
                }
            } catch (Exception ex) {
                proc.setNull("@para_rsl", java.sql.Types.NULL);
            }
            try {
                if (datetime.equals("null") || datetime == null) {
                    proc.setNull("@para_datetime", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp insert_timestamp = new java.sql.Timestamp(Long.parseLong(datetime));
                    proc.setTimestamp("@para_datetime", insert_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@para_datetime", java.sql.Types.NULL);
            }

            try {
                proc.setBoolean("@smsflag", smsFlag);
            } catch (Exception ex) {
                proc.setNull("@smsflag", java.sql.Types.NULL);
            }

            try {
                proc.setBoolean("@smssendornot", smsSend);
            } catch (Exception ex) {
                proc.setNull("@smssendornot", java.sql.Types.NULL);
            }

            try {
                proc.setBoolean("@ivrenable", ivrEnabled);
            } catch (Exception ex) {
                proc.setNull("@ivrenable", java.sql.Types.NULL);
            }

            try {
                proc.setBoolean("@ivrsend", ivrSend);
            } catch (Exception ex) {
                proc.setNull("@ivrsend", java.sql.Types.NULL);
            }

            try {
                if (ivrMenuCode == null) {
                    proc.setNull("@ivrmenucode", java.sql.Types.NULL);
                } else if (ivrMenuCode.equals("null")) {
                    proc.setNull("@ivrmenucode", java.sql.Types.NULL);
                } else {
                    proc.setString("@ivrmenucode", ivrMenuCode);
                }
            } catch (Exception ex) {
                proc.setNull("@ivrmenucode", java.sql.Types.NULL);
            }

            try {
                proc.setBoolean("@OnlineResolution", onlineResolved);
            } catch (Exception ex) {
                proc.setNull("@OnlineResolution", java.sql.Types.NULL);
            }

            boolean execute = proc.execute();
            proc.close();
            toReturn = new Boolean(execute).toString();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }
        return toReturn;
    }

    private String setWCDetail(String conn_id, String cust_cli, String wc_id, String wc,
            String category, String ws_result, String rsl, String datetime) {
        String toReturn = "";
        try {
            CallableStatement proc = conn_live.prepareCall("{ call sp_InsertIntoChomos(?, ?, ?, ?, ?, ?, ?, ?) }");

            try {

                if (conn_id.equals("null") || conn_id == null) {
                    proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_callconnectionid", conn_id);
                }
            } catch (Exception ex) {
                proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
            }
            try {

                if (cust_cli.equals("null") || cust_cli == null) {
                    proc.setNull("@para_custcli", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custcli", cust_cli);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custcli", java.sql.Types.NULL);
            }
            try {

                if (wc_id.equals("null") || wc_id == null) {
                    proc.setNull("@para_wcid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_wcid", wc_id);
                }
            } catch (Exception ex) {
                proc.setNull("@para_wcid", java.sql.Types.NULL);
            }
            try {

                if (wc.equals("null") || wc == null) {
                    proc.setNull("@para_wc", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_wc", wc);
                }
            } catch (Exception ex) {
                proc.setNull("@para_wc", java.sql.Types.NULL);
            }
            try {

                if (category.equals("null") || category == null) {
                    proc.setNull("@para_category", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_category", category);
                }
            } catch (Exception ex) {
                proc.setNull("@para_category", java.sql.Types.NULL);
            }
            try {

                if (ws_result.equals("null") || ws_result == null) {
                    proc.setNull("@para_wsresult", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_wsresult", ws_result);
                }
            } catch (Exception ex) {
                proc.setNull("@para_wsresult", java.sql.Types.NULL);
            }
            try {

                if (rsl.equals("null") || rsl == null) {
                    proc.setNull("@para_rsl", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_rsl", rsl);
                }
            } catch (Exception ex) {
                proc.setNull("@para_rsl", java.sql.Types.NULL);
            }
            try {
                if (datetime.equals("null") || datetime == null) {
                    proc.setNull("@para_datetime", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp insert_timestamp = new java.sql.Timestamp(Long.parseLong(datetime));
                    proc.setTimestamp("@para_datetime", insert_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@para_datetime", java.sql.Types.NULL);
            }
            boolean execute = proc.execute();
            proc.close();
            toReturn = new Boolean(execute).toString();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }
        return toReturn;
    }

    private String setWCSMSDetail(String datetime, String user_code, String msisdn, String wcname,
            String call_id, String result, String result_desc, String status) {
        String toReturn = "";

        try {
            CallableStatement proc = conn_live.prepareCall("{ call sp_InsertIntoPolaxiri(?, ?, ?, ?, ?, ?, ?, ?) }");

            try {

                if (datetime.equals("null") || datetime == null) {
                    proc.setNull("@para_datetime", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp insert_timestamp = new java.sql.Timestamp(Long.parseLong(datetime));
                    proc.setTimestamp("@para_datetime", insert_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@para_datetime", java.sql.Types.NULL);
            }
            try {

                if (user_code.equals("null") || user_code == null) {
                    proc.setNull("@para_User_code", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_User_code", user_code);
                }
            } catch (Exception ex) {
                proc.setNull("@para_User_code", java.sql.Types.NULL);
            }
            try {

                if (msisdn.equals("null") || msisdn == null) {
                    proc.setNull("@para_MSISDN", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_MSISDN", msisdn);
                }
            } catch (Exception ex) {
                proc.setNull("@para_MSISDN", java.sql.Types.NULL);
            }
            try {

                if (wcname.equals("null") || wcname == null) {
                    proc.setNull("@para_WorkCode", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_WorkCode", wcname);
                }
            } catch (Exception ex) {
                proc.setNull("@para_WorkCode", java.sql.Types.NULL);
            }
            try {

                if (call_id.equals("null") || call_id == null) {
                    proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_callconnectionid", call_id);
                }
            } catch (Exception ex) {
                proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
            }
            try {

                if (result.equals("null") || result == null) {
                    proc.setNull("@para_Result", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_Result", result);
                }
            } catch (Exception ex) {
                proc.setNull("@para_Result", java.sql.Types.NULL);
            }
            try {

                if (result_desc.equals("null") || result_desc == null) {
                    proc.setNull("@para_Result_Desc", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_Result_Desc", result_desc);
                }
            } catch (Exception ex) {
                proc.setNull("@para_Result_Desc", java.sql.Types.NULL);
            }
            try {

                if (status.equals("null") || status == null) {
                    proc.setNull("@para_status", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_status", status);
                }
            } catch (Exception ex) {
                proc.setNull("@para_status", java.sql.Types.NULL);
            }
            boolean execute = proc.execute();
            proc.close();
            toReturn = new Boolean(execute).toString();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }

        return toReturn;
    }

    public void LogSuperCardRecord(String msisdn, String isSuperCardUser, String balance, String agentId) {
        String storedProc = "{call sp_InsertIntoSupercardReporting(?,?,?,?)}";
        CallableStatement proc = null;
        int result = 0;
        try {
            proc = conn.prepareCall(storedProc);

            try {
                if (msisdn.equals("null")) {
                    proc.setNull(1, java.sql.Types.NULL);
                } else {
                    proc.setString(1, msisdn);
                }
            } catch (Exception ex) {
                proc.setNull(1, java.sql.Types.NULL);
            }
            try {
                if (isSuperCardUser.equals("null")) {
                    proc.setNull(2, java.sql.Types.NULL);
                } else {
                    proc.setString(2, isSuperCardUser);
                }
            } catch (Exception ex) {
                proc.setNull(2, java.sql.Types.NULL);
            }
            try {
                if (balance.equals("null")) {
                    proc.setNull(3, java.sql.Types.NULL);
                } else {
                    proc.setString(3, balance);
                }
            } catch (Exception ex) {
                proc.setNull(3, java.sql.Types.NULL);
            }
            try {
                if (agentId.equals("null")) {
                    proc.setNull(4, java.sql.Types.NULL);
                } else {
                    proc.setString(4, agentId);
                }
            } catch (Exception ex) {
                proc.setNull(4, java.sql.Types.NULL);
            }
            //proc.setString(1, msisdn);
            //proc.setString(2, isSuperCardUser);
            //proc.setString(3, balance);
            //proc.setString(4, agentId);

            result = proc.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, "Db connection not made", ex);
        }

    }

    public String setWSLogging(String datetime, String agent_id, String result, String resDesc, String status,
            String msisdn, String wc_name, String wc_group, String cli, String amt_charged, String conn_id,
            String ipAddr, String flag, String ws_name) {
        String toReturn = "";

        try {
            //CallableStatement proc = conn_live.prepareCall("{ call sp_InsertIntoSolisic(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
            CallableStatement proc = conn_live_2.prepareCall("{ call sp_InsertIntoSolisic(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");

            try {

                if (agent_id.equals("null") || agent_id == null) {
                    proc.setNull("@para_User_code", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_User_code", agent_id);
                }
            } catch (Exception ex) {
                proc.setNull("@para_User_code", java.sql.Types.NULL);
            }
            try {

                if (msisdn.equals("null") || msisdn == null) {
                    proc.setNull("@para_MSISDN", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_MSISDN", msisdn);
                }
            } catch (Exception ex) {
                proc.setNull("@para_MSISDN", java.sql.Types.NULL);
            }
            try {

                if (result.equals("null") || result == null) {
                    proc.setNull("@para_Result", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_Result", result);
                }
            } catch (Exception ex) {
                proc.setNull("@para_Result", java.sql.Types.NULL);
            }
            try {

                if (wc_name.equals("null") || wc_name == null) {
                    proc.setNull("@para_WC_Name", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_WC_Name", wc_name);
                }
            } catch (Exception ex) {
                proc.setNull("@para_WC_Name", java.sql.Types.NULL);
            }
            try {

                if (resDesc.equals("null") || resDesc == null) {
                    proc.setNull("@para_Result_Desc", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_Result_Desc", resDesc);
                }
            } catch (Exception ex) {
                proc.setNull("@para_Result_Desc", java.sql.Types.NULL);
            }
            try {

                if (amt_charged.equals("null") || amt_charged == null) {
                    proc.setNull("@para_Amount_charged", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_Amount_charged", amt_charged);
                }
            } catch (Exception ex) {
                proc.setNull("@para_Amount_charged", java.sql.Types.NULL);
            }
            try {

                if (datetime.equals("null") || datetime == null) {
                    proc.setNull("@para_datetime", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp insert_timestamp = new java.sql.Timestamp(Long.parseLong(datetime));
                    proc.setTimestamp("@para_datetime", insert_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@para_datetime", java.sql.Types.NULL);
            }
            try {

                if (wc_group.equals("null") || wc_group == null) {
                    proc.setNull("@para_WC_Group", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_WC_Group", wc_group);
                }
            } catch (Exception ex) {
                proc.setNull("@para_WC_Group", java.sql.Types.NULL);
            }
            try {

                if (status.equals("null") || status == null) {
                    proc.setNull("@para_status", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_status", status);
                }
            } catch (Exception ex) {
                proc.setNull("@para_status", java.sql.Types.NULL);
            }
            try {

                if (conn_id.equals("null") || conn_id == null) {
                    proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_callconnectionid", conn_id);
                }
            } catch (Exception ex) {
                proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
            }
            try {

                if (ipAddr.equals("null") || ipAddr == null) {
                    proc.setNull("@para_IPAdress", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_IPAdress", ipAddr);
                }
            } catch (Exception ex) {
                proc.setNull("@para_IPAdress", java.sql.Types.NULL);
            }
            try {

                if (cli.equals("null") || cli == null) {
                    proc.setNull("@para_cli", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_cli", cli);
                }
            } catch (Exception ex) {
                proc.setNull("@para_cli", java.sql.Types.NULL);
            }
            try {

                if (ws_name.equals("null") || ws_name == null) {
                    proc.setNull("@para_WSname", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_WSname", ws_name);
                }
            } catch (Exception ex) {
                proc.setNull("@para_WSname", java.sql.Types.NULL);
            }
            boolean execute = proc.execute();
            proc.close();
            toReturn = new Boolean(execute).toString();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }

        return toReturn;
    }

    private String setPostCallAdjustment(String _callid, String _clid, String _calltype, String _details,
            String _caresstatus, String _caredt, String _insertiondt, String _switchid, String _dnis,
            String _mobno, String _abal, String _custtype, String _prodtype, String _amtcharged, String _agentid,
            String _retries, String _lastretry, String _rsl, String _rdesc) {
        String toReturn = "";

        try {
            CallableStatement proc = conn_live.prepareCall("{ call sp_InsertIntoToulene(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");

            try {

                if (_callid.equals("null") || _callid == null) {
                    proc.setNull("@para_callid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_callid", _callid);
                }
            } catch (Exception ex) {
                proc.setNull("@para_callid", java.sql.Types.NULL);
            }
            try {

                if (_clid.equals("null") || _clid == null) {
                    proc.setNull("@para_clid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_clid", _clid);
                }
            } catch (Exception ex) {
                proc.setNull("@para_clid", java.sql.Types.NULL);
            }
            try {

                if (_calltype.equals("null") || _calltype == null) {
                    proc.setNull("@para_calltype", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_calltype", _calltype);
                }
            } catch (Exception ex) {
                proc.setNull("@para_calltype", java.sql.Types.NULL);
            }
            try {

                if (_details.equals("null") || _details == null) {
                    proc.setNull("@para_details", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_details", _details);
                }
            } catch (Exception ex) {
                proc.setNull("@para_details", java.sql.Types.NULL);
            }
            try {

                if (_rsl.equals("null") || _rsl == null) {
                    proc.setNull("@para_rsl", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_rsl", _rsl);
                }
            } catch (Exception ex) {
                proc.setNull("@para_rsl", java.sql.Types.NULL);
            }
            try {

                if (_rdesc.equals("null") || _rdesc == null) {
                    proc.setNull("@para_rdesc", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_rdesc", _rdesc);
                }
            } catch (Exception ex) {
                proc.setNull("@para_rdesc", java.sql.Types.NULL);
            }
            try {

                if (_caresstatus.equals("null") || _caresstatus == null) {
                    proc.setNull("@para_caresstatus", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_caresstatus", _caresstatus);
                }
            } catch (Exception ex) {
                proc.setNull("@para_caresstatus", java.sql.Types.NULL);
            }
            try {

                if (_caredt.equals("null") || _caredt == null) {
                    proc.setNull("@para_caredt", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp cares_timestamp = new java.sql.Timestamp(Long.parseLong(_caredt));
                    proc.setTimestamp("@para_caredt", cares_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@para_caredt", java.sql.Types.NULL);
            }
            try {

                if (_insertiondt.equals("null") || _insertiondt == null) {
                    proc.setNull("@para_insertiondt", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp insert_timestamp = new java.sql.Timestamp(Long.parseLong(_insertiondt));
                    proc.setTimestamp("@para_insertiondt", insert_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@para_insertiondt", java.sql.Types.NULL);
            }
            try {

                if (_switchid.equals("null") || _switchid == null) {
                    proc.setNull("@para_switchid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_switchid", _switchid);
                }
            } catch (Exception ex) {
                proc.setNull("@para_switchid", java.sql.Types.NULL);
            }
            try {

                if (_dnis.equals("null") || _dnis == null) {
                    proc.setNull("@para_dnis", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_dnis", _dnis);
                }
            } catch (Exception ex) {
                proc.setNull("@para_dnis", java.sql.Types.NULL);
            }
            try {

                if (_mobno.equals("null") || _mobno == null) {
                    proc.setNull("@para_mobno", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_mobno", _mobno);
                }
            } catch (Exception ex) {
                proc.setNull("@para_mobno", java.sql.Types.NULL);
            }
            try {

                if (_abal.equals("null") || _abal == null) {
                    proc.setNull("@para_abal", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_abal", _abal);
                }
            } catch (Exception ex) {
                proc.setNull("@para_abal", java.sql.Types.NULL);
            }
            try {

                if (_custtype.equals("null") || _custtype == null) {
                    proc.setNull("@para_custtype", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_custtype", _custtype);
                }
            } catch (Exception ex) {
                proc.setNull("@para_custtype", java.sql.Types.NULL);
            }
            try {

                if (_prodtype.equals("null") || _prodtype == null) {
                    proc.setNull("@para_prodtype", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_prodtype", _prodtype);
                }
            } catch (Exception ex) {
                proc.setNull("@para_prodtype", java.sql.Types.NULL);
            }
            try {

                if (_amtcharged.equals("null") || _amtcharged == null) {
                    proc.setNull("@para_amtcharged", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_amtcharged", _amtcharged);
                }
            } catch (Exception ex) {
                proc.setNull("@para_amtcharged", java.sql.Types.NULL);
            }
            try {

                if (_agentid.equals("null") || _agentid == null) {
                    proc.setNull("@para_agentid", java.sql.Types.NULL);
                } else {
                    proc.setString("@para_agentid", _agentid);
                }
            } catch (Exception ex) {
                proc.setNull("@para_agentid", java.sql.Types.NULL);
            }
            try {

                if (_retries.equals("null") || _retries == null) {
                    proc.setNull("@para_retries", java.sql.Types.NULL);
                } else {
                    proc.setInt("@para_retries", Integer.parseInt(_retries));
                }
            } catch (Exception ex) {
                proc.setNull("@para_retries", java.sql.Types.NULL);
            }
            try {

                if (_lastretry.equals("null") || _lastretry == null) {
                    proc.setNull("@para_lastretry", java.sql.Types.NULL);
                } else {
                    java.sql.Timestamp lastretry_timestamp = new java.sql.Timestamp(Long.parseLong(_lastretry));
                    proc.setTimestamp("@para_lastretry", lastretry_timestamp);
                }
            } catch (Exception ex) {
                proc.setNull("@para_lastretry", java.sql.Types.NULL);
            }
            boolean execute = proc.execute();
            proc.close();
            toReturn = new Boolean(execute).toString();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = ex.getLocalizedMessage().trim();
        }

        return toReturn;
    }

    private List<HashMap<String, String>> convertResultSetToList(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        while (rs.next()) {
            HashMap<String, String> row = new HashMap<String, String>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i), rs.getString(i));
            }
            list.add(row);
        }
        try {
            rs.close();
        } catch (Exception ex) {
        }
        rs = null;
        return list;
    }

    private String TDViewCall(String number, String cust_type) {
        String toReturn = "";
        String result = "";

        List<HashMap<String, String>> rs = null;
        Iterator<HashMap<String, String>> itr = null;
        HashMap<String, String> tmpHash = null;

        try {
            //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            //conn_teradata = java.sql.DriverManager.getConnection("jdbc:odbc:TD U");
            Class.forName("com.teradata.jdbc.TeraDriver");
            conn_teradata = java.sql.DriverManager.getConnection(TDurl, TDuserID, TDpassword);
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += ex.getLocalizedMessage().trim();
        }

        String sql = "select * from MP_BVEW_CCIT.HASH_CUBE where MSISDN=?";
        try {
            PreparedStatement pStat = conn_teradata.prepareStatement(sql);
            pStat.setString(1, number);

//	     stat.setQueryTimeout(TDTimeoutSec);
            //rs = stat.executeQuery(sql);
            rs = convertResultSetToList(pStat.executeQuery(sql));
            //conn_teradata.close();
            pStat.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += ex.getLocalizedMessage().trim();
        }

        try {
           // stat.close();
            conn_teradata.close();
        } catch (Exception ex) {
//			Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }

        result += "<tdxml>";

        //TDPrevHandSetInfo CODE Start
        result += "<TDPrevHandSetInfo>";
        if (cust_type.equals("Post-paid")) {
            try {
                //rs = stat.executeQuery(sql);
                itr = rs.iterator();
                while (itr.hasNext()) {
                    tmpHash = itr.next();
                    result += tmpHash.get("MSISDN") + "|" + tmpHash.get("PREV_HANDSET") + "|";
                }
                //conn_teradata.close();
            } catch (Exception ex) {
//				Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                result += "NDF|NDF|";
            }
        } else {
            try {
                //rs = stat.executeQuery(sql);
                itr = rs.iterator();
                while (itr.hasNext()) {
                    tmpHash = itr.next();
                    result += tmpHash.get("MSISDN") + "|" + tmpHash.get("PREV_HANDSET") + "|";
                }
                //conn_teradata.close();
            } catch (Exception ex) {
//				Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                result += "NDF|NDF|";
            }
        }
        result += "</TDPrevHandSetInfo>";
        //TDPrevHandSetInfo CODE End

        //TDBundleInfo CODE Start
        result += "<TDBundleInfo>";
        try {
            //rs = stat.executeQuery(sql);
            itr = rs.iterator();
            while (itr.hasNext()) {
                tmpHash = itr.next();
                result += tmpHash.get("MSISDN") + "|" + tmpHash.get("PACKAGE") + "|" + tmpHash.get("VOICE_BUNDLE") + "|" + tmpHash.get("SMS_BUNDLE") + "|" + tmpHash.get("GPRS_BUNDLE") + "|";
            }
            //conn_teradata.close();
        } catch (Exception ex) {
//			Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += "NDF|NDF|NDF|NDF|NDF|";
        }
        result += "</TDBundleInfo>";
        //TDBundleInfo CODE End		

        //TDDatesInfo CODE Start
        result += "<TDDatesInfo>";
        try {
            //rs = stat.executeQuery(sql);
            itr = rs.iterator();
            while (itr.hasNext()) {
                tmpHash = itr.next();
                result += tmpHash.get("MSISDN") + "|" + tmpHash.get("FCA_DATE") + "|" + tmpHash.get("SUB_AGE") + "|";

            }
            //conn_teradata.close();
        } catch (Exception ex) {
//			Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += "NDF|NDF|NDF|";
        }
        result += "</TDDatesInfo>";
        //TDDatesInfo CODE End

        //TDAvgInfo CODE Start
        Float sum = new Float(0.0);
        result += "<TDAvgInfo>";
        if (cust_type.equals("Post-paid")) {
            try {
                //rs = stat.executeQuery(sql);
                itr = rs.iterator();
                while (itr.hasNext()) {
                    tmpHash = itr.next();
                    sum += Float.parseFloat(tmpHash.get("VOICE")) + Float.parseFloat(tmpHash.get("SMS")) + Float.parseFloat(tmpHash.get("GPRS"));
                    result += tmpHash.get("VOICE") + "|" + tmpHash.get("SMS") + "|" + tmpHash.get("GPRS") + "|";

                }
                //conn_teradata.close();
            } catch (Exception ex) {
//				Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                sum += 0;
                result += "0|0|0|";
            }
        } else {
            try {
                //rs = stat.executeQuery(sql);
                itr = rs.iterator();
                while (itr.hasNext()) {
                    tmpHash = itr.next();
                    sum += Float.parseFloat(tmpHash.get("VOICE")) + Float.parseFloat(tmpHash.get("SMS")) + Float.parseFloat(tmpHash.get("GPRS"));
                    result += tmpHash.get("VOICE") + "|" + tmpHash.get("SMS") + "|" + tmpHash.get("GPRS") + "|";
                }
                //conn_teradata.close();
            } catch (Exception ex) {
//				Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                sum += 0;
                result += "0|0|0|";
            }
        }
        result = result + "," + sum.intValue();
        result += "</TDAvgInfo>";
        //TDAvgInfo CODE End

        //TDSumInfo CODE Start
        result += "<TDSumInfo>";
        if (cust_type.equals("Post-paid")) {
            try {
                //rs = stat.executeQuery(sql);
                itr = rs.iterator();
                Float s_voice = new Float(0.0);
                Float s_sms = new Float(0.0);
                Float s_gprs = new Float(0.0);
                while (itr.hasNext()) {
                    tmpHash = itr.next();
                    for (int i = 30; i >= 1; i--) {
                        String tmp = "VOICE" + Integer.toString(i);
                        try {
                            s_voice += Float.parseFloat(tmpHash.get(tmp));
                        } catch (Exception ex) {
                            s_voice += 0;
                        }
                        tmp = "SMS" + Integer.toString(i);
                        try {
                            s_sms += Float.parseFloat(tmpHash.get(tmp));
                        } catch (Exception ex) {
                            s_sms += 0;
                        }
                        tmp = "GPRS" + Integer.toString(i);
                        try {
                            s_gprs += Float.parseFloat(tmpHash.get(tmp));
                        } catch (Exception ex) {
                            s_gprs += 0;
                        }
                    }
                    result += s_voice + "|" + s_sms + "|" + s_gprs + "|";
                }
                //conn_teradata.close();
            } catch (Exception ex) {
//				Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                result += "0|0|0|";
                //result += ex.getLocalizedMessage().trim();
            }
        } else {
            try {
                //rs = stat.executeQuery(sql);
                itr = rs.iterator();
                Float s_voice = new Float(0.0);
                Float s_sms = new Float(0.0);
                Float s_gprs = new Float(0.0);
                while (itr.hasNext()) {
                    tmpHash = itr.next();
                    for (int i = 30; i >= 1; i--) {
                        String tmp = "VOICE" + Integer.toString(i);
                        try {
                            s_voice += Float.parseFloat(tmpHash.get(tmp));
                        } catch (Exception ex) {
                            s_voice += 0;
                        }
                        tmp = "SMS" + Integer.toString(i);
                        try {
                            s_sms += Float.parseFloat(tmpHash.get(tmp));
                        } catch (Exception ex) {
                            s_sms += 0;
                        }
                        tmp = "GPRS" + Integer.toString(i);
                        try {
                            s_gprs += Float.parseFloat(tmpHash.get(tmp));
                        } catch (Exception ex) {
                            s_gprs += 0;
                        }
                    }
                    result += s_voice + "|" + s_sms + "|" + s_gprs + "|";
                }
                //conn_teradata.close();
            } catch (Exception ex) {
//				Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                result += "0|0|0|";
                //result += ex.getLocalizedMessage().trim();
            }
        }
        result += "</TDSumInfo>";
        //TDSumInfo CODE End

        result += "</tdxml>";

        rs = null;
        itr = null;
        tmpHash = null;

        toReturn = result;
        return toReturn;

    }

    private String unicaStartSession(String sessionID, String msisdn, String datetime, String username, String agent_id, String amt_charged, String conn_id, String ipAddr) {
        String toReturn = "";
        String result = "";

        if (setUnicaConfigs()) {

            try {
                unica_api = InteractAPI.getInstance(InteractAPIURL);
            } catch (Exception e) {
                e.printStackTrace();
                result += e.getLocalizedMessage().trim();
            }

            Response response = null;

            NameValuePairImpl custId = new NameValuePairImpl();
            //custId.setName("MSISDN"); //CustomerID
            custId.setName("CUSTOMERID");
            custId.setValueAsString(msisdn);
            custId.setValueDataType(NameValuePair.DATA_TYPE_STRING);
            NameValuePairImpl[] initialAudienceId = {custId};

            NameValuePairImpl parm1 = new NameValuePairImpl();
            parm1.setName("ApplicationName");
            parm1.setValueAsString("HashCube");
            parm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

            NameValuePairImpl parm2 = new NameValuePairImpl();
            parm2.setName("TimeStamp");
            parm2.setValueAsDate(new Date());
            parm2.setValueDataType(NameValuePair.DATA_TYPE_DATETIME);

            NameValuePairImpl parm3 = new NameValuePairImpl();
            parm3.setName("AgentIP");
            parm3.setValueAsString(ipAddr);
            parm3.setValueDataType(NameValuePair.DATA_TYPE_STRING);

            NameValuePairImpl parm4 = new NameValuePairImpl();
            parm4.setName("ServerIP");
            try {
                parm4.setValueAsString(InetAddress.getLocalHost().getHostAddress());
            } catch (Exception e) {
                parm4.setValueAsString("unknown");
                e.printStackTrace();
            }
            parm4.setValueDataType(NameValuePair.DATA_TYPE_STRING);

            NameValuePairImpl parm5 = new NameValuePairImpl();
            parm5.setName("Location");
            parm5.setValueAsString("ISB");
            parm5.setValueDataType(NameValuePair.DATA_TYPE_STRING);

            NameValuePairImpl parm6 = new NameValuePairImpl();
            parm6.setName("AgentID");
            parm6.setValueAsString(agent_id);
            parm6.setValueDataType(NameValuePair.DATA_TYPE_STRING);

            NameValuePairImpl parm7 = new NameValuePairImpl();
            parm7.setName("AgentLogin");
            parm7.setValueAsString(username);
            parm7.setValueDataType(NameValuePair.DATA_TYPE_STRING);

            NameValuePairImpl parm8 = new NameValuePairImpl();
            parm8.setName("CallerMSISDN");
            parm8.setValueAsString(msisdn);
            parm8.setValueDataType(NameValuePair.DATA_TYPE_STRING);
            // Edit param 9 & param 10 Parameters for Hash3CC
            NameValuePairImpl parm9 = new NameValuePairImpl();
            parm9.setName("EVENT_TYPE");
            parm9.setValueAsString("startSession");
            parm9.setValueDataType(NameValuePair.DATA_TYPE_STRING);

            NameValuePairImpl parm10 = new NameValuePairImpl();
            parm10.setName("SESSION_ID");
            parm10.setValueAsString(sessionID);
            parm10.setValueDataType(NameValuePair.DATA_TYPE_STRING);

            NameValuePairImpl[] initialParameters = {parm1, parm2, parm3, parm4, parm5, parm6, parm7, parm8, parm9, parm10};

            try {
                //Customer
                response = unica_api.startSession(sessionID, unica_relyOnExistingSession, unica_initialDebugFlag, unica_interactiveChannel, initialAudienceId, unica_audienceLevel, initialParameters);
                result += unica_processStartSessionResponse(response);
                setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaStartSession");
                toReturn = "T";

            } catch (Exception e) {
                setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaStartSession");
                toReturn = "F";
                result = e.getLocalizedMessage().trim();
                e.printStackTrace();
            }
            toReturn += ",";

            toReturn += result;

        } else {
            toReturn = "Could not set Unica Configs";
        }

        return toReturn;
    }

    private String unica_processStartSessionResponse(Response response) {
        String toReturn = "";
        String result = "";

        if (response.getStatusCode() == Response.STATUS_SUCCESS) {
            result += ("startSession call processed with no warnings or errors");
        } else if (response.getStatusCode() == Response.STATUS_WARNING) {
            result += ("startSession call processed with a warning");
        } else {
            result += ("startSession call processed with an error");
        }

        if (response.getStatusCode() != Response.STATUS_SUCCESS) {
            result += ",";
            result += unica_printDetailMessageOfWarningOrError("StartSession", response.getAdvisoryMessages());
        }

        toReturn = result;
        return toReturn;
    }

    private String unicaGetOffers(String sessionID, String username, String msisdn, String datetime, String agent_id, String amt_charged, String conn_id, String ipAddr) {
        String toReturn = "";
        String result = "";

        if (setUnicaConfigs()) {

            try {
                unica_api = InteractAPI.getInstance(InteractAPIURL);
            } catch (Exception e) {
                e.printStackTrace();
                result += e.getLocalizedMessage().trim();
            }

            Response response = null;

            try {
                response = unica_api.getOffers(sessionID, unica_interactionPoint, unica_numberRequested);
                result += unica_processGetOffersResponse(response, conn_id);
                setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaGetOffers");
                toReturn += "T";

            } catch (Exception e) {
                toReturn += "F";
                setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaGetOffers");
                result = e.getLocalizedMessage().trim();
                e.printStackTrace();
            }

            toReturn += ",";

            toReturn += result;
        } else {
            toReturn = "Could not set Unica Configs";
        }
        return toReturn;
    }

    private String unica_processGetOffersResponse(Response response, String conn_id) {
        String toReturn = "";
        String result = "";

        result += "<unica_offers>";
        if (response.getStatusCode() == Response.STATUS_SUCCESS) {
            //System.out.println("getOffers call processed with no warnings or errors");

            OfferList offerList = response.getOfferList();

            if (offerList.getRecommendedOffers() != null) {
                for (Offer offer : offerList.getRecommendedOffers()) {
                    result += "<offer>";

                    result += "<offername>" + offer.getOfferName() + "</offername>";

                    result += "<offerdesc>" + offer.getDescription() + "</offerdesc>";

                    result += "<offerscore>" + offer.getScore() + "</offerscore>";

                    result += "<offertreatmentcode>" + offer.getTreatmentCode() + "</offertreatmentcode>";

                    result += "<offercodes>";
                    for (String codeString : offer.getOfferCode()) {
                        result += "<offercode>" + codeString + "</offercode>";
                    }
                    result += "</offercodes>";

                    for (NameValuePair offerAttribute : offer.getAdditionalAttributes()) {
                        /*
                         * if(offerAttribute.getName().equalsIgnoreCase("effectiveDate"))
                         * { System.out.println("Found effective date"); } else
                         * if(offerAttribute.getName().equalsIgnoreCase("expirationDate"))
                         * { System.out.println("Found expiration date"); }
                         */
                        result += "<offerattributes>";
                        result += unica_printNameValuePair(offerAttribute);
                        result += "</offerattributes>";
                    }
                    result += "</offer>";
                    insertUnicaOffersPresented(conn_id, "success", offer.getTreatmentCode(), offer.getOfferName(), offer.getDescription(), result);
                }
            } else {
                result += "<offer>";
                result += "<defaultoffer>" + offerList.getDefaultString() + "</defaultoffer>";
                result += "</offer>";
                insertUnicaOffersPresented(conn_id, "success", offerList.getDefaultString(), offerList.getDefaultString(), offerList.getDefaultString(), result);
            }
        } else if (response.getStatusCode() == Response.STATUS_WARNING) {
            result += ("getOffers call processed with a warning");
        } else {
            result += ("getOffers call processed with an error");
        }

        if (response.getStatusCode() != Response.STATUS_SUCCESS) {
            result += ",";
            result += unica_printDetailMessageOfWarningOrError("getOffers", response.getAdvisoryMessages());
            if (response.getStatusCode() == Response.STATUS_WARNING) {
                insertUnicaOffersPresented(conn_id, "warning", "Warning", "Warning", result, result);
            } else {
                insertUnicaOffersPresented(conn_id, "error", "Error", "Error", result, result);
            }
        }
        result += "</unica_offers>";

        toReturn = result;

        return toReturn;
    }

    private String unica_printNameValuePair(NameValuePair nvp) {
        String toReturn = "";
        String result = "";

        result += "<name>" + nvp.getName() + "</name>";

        if (nvp.getValueDataType().equals(NameValuePair.DATA_TYPE_DATETIME)) {
            result += "<value>" + nvp.getValueAsDate() + "</value>";
        } else if (nvp.getValueDataType().equals(NameValuePair.DATA_TYPE_NUMERIC)) {
            result += "<value>" + nvp.getValueAsNumeric() + "</value>";
        } else {
            result += "<value>" + nvp.getValueAsString() + "</value>";
        }

        toReturn = result;
        return toReturn;
    }

    private String unicaPostEvent(String eventName, String unicaCode, String unicaResegOfferCode, String sessionID, String msisdn, String datetime, String agent_id, String username, String amt_charged, String conn_id, String ipAddr) {
        String toReturn = "";
        String result = "";

        if (setUnicaConfigs()) {

            try {
                unica_api = InteractAPI.getInstance(InteractAPIURL);
            } catch (Exception e) {
                e.printStackTrace();
                result += e.getLocalizedMessage().trim();
            }

            Response response = null;

            if (eventName.equals("accept")) {

                String trackingCode = "";
                String offerCode = "";
                String offerType = "";
                String APID = "";
                String NoOfSMS = "";
                String SMSValidity = "";
                String color = "";
                String validity = "";
                String offerCharges = "";
                String resegmentFlag = "";
                String flowchartName = "";
                String callID = "";
                try {
                    trackingCode = unicaCode.split("[|]")[0];
                    offerCode = unicaCode.split("[|]")[1];
                    offerType = unicaCode.split("[|]")[2];
                    APID = unicaCode.split("[|]")[3];
                    NoOfSMS = unicaCode.split("[|]")[4];
                    SMSValidity = unicaCode.split("[|]")[5];
                    color = unicaCode.split("[|]")[6];
                    validity = unicaCode.split("[|]")[7];
                    offerCharges = unicaCode.split("[|]")[8];
                    resegmentFlag = unicaCode.split("[|]")[9].split("[,]")[1];
                    flowchartName = unicaCode.split("[|]")[10].split("[,]")[1];
                    callID = unicaCode.split("[|]")[11];
                } catch (Exception ex) {
                    toReturn += "F";
                    result += ex.getLocalizedMessage().trim();
                    Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                }

                NameValuePairImpl parm1 = new NameValuePairImpl();
                parm1.setName("UACIOfferTrackingCode");
                parm1.setValueAsString(trackingCode);
                parm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm2 = new NameValuePairImpl();
                parm2.setName("UACICustomLoggerTableName");
                parm2.setValueAsString(unica_CustomLoggerTableName_Accept);
                parm2.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm3 = new NameValuePairImpl();
                //parm3.setName("MSISDN");
                parm3.setName("CUSTOMERID");
                parm3.setValueAsString(msisdn);
                parm3.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm4 = new NameValuePairImpl();
                parm4.setName("OFFERCODE1");
                parm4.setValueAsString(offerCode);
                parm4.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm5 = new NameValuePairImpl();
                parm5.setName("PRODUCT_ID");
                parm5.setValueAsString(unica_Product_ID_Accept);
                parm5.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm6 = new NameValuePairImpl();
                parm6.setName("FINAL_DATE");
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DATE, new Double(validity).intValue());
                parm6.setValueAsDate(cal.getTime());
                parm6.setValueDataType(NameValuePair.DATA_TYPE_DATETIME);

                NameValuePairImpl parm7 = new NameValuePairImpl();
                parm7.setName("Offer_Type");
                parm7.setValueAsNumeric(new Double(offerType));
                parm7.setValueDataType(NameValuePair.DATA_TYPE_NUMERIC);

                NameValuePairImpl parm8 = new NameValuePairImpl();
                parm8.setName("AP_ID");
                parm8.setValueAsString((APID));
                parm8.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm9 = new NameValuePairImpl();
                parm9.setName("CALL_ID");
                parm9.setValueAsString(callID);
                parm9.setValueDataType(NameValuePair.DATA_TYPE_STRING);

//				NameValuePairImpl parm10 = new NameValuePairImpl();
//				parm10.setName("Validity_Date");
//				parm10.setValueAsNumeric(new Double(SMSValidity));
//				parm10.setValueDataType(NameValuePair.DATA_TYPE_NUMERIC);
//
//				NameValuePairImpl parm11 = new NameValuePairImpl();
//				parm11.setName("Offer_Charges");
//				parm11.setValueAsNumeric(new Double(offerCharges));
//				parm11.setValueDataType(NameValuePair.DATA_TYPE_NUMERIC);
                NameValuePairImpl parm12 = new NameValuePairImpl();
                parm12.setName("UACIExecuteFlowchartByName");
                parm12.setValueAsString(flowchartName);
                parm12.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm13 = new NameValuePairImpl();
                parm13.setName("EventName");
                parm13.setValueAsString(unica_EventName_Accept);
                parm13.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm14 = new NameValuePairImpl();
                parm14.setName("Agent_ID");
                parm14.setValueAsString(agent_id);
                parm14.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm15 = new NameValuePairImpl();
                parm15.setName("EVENT_TYPE");
                parm15.setValueAsString(eventName);
                parm15.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm1 = new NameValuePairImpl();
                pageLoadparm1.setName("EventName");
                pageLoadparm1.setValueAsString(unica_EventName_PageLoad);
                pageLoadparm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm2 = new NameValuePairImpl();
                pageLoadparm2.setName("UACIOfferTrackingCode");
                pageLoadparm2.setValueAsString(trackingCode);
                pageLoadparm2.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                //Edit Parameters for Hash3CC
                NameValuePairImpl pageLoadparm3 = new NameValuePairImpl();
                pageLoadparm3.setName("UACICustomLoggerTableName");
                pageLoadparm3.setValueAsString("VW_PAGELOAD");
                pageLoadparm3.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm4 = new NameValuePairImpl();
                pageLoadparm4.setName("ApplicationName");
//                pageLoadparm4.setValueAsString("HashCubeServiceCenter");
                pageLoadparm4.setValueAsString("HashCube");
                pageLoadparm4.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm5 = new NameValuePairImpl();
                pageLoadparm5.setName("SESSION_ID");
                pageLoadparm5.setValueAsString(sessionID);
                pageLoadparm5.setValueDataType(NameValuePair.DATA_TYPE_STRING);

//                NameValuePairImpl pageLoadparm6 = new NameValuePairImpl();
//                pageLoadparm6.setName("INSERT_DATE");
//                pageLoadparm6.setValueAsDate(new Date());
//                pageLoadparm6.setValueDataType(NameValuePair.DATA_TYPE_DATETIME);
//                NameValuePairImpl[] pageLoadEventParameters = {pageLoadparm1, pageLoadparm2};
                // Edit Parameters in Array for Hash3CC
//                NameValuePairImpl[] pageLoadEventParameters = {pageLoadparm1, pageLoadparm2, pageLoadparm3, parm14, pageLoadparm4, pageLoadparm5, parm15, parm3, pageLoadparm6};
                NameValuePairImpl[] pageLoadEventParameters = {pageLoadparm1, pageLoadparm2, pageLoadparm3, parm14, pageLoadparm4, pageLoadparm5, parm15, parm3};

                try {
                    response = unica_api.postEvent(sessionID, "pageLoad", pageLoadEventParameters);
                    result += unica_processPostEventResponse(response, conn_id, trackingCode, "pageLoad");
                    setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + "pageLoad");
                    toReturn += "T";
                } catch (Exception e) {
                    setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + "pageLoad");
                    toReturn += "F";
                    result += e.getLocalizedMessage().trim();
                    e.printStackTrace();
                }

                if (new Double(resegmentFlag).intValue() == 1) {
                    //NameValuePairImpl[] postEventParameters = {parm1, parm2, parm3, parm4, parm5, parm6, parm7, parm8, parm9, parm10, parm11, parm12, parm13, parm14};
                    NameValuePairImpl[] postEventParameters = {parm1, parm2, parm3, parm4, parm5, parm6, parm7, parm8, parm9, parm12, parm13, parm14, parm15};

                    if (unicaResegOfferCode == null) {
                    } else if (unicaResegOfferCode.equals("null")) {
                    } else {
                        NameValuePairImpl parmOFRCODE = new NameValuePairImpl();
                        parmOFRCODE.setName("OFR_CODE");
                        parmOFRCODE.setValueAsString(unicaResegOfferCode);
                        parmOFRCODE.setValueDataType(NameValuePair.DATA_TYPE_STRING);
                        postEventParameters[postEventParameters.length] = parmOFRCODE;
                    }

                    try {
                        response = unica_api.postEvent(sessionID, eventName, postEventParameters);
                        result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                        setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "T";
                    } catch (Exception e) {
                        setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "F";
                        result += e.getLocalizedMessage().trim();
                        e.printStackTrace();
                    }

                    eventName += "_resegmentation";
                    //Edit Parameters for Hash3CC
                    NameValuePairImpl resegParm1 = new NameValuePairImpl();
                    resegParm1.setName("EventName");
                    resegParm1.setValueAsString(eventName);
                    resegParm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

//                    NameValuePairImpl[] postEventResegmentationParameters = {parm12};
                    //Edit parameters in Array for Hash3CC
                    NameValuePairImpl[] postEventResegmentationParameters = {parm12, pageLoadparm2, pageLoadparm5, resegParm1};

                    try {
                        response = unica_api.postEvent(sessionID, eventName, postEventResegmentationParameters);
                        result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                        setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "T";
                    } catch (Exception e) {
                        setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "F";
                        result += e.getLocalizedMessage().trim();
                        e.printStackTrace();
                    }

                    toReturn += ",";

                    toReturn += result;

                } else {
                    //NameValuePairImpl[] postEventParameters = {parm1, parm2, parm3, parm4, parm5, parm6, parm7, parm8, parm9, parm10, parm11, parm13, parm14};
                    NameValuePairImpl[] postEventParameters = {parm1, parm2, parm3, parm4, parm5, parm6, parm7, parm8, parm9, parm13, parm14, parm15};

                    if (unicaResegOfferCode == null) {
                    } else if (unicaResegOfferCode.equals("null")) {
                    } else {
                        NameValuePairImpl parmOFRCODE = new NameValuePairImpl();
                        parmOFRCODE.setName("OFR_CODE");
                        parmOFRCODE.setValueAsString(unicaResegOfferCode);
                        parmOFRCODE.setValueDataType(NameValuePair.DATA_TYPE_STRING);
                        postEventParameters[postEventParameters.length] = parmOFRCODE;
                    }

                    try {
                        response = unica_api.postEvent(sessionID, eventName, postEventParameters);
                        result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                        setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "T";
                    } catch (Exception e) {
                        setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "F";
                        result += e.getLocalizedMessage().trim();
                        e.printStackTrace();
                    }

                    toReturn += ",";

                    toReturn += result;

                }

            } else if (eventName.equals("reject")) {

                String trackingCode = "";
                String offerCode = "";
                String offerType = "";
                String APID = "";
                String NoOfSMS = "";
                String SMSValidity = "";
                String color = "";
                String validity = "";
                String offerCharges = "";
                String resegmentFlag = "";
                String flowchartName = "";
                String callID = "";
                try {
                    trackingCode = unicaCode.split("[|]")[0];
                    offerCode = unicaCode.split("[|]")[1];
                    offerType = unicaCode.split("[|]")[2];
                    APID = unicaCode.split("[|]")[3];
                    NoOfSMS = unicaCode.split("[|]")[4];
                    SMSValidity = unicaCode.split("[|]")[5];
                    color = unicaCode.split("[|]")[6];
                    validity = unicaCode.split("[|]")[7];
                    offerCharges = unicaCode.split("[|]")[8];
                    resegmentFlag = unicaCode.split("[|]")[9].split("[,]")[2];
                    flowchartName = unicaCode.split("[|]")[10].split("[,]")[2];
                    callID = unicaCode.split("[|]")[11];
                } catch (Exception ex) {
                    toReturn += "F";
                    result += ex.getLocalizedMessage().trim();
                    Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                }

                NameValuePairImpl parm1 = new NameValuePairImpl();
                parm1.setName("UACIOfferTrackingCode");
                parm1.setValueAsString(trackingCode);
                parm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm2 = new NameValuePairImpl();
                parm2.setName("UACICustomLoggerTableName");
                parm2.setValueAsString(unica_CustomLoggerTableName_Reject);
                parm2.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm3 = new NameValuePairImpl();
                //parm3.setName("MSISDN");
                parm3.setName("CUSTOMERID");
                parm3.setValueAsString(msisdn);
                parm3.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm4 = new NameValuePairImpl();
                parm4.setName("OFFERCODE1");
                parm4.setValueAsString(offerCode);
                parm4.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm5 = new NameValuePairImpl();
                parm5.setName("PRODUCT_ID");
                parm5.setValueAsString(unica_Product_ID_Reject);
                parm5.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm6 = new NameValuePairImpl();
                parm6.setName("FINAL_DATE");
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DATE, new Double(validity).intValue());
                parm6.setValueAsDate(cal.getTime());
                parm6.setValueDataType(NameValuePair.DATA_TYPE_DATETIME);

                NameValuePairImpl parm7 = new NameValuePairImpl();
                parm7.setName("EventName");
                parm7.setValueAsString(unica_EventName_Reject);
                parm7.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm8 = new NameValuePairImpl();
                parm8.setName("UACIExecuteFlowchartByName");
                parm8.setValueAsString(flowchartName);
                parm8.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm9 = new NameValuePairImpl();
                parm9.setName("Agent_ID");
                parm9.setValueAsString(agent_id);
                parm9.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm10 = new NameValuePairImpl();
                parm10.setName("CALL_ID");
                parm10.setValueAsString(callID);
                parm10.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm11 = new NameValuePairImpl();
                parm11.setName("EVENT_TYPE");
                parm11.setValueAsString(eventName);
                parm11.setValueDataType(NameValuePair.DATA_TYPE_STRING);
                //Edit Parameters for Hash3CC

                NameValuePairImpl parm14 = new NameValuePairImpl();
                parm14.setName("Agent_ID");
                parm14.setValueAsString(agent_id);
                parm14.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm15 = new NameValuePairImpl();
                parm15.setName("EVENT_TYPE");
                parm15.setValueAsString(eventName);
                parm15.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm1 = new NameValuePairImpl();
                pageLoadparm1.setName("EventName");
                pageLoadparm1.setValueAsString(unica_EventName_PageLoad);
                pageLoadparm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm2 = new NameValuePairImpl();
                pageLoadparm2.setName("UACIOfferTrackingCode");
                pageLoadparm2.setValueAsString(trackingCode);
                pageLoadparm2.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                // Edit Parameters for Hash3CC
                NameValuePairImpl pageLoadparm3 = new NameValuePairImpl();
                pageLoadparm3.setName("UACICustomLoggerTableName");
                pageLoadparm3.setValueAsString("VW_PAGELOAD");
                pageLoadparm3.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm4 = new NameValuePairImpl();
                pageLoadparm4.setName("ApplicationName");
//                pageLoadparm4.setValueAsString("HashCubeServiceCenter");
                pageLoadparm4.setValueAsString("HashCube");
                pageLoadparm4.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm5 = new NameValuePairImpl();
                pageLoadparm5.setName("SESSION_ID");
                pageLoadparm5.setValueAsString(sessionID);
                pageLoadparm5.setValueDataType(NameValuePair.DATA_TYPE_STRING);

//                NameValuePairImpl pageLoadparm6 = new NameValuePairImpl();
//                pageLoadparm6.setName("INSERT_DATE");
//                pageLoadparm6.setValueAsDate(new Date());
//                pageLoadparm6.setValueDataType(NameValuePair.DATA_TYPE_DATETIME);
                //NameValuePairImpl[] pageLoadEventParameters = {pageLoadparm1, pageLoadparm2};
                //Edit Parameters in Array for Hash3CC
//                NameValuePairImpl[] pageLoadEventParameters = {pageLoadparm1, pageLoadparm2, pageLoadparm3, parm14, pageLoadparm4, pageLoadparm5, parm15, parm3, pageLoadparm6};
                NameValuePairImpl[] pageLoadEventParameters = {pageLoadparm1, pageLoadparm2, pageLoadparm3, parm14, pageLoadparm4, pageLoadparm5, parm15, parm3};

                try {
                    response = unica_api.postEvent(sessionID, "pageLoad", pageLoadEventParameters);
                    result += unica_processPostEventResponse(response, conn_id, trackingCode, "pageLoad");
                    setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + "pageLoad");
                    toReturn += "T";
                } catch (Exception e) {
                    setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + "pageLoad");
                    toReturn += "F";
                    result += e.getLocalizedMessage().trim();
                    e.printStackTrace();
                }

                if (new Double(resegmentFlag).intValue() == 1) {
                    NameValuePairImpl[] postEventParameters = {parm1, parm2, parm3, parm4, parm5, parm6, parm7, parm8, parm9, parm10, parm11};

                    if (unicaResegOfferCode == null) {
                    } else if (unicaResegOfferCode.equals("null")) {
                    } else {
                        NameValuePairImpl parmOFRCODE = new NameValuePairImpl();
                        parmOFRCODE.setName("OFR_CODE");
                        parmOFRCODE.setValueAsString(unicaResegOfferCode);
                        parmOFRCODE.setValueDataType(NameValuePair.DATA_TYPE_STRING);
                        postEventParameters[postEventParameters.length] = parmOFRCODE;
                    }

                    try {
                        response = unica_api.postEvent(sessionID, eventName, postEventParameters);
                        result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                        setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "T";
                    } catch (Exception e) {
                        setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "F";
                        result += e.getLocalizedMessage().trim();
                        e.printStackTrace();
                    }

                    eventName += "_resegmentation";

                    //Edit Parameters for Hash3CC
                    NameValuePairImpl resegParm1 = new NameValuePairImpl();
                    resegParm1.setName("EventName");
                    resegParm1.setValueAsString(eventName);
                    resegParm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);
                    //Edit Parameters in Array for Hash3CC

                    NameValuePairImpl[] postEventResegmentationParameters = {parm8, pageLoadparm2, pageLoadparm5, resegParm1};

//                    NameValuePairImpl[] postEventResegmentationParameters = {parm8};
                    try {
                        response = unica_api.postEvent(sessionID, eventName, postEventResegmentationParameters);
                        result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                        setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "T";
                    } catch (Exception e) {
                        setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "F";
                        result += e.getLocalizedMessage().trim();
                        e.printStackTrace();
                    }

                    toReturn += ",";

                    toReturn += result;
                } else {
                    NameValuePairImpl[] postEventParameters = {parm1, parm2, parm3, parm4, parm5, parm6, parm7, parm9, parm10, parm11};

                    if (unicaResegOfferCode == null) {
                    } else if (unicaResegOfferCode.equals("null")) {
                    } else {
                        NameValuePairImpl parmOFRCODE = new NameValuePairImpl();
                        parmOFRCODE.setName("OFR_CODE");
                        parmOFRCODE.setValueAsString(unicaResegOfferCode);
                        parmOFRCODE.setValueDataType(NameValuePair.DATA_TYPE_STRING);
                        postEventParameters[postEventParameters.length] = parmOFRCODE;
                    }

                    try {
                        response = unica_api.postEvent(sessionID, eventName, postEventParameters);
                        result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                        setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "T";
                    } catch (Exception e) {
                        setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "F";
                        result += e.getLocalizedMessage().trim();
                        e.printStackTrace();
                    }

                    toReturn += ",";

                    toReturn += result;
                }

            } else if (eventName.equals("interest")) {

                String trackingCode = "";
                String offerCode = "";
                String offerType = "";
                String APID = "";
                String NoOfSMS = "";
                String SMSValidity = "";
                String color = "";
                String validity = "";
                String offerCharges = "";
                String resegmentFlag = "";
                String flowchartName = "";
                String callID = "";
                try {
                    trackingCode = unicaCode.split("[|]")[0];
                    offerCode = unicaCode.split("[|]")[1];
                    offerType = unicaCode.split("[|]")[2];
                    APID = unicaCode.split("[|]")[3];
                    NoOfSMS = unicaCode.split("[|]")[4];
                    SMSValidity = unicaCode.split("[|]")[5];
                    color = unicaCode.split("[|]")[6];
                    validity = unicaCode.split("[|]")[7];
                    offerCharges = unicaCode.split("[|]")[8];
                    resegmentFlag = unicaCode.split("[|]")[9].split("[,]")[0];
                    flowchartName = unicaCode.split("[|]")[10].split("[,]")[0];
                    callID = unicaCode.split("[|]")[11];
                } catch (Exception ex) {
                    toReturn += "F";
                    result += ex.getLocalizedMessage().trim();
                    Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                }

                NameValuePairImpl parm1 = new NameValuePairImpl();
                parm1.setName("UACIOfferTrackingCode");
                parm1.setValueAsString(trackingCode);
                parm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm2 = new NameValuePairImpl();
                parm2.setName("UACICustomLoggerTableName");
                parm2.setValueAsString(unica_CustomLoggerTableName_Interest);
                parm2.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm3 = new NameValuePairImpl();
                //parm3.setName("MSISDN");
                parm3.setName("CUSTOMERID");
                parm3.setValueAsString(msisdn);
                parm3.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm4 = new NameValuePairImpl();
                parm4.setName("OFFERCODE1");
                parm4.setValueAsString(offerCode);
                parm4.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm5 = new NameValuePairImpl();
                parm5.setName("SCORE");
                parm5.setValueAsNumeric(new Integer(unica_Score_Interest).doubleValue());
                parm5.setValueDataType(NameValuePair.DATA_TYPE_NUMERIC);

                NameValuePairImpl parm6 = new NameValuePairImpl();
                parm6.setName("OVERRIDETYPEID");
                parm6.setValueAsNumeric(new Integer(unica_OverRideTypeID_Interest).doubleValue());
                parm6.setValueDataType(NameValuePair.DATA_TYPE_NUMERIC);

                NameValuePairImpl parm7 = new NameValuePairImpl();
                parm7.setName("PREDICATE");
                parm7.setValueAsString(unica_Predicate_Interest);
                parm7.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm8 = new NameValuePairImpl();
                parm8.setName("FINALSCORE");
                parm8.setValueAsNumeric(new Integer(unica_FinalScore_Interest).doubleValue());
                parm8.setValueDataType(NameValuePair.DATA_TYPE_NUMERIC);

                NameValuePairImpl parm9 = new NameValuePairImpl();
                parm9.setName("CELLCODE");
                parm9.setValueAsString(unica_CellCode_Interest);
                parm9.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm10 = new NameValuePairImpl();
                parm10.setName("ZONE");
                parm10.setValueAsString(unica_Zone_Interest);
                parm10.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm11 = new NameValuePairImpl();
                parm11.setName("ENABLESTATEID");
                parm11.setValueAsNumeric(new Integer(unica_EnableStateID_Interest).doubleValue());
                parm11.setValueDataType(NameValuePair.DATA_TYPE_NUMERIC);

                NameValuePairImpl parm12 = new NameValuePairImpl();
                parm12.setName("EventName");
                parm12.setValueAsString(unica_EventName_Interest);
                parm12.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm13 = new NameValuePairImpl();
                parm13.setName("UACIExecuteFlowchartByName");
                parm13.setValueAsString(flowchartName);
                parm13.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm14 = new NameValuePairImpl();
                parm14.setName("Agent_ID");
                parm14.setValueAsString(agent_id);
                parm14.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm15 = new NameValuePairImpl();
                parm15.setName("CALL_ID");
                parm15.setValueAsString(callID);
                parm15.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                //Edit Parameters for Hash3CC
                NameValuePairImpl parm16 = new NameValuePairImpl();
                parm16.setName("EVENT_TYPE");
                parm16.setValueAsString(eventName);
                parm16.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm1 = new NameValuePairImpl();
                pageLoadparm1.setName("EventName");
                pageLoadparm1.setValueAsString(unica_EventName_PageLoad);
                pageLoadparm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm2 = new NameValuePairImpl();
                pageLoadparm2.setName("UACIOfferTrackingCode");
                pageLoadparm2.setValueAsString(trackingCode);
                pageLoadparm2.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                // Edit Parameters for Hash3CC
                NameValuePairImpl pageLoadparm3 = new NameValuePairImpl();
                pageLoadparm3.setName("UACICustomLoggerTableName");
                pageLoadparm3.setValueAsString("VW_PAGELOAD");
                pageLoadparm3.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm4 = new NameValuePairImpl();
                pageLoadparm4.setName("ApplicationName");
//                pageLoadparm4.setValueAsString("HashCubeServiceCenter");
                pageLoadparm4.setValueAsString("HashCube");
                pageLoadparm4.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl pageLoadparm5 = new NameValuePairImpl();
                pageLoadparm5.setName("SESSION_ID");
                pageLoadparm5.setValueAsString(sessionID);
                pageLoadparm5.setValueDataType(NameValuePair.DATA_TYPE_STRING);

//                NameValuePairImpl pageLoadparm6 = new NameValuePairImpl();
//                pageLoadparm6.setName("INSERT_DATE");
//                pageLoadparm6.setValueAsDate(new Date());
//                pageLoadparm6.setValueDataType(NameValuePair.DATA_TYPE_DATETIME);
                // Edit Parameters in Array Hash3CC
//                NameValuePairImpl[] pageLoadEventParameters = {pageLoadparm1, pageLoadparm2, pageLoadparm3, parm14, pageLoadparm4, pageLoadparm5, parm16, parm3, pageLoadparm6};
                NameValuePairImpl[] pageLoadEventParameters = {pageLoadparm1, pageLoadparm2, pageLoadparm3, parm14, pageLoadparm4, pageLoadparm5, parm16, parm3};

//                NameValuePairImpl[] pageLoadEventParameters = {pageLoadparm1, pageLoadparm2};
                try {
                    response = unica_api.postEvent(sessionID, "pageLoad", pageLoadEventParameters);
                    result += unica_processPostEventResponse(response, conn_id, trackingCode, "pageLoad");
                    setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + "pageLoad");
                    toReturn += "T";
                } catch (Exception e) {
                    setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + "pageLoad");
                    toReturn += "F";
                    result += e.getLocalizedMessage().trim();
                    e.printStackTrace();
                }

                if (new Double(resegmentFlag).intValue() == 1) {
                    NameValuePairImpl[] postEventParameters = {parm1, parm2, parm3, parm4, parm5, parm6, parm7, parm8, parm9, parm10, parm11, parm12, parm13, parm14, parm15};

                    if (unicaResegOfferCode == null) {
                    } else if (unicaResegOfferCode.equals("null")) {
                    } else {
                        NameValuePairImpl parmOFRCODE = new NameValuePairImpl();
                        parmOFRCODE.setName("OFR_CODE");
                        parmOFRCODE.setValueAsString(unicaResegOfferCode);
                        parmOFRCODE.setValueDataType(NameValuePair.DATA_TYPE_STRING);
                        postEventParameters[postEventParameters.length] = parmOFRCODE;
                    }

                    try {
                        response = unica_api.postEvent(sessionID, eventName, postEventParameters);
                        result += unica_processPostEventResponse(response, conn_id, trackingCode, "pageLoad");
                        setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "T";
                    } catch (Exception e) {
                        setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "F";
                        result += e.getLocalizedMessage().trim();
                        e.printStackTrace();
                    }

                    eventName += "_resegmentation";

                    //Edit Parameters for Hash3CC
                    NameValuePairImpl resegParm1 = new NameValuePairImpl();
                    resegParm1.setName("EventName");
                    resegParm1.setValueAsString(eventName);
                    resegParm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                    NameValuePairImpl[] postEventResegmentationParameters = {parm13, pageLoadparm2, pageLoadparm5, resegParm1};
//                    NameValuePairImpl[] postEventResegmentationParameters = {parm13};

                    try {
                        response = unica_api.postEvent(sessionID, eventName, postEventResegmentationParameters);
                        result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                        setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "T";
                    } catch (Exception e) {
                        setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "F";
                        result += e.getLocalizedMessage().trim();
                        e.printStackTrace();
                    }

                    toReturn += ",";

                    toReturn += result;
                } else {
                    NameValuePairImpl[] postEventParameters = {parm1, parm2, parm3, parm4, parm5, parm6, parm7, parm8, parm9, parm10, parm11, parm12, parm14, parm15};

                    if (unicaResegOfferCode == null) {
                    } else if (unicaResegOfferCode.equals("null")) {
                    } else {
                        NameValuePairImpl parmOFRCODE = new NameValuePairImpl();
                        parmOFRCODE.setName("OFR_CODE");
                        parmOFRCODE.setValueAsString(unicaResegOfferCode);
                        parmOFRCODE.setValueDataType(NameValuePair.DATA_TYPE_STRING);
                        postEventParameters[postEventParameters.length] = parmOFRCODE;
                    }

                    try {
                        response = unica_api.postEvent(sessionID, eventName, postEventParameters);
                        result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                        setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "T";
                    } catch (Exception e) {
                        setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                        toReturn += "F";
                        result += e.getLocalizedMessage().trim();
                        e.printStackTrace();
                    }

                    toReturn += ",";

                    toReturn += result;
                }

            } else if (eventName.equals("pageLoad")) {

                String trackingCode = "";
                String offerCode = "";
                String offerType = "";
                String APID = "";
                String NoOfSMS = "";
                String SMSValidity = "";
                String color = "";
                String validity = "";
                String offerCharges = "";
                String resegmentFlag = "";
                String flowchartName = "";
                String EventName = "";
                try {
                    trackingCode = unicaCode.split("[|]")[0];
                    offerCode = unicaCode.split("[|]")[1];
                    offerType = unicaCode.split("[|]")[2];
                    APID = unicaCode.split("[|]")[3];
                    NoOfSMS = unicaCode.split("[|]")[4];
                    SMSValidity = unicaCode.split("[|]")[5];
                    color = unicaCode.split("[|]")[6];
                    validity = unicaCode.split("[|]")[7];
                    offerCharges = unicaCode.split("[|]")[8];
                    resegmentFlag = unicaCode.split("[|]")[9];
                    flowchartName = unicaCode.split("[|]")[10];
                    EventName = unicaCode.split("[|]")[11];
                } catch (Exception ex) {
                    toReturn += "F";
                    result += ex.getLocalizedMessage().trim();
                    Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                }

                NameValuePairImpl parm1 = new NameValuePairImpl();
                parm1.setName("EventName");
                parm1.setValueAsString(unica_EventName_PageLoad);
                parm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm2 = new NameValuePairImpl();
                parm2.setName("UACIOfferTrackingCode");
                parm2.setValueAsString(trackingCode);
                parm2.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                // Event Type for Page Load is Page Load Edit Parameter (EVENT_TYPE)
                NameValuePairImpl parm3 = new NameValuePairImpl();
                parm3.setName("EVENT_TYPE");
                parm3.setValueAsString(unica_EventName_PageLoad);
                parm3.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl[] postEventParameters = {parm1, parm2, parm3};

                try {
                    response = unica_api.postEvent(sessionID, eventName, postEventParameters);
                    result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                    setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                    toReturn += "T";
                } catch (Exception e) {
                    setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                    toReturn += "F";
                    result += e.getLocalizedMessage().trim();
                    e.printStackTrace();
                }

                toReturn += ",";

                toReturn += result;

            } else if (eventName.equals("resegmentation")) {

                String trackingCode = "";
                String offerCode = "";
                String offerType = "";
                String APID = "";
                String NoOfSMS = "";
                String SMSValidity = "";
                String color = "";
                String validity = "";
                String offerCharges = "";
                String resegmentFlag = "";
                String flowchartName = "";
                String EventName = "";
                try {
                    trackingCode = unicaCode.split("[|]")[0];
                    offerCode = unicaCode.split("[|]")[1];
                    offerType = unicaCode.split("[|]")[2];
                    APID = unicaCode.split("[|]")[3];
                    NoOfSMS = unicaCode.split("[|]")[4];
                    SMSValidity = unicaCode.split("[|]")[5];
                    color = unicaCode.split("[|]")[6];
                    validity = unicaCode.split("[|]")[7];
                    offerCharges = unicaCode.split("[|]")[8];
                    resegmentFlag = unicaCode.split("[|]")[9];
                    flowchartName = unicaCode.split("[|]")[10];
                    EventName = unicaCode.split("[|]")[11];
                } catch (Exception ex) {
                    toReturn += "F";
                    result += ex.getLocalizedMessage().trim();
                    Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                }

                NameValuePairImpl parm1 = new NameValuePairImpl();
                parm1.setName("EventName");
                parm1.setValueAsString(unica_EventName_ReSeg);
                parm1.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl parm2 = new NameValuePairImpl();
                parm2.setName("UACIExecuteFlowchartByName");
                parm2.setValueAsString(flowchartName);
                parm2.setValueDataType(NameValuePair.DATA_TYPE_STRING);

                NameValuePairImpl[] postEventParameters = {parm1, parm2};

                try {
                    response = unica_api.postEvent(sessionID, eventName, postEventParameters);
                    result += unica_processPostEventResponse(response, conn_id, trackingCode, eventName);
                    setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                    toReturn += "T";
                } catch (Exception e) {
                    setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaPostEvent_" + eventName);
                    toReturn += "F";
                    result += e.getLocalizedMessage().trim();
                    e.printStackTrace();
                }

                toReturn += ",";

                toReturn += result;

            }

        } else {
            toReturn = "Could not set Unica Configs";
        }
        return toReturn;
    }

    private String unica_processPostEventResponse(Response response, String conn_id, String offer_id, String offer_action) {
        String toReturn = "";
        String result = "";

        if (response.getStatusCode() == Response.STATUS_SUCCESS) {
            result += ("postEvent call processed with no warnings or errors");
            insertUnicaOfferAction(conn_id, "success", offer_id, offer_action);
        } else if (response.getStatusCode() == Response.STATUS_WARNING) {
            result += ("postEvent call processed with a warning");
            insertUnicaOfferAction(conn_id, "warning", offer_id, offer_action);
        } else {
            result += ("postEvent call processed with an error");
            insertUnicaOfferAction(conn_id, "error", offer_id, offer_action);
        }

        if (response.getStatusCode() != Response.STATUS_SUCCESS) {
            result += ",";
            result += unica_printDetailMessageOfWarningOrError("postEvent", response.getAdvisoryMessages());
        }

        toReturn += result;
        return toReturn;
    }

    private String unicaEndSession(String sessionID, String username, String msisdn, String datetime, String agent_id, String amt_charged, String conn_id, String ipAddr) {
        String toReturn = "";
        String result = "";

        if (setUnicaConfigs()) {

            try {
                unica_api = InteractAPI.getInstance(InteractAPIURL);
            } catch (Exception e) {
                e.printStackTrace();
                result += e.getLocalizedMessage().trim();
            }

            Response response = null;

            try {
                response = unica_api.endSession(sessionID);
                result += unica_processEndSessionResponse(response);
                toReturn += "T";
                setWSLogging(datetime, username, "True", result, result, msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaEndSession");

            } catch (Exception e) {
                toReturn += "F";
                setWSLogging(datetime, username, "False", "Exception Occured", "Exception Occured", msisdn, "null", "null", msisdn, amt_charged, conn_id, ipAddr, "null", "unicaEndSession");
                result += e.getLocalizedMessage().trim();
                e.printStackTrace();
            }

            toReturn += ",";

            toReturn += result;
        } else {
            toReturn = "Could not set Unica Configs";
        }
        return toReturn;
    }

    private String unica_processEndSessionResponse(Response response) {
        String toReturn = "";
        String result = "";

        if (response.getStatusCode() == Response.STATUS_SUCCESS) {
            result += ("endSession call processed with no warnings or errors");
        } else if (response.getStatusCode() == Response.STATUS_WARNING) {
            result += ("endSession call processed with a warning");
        } else {
            result += ("endSession call processed with an error");
        }

        // For any non-successes, there should be advisory messages explaining why
        if (response.getStatusCode() != Response.STATUS_SUCCESS) {
            result += ",";
            result += unica_printDetailMessageOfWarningOrError("endSession", response.getAdvisoryMessages());
        }
        toReturn = result;
        return toReturn;
    }

    private String unica_printDetailMessageOfWarningOrError(String command, AdvisoryMessage[] messages) {
        String toRerurn = "";
        String result = "";
        int messageCode;

        result += ("Calling " + command);
        result += ",";
        for (AdvisoryMessage msg : messages) {
            result += (msg.getMessage());
            result += ",";
            result += (msg.getDetailMessage());
            result += ",";

            messageCode = msg.getMessageCode();

            if (messageCode == AdvisoryMessageCodes.AUDIENCE_ID_NOT_FOUND_IN_PROFILE_TABLE) {
                result += ("AUDIENCE_ID_NOT_FOUND_IN_PROFILE_TABLE");
            } else if (messageCode == AdvisoryMessageCodes.AUDIENCE_NOT_DEFINED_IN_CONFIGURATION) {
                result += ("AUDIENCE_NOT_DEFINED_IN_CONFIGURATION");
            } else if (messageCode == AdvisoryMessageCodes.COMMAND_METHOD_UNRECOGNIZED) {
                result += ("COMMAND_METHOD_UNRECOGNIZED");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_TRYING_RUN_FLOWCHART) {
                result += ("ERROR_TRYING_RUN_FLOWCHART");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_TRYING_TO_ABORT_SEGMENTATION) {
                result += ("ERROR_TRYING_TO_ABORT_SEGMENTATION");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_TRYING_TO_EXECUTE_ASSOCIATED_ACTION) {
                result += ("ERROR_TRYING_TO_EXECUTE_ASSOCIATED_ACTION");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_TRYING_TO_LOOK_UP_EVENT) {
                result += ("ERROR_TRYING_TO_LOOK_UP_EVENT");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_TRYING_TO_LOOK_UP_INTERACTION_POINT) {
                result += ("ERROR_TRYING_TO_LOOK_UP_INTERACTION_POINT");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_TRYING_TO_LOOK_UP_INTERACTIVE_CHANNEL) {
                result += ("ERROR_TRYING_TO_LOOK_UP_INTERACTIVE_CHANNEL");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_TRYING_TO_POST_EVENT_PARAMETERS) {
                result += ("ERROR_TRYING_TO_POST_EVENT_PARAMETERS");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_WHILE_LOADING_RECOMMENDED_OFFERS) {
                result += ("ERROR_WHILE_LOADING_RECOMMENDED_OFFERS");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_WHILE_LOGGING_DEFAULT_TEXT_STATISTICS) {
                result += ("ERROR_WHILE_LOGGING_DEFAULT_TEXT_STATISTICS");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_WHILE_MAKING_INITIAL_SEGMENTATION_REQUEST) {
                result += ("ERROR_WHILE_MAKING_INITIAL_SEGMENTATION_REQUEST");
            } else if (messageCode == AdvisoryMessageCodes.ERROR_WHILE_READING_PARAMETERS) {
                result += ("ERROR_WHILE_READING_PARAMETERS");
            } else if (messageCode == AdvisoryMessageCodes.FLOWCHART_ABORTED) {
                result += ("FLOWCHART_ABORTED");
            } else if (messageCode == AdvisoryMessageCodes.FLOWCHART_FAILED) {
                result += ("FLOWCHART_FAILED");
            } else if (messageCode == AdvisoryMessageCodes.FLOWCHART_NEVER_RUN) {
                result += ("FLOWCHART_NEVER_RUN");
            } else if (messageCode == AdvisoryMessageCodes.FLOWCHART_STILL_RUNNING) {
                result += ("FLOWCHART_STILL_RUNNING");
            } else if (messageCode == AdvisoryMessageCodes.INTERACT_INITIALIZATION_NOT_COMPLETED_SUCCESSFULLY) {
                result += ("INTERACT_INITIALIZATION_NOT_COMPLETED_SUCCESSFULLY");
            } else if (messageCode == AdvisoryMessageCodes.INVALID_AUDIENCE_FIELD_TYPE) {
                result += ("INVALID_AUDIENCE_FIELD_TYPE");
            } else if (messageCode == AdvisoryMessageCodes.INVALID_DUPLICATION_POLICY_TO_GET_OFFERS) {
                result += ("INVALID_DUPLICATION_POLICY_TO_GET_OFFERS");
            } else if (messageCode == AdvisoryMessageCodes.INVALID_EVENT_NAME) {
                result += ("INVALID_EVENT_NAME");
            } else if (messageCode == AdvisoryMessageCodes.INVALID_INTERACTION_POINT) {
                result += ("INVALID_INTERACTION_POINT");
            } else if (messageCode == AdvisoryMessageCodes.INVALID_INTERACTIVE_CHANNEL) {
                result += ("INVALID_INTERACTIVE_CHANNEL");
            } else if (messageCode == AdvisoryMessageCodes.INVALID_NUMBER_OF_OFFERS_REQUESTED) {
                result += ("INVALID_NUMBER_OF_OFFERS_REQUESTED");
            } else if (messageCode == AdvisoryMessageCodes.INVALID_SESSION_ID) {
                result += ("INVALID_SESSION_ID");
            } else if (messageCode == AdvisoryMessageCodes.LOG_CUSTOM_LOGGER_EVENT_EXCEPTION) {
                result += ("LOG_CUSTOM_LOGGER_EVENT_EXCEPTION");
            } else if (messageCode == AdvisoryMessageCodes.LOG_SYSTEM_EVENT_EXCEPTION) {
                result += ("LOG_SYSTEM_EVENT_EXCEPTION");
            } else if (messageCode == AdvisoryMessageCodes.LOG_USER_EVENT_EXCEPTION) {
                result += ("LOG_USER_EVENT_EXCEPTION");
            } else if (messageCode == AdvisoryMessageCodes.MISSING_AUDIENCE_FIELD) {
                result += ("MISSING_AUDIENCE_FIELD");
            } else if (messageCode == AdvisoryMessageCodes.NO_SESSION_EXIST_BUT_WILL_CREATE_NEW_ONE) {
                result += ("NO_SESSION_EXIST_BUT_WILL_CREATE_NEW_ONE");
            } else if (messageCode == AdvisoryMessageCodes.NULL_AUDIENCE_ID) {
                result += ("NULL_AUDIENCE_ID");
            } else if (messageCode == AdvisoryMessageCodes.OFFER_SUPPRESSION_LOAD_FAILED) {
                result += ("OFFER_SUPPRESSION_LOAD_FAILED");
            } else if (messageCode == AdvisoryMessageCodes.PROFILE_LOAD_FAILED) {
                result += ("PROFILE_LOAD_FAILED");
            } else if (messageCode == AdvisoryMessageCodes.RUNTIME_EXCEPTION_ENCOUNTERED) {
                result += ("RUNTIME_EXCEPTION_ENCOUNTERED");
            } else if (messageCode == AdvisoryMessageCodes.SCORE_OVERRIDE_LOAD_FAILED) {
                result += ("SCORE_OVERRIDE_LOAD_FAILED");
            } else if (messageCode == AdvisoryMessageCodes.SEGMENTATION_RUN_FAILED) {
                result += ("SEGMENTATION_RUN_FAILED");
            } else if (messageCode == AdvisoryMessageCodes.SESSION_ID_UNDEFINED) {
                result += ("SESSION_ID_UNDEFINED");
            } else if (messageCode == AdvisoryMessageCodes.SPECIFIED_FLOWCHART_FOR_EXECUTION_DOES_NOT_EXIST) {
                result += ("SPECIFIED_FLOWCHART_FOR_EXECUTION_DOES_NOT_EXIST");
            } else if (messageCode == AdvisoryMessageCodes.TIMEOUT_REACHED_ON_GET_OFFERS_CALL) {
                result += ("TIMEOUT_REACHED_ON_GET_OFFERS_CALL");
            } else if (messageCode == AdvisoryMessageCodes.UNABLE_TO_LOAD_OFFERS_BY_RAW_SQL) {
                result += ("UNABLE_TO_LOAD_OFFERS_BY_RAW_SQL");
            } else if (messageCode == AdvisoryMessageCodes.UNRECOGNIZED_AUDIENCE_LEVEL) {
                result += ("UNRECOGNIZED_AUDIENCE_LEVEL");
            } else if (messageCode == AdvisoryMessageCodes.UNSUPPORTED_AUDIENCE_FIELD_TYPE) {
                result += ("UNSUPPORTED_AUDIENCE_FIELD_TYPE");
            } else {
                result += ("SOMETHING REALLY WENT WRONG!!!!!!!");
            }
            result += ",";
            result += Integer.toString(messageCode);
            result += ",";
        }

        toRerurn = result;
        return toRerurn;
    }

    private String caresHandSetInfo(String number, String datetime, String agent_id, String amt_charged, String conn_id, String ipAddr) {
        String toReturn = "";
        String result = "";
        String desc = "";

        javax.xml.ws.Holder<String> res = new javax.xml.ws.Holder<String>();
        javax.xml.ws.Holder<String> resdesc = new javax.xml.ws.Holder<String>();
        javax.xml.ws.Holder<String> getHandSetInfoResult = new javax.xml.ws.Holder<String>();
        try {

            AjaxHandlerClass_ISB_Cisco.getHandSetInfo(adt_username, adt_password, number, res, resdesc, getHandSetInfoResult);

            result = res.value;
            desc = resdesc.value;

        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += ex.getLocalizedMessage().trim();
            res.value = "F";
            resdesc.value = "Timeout";
        }

        setWSLogging(datetime, agent_id, res.value, resdesc.value, resdesc.value, number, "null", "null", number, amt_charged, conn_id, ipAddr, "null", "getHandSetInfo");

        toReturn = result;

        return toReturn;
    }

    private String caresChangePassword(String user_code, String old_password, String new_password,
            String datetime, String agent_id, String amt_charged, String conn_id, String ipAddr) {
        String result = "";
        String desc = "";
        String toReturn = "";

        javax.xml.ws.Holder<String> res = new javax.xml.ws.Holder<String>();
        javax.xml.ws.Holder<String> resdesc = new javax.xml.ws.Holder<String>();
        try {

            AjaxHandlerClass_ISB_Cisco.changePassword(adt_username, adt_password, user_code, old_password, new_password, res, resdesc);

            result = res.value;
            desc = resdesc.value;

        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += ex.getLocalizedMessage().trim();
            res.value = "F";
            resdesc.value = "Timeout";
        }

        setWSLogging(datetime, agent_id, res.value, resdesc.value, resdesc.value, "null", "null", "null", "null", amt_charged, conn_id, ipAddr, "null", "changePassword");

        toReturn = result + "," + desc;
        return toReturn;
    }

    private String isSuperCardUser(String user_msisdn, String agentId) {
        SecurityParamType credentials = new SecurityParamType();
        credentials.setUserName("cares_mw");
        credentials.setPassword("ptml@2288");
        System.out.println("Calling Super Card");
        String msisdn = user_msisdn;
        String accountType = this.ACCOUNTTYPE;
        javax.xml.ws.Holder<ReturnParams> superCardQueryResult = new javax.xml.ws.Holder<ReturnParams>();
        String isSuperCardUser = "";
        javax.xml.ws.Holder<String> balance = new javax.xml.ws.Holder<String>();
        try {
            AjaxHandlerClass_ISB_Cisco.superCardQuery(credentials, msisdn, accountType, superCardQueryResult, balance);
            isSuperCardUser = superCardQueryResult.value.getReturnCode();
            System.out.println("Super Card return code:" + isSuperCardUser);
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, "SuperCardQuery not called", ex);
            isSuperCardUser = "N";
        }
        LogSuperCardRecord(msisdn, isSuperCardUser, balance.value, agentId);
        return isSuperCardUser;
    }

    private String caresPostCallAdjustment(String user_msisdn, String agent_detail, String _callid, String _clid,
            String _calltype, String _details, String _caresstatus, String _caredt, String _insertiondt,
            String _switchid, String _dnis, String _mobno, String _abal, String _custtype, String _prodtype,
            String _amtcharged, String _agentid, String _retries, String _lastretry, String datetime, String agent_id,
            String amt_charged, String conn_id, String ipAddr,
            String _ndfStatus) {

        String result = "";
        String desc = "";
        String toReturn = "";
        String spReturn = "";
        String isSuperCardUser = isSuperCardUser(user_msisdn, agent_id);
        boolean rowexists = true;
        _details = "101";

        javax.xml.ws.Holder<String> res = new javax.xml.ws.Holder<String>();
        javax.xml.ws.Holder<String> resdesc = new javax.xml.ws.Holder<String>();

        //String sql = "SELECT * FROM Toulene WHERE toulene2='" + _clid + "' AND toulene10='" + _switchid + "';";
        String sql = "SELECT * FROM [dbo].[TOULENE] WHERE [dbo].[TOULENE].[Toulene2]=? AND [dbo].[TOULENE].[Toulene10]=?;";
        try {
            PreparedStatement pStat = conn.prepareStatement(sql);
            pStat.setString(1, _clid);
            pStat.setString(2, _switchid);
            rs = pStat.executeQuery(sql);

            if (rs.next()) {
                rowexists = true;
                _details = "PExists";
            } else {
                rowexists = false;
            }
            rs.close();
            pStat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (_ndfStatus.equals("true")) {

            if (rowexists == false) {
                result = "";
                desc = "";
                try {
                    //New changed before that postCallAdjusment was with desicion. 11/05/2018
                    if (!isSuperCardUser.equals("Y")) {//If Not Y then call
                        AjaxHandlerClass_ISB_Cisco.postCallAdjustment(adt_username, adt_password, user_msisdn, _details, res, resdesc);
                        result = res.value;
                        desc = resdesc.value;
                    }
                } catch (Exception ex) {
                    Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                    result += "F";
                    desc += "Timeout";
                    res.value = "F";
                    resdesc.value = "Timeout";
                }

                setWSLogging(datetime, agent_id, res.value, resdesc.value, resdesc.value, user_msisdn, "null", "null", user_msisdn, amt_charged, conn_id, ipAddr, "null", "postCallAdjustment");

            }
        } else {
            if (_calltype == null) {
                _calltype = "";
            }
            if (_calltype.isEmpty()) {
                if (rowexists == false) {
                    result = "";
                    desc = "";

                    try {
                        //New changed before that postCallAdjusment was with desicion. 11/05/2018
                        if (!isSuperCardUser.equals("Y")) {
                            AjaxHandlerClass_ISB_Cisco.postCallAdjustment(adt_username, adt_password, user_msisdn, _details, res, resdesc);
                            result = res.value;
                            desc = resdesc.value;
                        }

                    } catch (Exception ex) {
                        Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                        result += "F";
                        desc += "Timeout";
                        res.value = "F";
                        resdesc.value = "Timeout";
                    }

                    setWSLogging(datetime, agent_id, res.value, resdesc.value, resdesc.value, user_msisdn, "null", "null", user_msisdn, amt_charged, conn_id, ipAddr, "null", "postCallAdjustment");
                }
            } else {
                _details = "Transferred";
            }
        }

        spReturn = setPostCallAdjustment(_callid, _clid,
                _calltype, _details, _caresstatus, _caredt, _insertiondt,
                _switchid, _dnis, _mobno, _abal, _custtype, _prodtype,
                _amtcharged, _agentid, _retries, _lastretry, result, desc);

        toReturn = result + "," + desc + "," + spReturn + ",SuperCardReturnCode: " + isSuperCardUser;
        return toReturn;
    }

    private String caresWorkCode(String user_msisdn, String workcodes, String user_code,
            int location_id, int user_id, String date, String datetime, String agent_id, String amt_charged,
            String conn_id, String ipAddr, String resolvedArray, String smsSendArray, String ivrSendArray) {
        String result = "";
        String desc = "";
        String toReturn = "";

        int smsCode = -1;
        int workCode = -1;
        String wcName = "";
        String wcHead = "";

        String ivrcode = "";
        String smsstring = "";
        String sop = "";
        String wctype = "";

        try {
            String[] workcodeArray = workcodes.split("[|]");

            String[] _resolvedArray = resolvedArray.split("[|]");
            String[] _smsSendArray = smsSendArray.split("[|]");
            String[] _ivrSendArray = ivrSendArray.split("[|]");

            //int limit = workcodeArray.length-1;
            for (int i = 0; i < workcodeArray.length; i++) {
                workCode = Integer.parseInt(workcodeArray[i].split("[,]")[0]);
                smsCode = Integer.parseInt(workcodeArray[i].split("[,]")[1]);
                wcName = workcodeArray[i].split("[,]")[2].replaceAll("  ", " ");
                wcHead = workcodeArray[i].split("[,]")[3].replaceAll("  ", " ");

                ivrcode = workcodeArray[i].split("[,]")[4].replaceAll("  ", " ");
                smsstring = workcodeArray[i].split("[,]")[5].replaceAll("  ", " ");
                sop = workcodeArray[i].split("[,]")[6].replaceAll("  ", " ");
                wctype = workcodeArray[i].split("[,]")[7].replaceAll("  ", " ");

                boolean smsFlag = false;
                if (smsCode == 1) {
                    smsFlag = true;
                } else {
                    smsFlag = false;
                }

                boolean smsSend = false;
                for (int _i = 0; _i < _smsSendArray.length; _i++) {
                    if (_smsSendArray[_i].equals("")) {
                    } else {
                        if (workCode == Integer.parseInt(_smsSendArray[_i].split("[,]")[0])) {
                            smsSend = true;
                        }
                    }
                }

                boolean ivrEnabled = false;
                if (ivrcode.equals("null")) {
                    ivrEnabled = false;
                } else {
                    ivrEnabled = true;
                }

                boolean ivrSend = false;
                for (int _i = 0; _i < _ivrSendArray.length; _i++) {
                    if (_ivrSendArray[_i].equals("")) {
                    } else {
                        if (workCode == Integer.parseInt(_ivrSendArray[_i].split("[,]")[0])) {
                            ivrSend = true;
                        }
                    }
                }

                String ivrMenuCode = null;
                for (int _i = 0; _i < _ivrSendArray.length; _i++) {
                    if (_ivrSendArray[_i].equals("")) {
                    } else {
                        if (workCode == Integer.parseInt(_ivrSendArray[_i].split("[,]")[0])) {
                            ivrMenuCode = _ivrSendArray[_i].split("[,]")[4].replaceAll("  ", " ");
                        }
                    }
                }
                boolean onlineResolved = false;
                for (int _i = 0; _i < _resolvedArray.length; _i++) {
                    if (_resolvedArray[_i].equals("")) {
                    } else {
                        if (workCode == Integer.parseInt(_resolvedArray[_i].split("[,]")[0])) {
                            onlineResolved = true;
                        }
                    }
                }

                javax.xml.ws.Holder<String> res = new javax.xml.ws.Holder<String>();
                javax.xml.ws.Holder<String> resdesc = new javax.xml.ws.Holder<String>();
                try {

                    AjaxHandlerClass_ISB_Cisco.logInfoWc(adt_username, adt_password, user_msisdn, wcName, wcHead, date, user_code, location_id, user_id, res, resdesc);

                    result += res.value + ",";
                    desc += resdesc.value + ",";

                } catch (Exception ex) {
                    Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                    result += ex.getLocalizedMessage().trim() + ",";
                    desc += "" + ",";
                    res.value = "F";
                    resdesc.value = "Timeout";

                    caresWorkCode_retry(user_msisdn, wcName, wcHead, user_code, user_id, false, workCode);
                }

                setWCDetail(conn_id, user_msisdn, workcodeArray[i].split("[,]")[0], wcName,
                        wcHead, res.value, res.value, datetime,
                        smsFlag, smsSend, ivrEnabled, ivrSend, ivrMenuCode, onlineResolved);

                setWSLogging(datetime, agent_id, res.value, resdesc.value, resdesc.value, user_msisdn, wcName, wcHead, user_msisdn, amt_charged, conn_id, ipAddr, "null", "logInfoWc");

//			System.out.println("___________________________________");
//			System.out.println(smsCode+" | "+smsSend);
//			System.out.println("___________________________________");
                if (smsCode == 1 && smsSend) {
                    try {

                        AjaxHandlerClass_ISB_Cisco.sendWrkCodeSms(adt_username, adt_password, user_msisdn, workCode, res, resdesc);

                        result += res.value + ",";
                        desc += resdesc.value + ",";

                    } catch (Exception ex) {
                        Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
                        result += ex.getLocalizedMessage().trim() + ",";
                        desc += "" + ",";
                        res.value = "F";
                        resdesc.value = "Timeout";

                        caresWorkCode_retry(user_msisdn, wcName, wcHead, user_code, user_id, true, workCode);
                    }

                    setWCSMSDetail(datetime, agent_id, user_msisdn, wcName, conn_id, res.value, resdesc.value, resdesc.value);

                    setWSLogging(datetime, agent_id, res.value, resdesc.value, resdesc.value, user_msisdn, wcName, wcHead, user_msisdn, amt_charged, conn_id, ipAddr, "null", "sendWrkCodeSms");
                }
            }
        } catch (Exception e) {
            toReturn = "Error: " + e.getMessage();
            return toReturn;
        }
        toReturn = "result=" + result + "|" + "desc=" + desc;
        //toReturn = result;
        return toReturn;
    }

    public static void main(String[] args) {
        new AjaxHandlerClass_ISB_Cisco().caresWorkCode_retry("923365054114", "hash workcode", "hash wc group", "haris", 114, true, 123456789);
    }

    private void caresWorkCode_retry(String msisdn, String wcName, String wcGroup, String userName, int userID, boolean smsFlag, int wcID) {

        int _sms_flag = 0;
        if (smsFlag) {
            _sms_flag = 1;
        }
        String sql = "INSERT INTO [Chomos_Retry] "
                + "([MSISDN] "
                + ",[WC_NAME] "
                + ",[WC_group] "
                + ",[User_name] "
                + ",[user_id] "
                + ",[Status] "
                + ",[Retry] "
                + ",[sms_flag]"
                + ",[wcID])"
                + "VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement pStat = null;
        try {
            pStat = conn_live.prepareStatement(sql);
            pStat.setString(1, msisdn);
            pStat.setString(2, wcName);
            pStat.setString(3, wcGroup);
            pStat.setString(4, userName);
            pStat.setInt(5, userID);
            pStat.setString(6, "failed");
            pStat.setInt(7, 0);
            pStat.setInt(8, _sms_flag);
            pStat.setInt(9, wcID);
            int executeUpdate = pStat.executeUpdate(sql);

        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            pStat.close();
        } catch (Exception ex) {
        }
    }

    private String caresUserLogOut(String user_code, int flag, String datetime, String agent_id, String amt_charged, String conn_id, String ipAddr) {
        String result = "";
        String desc = "";
        String toReturn = "";

        javax.xml.ws.Holder<String> res = new javax.xml.ws.Holder<String>();
        javax.xml.ws.Holder<String> resdesc = new javax.xml.ws.Holder<String>();
        try {

            //AjaxHandlerClass_ISB_Cisco.userLogOut(adt_username, adt_password, user_code, flag, res, resdesc);
            res.value = "T";
            resdesc.value = "Success";

            result = res.value;
            desc = resdesc.value;

        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += ex.getLocalizedMessage().trim();
            res.value = "F";
            resdesc.value = "Timeout";
        }

        setWSLogging(datetime, agent_id, res.value, resdesc.value, resdesc.value, "null", "null", "null", "null", amt_charged, conn_id, ipAddr, "null", "userLogOut");

        toReturn = result + "," + desc;
        return toReturn;
    }

    private String getCustomerInfo(String number, String datetime, String agent_id, String amt_charged,
            String conn_id, String ipAddr) {
        String result = "";
        String desc = "";
        String toReturn = "";

        javax.xml.ws.Holder<String> res = new javax.xml.ws.Holder<String>();
        javax.xml.ws.Holder<String> resdesc = new javax.xml.ws.Holder<String>();
        javax.xml.ws.Holder<Integer> queryCustomerResult = new javax.xml.ws.Holder<Integer>();
        try {

            AjaxHandlerClass_ISB_Cisco.queryCustomer(cares_username, cares_password, number, 10, res, resdesc, queryCustomerResult);

            result = res.value;
            desc = resdesc.value;

        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            result += ex.getLocalizedMessage().trim();
            res.value = "F";
            resdesc.value = "Timeout";
        }

        setWSLogging(datetime, agent_id, res.value, resdesc.value, resdesc.value, number, "null", "null", number, amt_charged, conn_id, ipAddr, "null", "queryCustomer_10");

        toReturn = result;
        return toReturn;
    }

    private Boolean isCaresDown() {
        Boolean toReturn = false;

        String sql = "SELECT isCaresDown FROM CCT_CONFIG_MST_ISB";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                toReturn = rs.getBoolean("isCaresDown");
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toReturn;
    }

    private String caresUserLogOn(String user_code, String user_pwd, String user_ip,
            String user_host, String user_mac, String os_user, String datetime, String agent_id,
            String amt_charged, String conn_id, String ipAddr) {
        String result = "";
        String desc = "";
        String toReturn = "";

        javax.xml.ws.Holder<String> res = new javax.xml.ws.Holder<String>();
        javax.xml.ws.Holder<String> resdesc = new javax.xml.ws.Holder<String>();
        /*
         try {

         AjaxHandlerClass_ISB_Cisco.userLogOn(adt_username, adt_password, user_code, user_pwd, user_ip, user_host, user_mac, os_user, res, resdesc);

         result = res.value;
         desc = resdesc.value;

         } catch (Exception ex) {
         Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
         setWSLogging(datetime, agent_id, "F", "Timeout", "Timeout", "null", "null", "null", "null", amt_charged, conn_id, ipAddr, "null", "userLogOn");
         return "F,timeout";
         }

         setWSLogging(datetime, agent_id, res.value, resdesc.value, resdesc.value, "null", "null", "null", "null", amt_charged, conn_id, ipAddr, "null", "userLogOn");

         toReturn = result + "," + desc;
         */

        if (isCaresDown()) {

            setWSLogging(datetime, agent_id, "LDAP bypassed", "LDAP bypassed", "LDAP bypassed", "null", "null", "null", "null", amt_charged, conn_id, ipAddr, "null", "userLogOn");

            toReturn = "LDAP bypassed,0|0|0|" + user_code + "|01|23|45|67|89";
        } else {
            result = "0|0|0|" + user_code + "|01|23|45|67|89";
            ADAuthenticator auth = new ADAuthenticator(AD_domain, AD_host, AD_dn);
            Map<Object, Object> authenticate = null;
            try {
                authenticate = auth.authenticate(user_code, user_pwd);
                if (!authenticate.isEmpty()) {
                    result = "T," + result;
                } else {
                    result = "F," + result;
//					result = "T," + result;
                }
            } catch (Exception ex) {
                result = "F," + result;
                System.out.println("ISB Call Center LDAP Down From Ufone ADT " + result + " At Time " + new java.util.Date());
                Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
//                result = "F," + result;  // Comment Out After Issue
//				result = "T," + result;
            }

            setWSLogging(datetime, agent_id, result.split("[,]")[0], result, result, "null", "null", "null", "null", amt_charged, conn_id, ipAddr, "null", "userLogOn");

            toReturn = result;
        }

        return toReturn;
    }

    private String getFreqWC() {
        String result = "";
        String toReturn = "";

        //String sql = "SELECT * FROM CMS_WorkCodes_new";
        String sql = "SELECT WORKCODE_SMSSTRING.*, IVR_MENUCODE_WC.*, Workcode_Type.*, CMS_WorkCodes_new.*, Workcode_Type.wcid AS Expr1 FROM WORKCODE_SMSSTRING RIGHT JOIN IVR_MENUCODE_WC ON WORKCODE_SMSSTRING.WCID = IVR_MENUCODE_WC.WCID RIGHT JOIN Workcode_Type ON IVR_MENUCODE_WC.WCID = Workcode_Type.wcid RIGHT JOIN CMS_WorkCodes_new ON Workcode_Type.wcid = CMS_WorkCodes_new.id ORDER BY head, wName";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            result += "<awc>";
            while (rs.next()) {
                result += "<wname>";
                result += "<id>" + rs.getString("id") + "</id>";
                result += "<name>" + rs.getString("wName") + "</name>";
                result += "<sms>" + rs.getString("sms") + "</sms>";
                result += "<wHead>" + rs.getString("head") + "</wHead>";
                ////
                result += "<ivrcode>" + rs.getString("IVRMENUCODE") + "</ivrcode>";
                result += "<smsstring>" + rs.getString("SMS_STRING") + "</smsstring>";
                result += "<sop>" + rs.getString("SOP") + "</sop>";
                String _tmp = "<wctype>" + rs.getString("wctype") + "</wctype>";
                result += _tmp.toLowerCase();
                result += "</wname>";
            }
            result += "</awc>";
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getAgentSkillSet_Ajax(String agentID) {
        String result = "";
        String toReturn = "";

        String sql = "SELECT * FROM Agent_skillset WHERE agent_id = ?";
        try {
            PreparedStatement pStat = conn.prepareStatement(sql);
            pStat.setString(1, agentID);
            rs = pStat.executeQuery(sql);

            while (rs.next()) {
                result += rs.getString("skillset") + ",";
            }
            try {
                result = result.substring(0, result.length() - 1);
            } catch (Exception ex) {
                result = "";
            }
            rs.close();
            pStat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getFreqIVR() {
        String result = "";
        String toReturn = "";

        String sql = "SELECT * FROM CCT_IVR_FREQ_PROMPTS";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("MENU_CODE") + "=" + rs.getString("MENU_DESCRIPTION") + "|";
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getIVRXML() {
        String result = "";
        String toReturn = "";
        String ivrcat = "";
        String ivrsubcat = "";

        String sql = "SELECT RTRIM((MENU_DESCRIPTION)) as MENU_DESCRIPTION, RTRIM((MENU_CODE)) as MENU_CODE, RTRIM(LOWER(Category)) as Category, RTRIM(LOWER(SubCategory)) as SubCategory FROM CCT_IVR_PROMPTS_MST group by MENU_DESCRIPTION, MENU_CODE, Category, SubCategory order by Category, SubCategory, MENU_DESCRIPTION";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            result += "<ivr>";
            while (rs.next()) {
                if (ivrcat.isEmpty()) {
                    result += "<ivrcat>";
                    result += "<ivrcatname>" + rs.getString("Category") + "</ivrcatname>";
                    ivrcat = rs.getString("Category");
                } else if (!ivrcat.equals(rs.getString("Category"))) {
                    result += "</ivrcat>";
                    result += "<ivrcat>";
                    result += "<ivrcatname>" + rs.getString("Category") + "</ivrcatname>";
                    ivrcat = rs.getString("Category");
                }
                if (ivrsubcat.isEmpty()) {
                    result += "<ivrsubcat>";
                    result += "<ivrsubcatname>" + rs.getString("SubCategory") + "</ivrsubcatname>";
                    ivrsubcat = rs.getString("SubCategory");
                } else if (!ivrsubcat.equals(rs.getString("SubCategory"))) {
                    result += "</ivrsubcat>";
                    result += "<ivrsubcat>";
                    result += "<ivrsubcatname>" + rs.getString("SubCategory") + "</ivrsubcatname>";
                    ivrsubcat = rs.getString("SubCategory");
                }
                result += "<ivrname>";
                result += "<menucode>" + rs.getString("MENU_CODE") + "</menucode>";
                result += "<menudesc>" + rs.getString("MENU_DESCRIPTION") + "</menudesc>";
                result += "</ivrname>";
            }
            result += "</ivrsubcat>";
            result += "</ivrcat>";
            result += "</ivr>";
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getPIAAWCXML() {
        String result = "";
        String toReturn = "";
        String head = "";

//		String sql = "SELECT CMS_WorkCodes.*, IVR_MENUCODE_WC.*, WORKCODE_SMSSTRING.* FROM CMS_WorkCodes left JOIN IVR_MENUCODE_WC ON CMS_WorkCodes.id = IVR_MENUCODE_WC.WCID left JOIN WORKCODE_SMSSTRING ON CMS_WorkCodes.id = WORKCODE_SMSSTRING.WCID;";
        String sql = "SELECT * FROM pia_information_workcode_view";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            result += "<awc>";
            while (rs.next()) {
                if (head.isEmpty()) {
                    result += "<wchead>";
                    result += "<wcheadname>" + rs.getString("head") + "</wcheadname>";
                    head = rs.getString("head");
                } else if (!head.equals(rs.getString("head"))) {
                    result += "</wchead>";
                    result += "<wchead>";
                    result += "<wcheadname>" + rs.getString("head") + "</wcheadname>";
                    head = rs.getString("head");
                }
                result += "<wname>";
                result += "<id>" + rs.getString("id") + "</id>";
                result += "<name>" + rs.getString("wName") + "</name>";
                result += "<sms>" + rs.getString("sms") + "</sms>";
                result += "<ivrcode>" + rs.getString("IVRMENUCODE") + "</ivrcode>";
                String smsstring = rs.getString("SMS_STRING");
                if (smsstring != null) {
                    smsstring = smsstring.replaceAll("<", "").replaceAll(">", "").replaceAll("&", "").replaceAll("'", "").replaceAll("\r?\n", "");
                }
                result += "<smsstring>" + smsstring + "</smsstring>";
                String smssop = rs.getString("SOP");
                if (smssop != null) {
                    smssop = smssop.replaceAll("<", "").replaceAll(">", "").replaceAll("&", "").replaceAll("'", "").replaceAll("\r?\n", "");
                }
                result += "<sop>" + smssop + "</sop>";
                String _tmp = "<wctype>" + rs.getString("wctype") + "</wctype>";
                result += _tmp.toLowerCase();
                result += "</wname>";
            }
            result += "</wchead>";
            result += "</awc>";
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_KHI.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getAWCXML() {
        String result = "";
        String toReturn = "";
        String head = "";

//		String sql = "SELECT CMS_WorkCodes.*, IVR_MENUCODE_WC.*, WORKCODE_SMSSTRING.* FROM CMS_WorkCodes left JOIN IVR_MENUCODE_WC ON CMS_WorkCodes.id = IVR_MENUCODE_WC.WCID left JOIN WORKCODE_SMSSTRING ON CMS_WorkCodes.id = WORKCODE_SMSSTRING.WCID;";
        String sql = "SELECT * FROM information_workcode_view";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            result += "<awc>";
            while (rs.next()) {
                if (head.isEmpty()) {
                    result += "<wchead>";
                    result += "<wcheadname>" + rs.getString("head") + "</wcheadname>";
                    head = rs.getString("head");
                } else if (!head.equals(rs.getString("head"))) {
                    result += "</wchead>";
                    result += "<wchead>";
                    result += "<wcheadname>" + rs.getString("head") + "</wcheadname>";
                    head = rs.getString("head");
                }
                result += "<wname>";
                result += "<id>" + rs.getString("id") + "</id>";
                result += "<name>" + rs.getString("wName") + "</name>";
                result += "<sms>" + rs.getString("sms") + "</sms>";
                result += "<ivrcode>" + rs.getString("IVRMENUCODE") + "</ivrcode>";
                String smsstring = rs.getString("SMS_STRING");
                if (smsstring != null) {
                    smsstring = smsstring.replaceAll("<", "").replaceAll(">", "").replaceAll("&", "").replaceAll("'", "").replaceAll("\r?\n", "");
                }
                result += "<smsstring>" + smsstring + "</smsstring>";
                String smssop = rs.getString("SOP");
                if (smssop != null) {
                    smssop = smssop.replaceAll("<", "").replaceAll(">", "").replaceAll("&", "").replaceAll("'", "").replaceAll("\r?\n", "");
                }
                result += "<sop>" + smssop + "</sop>";
                String _tmp = "<wctype>" + rs.getString("wctype") + "</wctype>";
                result += _tmp.toLowerCase();
                result += "</wname>";
            }
            result += "</wchead>";
            result += "</awc>";
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    /*private String getAWCXML() {
     String result = "";
     String toReturn = "";
     String head = "";

     String sql = "SELECT * FROM CMS_WorkCodes ORDER BY head, wName";
     try {
     stat = conn.createStatement();
     rs = stat.executeQuery(sql);
     result += "<awc>";
     while (rs.next()) {
     if (head.isEmpty()) {
     result += "<wchead>";
     result += "<wcheadname>" + rs.getString("head") + "</wcheadname>";
     head = rs.getString("head");
     } else if (!head.equals(rs.getString("head"))) {
     result += "</wchead>";
     result += "<wchead>";
     result += "<wcheadname>" + rs.getString("head") + "</wcheadname>";
     head = rs.getString("head");
     }
     result += "<wname>";
     result += "<id>" + rs.getString("id") + "</id>";
     result += "<name>" + rs.getString("wName") + "</name>";
     result += "<sms>" + rs.getString("sms") + "</sms>";
     result += "</wname>";
     }
     result += "</wchead>";
     result += "</awc>";
     rs.close();
     stat.close();
     conn.close();
     } catch (Exception ex) {
     Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
     }
     toReturn = result;
     return toReturn;
     }*/
    private String getUnicaWCXML() {
        String result = "";
        String toReturn = "";

        String sql = "SELECT * FROM CMS_WorkCodes where unica_flag = '1' ORDER BY head, wName";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += "" + rs.getString("id") + "";
                result += "|" + rs.getString("wName") + "";
                result += "|" + rs.getString("head") + "";
                result += "|" + rs.getString("sms") + "";
                result += "|" + rs.getString("unica_flag") + "";
                result += "|" + rs.getString("unica_flow") + "";
                result += ",";
            }
            try {
                result = result.substring(0, result.length() - 1);
            } catch (Exception ex) {
                result = "";
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String updateNotReadyCodes() {
        String result = "";
        String toReturn = "";

        String sql = "SELECT ACTIVITY_CD, ACTIVITY_NAME FROM CCT_ACTIVITY_MST WHERE ACTIVITY_TYPE = 'BRK';";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("ACTIVITY_CD") + "=" + rs.getString("ACTIVITY_NAME") + "|";
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getSupervisor() {
        String result = "";
        String toReturn = "";

        String sql = "SELECT * FROM Supervisor";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("superName").trim() + "|";
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getSystemConfigs() {
        String result = "";
        String toReturn = "";
        String sql = "SELECT * FROM CCT_CONFIG_MST_ISB";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("DNIS_Hangup_Disabled").trim() + "|";
                result += rs.getString("DNIS_Direct_Cares").trim() + "|";
                result += rs.getString("DNIS_Auto_Call_Pickup").trim() + "|";
                result += rs.getString("DNIS_Description_Text").trim() + "|";
                result += rs.getString("DNIS_Feedback_Disabled").trim() + "|";
                result += rs.getString("DNIS_Charging").trim() + "|";
                result += rs.getString("CDN_Description").trim() + "|";
                result += rs.getString("DNIS_Consent").trim() + "|";
                result += rs.getString("charging_rate").trim() + "|";
                result += rs.getString("EM_CDN").trim() + "|";
                result += rs.getString("acwDuration").trim() + "|";
                result += rs.getString("isCaresDown").trim() + "|";
                result += rs.getString("cct_password").trim() + "|";
                result += rs.getString("isTDDown").trim() + "|";
                result += rs.getString("isUnicaDown").trim() + "|";
                result += rs.getString("cardsToShow").trim() + "|";
                result += getLBServers().trim() + "|";
                result += getThirdPartyUrls().trim() + "|";
                if (setUnicaConfigs()) {
                    result += unica_numberRequested + "|";
                } else {
                    result += 10 + "|";
                }
                result += rs.getString("unica_skillset").trim().toLowerCase() + "|";
                result += rs.getString("location_url").trim() + "|";
                result += rs.getString("news_alert_timer_minutes").trim() + "|";
                result += getUnicaLBServers().trim() + "|";
                result += getLocalDBLBServers().trim() + "|";
                result += getCaresLBServers().trim() + "|";
                result += rs.getString("isPIAWCEnabled").trim() + "|";
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getThirdPartyUrls() {
        String result = "";
        String toReturn = "";
        ResultSet rs = null;
        String sql = "SELECT * FROM ThirdPartyURL";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("tp_server") + ",";
            }
            result = result.substring(0, result.length() - 1);
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            rs.close();
        } catch (Exception ex) {
        }
        rs = null;
        toReturn = result;
        return toReturn;
    }

    private String getLBServers() {
        String result = "";
        String toReturn = "";
        ResultSet rs = null;
        String sql = "SELECT * FROM WS_LB";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("ws_server") + ",";
            }
            result = result.substring(0, result.length() - 1);
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            rs.close();
        } catch (Exception ex) {
        }
        rs = null;
        toReturn = result;
        return toReturn;
    }

    private String getUnicaLBServers() {
        String result = "";
        String toReturn = "";
        ResultSet rs = null;
        String sql = "SELECT * FROM lb_unica";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("server") + ",";
            }
            result = result.substring(0, result.length() - 1);
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            rs.close();
        } catch (Exception ex) {
        }
        rs = null;
        toReturn = result;
        return toReturn;
    }

    private String getLocalDBLBServers() {
        String result = "";
        String toReturn = "";
        ResultSet rs = null;
        String sql = "SELECT * FROM lb_localdb";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("server") + ",";
            }
            result = result.substring(0, result.length() - 1);
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            rs.close();
        } catch (Exception ex) {
        }
        rs = null;
        toReturn = result;
        return toReturn;
    }

    private String getCaresLBServers() {
        String result = "";
        String toReturn = "";
        ResultSet rs = null;
        String sql = "SELECT * FROM lb_cares";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("server") + ",";
            }
            result = result.substring(0, result.length() - 1);
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            rs.close();
        } catch (Exception ex) {
        }
        rs = null;
        toReturn = result;
        return toReturn;
    }

    private String insertUnicaOffersPresented(String conn_ID, String status, String offer_ID, String offerName, String offerDesc, String dataDump) {
        String toReturn = "";
        try {
            CallableStatement proc = null;
            try {
                proc = conn_live_2.prepareCall("{ call InsertIntoUnicaofferspresented(?, ?, ?, ?, ?, ?, ?) }");
            } catch (SQLException ex) {
                Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                proc.setString("@para_callconnectionid", conn_ID);
            } catch (SQLException ex) {
                proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
            }
            try {
                proc.setDate("@para_datetime", new java.sql.Date(System.currentTimeMillis()));
            } catch (SQLException ex) {
                proc.setNull("@para_datetime", java.sql.Types.NULL);
            }
            try {
                proc.setString("@para_status", status);
            } catch (SQLException ex) {
                proc.setNull("@para_status", java.sql.Types.NULL);
            }
            try {
                proc.setString("@para_offerid", offer_ID);
            } catch (SQLException ex) {
                proc.setNull("@para_offerid", java.sql.Types.NULL);
            }
            try {
                proc.setString("@para_offername", offerName);
            } catch (SQLException ex) {
                proc.setNull("@para_offername", java.sql.Types.NULL);
            }
            try {
                proc.setString("@para_offerdesc", offerDesc);
            } catch (SQLException ex) {
                proc.setNull("@para_offerdesc", java.sql.Types.NULL);
            }
            try {
                proc.setString("@para_datadump", dataDump);
            } catch (SQLException ex) {
                proc.setNull("@para_datadump", java.sql.Types.NULL);
            }

            boolean execute = proc.execute();
            proc.close();
            toReturn = new Boolean(execute).toString();

        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toReturn;
    }

    private String insertUnicaOfferAction(String conn_ID, String status, String offer_ID, String offerAction) {
        String toReturn = "";
        try {
            CallableStatement proc = null;
            try {
                proc = conn_live_2.prepareCall("{ call InsertIntoUnicaoffersaction(?, ?, ?, ?, ?) }");
            } catch (SQLException ex) {
                Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                proc.setString("@para_callconnectionid", conn_ID);
            } catch (SQLException ex) {
                proc.setNull("@para_callconnectionid", java.sql.Types.NULL);
            }
            try {
                proc.setDate("@para_datetime", new java.sql.Date(System.currentTimeMillis()));
            } catch (SQLException ex) {
                proc.setNull("@para_datetime", java.sql.Types.NULL);
            }
            try {
                proc.setString("@para_status", status);
            } catch (SQLException ex) {
                proc.setNull("@para_status", java.sql.Types.NULL);
            }
            try {
                proc.setString("@para_offerid", offer_ID);
            } catch (SQLException ex) {
                proc.setNull("@para_offerid", java.sql.Types.NULL);
            }
            try {
                proc.setString("@para_action", offerAction);
            } catch (SQLException ex) {
                proc.setNull("@para_action", java.sql.Types.NULL);
            }

            boolean execute = proc.execute();
            proc.close();
            toReturn = new Boolean(execute).toString();

        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return toReturn;
    }

    private boolean setUnicaConfigs() {
        boolean toReturn = false;
        ResultSet rs = null;
        String sql = "SELECT * FROM UNICA_CONFIG";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                InteractAPIURL = rs.getString("unicaAPIURL"); // Comment Live Code Release After testing
//                InteractAPIURL = "https://172.16.20.109/interact/services/InteractService";
                //InteractAPIURL = "http://172.16.20.109/interact/servlet/InteractJSService"; // Testing IBM APIURL
                //InteractAPIURL = "http://sv01apunicatt02.ptml.pk:80/interact/servlet/InteractJSService";
                unica_relyOnExistingSession = rs.getBoolean("unicaRelyOnExistingSession");
                unica_initialDebugFlag = rs.getBoolean("unicaInitialDebugFlag");
                //unica_initialDebugFlag = true;
                unica_audienceLevel = rs.getString("unicaAudienceLevel"); // Comment Live Code Release After testing
                //unica_audienceLevel = "MSISDNText"; // Comment After Testing
                //unica_audienceLevel = "Customer";  // Testing IBM Column Value
                unica_interactiveChannel = rs.getString("unicaInteractiveChannel");
                //unica_interactiveChannel = "00-Test";
                unica_interactionPoint = rs.getString("unicaInteractionPoint");
                //unica_interactionPoint = "getOffers";
                unica_numberRequested = rs.getInt("unicaNumberRequested");
                if (unica_numberRequested > 4) {
                    unica_numberRequested = 4;
                }
                unica_CustomLoggerTableName_Interest = rs.getString("CustomLoggerTableName_Interest");
                unica_Score_Interest = rs.getInt("Score_Interest");
                unica_OverRideTypeID_Interest = rs.getInt("OverRideTypeID_Interest");
                unica_Predicate_Interest = rs.getString("Predicate_Interest");
                unica_FinalScore_Interest = rs.getInt("FinalScore_Interest");
                unica_CellCode_Interest = rs.getString("CellCode_Interest");
                unica_Zone_Interest = rs.getString("Zone_Interest");
                unica_EnableStateID_Interest = rs.getInt("EnableStateID_Interest");
                unica_EventName_Interest = rs.getString("EventName_Interest");
                unica_CustomLoggerTableName_Accept = rs.getString("CustomLoggerTableName_Accept");
                unica_Product_ID_Accept = rs.getString("Product_ID_Accept");
                unica_EventName_Accept = rs.getString("EventName_Accept");
                unica_EventName_PageLoad = rs.getString("EventName_PageLoad");
                unica_CustomLoggerTableName_Reject = rs.getString("CustomLoggerTableName_Reject");
                unica_Product_ID_Reject = rs.getString("Product_ID_Reject");
                unica_EventName_Reject = rs.getString("EventName_Reject");
                unica_EventName_ReSeg = rs.getString("EventName_ReSeg");
                unica_ExecuteFlowchartByName_ReSeg = rs.getString("ExecuteFlowchartByName_ReSeg");
                toReturn = true;
            }
            rs.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = false;
        }
        return toReturn;
        //return InteractAPIURL + ", " +unica_relyOnExistingSession + ", " + unica_initialDebugFlag+", "+unica_audienceLevel+", "+unica_interactiveChannel+", "+unica_interactionPoint+", "+unica_numberRequested;
    }

    private String setUnicaConfigs1() {
        boolean toReturn = false;
        String sql = "SELECT * FROM UNICA_CONFIG";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                InteractAPIURL = rs.getString("unicaAPIURL");
                unica_relyOnExistingSession = rs.getBoolean("unicaRelyOnExistingSession");
                unica_initialDebugFlag = rs.getBoolean("unicaInitialDebugFlag");
                unica_audienceLevel = rs.getString("unicaAudienceLevel");
                unica_interactiveChannel = rs.getString("unicaInteractiveChannel");
                unica_interactionPoint = rs.getString("unicaInteractionPoint");
                unica_numberRequested = rs.getInt("unicaNumberRequested");
                unica_CustomLoggerTableName_Interest = rs.getString("CustomLoggerTableName_Interest");
                unica_Score_Interest = rs.getInt("Score_Interest");
                unica_OverRideTypeID_Interest = rs.getInt("OverRideTypeID_Interest");
                unica_Predicate_Interest = rs.getString("Predicate_Interest");
                unica_FinalScore_Interest = rs.getInt("FinalScore_Interest");
                unica_CellCode_Interest = rs.getString("CellCode_Interest");
                unica_Zone_Interest = rs.getString("Zone_Interest");
                unica_EnableStateID_Interest = rs.getInt("EnableStateID_Interest");
                unica_EventName_Interest = rs.getString("EventName_Interest");
                unica_CustomLoggerTableName_Accept = rs.getString("CustomLoggerTableName_Accept");
                unica_Product_ID_Accept = rs.getString("Product_ID_Accept");
                unica_EventName_Accept = rs.getString("EventName_Accept");
                unica_EventName_PageLoad = rs.getString("EventName_PageLoad");
                unica_CustomLoggerTableName_Reject = rs.getString("CustomLoggerTableName_Reject");
                unica_Product_ID_Reject = rs.getString("Product_ID_Reject");
                unica_EventName_Reject = rs.getString("EventName_Reject");
                unica_EventName_ReSeg = rs.getString("EventName_ReSeg");
                unica_ExecuteFlowchartByName_ReSeg = rs.getString("ExecuteFlowchartByName_ReSeg");
                toReturn = true;
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            toReturn = false;
        }
        //return toReturn;
        return InteractAPIURL + ", " + unica_relyOnExistingSession + ", " + unica_initialDebugFlag + ", " + unica_audienceLevel + ", " + unica_interactiveChannel + ", " + unica_interactionPoint
                + ", " + unica_numberRequested + ", " + unica_CustomLoggerTableName_Interest + ", " + unica_Score_Interest + ", " + unica_OverRideTypeID_Interest + ", " + unica_Predicate_Interest
                + ", " + unica_FinalScore_Interest + ", " + unica_CellCode_Interest + ", " + unica_Zone_Interest + ", " + unica_EnableStateID_Interest + ", " + unica_EventName_Interest
                + ", " + unica_CustomLoggerTableName_Accept + ", " + unica_Product_ID_Accept + ", " + unica_EventName_Accept + ", " + unica_EventName_PageLoad + ", " + unica_CustomLoggerTableName_Reject
                + ", " + unica_Product_ID_Reject + ", " + unica_EventName_Reject + ", " + unica_EventName_ReSeg + ", " + unica_ExecuteFlowchartByName_ReSeg;
    }

    private String getNotReadyCode(String code) {
        String result = "";
        String toReturn = "";

        String sql = "SELECT ACTIVITY_CD FROM CCT_ACTIVITY_MST WHERE ACTIVITY_TYPE = ?;";
        try {
            PreparedStatement pStat = conn.prepareStatement(sql);
            pStat.setString(1, code);
            rs = pStat.executeQuery(sql);

            while (rs.next()) {
                result = rs.getString("ACTIVITY_CD") + "";
            }
            rs.close();
            pStat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getAgentStats(String agent_id) {
        String result = "";
        String toReturn = "";

        String sql = "SELECT * FROM Agentstats_history_Cisco WHERE SkillTargetID = ?;";
        try {
            PreparedStatement pStat = conn_live_2.prepareStatement(sql);
            pStat.setString(1, agent_id);
            pStat.setQueryTimeout(DBTimeoutSec);
            rs = pStat.executeQuery(sql);
            
            while (rs.next()) {
                result += "Login Duration" + "=" + (rs.getLong("loggedintime")) + "=" + formatIntoHHMMSS(rs.getLong("loggedintime")) + "|";
                result += "NR Duration" + "=" + (rs.getLong("notreadytime")) + "=" + formatIntoHHMMSS(rs.getLong("notreadytime")) + "|";
                result += "Productive Duration" + "=" + (rs.getLong("ProductiveDuration")) + "=" + formatIntoHHMMSS(rs.getLong("ProductiveDuration")) + "|";
                result += "Break Duration" + "=" + (rs.getLong("breaktime")) + "=" + formatIntoHHMMSS(rs.getLong("breaktime")) + "|";
                result += "ACW Duration" + "=" + (rs.getLong("acwtime")) + "=" + formatIntoHHMMSS(rs.getLong("acwtime")) + "|";
                result += "Idle Duration" + "=" + (rs.getLong("idleduration")) + "=" + formatIntoHHMMSS(rs.getLong("idleduration")) + "|";
                result += "Hold Duration" + "=" + (rs.getLong("holdtime")) + "=" + formatIntoHHMMSS(rs.getLong("holdtime")) + "|";
                result += "Avg Talk Time" + "=" + (rs.getLong("Avgtalktime")) + "=" + formatIntoHHMMSS(rs.getLong("Avgtalktime")) + "|";
                result += "Calls Offered" + "=" + (rs.getLong("Callsoffered")) + "|";
                result += "Calls Answered" + "=" + (rs.getLong("callsanswered")) + "|";
                result += "Short Calls" + "=" + (rs.getLong("ShortCalls")) + "|";
                result += "Calls Requeued" + "=" + (rs.getLong("callsreq")) + "|";

            }
            rs.close();
            pStat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getConsentInfo(String ipPhoneType) {
        String result = "";
        String toReturn = "";
        String sql = "";
        if (ipPhoneType.equals("Nortel")) {
            sql = "SELECT * FROM tbl_Nortel_Language;";
        } else if (ipPhoneType.equals("Cisco_LHR")) {
            sql = "SELECT * FROM tbl_Cisco_Language;";
        } else if (ipPhoneType.equals("Cisco_KHI")) {
            sql = "SELECT * FROM tbl_Cisco_Language;";
        } else if (ipPhoneType.equals("Cisco_ISB")) {
            sql = "SELECT * FROM tbl_Cisco_Language;";
        } else {
            sql = "SELECT * FROM tbl_Nortel_Language;";
        }
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                result += rs.getString("language") + "=" + rs.getString("value") + "|";
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getSessionHistory(String number) {
        String result = "";
        String toReturn = "";

        String number2 = "";

        if (number.startsWith("92")) {
            number2 = number.substring(2);
        }

        //String sql = "SELECT TOP 30 * FROM session_history_view WHERE cust_num = '" + number + "' order by currentdate desc;";
        String sql = "SELECT TOP 30 * FROM session_history_view WHERE cust_num = ? OR cust_num = ? order by currentdate desc;";
        try {
            PreparedStatement pStat = conn_live_2.prepareStatement(sql);
            pStat.setString(1, number);
            pStat.setString(2, number2);
            rs = pStat.executeQuery(sql);
           
            while (rs.next()) {
                result += rs.getString("currentdate") + "," + rs.getString("agentlogin") + "," + rs.getString("workcode") + "," + rs.getString("NewDate") + "|";
            }
            rs.close();
            pStat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
        }
        toReturn = result;
        return toReturn;
    }

    private String getSessionHistory(String number, String datetime, String username,
            String cli, String amt_charged, String conn_id,
            String ipAddress) {
        String toReturn = "";

        if (number.startsWith("03") && number.length() == 11) {
            number = "92" + number.substring(1);
        } else if (number.startsWith("923") && number.length() == 12) {
        } else if (number.startsWith("3") && number.length() == 10) {
            number = "92" + number;
        }

        WCMSClassCiscoISB wcmsClassISB = new WCMSClassCiscoISB();
        String lastInformationalWorkCodes = wcmsClassISB.getLastInformationalWorkCodes(number, datetime, username, cli, amt_charged, conn_id, ipAddress);

        toReturn = lastInformationalWorkCodes;
        return toReturn;
    }

    private String getNewsUpdate() {

        String toReturn = "";
        JSONObject jsonObj = new JSONObject();

        String sql = "SELECT TOP 1 * FROM [new_cct_live_cisco].[dbo].[tbl_newsalert]";
        try {
            //stat = conn.createStatement();
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            jsonObj.put("result", -1);
            while (rs.next()) {
                if (rs.getString("news_alert_desc").trim().isEmpty()) {
                    jsonObj.put("result", -1);
                    jsonObj.put("newsDesc", "");
                } else {
                    jsonObj.put("result", 1);
                    jsonObj.put("newsDesc", rs.getString("news_alert_desc").trim());
                }
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(AjaxHandlerClass_ISB_Cisco.class.getName()).log(Level.SEVERE, null, ex);
            jsonObj.put("result", -1);
        }
        toReturn = jsonObj.toJSONString();
        return toReturn;
    }

    private String formatIntoHHMMSS(String StrsecsIn) {

        int secsIn = new Float(Float.parseFloat(StrsecsIn)).intValue();

        int hours = secsIn / 3600;
        int remainder = secsIn % 3600;
        int minutes = remainder / 60;
        int seconds = remainder % 60;

        return ((hours < 10 ? "" : "") + hours
                + ":" + (minutes < 10 ? "0" : "") + minutes
                + ":" + (seconds < 10 ? "0" : "") + seconds);
    }

    private String formatIntoHHMMSS(Long LongsecsIn) {

        String StrsecsIn = LongsecsIn.toString();

        int secsIn = new Float(Float.parseFloat(StrsecsIn)).intValue();

        int hours = secsIn / 3600;
        int remainder = secsIn % 3600;
        int minutes = remainder / 60;
        int seconds = remainder % 60;

        return ((hours < 10 ? "" : "") + hours
                + ":" + (minutes < 10 ? "0" : "") + minutes
                + ":" + (seconds < 10 ? "0" : "") + seconds);
    }

    private static void queryCustomer(java.lang.String operusername, java.lang.String operpassword, java.lang.String msisdn, int functionId, javax.xml.ws.Holder<java.lang.String> res, javax.xml.ws.Holder<java.lang.String> resultDesc, javax.xml.ws.Holder<Integer> queryCustomerResult) {
        cares.WS1.USSDQuery service = new cares.WS1.USSDQuery();
        cares.WS1.USSDQuerySoap port = service.getUSSDQuerySoap();

//		String endpointURL = "http://172.17.32.51/ussd_gw_syn/USSD_Query.asmx?WSDL";
//		BindingProvider bindingprovider = (BindingProvider) port;
//		bindingprovider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
//		Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
//		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, WSTimeoutSec * 1000);
        port.queryCustomer(operusername, operpassword, msisdn, functionId, res, resultDesc, queryCustomerResult);
    }

    private static void changePassword(java.lang.String operusername, java.lang.String operpassword, java.lang.String userCode, java.lang.String oldPassword, java.lang.String newPassword, javax.xml.ws.Holder<java.lang.String> rsl, javax.xml.ws.Holder<java.lang.String> resultDesc) {
        cares.WS2.ADTService service = new cares.WS2.ADTService();
        cares.WS2.ADTServiceSoap port = service.getADTServiceSoap();
//		Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
//		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, WSTimeoutSec * 1000);
        port.changePassword(operusername, operpassword, userCode, oldPassword, newPassword, rsl, resultDesc);
    }

    private static void postCallAdjustment(java.lang.String operusername, java.lang.String operpassword, java.lang.String msisdn, java.lang.String details, javax.xml.ws.Holder<java.lang.String> rsl, javax.xml.ws.Holder<java.lang.String> resultDesc) {
        cares.WS2.ADTService service = new cares.WS2.ADTService();
        cares.WS2.ADTServiceSoap port = service.getADTServiceSoap();
//		Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
//		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, WSTimeoutSec * 1000);
        port.postCallAdjustment(operusername, operpassword, msisdn, details, rsl, resultDesc);
    }

    private static void logInfoWc(java.lang.String operusername, java.lang.String operpassword, java.lang.String msisdn, java.lang.String wcName, java.lang.String wcGroup, java.lang.String dateTime, java.lang.String loggedByUser, int userLocId, int userId, javax.xml.ws.Holder<java.lang.String> rsl, javax.xml.ws.Holder<java.lang.String> resultDesc) {
        cares.WS2.ADTService service = new cares.WS2.ADTService();
        cares.WS2.ADTServiceSoap port = service.getADTServiceSoap();
//		Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
//		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, WSTimeoutSec * 1000);
        port.logInfoWc(operusername, operpassword, msisdn, wcName, wcGroup, dateTime, loggedByUser, userLocId, userId, rsl, resultDesc);
    }

    private static void sendWrkCodeSms(java.lang.String operusername, java.lang.String operpassword, java.lang.String msisdn, int workcodeId, javax.xml.ws.Holder<java.lang.String> rsl, javax.xml.ws.Holder<java.lang.String> resultDesc) {
        cares.WS2.ADTService service = new cares.WS2.ADTService();
        cares.WS2.ADTServiceSoap port = service.getADTServiceSoap();
//		Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
//		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, WSTimeoutSec * 1000);
        port.sendWrkCodeSms(operusername, operpassword, msisdn, workcodeId, rsl, resultDesc);
    }

    private static void userLogOut(java.lang.String operusername, java.lang.String operpassword, java.lang.String userCode, int flag, javax.xml.ws.Holder<java.lang.String> rsl, javax.xml.ws.Holder<java.lang.String> resultDesc) {
        cares.WS2.ADTService service = new cares.WS2.ADTService();
        cares.WS2.ADTServiceSoap port = service.getADTServiceSoap();
//		Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
//		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, WSTimeoutSec * 1000);
        port.userLogOut(operusername, operpassword, userCode, flag, rsl, resultDesc);
    }

    private static void userLogOn(java.lang.String operusername, java.lang.String operpassword, java.lang.String userCode, java.lang.String userPwd, java.lang.String userIp, java.lang.String userHost, java.lang.String userMac, java.lang.String osUser, javax.xml.ws.Holder<java.lang.String> rsl, javax.xml.ws.Holder<java.lang.String> resultDesc) {
        cares.WS2.ADTService service = new cares.WS2.ADTService();
        cares.WS2.ADTServiceSoap port = service.getADTServiceSoap();
//		Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
//		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, WSTimeoutSec * 1000);
        port.userLogOn(operusername, operpassword, userCode, userPwd, userIp, userHost, userMac, osUser, rsl, resultDesc);
    }

    //Old HandSet WebService Code
//    private static void getHandSetInfo(java.lang.String operusername, java.lang.String operpassword, java.lang.String msisdn, javax.xml.ws.Holder<java.lang.String> res, javax.xml.ws.Holder<java.lang.String> resultDesc, javax.xml.ws.Holder<java.lang.String> getHandSetInfoResult) {
//        cares.WS3.HandSetQuery service = new cares.WS3.HandSetQuery();
//        cares.WS3.HandSetQuerySoap port = service.getHandSetQuerySoap();
////		Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
////		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, WSTimeoutSec * 1000);
//        port.getHandSetInfo(operusername, operpassword, msisdn, res, resultDesc, getHandSetInfoResult);
//    }
    //New HandSet WebService Code
//    private static void getHandSetInfo(java.lang.String operusername, java.lang.String operpassword, java.lang.String msisdn, javax.xml.ws.Holder<java.lang.String> res, javax.xml.ws.Holder<java.lang.String> resultDesc, javax.xml.ws.Holder<java.lang.String> getHandSetInfoResult) {
//        net.ufonegsm.cust_handset.HandSetQuery service = new net.ufonegsm.cust_handset.HandSetQuery();
//        net.ufonegsm.cust_handset.HandSetQuerySoap port = service.getHandSetQuerySoap();
//        port.getHandSetInfo(operusername, operpassword, msisdn, res, resultDesc, getHandSetInfoResult);
//    }
    //New HandSet WebService Code Using URL
    private static void getHandSetInfo(java.lang.String operusername, java.lang.String operpassword, java.lang.String msisdn, javax.xml.ws.Holder<java.lang.String> res, javax.xml.ws.Holder<java.lang.String> resultDesc, javax.xml.ws.Holder<java.lang.String> getHandSetInfoResult) {
        net.ufonegsm.cust_handset.HandSetQuery service = new net.ufonegsm.cust_handset.HandSetQuery();
        net.ufonegsm.cust_handset.HandSetQuerySoap port = service.getHandSetQuerySoap();
        port.getHandSetInfo(operusername, operpassword, msisdn, res, resultDesc, getHandSetInfoResult);
    }

    private static void superCardQuery(cares.getcusinfo.isb.SecurityParamType securityParams, java.lang.String msisdn, java.lang.String accountType, javax.xml.ws.Holder<cares.getcusinfo.isb.ReturnParams> returnParams, javax.xml.ws.Holder<java.lang.String> balance) {
        cares.getcusinfo.isb.GetCustomerInfo_Service service = new cares.getcusinfo.isb.GetCustomerInfo_Service();
        cares.getcusinfo.isb.GetCustomerInfo port = service.getGetCustomerInfoSOAP();
        port.superCardQuery(securityParams, msisdn, accountType, returnParams, balance);
    }

}
