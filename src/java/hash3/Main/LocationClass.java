/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hash3.Main;

import cares.location.GetIMSILocationRequestMsgType;
import cares.location.GetIMSILocationRequestType;
import cares.location.GetIMSILocationResultMsgType;
import cares.location.GetIMSILocationResultType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author Haris
 */
public class LocationClass implements Runnable {

    JSONObject jsonObj = new JSONObject();
    String _msisdn = "";

    public LocationClass() {
    }

    public LocationClass(String msisdn) {
        _msisdn = msisdn;
    }

    public String getLocationCoordinates(String msisdn) {
        String toReturn = "";

        jsonObj.clear();

        GetIMSILocationRequestType getIMSILocationRequest = new GetIMSILocationRequestType();
        getIMSILocationRequest.setCallingMode("SYNC");
        getIMSILocationRequest.setRequestID(new Long(new Date().getTime()).toString());
        getIMSILocationRequest.setSubscriberNo(msisdn);
        GetIMSILocationRequestMsgType parameters = new GetIMSILocationRequestMsgType();
        parameters.setGetIMSILocationRequest(getIMSILocationRequest);

        GetIMSILocationResultMsgType imsiLocation = null;
        try {
            imsiLocation = LocationClass.getIMSILocation(parameters);

            GetIMSILocationResultType getIMSILocationResult = imsiLocation.getGetIMSILocationResult();

            jsonObj.put("result", getIMSILocationResult.getReturnCode());
            jsonObj.put("resultDesc", getIMSILocationResult.getReturnDesc());
            if (getIMSILocationResult.getReturnCode() == 0) {
                jsonObj.put("cellID", getIMSILocationResult.getCellID());
                jsonObj.put("correlID", getIMSILocationResult.getCorrelID());
                jsonObj.put("location", getIMSILocationResult.getLocation());
                String location = getIMSILocationResult.getLocation();

//				location = "Ufone Tower_Islamabad_[33.7116,73.0573]";
                String lat_long = location.substring(location.indexOf("[") + 1, location.indexOf("]"));
                jsonObj.put("lat", lat_long.split("[,]")[0]);
                jsonObj.put("lon", lat_long.split("[,]")[1]);
            }

        } catch (Exception ex) {
            Logger.getLogger(LocationClass.class.getName()).log(Level.SEVERE, null, ex);
            jsonObj.put("result", -1);
        }

//		if (msisdn.equals("923365054114")) {
//			jsonObj.put("result", 1);
//			jsonObj.put("lat", "24.60147");
//			jsonObj.put("lon", "68.07804");
//		} else if (msisdn.equals("923335500073")) {
//			jsonObj.put("result", 1);
//			jsonObj.put("lat", "24.786472");
//			jsonObj.put("lon", "67.343444");
//		} else if (msisdn.equals("923335555270")) {
//			jsonObj.put("result", 1);
//			jsonObj.put("lat", "24.83611111");
//			jsonObj.put("lon", "67.23416667");
//		} else if (msisdn.equals("923335950000")) {
//			jsonObj.put("result", 1);
//			jsonObj.put("lat", "24.8924");
//			jsonObj.put("lon", "67.17312");
//		} else if (msisdn.equals("923338880008")) {
//			jsonObj.put("result", 1);
//			jsonObj.put("lat", "24.74468");
//			jsonObj.put("lon", "67.92568");
//		} else if (msisdn.equals("923335980086")) {
//			jsonObj.put("result", 1);
//			jsonObj.put("lat", "24.84469");
//			jsonObj.put("lon", "66.99511");
//		} else {
//			jsonObj.put("result", 1);
//			jsonObj.put("lat", "24.88783333");
//			jsonObj.put("lon", "67.1495");
//		}
        toReturn = jsonObj.toJSONString();
        return toReturn;
    }

    private static GetIMSILocationResultMsgType getIMSILocation(cares.location.GetIMSILocationRequestMsgType parameters) {
        cares.location.GetIMSILocationService_Service service = new cares.location.GetIMSILocationService_Service();
        service.setHandlerResolver(new HeaderHandlerResolver_Location_ISB());
        cares.location.GetIMSILocationService port = service.getGetIMSILocationSOAP();
        return port.getIMSILocation(parameters);
    }

    @Override
    public void run() {

        JSONObject jsonObj = new JSONObject();

        GetIMSILocationRequestType getIMSILocationRequest = new GetIMSILocationRequestType();
        getIMSILocationRequest.setCallingMode("SYNC");
        getIMSILocationRequest.setRequestID(new Long(new Date().getTime()).toString());
        getIMSILocationRequest.setSubscriberNo(_msisdn);
        GetIMSILocationRequestMsgType parameters = new GetIMSILocationRequestMsgType();
        parameters.setGetIMSILocationRequest(getIMSILocationRequest);

        GetIMSILocationResultMsgType imsiLocation = null;
        try {
            imsiLocation = LocationClass.getIMSILocation(parameters);

            GetIMSILocationResultType getIMSILocationResult = imsiLocation.getGetIMSILocationResult();

            jsonObj.put("result", getIMSILocationResult.getReturnCode());
            jsonObj.put("resultDesc", getIMSILocationResult.getReturnDesc());
            if (getIMSILocationResult.getReturnCode() == 0) {
                jsonObj.put("cellID", getIMSILocationResult.getCellID());
                jsonObj.put("correlID", getIMSILocationResult.getCorrelID());
                jsonObj.put("location", getIMSILocationResult.getLocation());
                String location = getIMSILocationResult.getLocation();

                String lat_long = location.substring(location.indexOf("[") + 1, location.indexOf("]"));
                jsonObj.put("lat", lat_long.split("[,]")[0]);
                jsonObj.put("lon", lat_long.split("[,]")[1]);
            }

        } catch (Exception ex) {
            Logger.getLogger(LocationClass.class.getName()).log(Level.SEVERE, null, ex);
            jsonObj.put("result", -1);
        }

        if (jsonObj.get("result").equals(0)) {
            String lat = jsonObj.get("lat").toString();
            String lon = jsonObj.get("lon").toString();

//			String dbURL = "172.16.105.50";
//			String dbUrl_live = "jdbc:jtds:sqlserver://" + dbURL + "/new_cct_live_cisco;instance=ccinstdb;";
//			String userId_live = "apollo";
//			String pass_live = "apollo@123";
            String dbUrl_live = "jdbc:jtds:sqlserver://172.16.12.69/new_cct_live_cisco;instance=HASHISBMAINDB;";
            String userId_live = "hashcube_cc";
            String pass_live = "ufone@123";

            Connection conn = null;
            Statement stat = null;
            ResultSet rs = null;

            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);

            } catch (Exception ex) {
                Logger.getLogger(LocationClass.class.getName()).log(Level.SEVERE, null, ex);
            }

//            String sql = "SELECT * FROM [new_cct_live_cisco].[dbo].[data_lbs] WHERE Latitude = '" + lat + "' AND Longitude = '" + lon + "';";
            String sql = "SELECT * FROM [new_cct_live_cisco].[dbo].[data_lbs] WHERE Latitude = ? AND Longitude = ?;";

            try {
                PreparedStatement pStat = conn.prepareStatement(sql);
                pStat.setString(1, lat);
                pStat.setString(2, lon);
                rs = pStat.executeQuery(sql);
//                stat = conn.createStatement();
//                rs = stat.executeQuery(sql);
                if (rs.next()) {
                } else {
                    Statement _stat = null;
                    String location = jsonObj.get("location").toString();
                  //  String _sql = "insert into [new_cct_live_cisco].[dbo].[data_lbs] values('" + lon + "', '" + lat + "', '" + location + "', '" + location + "', '', '', '')";
                     String _sql = "insert into [new_cct_live_cisco].[dbo].[data_lbs] values(?, ?, ?, ?, ?, ?, ?)";
                    try {

                        PreparedStatement _pStat = conn.prepareStatement(_sql);
                        _pStat.setString(1, lon);
                        _pStat.setString(2, lat);
                        _pStat.setString(3, location);
                        _pStat.setString(4, location);
                        _pStat.setString(5, "");
                        _pStat.setString(6, "");
                        _pStat.setString(7, "");
                        int executeUpdate = _pStat.executeUpdate(_sql);
                        _pStat.close();
                    } catch (Exception ex) {
                        Logger.getLogger(LocationClass.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                try {
                    rs.close();
                } catch (Exception ex) {
                }
                try {
                    pStat.close();
                } catch (Exception ex) {
                }
                try {
                    conn.close();
                } catch (Exception ex) {
                }
            } catch (Exception ex) {
                Logger.getLogger(LocationClass.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
        }

    }
}
