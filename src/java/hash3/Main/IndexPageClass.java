package hash3.Main;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

public class IndexPageClass {

	Connection conn = null;
	Statement stat = null;
	ResultSet rs = null;
	String dbUrl = "jdbc:jtds:sqlserver://localhost/Hash3";
	String userId = "sa";
	String pass = "apollopc";
//	String dbUrl_live = "jdbc:jtds:sqlserver://172.16.29.214/new_cct_live";
//	String dbUrl_live = "jdbc:jtds:sqlserver://172.16.105.50/new_cct_live_cisco;instance=ccinstdb;";
//	String userId_live = "apollo";
//	String pass_live = "apollo@123";
	String dbUrl_live = "jdbc:jtds:sqlserver://172.16.12.69/new_cct_live_cisco;instance=HASHISBMAINDB;";
	String userId_live = "hashcube_cc";
	String pass_live = "ufone@123";
	String dbUrl_MS = "jdbc:sqlserver://172.16.29.214;databaseName=new_cct_live";

	public IndexPageClass() {
		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			//Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			//conn = java.sql.DriverManager.getConnection(dbUrl, userId, pass);
			conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);
			//conn = java.sql.DriverManager.getConnection(dbUrl_MS, userId_live, pass_live);
		} catch (Exception ex) {
			Logger.getLogger(IndexPageClass.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public String generateButtonStatesJS(HttpServletRequest req) {
		String result = "";
		String toReturn = "";
		String localAddr = req.getLocalAddr();
		
		if(localAddr.contains("172.16.")){
			dbUrl_live = "jdbc:jtds:sqlserver://172.16.12.69/new_cct_live_cisco;instance=HASHISBMAINDB;";
			userId_live = "hashcube_cc";
			pass_live = "ufone@123";
		} else if(localAddr.contains("172.18.")){
//			dbUrl_live = "jdbc:jtds:sqlserver://172.18.32.117/new_cct_live";
//			userId_live = "sa";
//			pass_live = "ptml@123";
			dbUrl_live = "jdbc:jtds:sqlserver://172.18.1.228/new_cct_live;instance=HASHLHRMAINDB";
			userId_live = "hashcube_cc";
			pass_live = "ufone@123";
		} else if(localAddr.contains("172.17.")){
			dbUrl_live = "jdbc:jtds:sqlserver://172.17.1.76/new_cct_live;instance=HASHKHIMAINDB";
			userId_live = "hashcube_cc";
			pass_live = "ufone@123";
		} else {
			dbUrl_live = "jdbc:jtds:sqlserver://172.16.12.69/new_cct_live_cisco;instance=HASHISBMAINDB;";
			userId_live = "hashcube_cc";
			pass_live = "ufone@123";
		}
		
		try{
			conn.close();
			conn = null;
		} catch(Exception ex){
		}
		
		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);
		} catch (Exception ex) {
			Logger.getLogger(IndexPageClass.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		result += "<script type=\"text/javascript\" > \nfunction ButtonStatesClass()\n{ \nvar statesArray = new Array(); \nthis.GetButtonStates = function(key)\n{ \nswitch(key)\n{\n";
		String sql = "SELECT * FROM tbl_BtnStates";
		try {
			stat = conn.createStatement();
			rs = stat.executeQuery(sql);
			//System.out.println(conn.getMetaData());
			while (rs.next()) {
				String casestr = rs.getString(1);
				result += "case '" + casestr + "' :\n";
				for (int i = 2; i <= rs.getMetaData().getColumnCount(); i++) {
					String value = "";
					if (rs.getObject(i) == null) {
						value = "null";
					} else {
						value = new Boolean(rs.getBoolean(i)).toString();
					}
					result += "statesArray[" + (i - 2) + "] = " + value + ";\n";
				}
				result += "return statesArray;\nbreak;\n";
			}
			result += "}\n};\n}\n</script>";
		} catch (Exception ex) {
			Logger.getLogger(IndexPageClass.class.getName()).log(Level.SEVERE, null, ex);
		}

		try {
			rs.close();
		} catch (Exception ex) {
		}
		try {
			stat.close();
		} catch (Exception ex) {
		}
		try {
			conn.close();
		} catch (Exception ex) {
		}

		rs = null;
		stat = null;
		conn = null;

		toReturn = result;
		return toReturn;
	}
	/*
	 public static void main(String[] args){
	 System.out.println(new IndexPageClass().generateButtonStatesJS());
	 }
	 */
}
