/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hash3.Main;

import cares.WCMS.AutoFillControlBO;
import cares.WCMS.CityAreaClusterBO;
import cares.WCMS.ComplaintBO;
import cares.WCMS.ComplaintInfo;
import cares.WCMS.InformationalWorkCodeBO;
import cares.WCMS.InformationalWorkCodeListBO;
import cares.WCMS.LovDataBO;
import cares.WCMS.LovDataListBO;
import cares.WCMS.MetaDataBO;
import cares.WCMS.MetaDataListBO;
import cares.WCMS.TaskBO;
import cares.WCMS.TasksListBO;
import cares.WCMS.WCMSLogResponse;
import cares.WCMS.WorkCodeBO;
import cares.WCMS.WorkCodeListBO;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import javax.xml.ws.Holder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Haris
 */
public class WCMSClassLHR {

//	static Connection conn = null;
    public static void main(String[] args) {
        String toReturn = "";
//		String dbURL = "172.18.32.117";
//		String dbUrl_live = "jdbc:jtds:sqlserver://" + dbURL + "/new_cct_live";
//		String userId_live = "sa";
//		String pass_live = "ptml@123";
        String dbUrl_live = "jdbc:jtds:sqlserver://172.18.1.228/new_cct_live;instance=HASHLHRMAINDB";
        String userId_live = "hashcube_cc";
        String pass_live = "ufone@123";

        Connection conn = null;

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);

        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }

        String callingSystem = "HASH";
        String callingIP = "172.16.4.44";
        Holder<WorkCodeListBO> workCodeListOutput = new Holder<WorkCodeListBO>();
        Holder<Integer> returnCode = new Holder<Integer>();
        Holder<String> returnDesc = new Holder<String>();

        WCMSClassLHR.getComplaintWorkCodeList(callingSystem, callingIP, workCodeListOutput, returnCode, returnDesc);
        List<WorkCodeBO> workCodes = workCodeListOutput.value.getWorkCodes();
        String _sql = "DELETE FROM [new_cct_live].[dbo].[Complaint_Workcodes]";
        try {
            Statement stat = conn.createStatement();
            int executeUpdate = stat.executeUpdate(_sql);
            stat.close();
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }
        String _sql1 = "DELETE FROM [new_cct_live].[dbo].[Complaint_WC_Metadata]";
        try {
            Statement stat1 = conn.createStatement();
            int executeUpdate = stat1.executeUpdate(_sql1);
            stat1.close();
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }
        String _sql2 = "DELETE FROM [new_cct_live].[dbo].[Complaint_lov_data]";
        try {
            Statement stat2 = conn.createStatement();
            int executeUpdate = stat2.executeUpdate(_sql2);
            stat2.close();
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (WorkCodeBO workCodeBO : workCodes) {
            Integer wcID = workCodeBO.getWcId();
            String wcName = workCodeBO.getWcName().replaceAll("'", "''");
            String wcGroupName = workCodeBO.getWcGroupName().replaceAll("'", "''");
            Integer wcRevision = workCodeBO.getWcRevision();
            //   String sql = "insert into [new_cct_live].[dbo].[Complaint_Workcodes] values(" + wcID + ", '" + wcName + "', " + wcRevision + ", '" + wcGroupName + "')";
            String sql = "insert into [new_cct_live].[dbo].[Complaint_Workcodes] values(?, ?, ?, ?)";
            try {
                PreparedStatement pStat = conn.prepareStatement(sql);
                pStat.setInt(1, wcID);
                pStat.setString(2, wcName);
                pStat.setInt(3, wcRevision);
                pStat.setString(4, wcGroupName);
                int executeUpdate = pStat.executeUpdate(sql);
                pStat.close();
            } catch (Exception ex) {
                Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
            }
            BigDecimal workCodeId = new BigDecimal(wcID);
            Holder<MetaDataListBO> metaDataListOutput = new Holder<MetaDataListBO>();
            Holder<Integer> returnCode1 = new Holder<Integer>();
            Holder<String> returnDesc1 = new Holder<String>();
            WCMSClassLHR.getCwMetaData(workCodeId, callingSystem, callingIP, metaDataListOutput, returnCode1, returnDesc1);
            List<MetaDataBO> metaDataList = metaDataListOutput.value.getMetaDataList();
            for (MetaDataBO metaDataBO : metaDataList) {
                String value = metaDataBO.getWorkcodeu32Title().getValue().replaceAll("'", "''");
                String value1 = metaDataBO.getScript().getValue().replaceAll("'", "''");
                String value2 = metaDataBO.getControlu32Name().getValue().replaceAll("'", "''");
                String value3 = metaDataBO.getFieldu32Name().getValue().replaceAll("'", "''");
                String value4 = metaDataBO.getFieldu32Tooltip().getValue().replaceAll("'", "''");
                BigDecimal value5 = metaDataBO.getIsmandatory().getValue();
                BigDecimal value6 = metaDataBO.getSequencenumber().getValue();
                String controlu32Fieldu32Type = metaDataBO.getControlu32Fieldu32Type().replaceAll("'", "''");
                BigDecimal autou32Filledu32Control = metaDataBO.getAutou32Filledu32Control();
                BigDecimal batchu32Logging = metaDataBO.getBatchu32Logging();
                String worku32Codeu32Type = metaDataBO.getWorku32Codeu32Type();
                BigDecimal lovu32Idu32Foru32Comboboxu32Control = metaDataBO.getLovu32Idu32Foru32Comboboxu32Control();
                String lovu32Nameu32Foru32Treeviewu32Control = metaDataBO.getLovu32Nameu32Foru32Treeviewu32Control();
//                String sql1 = "insert into [new_cct_live].[dbo].[Complaint_WC_Metadata] values(";
//                sql1 += "" + workCodeId + ",";
//                sql1 += "'" + value + "',";
//                sql1 += "'" + value1 + "',";
//                sql1 += "'" + value2 + "',";
//                sql1 += "'" + value3 + "',";
//                sql1 += "'" + value4 + "',";
//                sql1 += "" + value5 + ",";
//                sql1 += "" + value6 + ",";
//                sql1 += "'" + controlu32Fieldu32Type + "',";
//                sql1 += "" + autou32Filledu32Control + ",";
//                sql1 += "" + batchu32Logging + ",";
//                sql1 += "'" + worku32Codeu32Type + "',";
//                sql1 += "" + lovu32Idu32Foru32Comboboxu32Control + ",";
//                sql1 += "'" + lovu32Nameu32Foru32Treeviewu32Control + "'";
//                sql1 += ")";
                String sql1 = "insert into [new_cct_live].[dbo].[Complaint_WC_Metadata] values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                try {
                    PreparedStatement pStat1 = conn.prepareStatement(sql1);
                    pStat1.setBigDecimal(1, workCodeId);
                    pStat1.setString(2, value);
                    pStat1.setString(3, value1);
                    pStat1.setString(4, value2);
                    pStat1.setString(5, value3);
                    pStat1.setString(6, value4);
                    pStat1.setBigDecimal(7, value5);
                    pStat1.setBigDecimal(8, value6);
                    pStat1.setString(9, controlu32Fieldu32Type);
                    pStat1.setBigDecimal(10, autou32Filledu32Control);
                    pStat1.setBigDecimal(11, batchu32Logging);
                    pStat1.setString(12, worku32Codeu32Type);
                    pStat1.setBigDecimal(13, lovu32Idu32Foru32Comboboxu32Control);
                    pStat1.setString(14, lovu32Nameu32Foru32Treeviewu32Control);
                    int executeUpdate = pStat1.executeUpdate(sql1);
                    pStat1.close();

                } catch (Exception ex) {
                    Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (lovu32Idu32Foru32Comboboxu32Control != null) {
                    if (lovu32Idu32Foru32Comboboxu32Control.intValue() > 0) {
                        Holder<LovDataListBO> lovDataList = new Holder<LovDataListBO>();
                        Holder<Integer> returnCode2 = new Holder<Integer>();
                        Holder<String> returnDesc2 = new Holder<String>();
                        WCMSClassLHR.getLovData(lovu32Idu32Foru32Comboboxu32Control, callingSystem, callingIP, lovDataList, returnCode2, returnDesc2);
                        List<LovDataBO> lovDataList1 = lovDataList.value.getLovDataList();
                        for (LovDataBO lovDataBO : lovDataList1) {
                            BigDecimal value7 = lovDataBO.getLovId().getValue();
                            String value8 = lovDataBO.getLovName().getValue().replaceAll("'", "''");
                            String value9 = lovDataBO.getLovValue().getValue().replaceAll("'", "''");
                            // String sql2 = "insert into [new_cct_live].[dbo].[Complaint_lov_data] values(" + value7 + ", '" + value8 + "', '" + value9 + "', " + lovu32Idu32Foru32Comboboxu32Control + ")";
                            String sql2 = "insert into [new_cct_live].[dbo].[Complaint_lov_data] values(?, ?, ?, ?)";
                            try {
                                PreparedStatement pStat2 = conn.prepareStatement(sql2);
                                pStat2.setBigDecimal(1, value7);
                                pStat2.setString(2, value8);
                                pStat2.setString(3, value9);
                                pStat2.setBigDecimal(4, lovu32Idu32Foru32Comboboxu32Control);

                                int executeUpdate = pStat2.executeUpdate(sql2);
                                pStat2.close();

                            } catch (Exception ex) {
                                Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
        }

        String _sql3 = "DELETE FROM [new_cct_live].[dbo].[Complaint_tree_lov_data]";
        try {
            Statement stat3 = conn.createStatement();
            int executeUpdate = stat3.executeUpdate(_sql3);
            stat3.close();
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }

        Holder<LovDataListBO> lovDataList = new Holder<LovDataListBO>();
        String tree_head_name = "Regions";
        WCMSClassLHR.getLovDataByName(tree_head_name, callingSystem, callingIP, lovDataList, returnCode, returnDesc);

        List<LovDataBO> lovDataList1 = lovDataList.value.getLovDataList();
        for (LovDataBO lovDataObj : lovDataList1) {
            BigDecimal lovID = lovDataObj.getLovId().getValue();
            String lovName = lovDataObj.getLovName().getValue().replaceAll("'", "''").trim();
            String lovValue = lovDataObj.getLovValue().getValue().replaceAll("'", "''").trim();

            // String _sql4 = "insert into [new_cct_live].[dbo].[Complaint_tree_lov_data] values ('" + tree_head_name + "', " + lovID + ", '" + lovName + "', '" + lovValue + "')";
            String _sql4 = "insert into [new_cct_live].[dbo].[Complaint_tree_lov_data] values (?, ?, ?, ?)";
            try {
                PreparedStatement pStat4 = conn.prepareStatement(_sql4);
                pStat4.setString(1, tree_head_name);
                pStat4.setBigDecimal(2, lovID);
                pStat4.setString(3, lovName);
                pStat4.setString(4, lovValue);
                int executeUpdate = pStat4.executeUpdate(_sql4);
                pStat4.close();
            } catch (Exception ex) {
                Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
            }

            _tempGetTreeData(tree_head_name, lovName, conn);

        }

        try {
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String _tempGetTreeData(String treeHead, String _lovName, Connection conn) {
        String toReturn = "";
//		String dbUrl_live = "jdbc:jtds:sqlserver://172.18.32.117/new_cct_live";
//		String userId_live = "sa";
//		String pass_live = "ptml@123";

        if (!treeHead.equals(_lovName)) {

//			Connection conn = null;
//
//			try {
//				Class.forName("net.sourceforge.jtds.jdbc.Driver");
//				conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);
//
//			} catch (Exception ex) {
//				Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
//			}
            String callingSystem = "HASH";
            String callingIP = "172.16.4.44";
            Holder<Integer> returnCode = new Holder<Integer>();
            Holder<String> returnDesc = new Holder<String>();

            Holder<LovDataListBO> lovDataList = new Holder<LovDataListBO>();
            String tree_head_name = _lovName;
            WCMSClassLHR.getLovDataByName(tree_head_name, callingSystem, callingIP, lovDataList, returnCode, returnDesc);

            List<LovDataBO> lovDataList1 = lovDataList.value.getLovDataList();
            for (LovDataBO lovDataObj : lovDataList1) {
                BigDecimal lovID = lovDataObj.getLovId().getValue();
                String lovName = lovDataObj.getLovName().getValue().replaceAll("'", "''").trim();
                String lovValue = lovDataObj.getLovValue().getValue().replaceAll("'", "''").trim();

                //  String _sql4 = "insert into [new_cct_live].[dbo].[Complaint_tree_lov_data] values ('" + tree_head_name + "', " + lovID + ", '" + lovName + "', '" + lovValue + "')";
                String _sql4 = "insert into [new_cct_live].[dbo].[Complaint_tree_lov_data] values (?, ?, ?, ?)";
                try {

                    PreparedStatement pStat4 = conn.prepareStatement(_sql4);
                    pStat4.setString(1, tree_head_name);
                    pStat4.setBigDecimal(2, lovID);
                    pStat4.setString(3, lovName);
                    pStat4.setString(4, lovValue);
                    int executeUpdate = pStat4.executeUpdate(_sql4);
                    pStat4.close();
                } catch (Exception ex) {
                    Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
                }

                _tempGetTreeData(tree_head_name, lovName, conn);
            }
//			try {
//				conn.close();
//			} catch (Exception ex) {
//				Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
//			}
        }
        return toReturn;
    }

    public String getTreeData(String treeHeadName) {
        String toReturn = "";
//		String dbUrl_live = "jdbc:jtds:sqlserver://172.18.32.117/new_cct_live";
//		String userId_live = "sa";
//		String pass_live = "ptml@123";
        String dbUrl_live = "jdbc:jtds:sqlserver://172.18.1.228/new_cct_live;instance=HASHLHRMAINDB";
        String userId_live = "hashcube_cc";
        String pass_live = "ufone@123";

        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);

        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }

        JSONObject jsonObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        String sql = "SELECT distinct * FROM [new_cct_live].[dbo].[Complaint_tree_lov_data] where tree_head_name = ? order by lov_name";

        try {
            PreparedStatement pStat = conn.prepareStatement(sql);
            pStat.setString(1, treeHeadName.replaceAll("'", "''"));
            rs = pStat.executeQuery(sql);
//            stat = conn.createStatement();
//            rs = stat.executeQuery(sql);
            while (rs.next()) {
                jsonObj.clear();
                jsonObj.put("tree_head_name", rs.getString("tree_head_name").trim());
                jsonObj.put("lov_id", rs.getString("lov_id").trim());
                jsonObj.put("lov_name", rs.getString("lov_name").trim());
                jsonObj.put("lov_value", rs.getString("lov_name").trim());

                jsonArray.add(jsonObj.clone());
            }
            rs.close();
            pStat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }
        conn = null;
        stat = null;
        rs = null;

        toReturn = jsonArray.toJSONString();

        return toReturn;
    }

    public String logComplaint(String agentRemarks, String cli, JSONArray _controlsDataObjArray,
            String customerEmail, String logAgentName, String msisdn, String wcId,
            String wcStatus, String wcType, String ipaddress, String datetime, String agent_id,
            String username, String amt_charged, String conn_id, String file_data, String file_name) {
        String toReturn = "";

        ComplaintBO complaintBO = new ComplaintBO();
        Holder<Integer> returnCode = new Holder<Integer>();
        Holder<String> returnDesc = new Holder<String>();
        Holder<WCMSLogResponse> wcmsLogResponse = new Holder<WCMSLogResponse>();

        complaintBO.setAgentRemarks(agentRemarks);
        complaintBO.setCli(cli);
        String controlsDataString = "";
        for (Object _controlsDataObj : _controlsDataObjArray) {
            JSONObject controlsDataObj = (JSONObject) _controlsDataObj;
            controlsDataString += controlsDataObj.get("fieldName");
            controlsDataString += ';';
            if (controlsDataObj.get("fieldName").toString().toLowerCase().equals("Regions_Cluster".toLowerCase())) {
                try {
                    String fieldValue = controlsDataObj.get("fieldValue").toString();
//					String[] _split = fieldValue.split("[]]");
                    String[] _split = fieldValue.split("[~]");
//					String areaName = _split[_split.length - 1].substring(2);
                    String areaName = _split[_split.length - 1];
                    Holder<CityAreaClusterBO> cityAreaClusterBO = new Holder<CityAreaClusterBO>();
                    Holder<Integer> _returnCode = new Holder<Integer>();
                    Holder<String> _returnDesc = new Holder<String>();
                    WCMSClassLHR.getCityAreaCluster(areaName, "HASH", ipaddress, cityAreaClusterBO, _returnCode, _returnDesc);
                    String areaCluster = cityAreaClusterBO.value.getPCityAreaCluster().getValue();
//					fieldValue = fieldValue + "-[" + areaCluster + "]";
                    fieldValue = fieldValue + "~" + areaCluster;
                    controlsDataString += fieldValue;
                } catch (Exception ex) {
                    controlsDataString += controlsDataObj.get("fieldValue");
                }
            } else {
                controlsDataString += controlsDataObj.get("fieldValue");
            }
            controlsDataString += "<br/>";
        }
        complaintBO.setControlsData(controlsDataString);
        complaintBO.setCustomerEmail(customerEmail);
        complaintBO.setLogAgentName(logAgentName);
        if (msisdn.startsWith("03") && msisdn.length() == 11) {
            msisdn = "92" + msisdn.substring(1);
        }
        complaintBO.setMSISDN(msisdn);
        complaintBO.setWcId(Integer.parseInt(wcId));
        complaintBO.setWcStatus(wcStatus);
        complaintBO.setWcType(wcType);

        if (!file_data.isEmpty()) {
            byte[] file_data_array = DatatypeConverter.parseBase64Binary(file_data);
            String[] file_name_array = file_name.split("[.]");
            complaintBO.setImageFile(file_data_array);
            complaintBO.setImageExt(file_name_array[file_name_array.length - 1]);
        }

        JSONObject jsonObj = new JSONObject();
        try {
            WCMSClassLHR.logComplaintService("HASH", ipaddress, complaintBO, returnCode, returnDesc, wcmsLogResponse);
            jsonObj.put("returnCode", returnCode.value);
            jsonObj.put("returnDesc", returnDesc.value);
            jsonObj.put("complaintId", wcmsLogResponse.value.getComplaintId());
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
            jsonObj.put("returnCode", -1);
            jsonObj.put("returnDesc", "Failed");
        }

        toReturn = jsonObj.toJSONString();

        AjaxHandlerClass_LHR ajaxHandlerClass_LHR = new AjaxHandlerClass_LHR();

        ajaxHandlerClass_LHR.setWSLogging(datetime, username, returnCode.value.toString(),
                toReturn, returnDesc.value.toString(), msisdn, "null", "null", cli,
                amt_charged, conn_id, ipaddress, "null", "cms_logComplaint");

        return toReturn;
    }

    public String getComplaintFields(String wcID, String msisdn, String ipAddress) {
        String toReturn = "";

//		String dbUrl_live = "jdbc:jtds:sqlserver://172.18.32.117/new_cct_live";
////		String dbUrl_live = "jdbc:jtds:sqlserver://172.18.32.117/new_cct_live_test";
//		String userId_live = "sa";
//		String pass_live = "ptml@123";
        String dbUrl_live = "jdbc:jtds:sqlserver://172.18.1.228/new_cct_live;instance=HASHLHRMAINDB";
        String userId_live = "hashcube_cc";
        String pass_live = "ufone@123";

        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);

        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }

        JSONArray jsonArray = new JSONArray();

        String sql = "SELECT * FROM [new_cct_live].[dbo].[Complaint_WC_Metadata] WHERE WC_ID = ? ORDER BY Seq_Number";
//		String sql = "SELECT * FROM [new_cct_live_test].[dbo].[Complaint_WC_Metadata] WHERE WC_ID = " + wcID + " ORDER BY Seq_Number";
        try {
            PreparedStatement pStat = conn.prepareStatement(sql);
            pStat.setString(1, wcID);
            rs = pStat.executeQuery(sql);
//            stat = conn.createStatement();
//            rs = stat.executeQuery(sql);
            while (rs.next()) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("wcID", rs.getInt("WC_ID"));
                jsonObj.put("wcTitle", rs.getString("WC_TITLE"));
                jsonObj.put("script", rs.getString("Script"));
                jsonObj.put("controlName", rs.getString("Control_Name"));
                jsonObj.put("fieldName", rs.getString("Field_Name"));
                jsonObj.put("fieldToolTip", rs.getString("Field_Tooltip"));
                jsonObj.put("isMandatory", rs.getBoolean("IsMandatory"));
                jsonObj.put("seqNumber", rs.getInt("Seq_Number"));
                jsonObj.put("controlFieldType", rs.getString("Control_field_type"));
                jsonObj.put("autoFilledControl", rs.getBoolean("Autofilled_Control"));
                jsonObj.put("batchLogging", rs.getBoolean("batch_logging"));
                jsonObj.put("wcType", rs.getString("work_code_type"));
                jsonObj.put("lovIDForComboBox", rs.getString("lov_id_for_combo_box"));
                jsonObj.put("lovNameForTreeView", rs.getString("lov_name_for_tree_view"));

                Holder<AutoFillControlBO> autoFillControlBO = new Holder<AutoFillControlBO>();
                Holder<Integer> returnCode = new Holder<Integer>();
                Holder<String> returnDesc = new Holder<String>();
                try {
                    WCMSClassLHR.getAutoFilledControls(msisdn, "HASH", ipAddress, autoFillControlBO, returnCode, returnDesc);
                    jsonObj.put("autoFilledReturnCode", returnCode.value);
                    jsonObj.put("autoFilledReturnDesc", returnDesc.value);
                } catch (Exception ex) {
                    Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
                    jsonObj.put("autoFilledReturnCode", -1);
                    jsonObj.put("autoFilledReturnDesc", "Failed");
                }
                if ((Integer) jsonObj.get("autoFilledReturnCode") == 0) {
                    AutoFillControlBO autoFillObj = autoFillControlBO.value;
                    jsonObj.put("autoFilledMSISDN", autoFillObj.getPMsisdn().getValue());
                    jsonObj.put("autoFilledAddress", autoFillObj.getPAddress().getValue());
                    jsonObj.put("autoFilledConnectionType", autoFillObj.getPConnectionType().getValue());
                    jsonObj.put("autoFilledNIC", autoFillObj.getPNic().getValue());
                    jsonObj.put("autoFilledPackageName", autoFillObj.getPPackageName().getValue());
                    jsonObj.put("autoFilledServiceCode", autoFillObj.getPServiceCode().getValue());
                    jsonObj.put("autoFilledSIMNo", autoFillObj.getPSimNo().getValue());
                }

                Holder<String> balance = new Holder<String>();
                Holder<String> expireTime = new Holder<String>();
                try {
                    WCMSClassLHR.getBalance("HASH", ipAddress, msisdn, balance, expireTime, returnCode, returnDesc);
                    jsonObj.put("getBalanceReturnCode", returnCode.value);
                    jsonObj.put("getBalanceReturnDesc", returnDesc.value);
                } catch (Exception ex) {
                    Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
                    jsonObj.put("getBalanceReturnCode", -1);
                    jsonObj.put("getBalanceReturnDesc", "Failed");
                }
                if ((Integer) jsonObj.get("getBalanceReturnCode") == 0) {
                    jsonObj.put("balance", balance.value);
                    jsonObj.put("expireTime", expireTime.value);
                }

                if (jsonObj.get("lovIDForComboBox") != null) {
                    JSONObject jsonObj_lov = new JSONObject();
                    JSONArray jsonArray_lov = new JSONArray();
                    PreparedStatement _pStat = null;
                    ResultSet _rs = null;
                    String _sql = "SELECT distinct * FROM [new_cct_live].[dbo].[Complaint_lov_data] WHERE LOV_Parent_ID =? ORDER BY Lov_Name";
//					String _sql = "SELECT distinct * FROM [new_cct_live_test].[dbo].[Complaint_lov_data] WHERE LOV_Parent_ID = " + jsonObj.get("lovIDForComboBox") + " ORDER BY Lov_Name";
                    try {
                        _pStat = conn.prepareStatement(_sql);
                        _pStat.setString(1, jsonObj.get("lovIDForComboBox").toString());
                        _rs = _pStat.executeQuery(_sql);
//                        _stat = conn.createStatement();
//                        _rs = _stat.executeQuery(_sql);
                        while (_rs.next()) {
                            jsonObj_lov.clear();
                            String lovName = _rs.getString("Lov_Name");
                            String lovValue = _rs.getString("Lov_Value");
                            String lovID = _rs.getString("LOV_ID");
                            jsonObj_lov.put("lovName", lovName);
//							jsonObj_lov.put("lovValue", lovValue);
                            jsonObj_lov.put("lovValue", lovName);
                            jsonObj_lov.put("lovID", lovID);

                            jsonArray_lov.add(jsonObj_lov.clone());
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    jsonObj.put("lovArrayObj", jsonArray_lov);
                    _rs.close();
                    _pStat.close();
                    _pStat = null;
                    _rs = null;
                }

                jsonArray.add(jsonObj.clone());
            }
            rs.close();
            pStat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }
        conn = null;
        stat = null;
        rs = null;

        toReturn = jsonArray.toJSONString();

        return toReturn;
    }

    public String getComplaintWorkCodeList() {
        String toReturn = "";

//		String dbUrl_live = "jdbc:jtds:sqlserver://172.18.32.117/new_cct_live";
////		String dbUrl_live = "jdbc:jtds:sqlserver://172.18.32.117/new_cct_live_test";
//		String userId_live = "sa";
//		String pass_live = "ptml@123";
        String dbUrl_live = "jdbc:jtds:sqlserver://172.18.1.228/new_cct_live;instance=HASHLHRMAINDB";
        String userId_live = "hashcube_cc";
        String pass_live = "ufone@123";

        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);

        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }

        JSONArray jsonArray = new JSONArray();

        String sql = "SELECT * FROM [new_cct_live].[dbo].[Complaint_Workcodes] ORDER BY WC_GROUP_NAME, WC_NAME";
//		String sql = "SELECT * FROM [new_cct_live_test].[dbo].[Complaint_Workcodes] ORDER BY WC_GROUP_NAME, WC_NAME";
        try {
            stat = conn.createStatement();
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("wcID", rs.getInt("WC_ID"));
                jsonObj.put("wcGroupName", rs.getString("WC_GROUP_NAME"));
                jsonObj.put("wcName", rs.getString("WC_NAME"));
                jsonObj.put("wcRevision", rs.getInt("WC_REVISION"));
                jsonArray.add(jsonObj.clone());
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }
        conn = null;
        stat = null;
        rs = null;

        toReturn = jsonArray.toJSONString();

        return toReturn;
    }

    public String getTasks(String msisdn, String ipaddress, String datetime,
            String agent_id, String username, String amt_charged,
            String conn_id, String cli) {
        String toReturn = "";
        JSONObject jsonObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        Holder<TasksListBO> tasksListOutput = new Holder<TasksListBO>();
        Holder<Integer> returnCode = new Holder<Integer>();
        Holder<String> returnDesc = new Holder<String>();

        try {
            WCMSClassLHR.getTasks("MSISDN", msisdn, "HASH", ipaddress, tasksListOutput, returnCode, returnDesc);
            jsonObj.put("returnCode", returnCode.value);
            jsonObj.put("returnDesc", returnDesc.value);
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
            jsonObj.put("returnCode", -1);
            jsonObj.put("returnDesc", "Failed");
        }
        jsonObj.put("customerMSISDN", msisdn);
        if (returnCode.value == 0) {
//			List<TaskBO> tasks = tasksListOutput.value.getTasks();
//			for (int _i = 0; _i < tasks.size() && _i < 100; _i++) {
            for (TaskBO task : tasksListOutput.value.getTasks()) {
                JSONObject _jsonObj = new JSONObject();
                _jsonObj.put("customerMSISDN", msisdn);
                try {
                    _jsonObj.put("complaintID", task.getComplaintid().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("complaintID", "");
                }
                try {
                    _jsonObj.put("workCodeID", task.getWorkcodeid().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("workCodeID", "");
                }
                try {
                    _jsonObj.put("workCodeName", task.getWcname().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("workCodeName", "");
                }
                String workCodeStatus = "";
                try {
                    _jsonObj.put("workCodeStatus", task.getWcstatus().getValue());
                    workCodeStatus = task.getWcstatus().getValue();
                } catch (Exception ex) {
                    _jsonObj.put("workCodeStatus", "");
                    workCodeStatus = "null";
                }
                if (workCodeStatus.toLowerCase().contains("open")) {
                    _jsonObj.put("isComplaintOpen", Boolean.valueOf(true));
                } else if (workCodeStatus.toLowerCase().contains("wip")) {
                    _jsonObj.put("isComplaintOpen", Boolean.valueOf(true));
                } else if (workCodeStatus.toLowerCase().contains("fwd")) {
                    _jsonObj.put("isComplaintOpen", Boolean.valueOf(true));
                } else if (workCodeStatus.toLowerCase().contains("close")) {
                    _jsonObj.put("isComplaintOpen", Boolean.valueOf(false));
                } else if (workCodeStatus.toLowerCase().contains("decline")) {
                    _jsonObj.put("isComplaintOpen", Boolean.valueOf(false));
                } else {
                    _jsonObj.put("isComplaintOpen", Boolean.valueOf(false));
                }
                try {
                    _jsonObj.put("loggedBy", task.getLoggedby().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("loggedBy", "");
                }
                try {
                    _jsonObj.put("actionRemarks", task.getActionremarks().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("actionRemarks", "");
                }
                try {
                    _jsonObj.put("agentRemarks", task.getAgentremarks().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("agentRemarks", "");
                }
                try {
                    _jsonObj.put("cliNumber", task.getClinumber().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("cliNumber", "");
                }
                try {
                    _jsonObj.put("complaintLogStartDate", task.getComplaintlogstartdate().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("complaintLogStartDate", "");
                }
                try {
                    _jsonObj.put("complaintLogEndDate", task.getComplaintlogenddate().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("complaintLogEndDate", "");
                }
                try {
                    _jsonObj.put("isWithInTATString", task.getIsWithInTatString());
                } catch (Exception ex) {
                    _jsonObj.put("isWithInTATString", "");
                }
                String isWithInTAT = "";
                try {
                    _jsonObj.put("isWithInTAT", task.getIswithintat().getValue());
                    isWithInTAT = task.getIswithintat().getValue();
                } catch (Exception ex) {
                    _jsonObj.put("isWithInTAT", "");
                    isWithInTAT = "null";
                }
//				if(workCodeStatus.toLowerCase().contains("close") && !isWithInTAT.equals("1")){
                if (workCodeStatus.toLowerCase().contains("close")) {
                    _jsonObj.put("hotComplaintEnabled", true);
                } else if (workCodeStatus.toLowerCase().contains("open") && !isWithInTAT.equals("1")) {
                    _jsonObj.put("hotComplaintEnabled", true);
                } else {
                    _jsonObj.put("hotComplaintEnabled", false);
                }

                try {
                    _jsonObj.put("logAgentID", task.getLogagentid().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("logAgentID", "");
                }
                try {
                    _jsonObj.put("loggedStatus", task.getLoggedstatus().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("loggedStatus", "");
                }
                try {
                    _jsonObj.put("logLocation", task.getLoglocation().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("logLocation", "");
                }
                try {
                    _jsonObj.put("previousRemarks", task.getPrevactionremarks().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("previousRemarks", "");
                }
                try {
                    _jsonObj.put("userInformation", task.getUserinformation().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("userInformation", "");
                }
                try {
                    _jsonObj.put("wcQualityStatus", task.getWcqualitystatus().getValue());
                } catch (Exception ex) {
                    _jsonObj.put("wcQualityStatus", "");
                }
                try {
                    _jsonObj.put("fileData", DatatypeConverter.printBase64Binary(task.getFileData()));
                } catch (Exception ex) {
                    _jsonObj.put("fileData", "");
                }
                try {
                    _jsonObj.put("fileExt", task.getFileExt());
                } catch (Exception ex) {
                    _jsonObj.put("fileExt", "");
                }
                try {
                    _jsonObj.put("filePath", task.getFilePath());
                } catch (Exception ex) {
                    _jsonObj.put("filePath", "");
                }

                Holder<Integer> returnCode_complaint = new Holder<Integer>();
                Holder<String> returnDesc_complaint = new Holder<String>();
                Holder<ComplaintInfo> complaintInfo = new Holder<ComplaintInfo>();

                try {
                    WCMSClassLHR.getComplaint("HASH", ipaddress, (String) _jsonObj.get("complaintID"), msisdn, complaintInfo, returnCode_complaint, returnDesc_complaint);
                    ComplaintInfo compInfo = complaintInfo.value;
                    try {
                        _jsonObj.put("resolutionTime", compInfo.getResolutionTime().getValue());
                    } catch (Exception ex) {
                        _jsonObj.put("resolutionTime", "");
                    }
                } catch (Exception ex) {
                    Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
                    _jsonObj.put("resolutionTime", "");
                }

                jsonArray.add(_jsonObj.clone());
            }
        }

        jsonObj.put("taskArray", jsonArray);

        toReturn = jsonObj.toJSONString();

        AjaxHandlerClass_LHR ajaxHandlerClass_LHR = new AjaxHandlerClass_LHR();

        ajaxHandlerClass_LHR.setWSLogging(datetime, username, returnCode.value.toString(),
                toReturn, returnDesc.value.toString(), msisdn, "null", "null", cli,
                amt_charged, conn_id, ipaddress, "null", "cms_getTasks");
        return toReturn;
    }

    public String declineTicket(String complaintID, String remarks, String agentID, String ipAddress, String msisdn,
            String datetime, String username, String amt_charged, String conn_id, String cli) {
        String toReturn = "";
        JSONObject jsonObj = new JSONObject();
        Holder<Integer> _returnCode = new Holder<Integer>();
        Holder<String> _returnDesc = new Holder<String>();
        Holder<BigDecimal> userId = new Holder<BigDecimal>();
        try {
            WCMSClassLHR.getUserId("HASH", ipAddress, username, userId, _returnCode, _returnDesc);
            int _agentID = userId.value.intValue();
            agentID = Integer.toString(_agentID);
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }
        Holder<Integer> returnCode = new Holder<Integer>();
        Holder<String> returnDesc = new Holder<String>();
        try {
            WCMSClassLHR.declineTicket(complaintID, remarks, agentID, "HASH", ipAddress, returnCode, returnDesc);
            jsonObj.put("returnCode", returnCode.value);
            jsonObj.put("returnDesc", returnDesc.value);
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
            jsonObj.put("returnCode", -1);
            jsonObj.put("returnDesc", "Failed");
        }
        toReturn = jsonObj.toJSONString();

        AjaxHandlerClass_LHR ajaxHandlerClass_LHR = new AjaxHandlerClass_LHR();

        ajaxHandlerClass_LHR.setWSLogging(datetime, username, returnCode.value.toString(),
                toReturn, returnDesc.value.toString(), msisdn, "null", "null", cli,
                amt_charged, conn_id, ipAddress, "null", "cms_declineTicket");

        return toReturn;
    }

    public String getLastInformationalWorkCodes(String msisdn, String datetime, String username, String cli,
            String amt_charged, String conn_id, String ipAddress) {
        String toReturn = "";
        Holder<InformationalWorkCodeListBO> informationalWorkCodeList = new Holder<InformationalWorkCodeListBO>();
        Holder<Integer> returnCode = new Holder<Integer>();
        Holder<String> returnDesc = new Holder<String>();
        try {
            WCMSClassLHR.getLastInformationalWorkCodes("HASH", ipAddress, msisdn, informationalWorkCodeList, returnCode, returnDesc);
        } catch (Exception ex) {
            Logger.getLogger(WCMSClassLHR.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (returnCode.value == 0) {
            List<InformationalWorkCodeBO> informationalWorkCodeBOList = informationalWorkCodeList.value.getInformationalWorkCodeBOList();
            for (InformationalWorkCodeBO _infoWC : informationalWorkCodeBOList) {
                toReturn += _infoWC.getComplaintlogstartdate().getValue() + "," + _infoWC.getLoggedby().getValue() + " (" + _infoWC.getLoggedlocation().getValue().toLowerCase() + ")" + "," + _infoWC.getWorkcodename().getValue() + "," + _infoWC.getComplaintlogstartdate().getValue() + "|";
            }
        }
        AjaxHandlerClass_LHR ajaxHandlerClass_LHR = new AjaxHandlerClass_LHR();

        ajaxHandlerClass_LHR.setWSLogging(datetime, username, returnCode.value.toString(),
                toReturn, returnDesc.value.toString(), msisdn, "null", "null", cli,
                amt_charged, conn_id, ipAddress, "null", "cms_getLastInformationalWorkCodes");

        return toReturn;
    }

    private static void getComplaint(java.lang.String callingSystem, java.lang.String callingIP, java.lang.String complaintId, java.lang.String msisdn, javax.xml.ws.Holder<cares.WCMS.ComplaintInfo> complaintInfo, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getComplaint(callingSystem, callingIP, complaintId, msisdn, complaintInfo, returnCode, returnDesc);
    }

    private static void getTasks(java.lang.String parameterType, java.lang.String parameterValue, java.lang.String callingSystem, java.lang.String callingIP, javax.xml.ws.Holder<cares.WCMS.TasksListBO> tasksListOutput, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getTasks(parameterType, parameterValue, callingSystem, callingIP, tasksListOutput, returnCode, returnDesc);
    }

    private static void declineTicket(java.lang.String complaintId, java.lang.String remarks, java.lang.String agentId, java.lang.String callingSystem, java.lang.String callingIP, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.declineTicket(complaintId, remarks, agentId, callingSystem, callingIP, returnCode, returnDesc);
    }

    private static void getAutoFilledControls(java.lang.String msisdn, java.lang.String callingSystem, java.lang.String callingIP, javax.xml.ws.Holder<cares.WCMS.AutoFillControlBO> autoFillControlBO, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getAutoFilledControls(msisdn, callingSystem, callingIP, autoFillControlBO, returnCode, returnDesc);
    }

    private static void getComplaintWorkCodeList(java.lang.String callingSystem, java.lang.String callingIP, javax.xml.ws.Holder<cares.WCMS.WorkCodeListBO> workCodeListOutput, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getComplaintWorkCodeList(callingSystem, callingIP, workCodeListOutput, returnCode, returnDesc);
    }

    private static void getCwMetaData(java.math.BigDecimal workCodeId, java.lang.String callingSystem, java.lang.String callingIP, javax.xml.ws.Holder<cares.WCMS.MetaDataListBO> metaDataListOutput, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getCwMetaData(workCodeId, callingSystem, callingIP, metaDataListOutput, returnCode, returnDesc);
    }

    private static void getLovData(java.math.BigDecimal lovId, java.lang.String callingSystem, java.lang.String callingIP, javax.xml.ws.Holder<cares.WCMS.LovDataListBO> lovDataList, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getLovData(lovId, callingSystem, callingIP, lovDataList, returnCode, returnDesc);
    }

    private static void getLovDataByName(java.lang.String lovName, java.lang.String callingSystem, java.lang.String callingIP, javax.xml.ws.Holder<cares.WCMS.LovDataListBO> lovDataList, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getLovDataByName(lovName, callingSystem, callingIP, lovDataList, returnCode, returnDesc);
    }

    private static void getBalance(java.lang.String callingSystem, java.lang.String callingIP, java.lang.String msisdn, javax.xml.ws.Holder<java.lang.String> balance, javax.xml.ws.Holder<java.lang.String> expireTime, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getBalance(callingSystem, callingIP, msisdn, balance, expireTime, returnCode, returnDesc);
    }

    private static void getLastInformationalWorkCodes(java.lang.String callingSystem, java.lang.String callingIP, java.lang.String msisdn, javax.xml.ws.Holder<cares.WCMS.InformationalWorkCodeListBO> informationalWorkCodeList, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getLastInformationalWorkCodes(callingSystem, callingIP, msisdn, informationalWorkCodeList, returnCode, returnDesc);
    }

    private static void logComplaintService(java.lang.String callingSystem, java.lang.String callingIP, cares.WCMS.ComplaintBO complaintBO, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc, javax.xml.ws.Holder<cares.WCMS.WCMSLogResponse> wcmsLogResponse) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.logComplaintService(callingSystem, callingIP, complaintBO, returnCode, returnDesc, wcmsLogResponse);
    }

    private static void getUserId(java.lang.String callingSystem, java.lang.String callingIP, java.lang.String userCode, javax.xml.ws.Holder<java.math.BigDecimal> userId, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getUserId(callingSystem, callingIP, userCode, userId, returnCode, returnDesc);
    }

    private static void getCityAreaCluster(java.lang.String areaName, java.lang.String callingSystem, java.lang.String callingIP, javax.xml.ws.Holder<cares.WCMS.CityAreaClusterBO> cityAreaClusterBO, javax.xml.ws.Holder<java.lang.Integer> returnCode, javax.xml.ws.Holder<java.lang.String> returnDesc) {
        cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService service = new cares.WCMS.IWCMSIntegrationExport1IWCMSIntegrationHttpService();
        service.setHandlerResolver(new HeaderHandlerResolver_LHR());
        cares.WCMS.IWCMSIntegration port = service.getIWCMSIntegrationExport1IWCMSIntegrationHttpPort();
        port.getCityAreaCluster(areaName, callingSystem, callingIP, cityAreaClusterBO, returnCode, returnDesc);
    }
}
