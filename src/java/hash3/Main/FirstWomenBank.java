/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hash3.Main;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.json.simple.JSONObject;

/**
 *
 * @author furqan.ahmad
 */
public class FirstWomenBank {

    private Connection conn = null;
    //Live db string start
    private String dbUrl_live = "jdbc:jtds:sqlserver://172.17.1.76/new_cct_live;instance=HASHKHIMAINDB";
    private String userId_live = "hashcube_cc";
    private String pass_live = "ufone@123";
    //Live db string end

    //Start of localdb testing
//    private String dbUrl_live = "jdbc:jtds:sqlserver://localhost/new_cct_live_cisco";
//    private String userId_live = "sa";
//    private String pass_live = "sa@123";
    //End of localdb Testing

    public FirstWomenBank() {
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            conn = java.sql.DriverManager.getConnection(dbUrl_live, userId_live, pass_live);
        } catch (Exception ex) {
            Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void closeDatabaseConnection() {
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Create and Update customer cli table.
     *
     * @param agentId
     * @param msisdn
     * @param status
     * @param callId
     * @return callId
     */
    public String createUpdateCustomerCli(String agentId, String msisdn, int status, String callId) {
        String sp = "{call fwbl_customer_cli_create_update(?,?,?,?)}";
        CallableStatement callProc = null;
//        UUID createdUuid = UUID.randomUUID();
//        String callId = createdUuid.toString();
        try {
            callProc = conn.prepareCall(sp);

            callProc.setString(1, agentId);
            callProc.setString(2, msisdn);
            callProc.setInt(3, status);
            callProc.setString(4, callId);

            callProc.executeUpdate();
        } catch (SQLException ex) {
            closeDatabaseConnection();
            Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
        }

        closeDatabaseConnection();

        try {
            callProc.close();
        } catch (SQLException ex) {
            Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        return callId;
    }

    /**
     * Log the activity into database.
     *
     * @param agentId
     * @param callId
     * @param ivrFlowName
     * @param agentVerifiesInfo
     */
    public void logPinChangeActivity(String agentId, String callId,
            String ivrFlowName, int agentVerifiesInfo) {
        String sp = "{call sp_fwbl_call_transfer_activity_log(?,?,?,?)}";
        CallableStatement callProc = null;

        try {
            callProc = conn.prepareCall(sp);

            callProc.setString(1, agentId);
            callProc.setString(2, callId);
            callProc.setString(3, ivrFlowName);
            callProc.setInt(4, 1);

            callProc.executeUpdate();
        } catch (SQLException ex) {
            closeDatabaseConnection();
            Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeDatabaseConnection();
    }

    public String getCustomerData(String guid) {
        //Use guid and get the details from web service
        JSONObject jsonObj = new JSONObject();
        try {
            String wsResponse = FirstWomenBank.wsCallStatus(guid);

            if (wsResponse.toLowerCase().equals("not found")) {
                jsonObj.put("error", wsResponse);
                return jsonObj.toJSONString();
            }
            //Response Structure: Change|ATM|44203-0381871-9|220550001000005;
            String[] splitResponse = wsResponse.split(Pattern.quote("|"));
            String ivrFlowName = splitResponse[0].trim() + " " + splitResponse[1].trim();
            String cnic = cnicFormatter(splitResponse[2]).trim();
            String panNum = splitResponse[3].trim();

            jsonObj.put("pan", panNum);
            jsonObj.put("cnic", cnic);
            jsonObj.put("ivrflow", ivrFlowName);

            return jsonObj.toJSONString();
        } catch (Exception ex) {
            Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
            jsonObj.put("error", ex.getMessage());
            return jsonObj.toJSONString();
        }

    }

    public String getPinChangeStatus(String callId) {
        String sp = "{call SP_FWBL_Get_Acknowledge(?)}";
        JSONObject pinChangeStatus = new JSONObject();

        CallableStatement callProc = null;

        try {
            callProc = conn.prepareCall(sp);

            callProc.setString(1, callId);

            callProc.execute();
            
            ResultSet rs = callProc.getResultSet();
            
            if (rs.next()) {
                String callIdGui = rs.getString("CALLID");
                String reason = rs.getString("REASON");
                String result = rs.getString("RESULT");
                String attempts = rs.getString("ATTEMPTS");
                int numberOfAttempts = 0;
                pinChangeStatus.put("callId", callIdGui);
                pinChangeStatus.put("reason", reason);
                pinChangeStatus.put("result", result);
                pinChangeStatus.put("attempts", attempts);
                try{
                  numberOfAttempts = Integer.parseInt(attempts);
                }catch(NumberFormatException ex){
                    Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
                    numberOfAttempts = 99;
                }
                String wsAckWebServiceResponse = "Unable to submit request";
                try
                {
                   wsAckWebServiceResponse =  FirstWomenBank.wsAcknowledgment(callIdGui, result, reason, numberOfAttempts);
                   pinChangeStatus.put("wsStatus", wsAckWebServiceResponse);
                }
                catch(Exception ex){
                    Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
                    pinChangeStatus.put("wsStatus", wsAckWebServiceResponse);
                }
            }else{
                pinChangeStatus.put("error", "No data found");
            }
            
            rs.close();//Close ResultSet
            callProc.close(); // Close Procedure Call
            
            return pinChangeStatus.toJSONString();
        } catch (SQLException ex) {
            closeDatabaseConnection();
            Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeDatabaseConnection();
        pinChangeStatus.put("error", "Something went wrong");
        return pinChangeStatus.toJSONString();
    }
    
    /**
     * @param callId callId is a unique identification number generated by IVR.
     * @param callStatus Its value should be set to <b>"disconnected"</b>.
     * @return In case of successful calling it will return <b>"success"</b> else <b>"fail"</b>
     */
    public String notifyCrmOfCallEnd(String callId, String callStatus){
        try{
            return FirstWomenBank.wsCallEnd(callId, callStatus);
        }catch(Exception ex){
            Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE,"Notify CRM of call end",ex);
            return "fail";
        }
    }
    /**
     * Removes the dashes from the CNIC number.<br/>
     *
     * @param cnic Input should be in this format
     * <strong>"12345-1234567-1"</strong>
     * @return CNIC without dashes 1234512345671
     */
    private static String cnicFormatter(String cnic) {
        //Split on "-"
        try {
            String[] cnicArr = cnic.split("-");
            return String.join("", cnicArr);
        } catch (Exception ex) {
            Logger.getLogger(FirstWomenBank.class.getName()).log(Level.SEVERE, null, ex);
            return cnic;
        }
    }

    private static String wsCallStatus(java.lang.String callId) {
        com.fwbl.ws.Service service = new com.fwbl.ws.Service();
        com.fwbl.ws.ServiceSoap port = service.getServiceSoap12();
        return port.wsCallStatus(callId);
    }

    public static void main(String[] args) {
        FirstWomenBank fwbl = new FirstWomenBank();
        fwbl.getPinChangeStatus("12345");
    }

    private static String wsAcknowledgment(java.lang.String callId, java.lang.String result, java.lang.String reason, int attempts) {
        com.fwbl.ws.Service service = new com.fwbl.ws.Service();
        com.fwbl.ws.ServiceSoap port = service.getServiceSoap12();
        return port.wsAcknowledgment(callId, result, reason, attempts);
    }

    private static String wsCallEnd(java.lang.String callId, java.lang.String callStatus) {
        com.fwbl.ws.Service service = new com.fwbl.ws.Service();
        com.fwbl.ws.ServiceSoap port = service.getServiceSoap12();
        return port.wsCallEnd(callId, callStatus);
    }
}
